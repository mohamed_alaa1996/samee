import com.android.build.gradle.internal.cxx.configure.gradleLocalProperties

plugins {
  id(Config.Plugins.androidApplication)
  id(Config.Plugins.kotlinAndroid)
  id(Config.Plugins.kotlinKapt)
  id(Config.Plugins.navigationSafeArgs)
  id(Config.Plugins.hilt)
  id(Config.Plugins.google_services)
}

android {
  compileSdk = Config.AppConfig.compileSdkVersion

  defaultConfig {
    applicationId = Config.AppConfig.appId
    minSdk = Config.AppConfig.minSdkVersion
    targetSdk = Config.AppConfig.compileSdkVersion
    versionCode = Config.AppConfig.versionCode
    versionName = Config.AppConfig.versionName

    vectorDrawables.useSupportLibrary = true
    multiDexEnabled = true
    testInstrumentationRunner = Config.AppConfig.testRunner

  }

  buildFeatures {
    viewBinding = true
  }

  buildTypes {
    getByName("debug") {
      manifestPlaceholders["appName"] = "@string/app_name_debug"
      manifestPlaceholders["appIcon"] = "@mipmap/ic_launcher"
      manifestPlaceholders["appRoundIcon"] = "@mipmap/ic_launcher"
      buildConfigField("String", "API_BASE_URL", Config.Environments.debugBaseUrl)
      buildConfigField("String", "ROOM_DB", Config.Environments.roomDb)
    }

    signingConfigs {
      create("releaseConfig") {
        storeFile = file(rootProject.file("GrandKey.jks"))
        storePassword = "grand2017"
        keyAlias = "grand"
        keyPassword = "grand2017"
      }
    }

    getByName("release") {
      signingConfig = signingConfigs.getByName("releaseConfig")

      isMinifyEnabled = false
      isShrinkResources = false

//      resValue("string", "google_api_key", gradleLocalProperties(rootDir).getProperty("GOOGLE_API_KEY"))
      manifestPlaceholders["appName"] = "@string/app_name"
      manifestPlaceholders["appIcon"] = "@mipmap/ic_launcher"
      manifestPlaceholders["appRoundIcon"] = "@mipmap/ic_launcher"

      buildConfigField("String", "API_BASE_URL", Config.Environments.releaseBaseUrl)
      buildConfigField("String", "ROOM_DB", Config.Environments.roomDb)
    }
  }

  compileOptions {
    isCoreLibraryDesugaringEnabled = true

    sourceCompatibility = JavaVersion.VERSION_11
    targetCompatibility = JavaVersion.VERSION_11
  }

  kotlinOptions {
    jvmTarget = "11"
  }

  dataBinding {
    isEnabled = true
  }
}

dependencies {
  implementation(fileTree(mapOf("dir" to "libs", "include" to listOf("*.jar"))))
	
	implementation("androidx.hilt:hilt-navigation-fragment:1.0.0")

	/*implementation("androidx.activity:activity-ktx:1.6.1")
	implementation("androidx.fragment:fragment-ktx:1.5.5")
	implementation("androidx.core:core-ktx:1.9.0")
	implementation("androidx.appcompat:appcompat:1.5.1")*/
	//implementation("androidx.activity:activity-ktx:1.5.1")

  coreLibraryDesugaring("com.android.tools:desugar_jdk_libs:1.1.5")
  
  implementation("androidx.paging:paging-runtime:3.1.0")
  
  implementation("com.google.android.gms:play-services-location:19.0.1")
  
  implementation("com.google.android.gms:play-services-auth:20.1.0")
  implementation("com.facebook.android:facebook-login:13.0.0")
  
  implementation("com.google.android.libraries.places:places:2.5.0")
  
  /*
  implementation 'se.akerfeldt:okhttp-signpost:1.1.0'
implementation 'com.squareup.okhttp3:okhttp:3.0.0-RC1'
implementation 'oauth.signpost:signpost-core:1.2.1.2'
   */
  implementation("se.akerfeldt:okhttp-signpost:1.1.0")
  implementation("oauth.signpost:signpost-core:1.2.1.2")
  
  implementation("com.google.android.exoplayer:exoplayer:2.16.1")
  
  //Room
  implementation(Libraries.roomVersion)
	implementation("androidx.appcompat:appcompat:1.4.1")
	implementation("com.google.android.material:material:1.5.0")
	implementation("androidx.constraintlayout:constraintlayout:2.1.3")
	kapt(Libraries.roomCompiler)
  implementation(Libraries.roomktx)
  implementation(Libraries.roomCommon)

  // Networking
  implementation(Libraries.retrofit)
  implementation(Libraries.retrofitConverter)
  implementation(Libraries.gson)
  implementation(Libraries.interceptor)
  implementation(Libraries.chuckLogging)
  
  implementation("com.jakewharton.timber:timber:5.0.1")
  
  implementation("com.github.bumptech.glide:glide:4.13.0")
  kapt("com.github.bumptech.glide:compiler:4.13.0")

  // Utils
  implementation(Libraries.playServices)
  implementation(Libraries.localization)
  implementation(Libraries.multidex)
  implementation(Libraries.permissions)
  implementation(Libraries.gson)

// paging
  implementation(Libraries.paging_version)
  implementation(Libraries.paging_version_ktx)
  
  // Hilt
  implementation(Libraries.hilt)
  implementation(Libraries.firebase_platform)
  implementation(Libraries.firebase_messaging)
  implementation(Libraries.firebase_analytics)
  implementation("com.google.maps.android:android-maps-utils:2.3.0")
  implementation("com.google.firebase:firebase-database-ktx:20.0.4")
  kapt(Libraries.hiltDaggerCompiler)
  // Support
  implementation(Libraries.appCompat)
  implementation(Libraries.coreKtx)
  implementation(Libraries.androidSupport)

  // Arch Components
  implementation(Libraries.viewModel)
  implementation(Libraries.lifeData)
  implementation(Libraries.lifecycle)
  implementation(Libraries.viewModelState)

  // Kotlin Coroutines
  implementation(Libraries.coroutinesCore)
  implementation(Libraries.coroutinesAndroid)
//DATA STORE
  implementation(Libraries.datastore_preferences)

  // UI
  implementation(Libraries.materialDesign)
  implementation(Libraries.navigationFragment)
  implementation(Libraries.navigationUI)
  implementation(Libraries.loadingAnimations)
  implementation(Libraries.alerter)
  implementation(Libraries.coil)

  // Map
  implementation(Libraries.map)
  implementation(Libraries.playServicesLocation)
  implementation(Libraries.rxLocation)

  //Ted bottom picker
  implementation(Libraries.ted_bottom_picker)
  
  // Image Slider
  implementation("com.github.smarteist:autoimageslider:1.4.0")

  //Pin code
  implementation(Libraries.pin_code)
  //smarteist
  implementation(Libraries.smartteist)
  //expandable
  implementation(Libraries.expandable)
  //circularprogressbar
  implementation(Libraries.circularprogressbar)

  // Project Modules
  implementation(project(Config.Modules.prettyPopUp))
  
  //Toastable
  implementation("com.github.GrenderG:Toasty:1.5.2")

}