package app.grand.tafwak.core.extensions

import android.content.Context
import android.graphics.Bitmap
import android.graphics.Canvas
import android.os.Build
import android.util.DisplayMetrics
import android.view.View
import android.view.ViewGroup
import android.view.WindowInsets
import android.view.WindowManager
import androidx.core.content.getSystemService
import androidx.core.graphics.createBitmap
import androidx.core.view.drawToBitmap
import kotlin.math.absoluteValue

fun View.toBitmapV3(fixedSize: Int? = null): Bitmap? = toBitmapV3(fixedSize, fixedSize)
fun View.toBitmapV3(fixedWidth: Int? = null, fixedHeight: Int? = null): Bitmap? {
	measure(View.MeasureSpec.UNSPECIFIED, View.MeasureSpec.UNSPECIFIED)

	val bitmap = Bitmap.createBitmap(fixedWidth ?: measuredWidth, fixedHeight ?: measuredHeight, Bitmap.Config.ARGB_8888)

	val canvas = Canvas(bitmap)

	layout(0, 0, fixedWidth ?: measuredWidth, fixedHeight ?: measuredHeight)

	draw(canvas)
	/*
    view.measure(MeasureSpec.UNSPECIFIED, MeasureSpec.UNSPECIFIED);
    Bitmap bitmap = Bitmap.createBitmap(view.getMeasuredWidth(), view.getMeasuredHeight(),
            Bitmap.Config.ARGB_8888);
    Canvas canvas = new Canvas(bitmap);
    view.layout(0, 0, view.getMeasuredWidth(), view.getMeasuredHeight());
    view.draw(canvas);
    return bitmap;
     */

	return bitmap
}

// https://stackoverflow.com/a/63409619
/*
blic static int getScreenWidth(@NonNull Activity activity) {
    if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.R) {
        WindowMetrics windowMetrics = activity.getWindowManager().getCurrentWindowMetrics();
        Insets insets = windowMetrics.getWindowInsets()
                .getInsetsIgnoringVisibility(WindowInsets.Type.systemBars());
        return windowMetrics.getBounds().width() - insets.left - insets.right;
    } else {
        DisplayMetrics displayMetrics = new DisplayMetrics();
        activity.getWindowManager().getDefaultDisplay().getMetrics(displayMetrics);
        return displayMetrics.widthPixels;
    }
 */
fun View.toBitmap(context: Context = this.context): Bitmap? {
	val windowManager = context.getSystemService<WindowManager>() ?: return null
	val (width, height) = if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.R) {
		val metrics = windowManager.currentWindowMetrics
		val insets = metrics.windowInsets.getInsetsIgnoringVisibility(WindowInsets.Type.systemBars())
		Pair(
			metrics.bounds.width().absoluteValue - insets.left - insets.right,
			metrics.bounds.height().absoluteValue - insets.bottom - insets.top
		)
	}else {
		val displayMetrics = DisplayMetrics()
		
		windowManager.defaultDisplay?.getMetrics(displayMetrics) ?: return null
		
		displayMetrics.widthPixels to displayMetrics.heightPixels
	}
	
	layoutParams = ViewGroup.LayoutParams(ViewGroup.LayoutParams.WRAP_CONTENT, ViewGroup.LayoutParams.WRAP_CONTENT)
	measure(
		View.MeasureSpec.makeMeasureSpec(width, View.MeasureSpec.UNSPECIFIED),
		View.MeasureSpec.makeMeasureSpec(height, View.MeasureSpec.UNSPECIFIED)
	)
	layout(0, 0, width, height)
	//buildDrawingCache() todo
	//drawToBitmap()
	
	val bitmap = createBitmap(measuredWidth, measuredHeight, Bitmap.Config.ARGB_8888)
	
	val canvas = Canvas(bitmap)
	
	draw(canvas)
	
	return bitmap
}

/*
public Bitmap createDrawableFromView(Context context, View view) {
        DisplayMetrics displayMetrics = new DisplayMetrics();
        ((Activity) context).getWindowManager().getDefaultDisplay().getMetrics(displayMetrics);
        view.setLayoutParams(new ViewGroup.LayoutParams(ViewGroup.LayoutParams.WRAP_CONTENT, ViewGroup.LayoutParams.WRAP_CONTENT));
        view.measure(displayMetrics.widthPixels, displayMetrics.heightPixels);
        view.layout(0, 0, displayMetrics.widthPixels, displayMetrics.heightPixels);
        view.buildDrawingCache();
        Bitmap bitmap = Bitmap.createBitmap(view.getMeasuredWidth(), view.getMeasuredHeight(), Bitmap.Config.ARGB_8888);

        Canvas canvas = new Canvas(bitmap);
        view.draw(canvas);

        return bitmap;
    }
 */
