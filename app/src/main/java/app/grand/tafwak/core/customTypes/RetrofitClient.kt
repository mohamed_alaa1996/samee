package app.grand.tafwak.core.customTypes

import com.google.gson.GsonBuilder
import com.structure.base_mvvm.BuildConfig
import okhttp3.OkHttpClient
import okhttp3.logging.HttpLoggingInterceptor
import retrofit2.Retrofit
import retrofit2.converter.gson.GsonConverterFactory
import se.akerfeldt.okhttp.signpost.OkHttpOAuthConsumer
import se.akerfeldt.okhttp.signpost.SigningInterceptor
import java.util.concurrent.TimeUnit

class RetrofitClient {
	companion object {
		private var retrofit: Retrofit? = null
		private val gSON = GsonBuilder()
			.setLenient()
			.create()
		
		fun getClient(baseUrl: String, consumer: OkHttpOAuthConsumer): Retrofit? {
			val logging = HttpLoggingInterceptor()
			if (BuildConfig.DEBUG) {
				logging.level = HttpLoggingInterceptor.Level.BODY
			} else {
				logging.level = HttpLoggingInterceptor.Level.NONE
			}
			
			val httpClient = OkHttpClient.Builder()
			httpClient.connectTimeout(60000, TimeUnit.SECONDS)
			httpClient.writeTimeout(120000, TimeUnit.SECONDS)
			httpClient.readTimeout(120000, TimeUnit.SECONDS)
			httpClient.retryOnConnectionFailure(true)
			httpClient.addInterceptor(SigningInterceptor(consumer))
			httpClient.addInterceptor { chain ->
				val request = chain.request()
				val requestBuilder = request.newBuilder()
				val modifiedRequest = requestBuilder.build()
				chain.proceed(modifiedRequest)
			}
			
			httpClient.addNetworkInterceptor(logging)
			
			if (retrofit == null) {
				retrofit = Retrofit.Builder()
					.baseUrl(baseUrl)
					.addConverterFactory(GsonConverterFactory.create(gSON))
					.client(httpClient.build())
					.build()
			}
			return retrofit
		}
		
	}
}