package app.grand.tafwak.core.extensions

fun String?.toIntOrZero() = this?.toIntOrNull() ?: 0

fun Int?.orZero() = this ?: 0

fun Float.toIntOrFloat(): Number = if (this - toInt().toFloat() == 0f) toInt() else this
