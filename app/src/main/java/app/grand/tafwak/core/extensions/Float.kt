package app.grand.tafwak.core.extensions

import java.math.BigDecimal

fun Float.roundHalfUp(scale: Int): Float {
	return toBigDecimal().setScale(scale, BigDecimal.ROUND_HALF_UP).toFloat()
}

fun Float?.orZero() = this ?: 0f
