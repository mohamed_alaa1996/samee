package app.grand.tafwak.core.extensions

import app.grand.tafwak.core.di.module.GsonModule
import com.google.gson.Gson
import com.google.gson.internal.`$Gson$Types`
import com.google.gson.reflect.TypeToken
import java.lang.reflect.ParameterizedType
import java.lang.reflect.Type
import kotlin.reflect.javaType
import kotlin.reflect.typeOf

inline fun <reified E> E?.toJsonOrNull(gson: Gson? = null): String? = kotlin.runCatching {
	toJson<E>(gson)
}.getOrNull()

inline fun <reified E> E?.toJson(gson: Gson? = null): String {
	return (gson ?: GsonModule.provideGson()).toJson(this, object : TypeToken<E>(){}.type)
}

inline fun <reified E> String?.fromJsonOrNull(gson: Gson? = null): E? = kotlin.runCatching {
	fromJson<E>(gson)
}.getOrNull()

inline fun <reified E> String?.fromJson(
	gson: Gson? = null,
): E {
	return (gson ?: GsonModule.provideGson()).fromJson(this, object : TypeToken<E>(){}.type)
}
