package app.grand.tafwak.core.extensions

import android.Manifest
import android.annotation.SuppressLint
import android.content.Context
import android.content.pm.PackageManager
import android.location.Address
import android.location.Geocoder
import android.provider.Settings
import android.util.TypedValue
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.annotation.LayoutRes
import androidx.annotation.MainThread
import androidx.annotation.WorkerThread
import androidx.core.content.ContextCompat
import com.structure.base_mvvm.R
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.withContext
import okio.IOException
import timber.log.Timber
import java.util.*
import kotlin.math.roundToInt

fun Context.dpToPx(value: Float): Float {
	return TypedValue.applyDimension(
		TypedValue.COMPLEX_UNIT_DIP, value, resources.displayMetrics
	)
}

/** - Layout inflater from `receiver`, by using [LayoutInflater.from] */
val Context.layoutInflater: LayoutInflater
	get() = LayoutInflater.from(this)

/**
 * - Inflates a layout from [layoutRes] isa.
 *
 * @param parent provide [ViewGroup.LayoutParams] to the returned root view, default is `null`
 * @param attachToRoot if true then the returned view will be attached to [parent] if not `null`,
 * default is false isa.
 *
 * @return rootView in the provided [layoutRes] isa.
 */
@JvmOverloads
fun Context.inflateLayout(
	@LayoutRes layoutRes: Int,
	parent: ViewGroup? = null,
	attachToRoot: Boolean = false
): View {
	return layoutInflater.inflate(layoutRes, parent, attachToRoot)
}

fun Context.checkSelfPermissionGranted(permission: String): Boolean {
	return ContextCompat.checkSelfPermission(this, permission) == PackageManager.PERMISSION_GRANTED
}

@WorkerThread
fun Context.getAddressFromLatitudeAndLongitude(
	latitude: Double,
	longitude: Double,
	fallbackAddress: String = getString(R.string.your_address_has_been_selected_successfully),
): String {
	val address = try {
		val geocoder = Geocoder(this, Locale(getProjectCurrentLocale()))
		val addresses: List<Address> = geocoder.getFromLocation(latitude, longitude, 1)
		if (addresses.isNotEmpty()) {
			addresses[0].getAddressLine(0).also {
				Timber.d(it)
			}
		}else {
			Timber.d("address NULL")

			null
		}
	}catch (throwable: Throwable) {
		Timber.e(throwable)
		
		null
	}
	
	return address ?: fallbackAddress
}

suspend fun Context.getAddressFromLatitudeAndLongitudeSuspend(
	latitude: Double,
	longitude: Double,
	fallbackAddress: String = getString(R.string.your_address_has_been_selected_successfully),
): String = withContext(Dispatchers.Default) {
	getAddressFromLatitudeAndLongitude(latitude, longitude, fallbackAddress)
}

@SuppressLint("HardwareIds")
fun Context.getDeviceIdWithoutPermission(fallbackOfNullOrEmpty: String = "device_id"): String {
	return kotlin.runCatching {
		Settings.Secure.getString(contentResolver, Settings.Secure.ANDROID_ID)
	}.getOrNull().let {
		if (it.isNullOrEmpty()) fallbackOfNullOrEmpty else it
	}
}
