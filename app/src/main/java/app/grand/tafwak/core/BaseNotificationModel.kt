package app.grand.tafwak.core

import com.google.gson.annotations.SerializedName

/*
'admin=> by admin (has no action)','new-order=> if has new order','wallet=>if added to wallet and
if deducted ','order-payment => pay order money'

no action open notification list but on click on notificaction item on list isn't clickable

type
target_id
title
body
sound
 */
data class BaseNotificationModel(
	var type: String,
	@SerializedName("target_id") var targetId: String,
	var title: String,
	var body: String,
	var sound: String,
) {
	
	companion object {
		fun fromMap(map: Map<String, String>) : BaseNotificationModel {
			return BaseNotificationModel(
				map.getValue("type"),
				map.getValue("target_id"),
				map.getValue("title"),
				map.getValue("body"),
				map.getValue("sound"),
			)
		}
	}
	
	val actualType get() = Type.values().firstOrNull { it.apiKey == type }
	
	enum class Type(val apiKey: String) {
		ADMIN("admin"),
		NEW_ORDER("new-order"),
		WALLET("wallet"),
		/**
		 * ba3d ma el provider by3mel accept le el order -> el user bygelo de 34an ydfa3 bs lesa
		 * mat3amaletsh el payment
		 */
		ORDER_PAYMENT("order-payment"),
    NEW_MESSAGE("new-message")
	}
	
}
