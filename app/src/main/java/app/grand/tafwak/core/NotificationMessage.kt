package app.grand.tafwak.core

import com.google.gson.Gson

data class NotificationMessage<T>(
  val targetId: String,
  val userId: String,
  val body: String,
  val data: T,
  val type: String,
  val sound: String,
  val title: String
) {
  companion object {
    fun <T> mapToNotificationMessage(map: Map<String, String>, dataClass: Class<T>): NotificationMessage<T> {
      val targetId = map["target_id"] as String
      val userId = map["user_id"] as String
      val body = map["body"] as String
      val dataMap = map["data"] as String
      val data = mapToDataObject(dataMap, dataClass)
      val type = map["type"] as String
      val sound = map["sound"] as String
      val title = map["title"] as String

      return NotificationMessage(targetId, userId, body, data, type, sound, title)
    }

    fun <T> mapToDataObject(map : String, dataClass: Class<T>): T {
      return Gson().fromJson(map, dataClass)
    }
  }
}


