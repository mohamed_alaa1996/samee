package app.grand.tafwak.core.di.module

import com.google.gson.Gson
import dagger.Module
import dagger.Provides
import dagger.hilt.InstallIn
import dagger.hilt.components.SingletonComponent
import okhttp3.OkHttpClient
import retrofit2.Retrofit
import retrofit2.converter.gson.GsonConverterFactory
import retrofit2.create
import javax.inject.Singleton

@Module
@InstallIn(SingletonComponent::class)
object MARetrofitModule {
	
	const val BASE_URL = "https://samee.my-staff.net/"
	
	@Singleton
	@Provides
	fun provideRetrofit(
		gsonConverterFactory: GsonConverterFactory,
		okHttpClient: OkHttpClient,
	): Retrofit {
		return Retrofit.Builder()
			.baseUrl(BASE_URL)
			.addConverterFactory(gsonConverterFactory)
			.client(okHttpClient)
			.build()
	}
	
	@Provides
	fun provideGsonConverterFactory(gson: Gson): GsonConverterFactory {
		return GsonConverterFactory.create(gson)
	}
	
}