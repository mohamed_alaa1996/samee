package app.grand.tafwak.core.customTypes

import app.grand.tafwak.domain.utils.MAResult

class MAPagingException(val failure: MAResult.Failure<*>) : Exception(failure.message)
