package app.grand.tafwak.core

import android.content.Context
import android.widget.Toast
import androidx.core.content.ContextCompat
import androidx.multidex.MultiDex
import app.grand.tafwak.core.extensions.getProjectCurrentLocale
import app.grand.tafwak.presentation.base.utils.LanguagesHelper
import com.facebook.FacebookSdk
import com.facebook.appevents.AppEventsLogger
import com.google.android.gms.common.GooglePlayServicesNotAvailableException
import com.google.android.gms.common.GooglePlayServicesRepairableException
import com.google.android.gms.security.ProviderInstaller
import com.structure.base_mvvm.R
import com.zeugmasolutions.localehelper.LocaleAwareApplication
import dagger.hilt.android.HiltAndroidApp
import es.dmoral.toasty.Toasty
import timber.log.Timber
import java.security.KeyManagementException
import java.security.NoSuchAlgorithmException
import javax.net.ssl.SSLContext

@HiltAndroidApp
class MyApplication : LocaleAwareApplication() {
	
	private var toast: Toast? = null
	
	override fun attachBaseContext(base: Context) {
		super.attachBaseContext(base)
		MultiDex.install(this)
	}
	
	override fun onCreate() {
		super.onCreate()

		FacebookSdk.sdkInitialize(applicationContext);
		AppEventsLogger.activateApp(this);
		
		Timber.plant(LineNumberDebugTree())
		
		updateAndroidSecurityProvider()
		
		setupToasting()
	}
	
	private fun updateAndroidSecurityProvider() {
		// To fix the following issue, when run app in cellular data, Apis not working
		// javax.net.ssl.SSLHandshakeException: SSL handshake aborted: ssl=0x7edfc49e08: I/O error during system call, Connection reset by peer
		try {
			ProviderInstaller.installIfNeeded(applicationContext)
			val sslContext: SSLContext = SSLContext.getInstance("TLSv1.2")
			sslContext.init(null, null, null)
			sslContext.createSSLEngine()
		} catch (e: GooglePlayServicesRepairableException) {
			e.printStackTrace()
		} catch (e: GooglePlayServicesNotAvailableException) {
			e.printStackTrace()
		} catch (e: NoSuchAlgorithmException) {
			e.printStackTrace()
		} catch (e: KeyManagementException) {
			e.printStackTrace()
		}
	}
	
	private fun setupToasting() {
		Toasty.Config.getInstance().allowQueue(false).apply()
	}
	
	fun showSuccessToast(msg: String) {
		Toasty.custom(this, msg, R.drawable.ic_check_white_24dp, R.color.successColor, Toast.LENGTH_SHORT, true, true).show()
	}
	
	fun showErrorToast(msg: String) {
		//Toasty.error(this, msg, Toast.LENGTH_SHORT, true).show()
		Toasty.custom(
			this,
			msg,
			ContextCompat.getDrawable(this, R.drawable.ic_clear_white_24dp),
			ContextCompat.getColor(this, R.color.white),
			ContextCompat.getColor(this, R.color.errorColor),
			Toast.LENGTH_SHORT,
			true,
			true
		).show()
	}
	
	fun showNormalToast(msg: String) {
		Toasty.normal(this, msg, Toast.LENGTH_SHORT, null, false).show()
	}
	
	class LineNumberDebugTree : Timber.DebugTree() {
		override fun createStackElementTag(element: StackTraceElement): String {
			return "(${element.fileName}:${element.lineNumber})#${element.methodName}"
		}
	}
	
}