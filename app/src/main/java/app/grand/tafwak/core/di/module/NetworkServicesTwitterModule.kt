package app.grand.tafwak.core.di.module

import app.grand.tafwak.data.auth.data_source.remote.TwitterServices
import dagger.Module
import dagger.Provides
import dagger.hilt.InstallIn
import dagger.hilt.components.SingletonComponent
import okhttp3.OkHttpClient
import okhttp3.logging.HttpLoggingInterceptor
import retrofit2.Retrofit
import retrofit2.converter.gson.GsonConverterFactory
import retrofit2.create
import se.akerfeldt.okhttp.signpost.OkHttpOAuthConsumer
import se.akerfeldt.okhttp.signpost.SigningInterceptor
import timber.log.Timber
import java.util.concurrent.TimeUnit
import javax.inject.Singleton

@Module
@InstallIn(SingletonComponent::class)
object NetworkServicesTwitterModule {
	
	private const val BASE_URL = "https://api.twitter.com/"
	
	@Singleton
	@Provides
	fun provideTwitterServices(
		gsonConverterFactory: GsonConverterFactory,
	): TwitterServices {
		val loggingInterceptor = HttpLoggingInterceptor { message ->
			Timber.d(message)
		}.also {
			it.level = HttpLoggingInterceptor.Level.BODY
		}

		// APi consumer key
		// OPnSKbrUsEUjfWFeQISjqRhOM
		// Secret
		// UuSel7MK2YJ4q6OAQSS17fFSTibvaWOtIi70o6LNPO3Re4SH49

		// Access Token after regeneration
		// 1491346428966453250-KbU1vjqDWtp7YbvUnfrsHU7hvrODW0
		// Access Token Secret
		// rke77lUD2pj8hAxGbOygFPWlXBZb9ADAXkHfOfhMDmBP8
		val consumer = OkHttpOAuthConsumer(
			"OPnSKbrUsEUjfWFeQISjqRhOM",
			"UuSel7MK2YJ4q6OAQSS17fFSTibvaWOtIi70o6LNPO3Re4SH49"
		)
		//consumer.setTokenWithSecret() // https://developer.twitter.com/en/portal/projects/1504890240615567362/apps/23691776/keys
		
		val okHttpClient = OkHttpClient.Builder().apply {
			connectTimeout(OkHttpModule.TIMEOUT_IN_SEC, TimeUnit.SECONDS)
			readTimeout(OkHttpModule.TIMEOUT_IN_SEC, TimeUnit.SECONDS)
			writeTimeout(OkHttpModule.TIMEOUT_IN_SEC, TimeUnit.SECONDS)
			
			addInterceptor(SigningInterceptor(consumer))
			
			addInterceptor { chain ->
				val request = chain.request()
				val requestBuilder = request.newBuilder()
				val modifiedRequest = requestBuilder.build()
				chain.proceed(modifiedRequest)
			}
			
			addNetworkInterceptor(loggingInterceptor)
		}.build()
		
		return Retrofit.Builder()
			.baseUrl(BASE_URL)
			.addConverterFactory(gsonConverterFactory)
			.client(okHttpClient)
			.build()
			.create()
	}

}
