package app.grand.tafwak.core.extensions

import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import app.grand.tafwak.core.customTypes.TransformationsUtils
import app.grand.tafwak.domain.auth.entity.response.ResponseVerifyPhone
import app.grand.tafwak.domain.utils.MABaseResponse
import app.grand.tafwak.domain.utils.MAResult

inline fun <X, Y> LiveData<X>.map(crossinline transform: (X) -> Y): MutableLiveData<Y> =
	TransformationsUtils.map(this) { transform(it) }

inline fun <X, Y> LiveData<X>.mapNullable(crossinline transform: (X?) -> Y): MutableLiveData<Y> =
	TransformationsUtils.map(this) { transform(it) }

inline fun <X, Y> LiveData<X>.switchMap(crossinline transform: (X) -> LiveData<out Y>): LiveData<out Y> =
	TransformationsUtils.switchMap(this) { transform(it) }

inline fun <Y> switchMapMultiple(
	vararg liveData: LiveData<*>,
	crossinline transform: () -> LiveData<Y>
): LiveData<Y> {
	return TransformationsUtils.switchMapMultiple(
		{ transform() },
		*liveData
	)
}
