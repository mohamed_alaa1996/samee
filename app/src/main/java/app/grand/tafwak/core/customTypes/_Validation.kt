package app.grand.tafwak.core.customTypes

import androidx.annotation.StringRes
import com.structure.base_mvvm.R

@StringRes
fun String?.isPhoneValid(): Int? {
	return if (isNullOrEmpty()) {
		R.string.field_required
	}else {
		if (true) return null

		val regex = Regex("^[0-9-+]{9,15}$", RegexOption.IGNORE_CASE)
		
		if (regex.matches(this)) null else R.string.invalid_phone
	}
}
