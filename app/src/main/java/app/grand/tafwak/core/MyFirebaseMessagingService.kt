package app.grand.tafwak.core

import android.app.Notification
import android.app.NotificationChannel
import android.app.NotificationManager
import android.app.PendingIntent
import android.content.Context
import android.content.Intent
import android.graphics.BitmapFactory
import android.media.RingtoneManager
import android.os.Build
import android.os.Bundle
import android.util.Log
import androidx.core.app.NotificationCompat
import app.grand.tafwak.core.extensions.addValue
import app.grand.tafwak.domain.providerFlow.ResponseChatMessage
import app.grand.tafwak.presentation.project.ProjectActivity
import com.google.firebase.messaging.FirebaseMessagingService
import com.google.firebase.messaging.RemoteMessage
import com.google.gson.Gson
import com.structure.base_mvvm.R
import com.readystatesoftware.chuck.internal.ui.MainActivity
import timber.log.Timber

/*
'admin=> by admin (has no action)','new-order=> if has new order','wallet=>if added to wallet and
if deducted ','order-payment => pay order money'

no action open notification list but on click on notificaction item on list isn't clickable

type
target_id
title
body
sound
 */


class MyFirebaseMessagingService : FirebaseMessagingService() {

  companion object {
    const val NOTIFICATION_ID = 2
  }

  override fun onMessageReceived(remoteMessage: RemoteMessage) {
    Log.e("dijowwed", "doewdoewkdkewo")
    Timber.e("onMessageReceived START")
    sendNotification(remoteMessage.data)
  }

  override fun onNewToken(token: String) {
  }

  private fun sendNotification(messageBody: MutableMap<String, String>) {
    Timber.e("onMessageReceived map $messageBody")


    val model = BaseNotificationModel.fromMap(messageBody)

    Timber.e("onMessageReceived model $model")

    val intent = Intent(this, ProjectActivity::class.java)
    intent.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK or Intent.FLAG_ACTIVITY_CLEAR_TASK)


    if (model.type == BaseNotificationModel.Type.NEW_MESSAGE.apiKey) {
      val notificationMessage =
        NotificationMessage.mapToNotificationMessage(messageBody, ResponseChatMessage::class.java)

      Intent().apply {
        action = BaseNotificationModel.Type.NEW_MESSAGE.apiKey

        putExtra(BaseNotificationModel.Type.NEW_MESSAGE.apiKey ,  Gson().toJson(notificationMessage.data))

        sendBroadcast(this)
      }

    }


    //sendBroadcast(Intent(model.type)) val broadcase and on register intentfilter(kaza)
    // todo sendBroadcast + deep link navigaiton isa.
    //  https://developer.android.com/guide/navigation/navigation-deep-link#explicit

    // todo pending intent from navigation jetpack isa.
    val pendingIntent = PendingIntent.getActivity(
      applicationContext, 0 /* Request code */, intent,
      PendingIntent.FLAG_IMMUTABLE
    )

    val channelId = "channelId"
    val defaultSoundUri = RingtoneManager.getDefaultUri(RingtoneManager.TYPE_NOTIFICATION)
    val notificationBuilder = NotificationCompat.Builder(this, channelId)
      .setPriority(NotificationManager.IMPORTANCE_HIGH)
      .setSmallIcon(R.mipmap.ic_launcher)
      .setLargeIcon(BitmapFactory.decodeResource(resources, R.mipmap.ic_launcher))
      .setContentTitle(model.title)
      .setContentText(model.body)
      .setStyle(NotificationCompat.BigTextStyle().bigText(model.body))
      .setBadgeIconType(NotificationCompat.BADGE_ICON_SMALL)
      .setAutoCancel(true)
      .setShowWhen(true)
      .setSound(defaultSoundUri)
      .setDefaults(Notification.DEFAULT_SOUND or Notification.DEFAULT_LIGHTS or Notification.DEFAULT_VIBRATE)
      .setContentIntent(pendingIntent)

    val notificationManager = getSystemService(Context.NOTIFICATION_SERVICE) as NotificationManager

    // Since android Oreo notification channel is needed.
    if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.O) {
      val channel = NotificationChannel(
        channelId,
        getString(R.string.notifications_2),
        NotificationManager.IMPORTANCE_HIGH
      )
      channel.description = getString(R.string.notifications_2)
      notificationBuilder.setChannelId(channelId)
      notificationManager.createNotificationChannel(channel)
    }

    notificationManager.notify(
      NOTIFICATION_ID /* ID of notification */,
      notificationBuilder.build()
    )
  }


}