package app.grand.tafwak.core.di.module.qualifiers

import javax.inject.Qualifier

@Qualifier
@Retention(AnnotationRetention.BINARY)
annotation class HeadersInterceptor
