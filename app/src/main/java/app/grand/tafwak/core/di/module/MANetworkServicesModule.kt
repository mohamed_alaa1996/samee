package app.grand.tafwak.core.di.module

import app.grand.tafwak.data.auth.data_source.remote.SameeAuthServices
import app.grand.tafwak.data.maSettings.SameeSettingsServices
import app.grand.tafwak.data.provider.dataSource.remote.SameeProviderServices
import app.grand.tafwak.data.user.dataSource.remote.SameeUserServices
import dagger.Module
import dagger.Provides
import dagger.hilt.InstallIn
import dagger.hilt.components.SingletonComponent
import retrofit2.Retrofit
import javax.inject.Singleton

@Module
@InstallIn(SingletonComponent::class)
object MANetworkServicesModule {
	
	@Provides
    @Singleton
    fun provideSameeUserServices(retrofit: Retrofit): SameeUserServices =
        retrofit.create(SameeUserServices::class.java)
	
	@Provides
    @Singleton
    fun provideSameeProviderServices(retrofit: Retrofit): SameeProviderServices =
        retrofit.create(SameeProviderServices::class.java)
	
	@Provides
    @Singleton
    fun provideSameeSettingsServices(retrofit: Retrofit): SameeAuthServices =
        retrofit.create(SameeAuthServices::class.java)
	
	@Provides
    @Singleton
    fun provideSameeAuthServices(retrofit: Retrofit): SameeSettingsServices =
        retrofit.create(SameeSettingsServices::class.java)
	
}
