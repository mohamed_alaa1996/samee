package app.grand.tafwak.core.di.module

import androidx.lifecycle.SavedStateHandle
import app.grand.tafwak.core.extensions.asBundle
import app.grand.tafwak.presentation.auth2.confirm.ConfirmPhoneFragmentArgs
import app.grand.tafwak.presentation.auth2.location.LocationSelectionFragmentArgs
import app.grand.tafwak.presentation.auth2.location.LocationTrackingFragmentArgs
import app.grand.tafwak.presentation.auth2.location.LocationUserRegisterFragmentArgs
import app.grand.tafwak.presentation.auth2.referral.CheckReferralCodeDialogFragmentArgs
import app.grand.tafwak.presentation.base.InfoImageAndListOfTextsFragmentArgs
import app.grand.tafwak.presentation.project.ChatDetailsFragmentArgs
import app.grand.tafwak.presentation.project.ContinuePaymentFragmentArgs
import app.grand.tafwak.presentation.providerFlow.CreateOrEditServiceDialogFragmentArgs
import app.grand.tafwak.presentation.providerFlow.OrderDetailsSeenByProviderFragmentArgs
import app.grand.tafwak.presentation.providerFlow.ServicesSelectionFragmentArgs
import app.grand.tafwak.presentation.userFlow.*
import app.grand.tafwak.presentation.userHome.MoreUserFragmentArgs
import app.grand.tafwak.presentation.userHome.OrdersUserFragmentArgs
import app.grand.tafwak.presentation.userHome.UserFragmentArgs
import dagger.Module
import dagger.Provides
import dagger.hilt.InstallIn
import dagger.hilt.android.components.ViewModelComponent
import timber.log.Timber

@Module
@InstallIn(ViewModelComponent::class)
object ViewModelArgsModule {
	
	@Provides
	fun provideUserFragmentArgs(state: SavedStateHandle): UserFragmentArgs {
		return UserFragmentArgs.fromBundle(state.asBundle())
	}

	@Provides
	fun provideCheckReferralCodeDialogFragmentArgs(state: SavedStateHandle): CheckReferralCodeDialogFragmentArgs {
		return CheckReferralCodeDialogFragmentArgs.fromBundle(state.asBundle())
	}
	
	@Provides
	fun provideInfoImageAndListOfTextsFragmentArgs(state: SavedStateHandle): InfoImageAndListOfTextsFragmentArgs {
		return InfoImageAndListOfTextsFragmentArgs.fromBundle(state.asBundle())
	}
	
	@Provides
	fun provideConfirmPhoneFragmentArgs(state: SavedStateHandle): ConfirmPhoneFragmentArgs {
		return ConfirmPhoneFragmentArgs.fromBundle(state.asBundle())
	}
	
	@Provides
	fun provideLocationUserRegisterFragmentArgs(state: SavedStateHandle): LocationUserRegisterFragmentArgs {
		return LocationUserRegisterFragmentArgs.fromBundle(state.asBundle())
	}
	
	@Provides
	fun provideSearchResultsProvidersFragmentArgs(state: SavedStateHandle): SearchResultsProvidersFragmentArgs {
		return SearchResultsProvidersFragmentArgs.fromBundle(state.asBundle())
	}
	
	@Provides
	fun provideFilterPickerDialogFragmentArgs(state: SavedStateHandle): FilterPickerDialogFragmentArgs {
		return FilterPickerDialogFragmentArgs.fromBundle(state.asBundle())
	}
	
	@Provides
	fun provideProviderDetailsFragmentArgs(state: SavedStateHandle): ProviderDetailsFragmentArgs {
		return ProviderDetailsFragmentArgs.fromBundle(state.asBundle())
	}
	
	@Provides
	fun provideOrderConfirmationFragmentArgs(state: SavedStateHandle): OrderConfirmationFragmentArgs {
		return OrderConfirmationFragmentArgs.fromBundle(state.asBundle())
	}
	
	@Provides
	fun provideOrderDetailsFragmentArgs(state: SavedStateHandle): OrderDetailsFragmentArgs {
		return OrderDetailsFragmentArgs.fromBundle(state.asBundle())
	}
	
	@Provides
	fun provideLocationSelectionFragmentArgs(state: SavedStateHandle): LocationSelectionFragmentArgs {
		return LocationSelectionFragmentArgs.fromBundle(state.asBundle())
	}
	
	@Provides
	fun provideManualLocationFragmentArgs(state: SavedStateHandle): ManualLocationFragmentArgs {
		return ManualLocationFragmentArgs.fromBundle(state.asBundle())
	}
	
	@Provides
	fun provideCancelOrderDialogFragmentArgs(state: SavedStateHandle): CancelOrderDialogFragmentArgs {
		return CancelOrderDialogFragmentArgs.fromBundle(state.asBundle())
	}
	
	@Provides
	fun provideRateDialogFragmentArgs(state: SavedStateHandle): RateDialogFragmentArgs {
		return RateDialogFragmentArgs.fromBundle(state.asBundle())
	}
	
	@Provides
	fun provideShowAllPreviousWorksFragmentArgs(state: SavedStateHandle): ShowAllPreviousWorksFragmentArgs {
		return ShowAllPreviousWorksFragmentArgs.fromBundle(state.asBundle())
	}
	
	@Provides
	fun provideSelectProviderOnMapFragmentArgs(state: SavedStateHandle): SelectProviderOnMapFragmentArgs {
		return SelectProviderOnMapFragmentArgs.fromBundle(state.asBundle())
	}
	
	@Provides
	fun provideServicesSelectionFragmentArgs(state: SavedStateHandle): ServicesSelectionFragmentArgs {
		return ServicesSelectionFragmentArgs.fromBundle(state.asBundle())
	}
	
	@Provides
	fun provideCreateOrEditServiceDialogFragmentArgs(state: SavedStateHandle): CreateOrEditServiceDialogFragmentArgs {
		return CreateOrEditServiceDialogFragmentArgs.fromBundle(state.asBundle())
	}
	
	@Provides
	fun provideMoreUserFragmentArgs(state: SavedStateHandle): MoreUserFragmentArgs {
		return MoreUserFragmentArgs.fromBundle(state.asBundle())
	}
	
	@Provides
	fun provideOrdersUserFragmentArgs(state: SavedStateHandle): OrdersUserFragmentArgs {
		return OrdersUserFragmentArgs.fromBundle(state.asBundle())
	}
	
	@Provides
	fun providePersonalDataFragmentArgs(state: SavedStateHandle): PersonalDataFragmentArgs {
		return PersonalDataFragmentArgs.fromBundle(state.asBundle())
	}
	
	@Provides
	fun provideChatDetailsFragmentArgs(state: SavedStateHandle): ChatDetailsFragmentArgs {
		return ChatDetailsFragmentArgs.fromBundle(state.asBundle())
	}
	
	@Provides
	fun provideContinuePaymentFragmentArgs(state: SavedStateHandle): ContinuePaymentFragmentArgs {
		return ContinuePaymentFragmentArgs.fromBundle(state.asBundle())
	}
	
	@Provides
	fun provideOrderDetailsSeenByProviderFragmentArgs(state: SavedStateHandle): OrderDetailsSeenByProviderFragmentArgs {
		return OrderDetailsSeenByProviderFragmentArgs.fromBundle(state.asBundle())
	}
	
	@Provides
	fun provideLocationTrackingFragmentArgs(state: SavedStateHandle): LocationTrackingFragmentArgs {
		return LocationTrackingFragmentArgs.fromBundle(state.asBundle())
	}
	
}
