package app.grand.tafwak.core.customTypes

/**
 * @param month 1 (Jan) to 12 (Dec)
 */
data class MADateAndTime(
	var year: Int,
	var month: Int,
	var day: Int,
	var hour24: Int,
	var minute: Int,
) {
	
	val isAm get() = hour24 < 12

	val hour12 get() = when {
		hour24 == 0 -> 12
		hour24 > 12 -> (hour24 - 12)
		else -> hour24
	}
	
	/**
	 * @return format is yyyy-mm-dd and if single digit prefix by `0` ex. April -> 04
	 */
	fun getApiDateFormat(): String {
		val mm = if (month < 10) "0$month" else month.toString()
		val dd = if (day < 10) "0$day" else day.toString()
		
		return "$year-$mm-$dd"
	}
	
	/**
	 * @return format is hh:mm and if single digit prefix by `0` ex. 5 -> 05
	 */
	fun getApiTimeFormat(): String {
		val hh = if (hour24 < 10) "0$hour24" else hour24.toString()
		val mm = if (minute < 10) "0$minute" else minute.toString()
		
		return "$hh:$mm"
	}
	
}
