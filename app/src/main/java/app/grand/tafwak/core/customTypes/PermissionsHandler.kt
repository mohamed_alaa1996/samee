package app.grand.tafwak.core.customTypes

import android.Manifest
import android.content.Context
import android.content.Intent
import android.content.pm.PackageManager
import android.net.Uri
import android.provider.Settings
import androidx.activity.result.ActivityResultCallback
import androidx.activity.result.ActivityResultLauncher
import androidx.activity.result.contract.ActivityResultContract
import androidx.activity.result.contract.ActivityResultContracts
import androidx.appcompat.app.AppCompatActivity
import androidx.core.content.ContextCompat
import androidx.fragment.app.Fragment
import app.grand.tafwak.core.extensions.showErrorToast
import app.grand.tafwak.presentation.base.extensions.launchSafely
import app.grand.tafwak.presentation.base.extensions.showAlertDialog
import com.structure.base_mvvm.R
import java.lang.ref.WeakReference

class PermissionsHandler private constructor(
    host: Any,
    context: Context,
    listener: Listener,
    private val permissions: List<String>,
    private val onPermissionsGranted: () -> Unit
) {

    private val weakRefHost = WeakReference(host)
    private val weakRefContext = WeakReference(context)
    private val weakRefListener = WeakReference(listener)

    private val activityResult = registerForActivityResult(
        ActivityResultContracts.RequestMultiplePermissions()
    ) { permissions ->
        val activity = getActivityOrNull()

        when {
            permissions.values.toList().all { it == true } -> {
                onPermissionsGranted()
            }
            activity != null && (shouldShowRequestPermissionRationale(Manifest.permission.ACCESS_COARSE_LOCATION)
                    || shouldShowRequestPermissionRationale(Manifest.permission.ACCESS_FINE_LOCATION)) -> {
                activity.showAlertDialog(
                    activity.getString(R.string.allow_location_permission),
                    activity.getString(R.string.this_app_need_allow_location),
                    onDismissListener = {
                        activity.showErrorToast(activity.getString(R.string.you_didn_t_accept_permission))
                    }
                ) {
                    startGrantingPermissions()
                }
            }
            else -> {
                weakRefListener.get()?.onDenyLocationPermissions(this)
            }
        }
    }

    private val activityResultPermissionsSystemSettings = registerForActivityResult(
        ActivityResultContracts.StartActivityForResult()
    ) {
        if (arePermissionsGranted()) {
            onPermissionsGranted()
        }else {
            startGrantingPermissions()
        }
    }

    fun arePermissionsGranted(): Boolean {
        val context = weakRefContext.get() ?: return false

        return permissions.firstOrNull {
            ContextCompat.checkSelfPermission(context, it) != PackageManager.PERMISSION_GRANTED
        } != null
    }

    fun startGrantingPermissions() {
        activityResult.launchSafely(
            weakRefContext.get(),
            permissions.toTypedArray()
        )
    }

    private fun <I, O> registerForActivityResult(
        contract: ActivityResultContract<I, O>,
        callback: ActivityResultCallback<O>
    ): ActivityResultLauncher<I> {
        return when (val host = weakRefHost.get()) {
            is Fragment -> {
                host.registerForActivityResult(contract, callback)
            }
            is AppCompatActivity -> {
                host.registerForActivityResult(contract, callback)
            }
            else -> throw RuntimeException("Unexpected host $host")
        }
    }

    private fun shouldShowRequestPermissionRationale(permission: String): Boolean = when (val host = weakRefHost.get()) {
        is Fragment -> host.shouldShowRequestPermissionRationale(permission)
        is AppCompatActivity -> host.shouldShowRequestPermissionRationale(permission)
        else -> false
    }

    private fun getActivityOrNull() = when (val value = weakRefHost.get()) {
        is Fragment -> value.activity
        is AppCompatActivity -> value
        else -> null
    }

    interface Listener {
        fun onDenyLocationPermissions(permissionsHandler: PermissionsHandler) {
            val context = permissionsHandler.weakRefContext.get()

            permissionsHandler.getActivityOrNull()?.apply {
                showAlertDialog(
                    getString(R.string.change_permission_in_settings_of_device),
                    getString(R.string.this_app_need_allow_location),
                    onDismissListener = {
                        context?.showErrorToast(context.getString(R.string.you_didn_t_accept_permission))
                    }
                ) {
                    val intent = Intent(Settings.ACTION_APPLICATION_DETAILS_SETTINGS).also {
                        it.data = Uri.fromParts("package", packageName, null)
                    }

                    permissionsHandler.activityResultPermissionsSystemSettings.launchSafely(
                        permissionsHandler.weakRefContext.get(),
                        intent
                    )
                }
            }
        }
    }

}
