package app.grand.tafwak.core.extensions

import androidx.annotation.Px
import com.google.android.gms.maps.CameraUpdateFactory
import com.google.android.gms.maps.GoogleMap
import com.google.android.gms.maps.model.LatLng
import com.google.android.gms.maps.model.LatLngBounds

fun GoogleMap.animateCameraIncludingAllPositions(@Px padding: Int, locations: List<LatLng>) {
    val builder = LatLngBounds.Builder()
    for (location in locations) {
        builder.include(location)
    }
    val bounds = builder.build()

    val cameraUpdate = CameraUpdateFactory.newLatLngBounds(bounds, padding)

    animateCamera(cameraUpdate)
}
