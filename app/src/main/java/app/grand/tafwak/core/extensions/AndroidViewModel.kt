package app.grand.tafwak.core.extensions

import android.app.Application
import androidx.lifecycle.AndroidViewModel

val AndroidViewModel.app get() = getApplication<Application>()
