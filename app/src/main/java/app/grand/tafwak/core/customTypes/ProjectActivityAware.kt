package app.grand.tafwak.core.customTypes

import app.grand.tafwak.data.local.preferences.PrefsSplash
import app.grand.tafwak.presentation.project.viewModel.ProjectViewModel

interface sProjectActivityAware {
	
	fun projectViewModel(): ProjectViewModel
	
	fun prefsSplash(): PrefsSplash
	
}