package app.grand.tafwak.core.extensions

import androidx.fragment.app.Fragment
import androidx.navigation.fragment.findNavController
import app.grand.tafwak.presentation.auth2.referral.CheckReferralCodeDialogFragment

fun Fragment.observeSavedStateHandleResultForStringEmptyOrNull(
	key: String,
	onNewValue: (String) -> Unit
) = observeSavedStateHandleResult(key, "", { !it.isNullOrEmpty() }) {
	onNewValue(it ?: return@observeSavedStateHandleResult)
}

fun <T> Fragment.observeSavedStateHandleResult(
	key: String,
	initialValue: T?,
	isNotInitialValue: (T?) -> Boolean = { it != initialValue },
	onNewValue: (T?) -> Unit
) {
	val savedStateHandle = findNavController().currentBackStackEntry?.savedStateHandle ?: return

	savedStateHandle.getLiveData(
		key,
		initialValue
	).observe(viewLifecycleOwner) { newValue ->
		if (isNotInitialValue(newValue)) {
			savedStateHandle[key] = initialValue

			onNewValue(newValue)
		}
	}
}
