package app.grand.tafwak.presentation.auth2

import android.os.Bundle
import android.view.View
import androidx.fragment.app.activityViewModels
import androidx.fragment.app.viewModels
import androidx.navigation.fragment.findNavController
import app.grand.tafwak.data.local.preferences.PrefsSplash
import app.grand.tafwak.presentation.auth2.logIn.LoginFragmentDirections
import app.grand.tafwak.presentation.auth2.logIn.viewModels.LoginViewModel
import app.grand.tafwak.presentation.base.MABaseFragment
import app.grand.tafwak.presentation.base.extensions.handleRetryAbleFlowWithMustHaveResultWithNullability
import app.grand.tafwak.presentation.project.viewModel.ProjectViewModel
import com.structure.base_mvvm.R
import com.structure.base_mvvm.databinding.FragmentLogIn2Binding
import dagger.hilt.android.AndroidEntryPoint
import javax.inject.Inject

@AndroidEntryPoint
class LogInAgainFragment : MABaseFragment<FragmentLogIn2Binding>() {
	
	private val viewModel by viewModels<LoginViewModel>()
	
	override fun getLayoutId(): Int = R.layout.fragment_log_in_2
	
	override fun initializeBindingVariables() {
		binding?.viewModel = viewModel
	}
	
	override fun onViewCreated(view: View, savedInstanceState: Bundle?) {

	}
	
}
