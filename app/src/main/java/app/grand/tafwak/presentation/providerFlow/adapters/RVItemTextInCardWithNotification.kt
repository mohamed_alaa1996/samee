package app.grand.tafwak.presentation.providerFlow.adapters

import android.view.ViewGroup
import androidx.paging.PagingDataAdapter
import androidx.recyclerview.widget.DiffUtil
import androidx.recyclerview.widget.ListAdapter
import app.grand.tafwak.domain.providerFlow.CategoryWithOrders
import app.grand.tafwak.domain.userFlow.response.CategoryWithWorkings
import app.grand.tafwak.presentation.providerFlow.adapters.viewHolders.VHItemTextInCardWithNotification
import app.grand.tafwak.presentation.userFlow.adapters.viewHolders.VHItemTextInCard

class RVItemTextInCardWithNotification(private val listener: Listener) : ListAdapter<CategoryWithOrders, VHItemTextInCardWithNotification>(COMPARATOR) {
	
	companion object {
		val COMPARATOR = object : DiffUtil.ItemCallback<CategoryWithOrders>() {
			override fun areItemsTheSame(
				oldItem: CategoryWithOrders,
				newItem: CategoryWithOrders
			): Boolean = oldItem.id == newItem.id
			
			override fun areContentsTheSame(
				oldItem: CategoryWithOrders,
				newItem: CategoryWithOrders
			): Boolean  = oldItem == newItem
		}
	}
	
	override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): VHItemTextInCardWithNotification {
		return VHItemTextInCardWithNotification(parent, listener)
	}
	
	override fun onBindViewHolder(holder: VHItemTextInCardWithNotification, position: Int) {
		val item = getItem(position) ?: return
		
		holder.bind(item, listener.isSelected(position), position)
	}
	
	interface Listener {
		fun changeCategoryWithWorking(index: Int)
		
		fun isSelected(index: Int): Boolean
	}
	
}
