package app.grand.tafwak.presentation.userFlow.adapters

import android.view.ViewGroup
import androidx.recyclerview.widget.DiffUtil
import androidx.recyclerview.widget.ListAdapter
import app.grand.tafwak.domain.user.entity.model.PreviousWork
import app.grand.tafwak.presentation.userFlow.adapters.viewHolders.VHItemImageRoundedInCard

class RVItemImageRoundedInCard : ListAdapter<PreviousWork, VHItemImageRoundedInCard>(COMPARATOR) {
	
	companion object {
		val COMPARATOR = object : DiffUtil.ItemCallback<PreviousWork>() {
			override fun areItemsTheSame(
				oldItem: PreviousWork,
				newItem: PreviousWork
			): Boolean = oldItem.id == newItem.id
			
			override fun areContentsTheSame(
				oldItem: PreviousWork,
				newItem: PreviousWork
			): Boolean = oldItem == newItem
		}
	}
	
	override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): VHItemImageRoundedInCard {
		return VHItemImageRoundedInCard(parent)
	}
	
	override fun onBindViewHolder(holder: VHItemImageRoundedInCard, position: Int) {
		holder.bind(getItem(position))
	}
	
}
