package app.grand.tafwak.presentation.userFlow.adapters

import android.view.View
import android.view.ViewGroup
import androidx.paging.PagingDataAdapter
import androidx.recyclerview.widget.DiffUtil
import androidx.recyclerview.widget.ListAdapter
import app.grand.tafwak.domain.user.entity.model.ServiceCategory
import app.grand.tafwak.presentation.userFlow.adapters.viewHolders.VHItemCheckable
import app.grand.tafwak.presentation.userFlow.adapters.viewHolders.VHItemTextSearch

class RVItemCheckable(private val listener: Listener) : PagingDataAdapter<ServiceCategory, VHItemCheckable>(COMPARATOR) {
	
	companion object {
		val COMPARATOR = object : DiffUtil.ItemCallback<ServiceCategory>() {
			override fun areItemsTheSame(
				oldItem: ServiceCategory,
				newItem: ServiceCategory
			): Boolean = oldItem.id == newItem.id
			
			override fun areContentsTheSame(
				oldItem: ServiceCategory,
				newItem: ServiceCategory
			): Boolean = oldItem == newItem
		}
	}
	
	override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): VHItemCheckable {
		return VHItemCheckable(parent, listener)
	}
	
	override fun onBindViewHolder(holder: VHItemCheckable, position: Int) {
		val item = getItem(position) ?: return
		
		holder.bind(item.id, item.name, position)
	}
	
	interface Listener {
		fun isChecked(id: Int): Boolean
		
		fun toggleCheckState(id: Int, adapterPosition: Int)
	}
	
}
