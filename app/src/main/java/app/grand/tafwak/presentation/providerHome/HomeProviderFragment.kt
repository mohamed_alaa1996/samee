package app.grand.tafwak.presentation.providerHome

import android.content.Context
import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.core.view.postDelayed
import androidx.fragment.app.viewModels
import androidx.recyclerview.widget.LinearLayoutManager
import app.grand.tafwak.presentation.base.MABaseFragment
import app.grand.tafwak.presentation.base.extensions.changeStatusBarToDefaultBackgroundAndIcons
import app.grand.tafwak.presentation.base.extensions.changeStatusBarToWhiteBackgroundDarkIcons
import app.grand.tafwak.presentation.base.extensions.handleRetryAbleFlowWithMustHaveResultWithNullability
import app.grand.tafwak.presentation.base.extensions.trackOrdersInActivity
import app.grand.tafwak.presentation.providerHome.viewModels.HomeProviderViewModel
import com.google.gson.Gson
import com.structure.base_mvvm.R
import com.structure.base_mvvm.databinding.FragmentHomeProviderBinding
import dagger.hilt.android.AndroidEntryPoint
import javax.inject.Inject

@AndroidEntryPoint
class HomeProviderFragment : MABaseFragment<FragmentHomeProviderBinding>() {
	
	private val viewModel by viewModels<HomeProviderViewModel>()
	
	@Inject
	protected lateinit var gson: Gson

	override fun getLayoutId(): Int = R.layout.fragment_home_provider

	override fun initializeBindingVariables() {
		binding?.viewModel = viewModel
	}

	override fun onPause() {
		changeStatusBarToDefaultBackgroundAndIcons()

		super.onPause()
	}
	
	override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
		binding?.root?.postDelayed(350) {
			changeStatusBarToWhiteBackgroundDarkIcons()
		}

		binding?.sliderView?.setSliderAdapter(viewModel.adapterSliders)
		binding?.sliderView?.post {
			binding?.sliderView?.setIndicatorEnabled(true)
			binding?.sliderView?.indicatorSelectedColor = requireContext().getColor(R.color.indicator_selected_color)
			binding?.sliderView?.indicatorUnselectedColor = requireContext().getColor(R.color.indicator_unselected_color)

			binding?.sliderView?.isAutoCycle = true

			binding?.sliderView?.startAutoCycle()
		}
		
		binding?.categoriesRecyclerView?.layoutManager = LinearLayoutManager(requireContext(), LinearLayoutManager.HORIZONTAL, false)
		binding?.categoriesRecyclerView?.adapter = viewModel.adapterCategories
		
		binding?.ordersRecyclerView?.layoutManager = LinearLayoutManager(requireContext(), LinearLayoutManager.VERTICAL, false)
		binding?.ordersRecyclerView?.adapter = viewModel.adapterOrders
		
		handleRetryAbleFlowWithMustHaveResultWithNullability(viewModel.response) { response ->
			trackOrdersInActivity(response.onTheWayOrders.orEmpty().mapNotNull { it?.id })

			viewModel.adapterSliders.updateList(response.sliders)
			
			viewModel.listOfCategoryWithOrders = response.categories
			
			viewModel.adapterCategories.submitList(response.categories)
			
			viewModel.adapterOrders.submitList(response.categories.getOrNull(viewModel.selectedIndex)?.orders.orEmpty())
		}
	}

}
