package app.grand.tafwak.presentation.userFlow.viewModel

import android.view.View
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel
import app.grand.tafwak.core.customTypes.RetryAbleFlow
import app.grand.tafwak.core.customTypes.RetryAbleFlow2
import app.grand.tafwak.core.extensions.showErrorToast
import app.grand.tafwak.core.extensions.switchMap
import app.grand.tafwak.data.user.repository.UserRepositoryImpl
import app.grand.tafwak.presentation.userFlow.adapters.RVItemCheckable
import com.structure.base_mvvm.R
import dagger.hilt.android.lifecycle.HiltViewModel
import javax.inject.Inject

@HiltViewModel
class RegisterAsProviderViewModel @Inject constructor(
	repository: UserRepositoryImpl
) : ViewModel(), RVItemCheckable.Listener {
	
	val categories = repository.getRegisterAsProviderCategories()
	
	val name = MutableLiveData("")
	
	val phone = MutableLiveData("")
	
	val adapter = RVItemCheckable(this)
	
	private val selectedIds = mutableListOf<Int>()
	
	val performRegister = MutableLiveData(false)
	val registrationResponse = performRegister.switchMap {
		if (it == true) {
			repository.registerAsProviderViaForm(
				name.value.orEmpty(),
				phone.value.orEmpty(),
				selectedIds,
			)
		}else {
			MutableLiveData()
		}
	}
	
	fun sendRequest(view: View) {
		if (name.value.isNullOrEmpty() || phone.value.isNullOrEmpty()) {
			return view.context.showErrorToast(view.context.getString(R.string.all_fields_required))
		}
		
		if (selectedIds.isEmpty()) {
			return view.context.showErrorToast(view.context.getString(R.string.you_must_meke_at_least_1_selection))
		}
		
		performRegister.value = true
	}
	
	override fun isChecked(id: Int): Boolean = id in selectedIds
	
	override fun toggleCheckState(id: Int, adapterPosition: Int) {
		if (isChecked(id)) {
			selectedIds.remove(id)
		}else {
			selectedIds.add(id)
		}
		
		adapter.notifyItemChanged(adapterPosition)
	}
}
