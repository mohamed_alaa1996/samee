package app.grand.tafwak.presentation.auth2.logIn.viewModels

import android.content.Intent
import android.net.Uri
import android.view.View
import androidx.fragment.app.Fragment
import androidx.fragment.app.FragmentManager
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel
import androidx.navigation.NavDeepLinkRequest
import androidx.navigation.NavOptions
import androidx.navigation.findNavController
import app.grand.tafwak.core.customTypes.RetryAbleFlow
import app.grand.tafwak.core.customTypes.isPhoneValid
import app.grand.tafwak.core.extensions.map
import app.grand.tafwak.core.extensions.showErrorToast
import app.grand.tafwak.data.auth.repository.MAAuthRepositoryImpl
import app.grand.tafwak.presentation.auth2.logIn.LoginFragmentDirections
import app.grand.tafwak.presentation.base.customTypes.DataOfInfoImageAndListOfTexts
import app.grand.tafwak.presentation.project.ProjectActivity
import com.structure.base_mvvm.R
import dagger.hilt.android.lifecycle.HiltViewModel
import javax.inject.Inject

@HiltViewModel
class LogInAgainViewModel @Inject constructor(
	repository: MAAuthRepositoryImpl
) : ViewModel() {
	
	val isUserNotProvider = MutableLiveData(false)
	
}

/*
{
	
	// todo from args isa. isUserNotProvider
	val isUserNotProvider = MutableLiveData(false)
	
	val phone = MutableLiveData("")
	
	val sendPhoneToApiToLetApiSendVerificationCode = MutableLiveData(false)
	val response by lazy {
		RetryAbleFlow(getFlow = {
			if (showUser.value == true) {
				repository.registerUserSendPhone(phone.value.orEmpty())
			}else {
				repository.registerProviderSendPhone(phone.value.orEmpty())
			}
		})
	}
	
	fun onSkipClick(view: View) {
		view.findNavController().navigate(LoginFragmentDirections.actionDestLogInToDestLocationUserRegister(""))
	}
	
	fun toggleLanguage(view: View) {
		view.context.setProjectCurrentLocaleByToggling()
		
		val activity = FragmentManager.findFragment<Fragment>(view).requireActivity()
		
		activity.finishAffinity()
		activity.startActivity(Intent(activity, ProjectActivity::class.java))
	}
	
	fun toggleShowUser() {
		val value = showUser.value ?: false
		showUser.value = !value
	}
	
	fun onLoginClick(view: View) {
		if (termsAndConditionsIsAccepted.value != true) {
			return view.context.showErrorToast(
				view.context.getString(R.string.you_must_agree_terms_and_conditions)
			)
		}
		
		phone.value.isPhoneValid()?.also {
			errorPhone.value = view.context.getString(it)
		} ?: sendPhoneToApiToLetApiSendVerificationCode.setValue(true)
	}
	
	fun toggleAcceptingTermsAndConditions() {
		val value = termsAndConditionsIsAccepted.value ?: false
		termsAndConditionsIsAccepted.value = !value
	}
	
	fun showTermsAndConditions(view: View) {
		val data = if (showUser.value == true) {
			DataOfInfoImageAndListOfTexts.USER_TERMS_AND_CONDITIONS
		}else {
			DataOfInfoImageAndListOfTexts.PROVIDER_TERMS_AND_CONDITIONS
		}
		val uri = Uri.Builder()
			.scheme("fragment-dest")
			.authority("app.grand.tafwak.global.info.app")
			.appendPath(data.name)
			.build()
		val request = NavDeepLinkRequest.Builder.fromUri(uri).build()
		val options = NavOptions.Builder()
			.setEnterAnim(R.anim.anim_slide_in_right)
			.setExitAnim(R.anim.anim_slide_out_left)
			.setPopEnterAnim(R.anim.anim_slide_in_left)
			.setPopExitAnim(R.anim.anim_slide_out_right)
			.build()
		view.findNavController().navigate(request, options)
	}
	
	fun onTwitterClick(view: View) {
		// todo
	}
	
	fun onFacebookClick(view: View) {
		// todo
	}
	
	fun onGoogleClick(view: View) {
		// todo
	}
	
}
 */
