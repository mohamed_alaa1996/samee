package app.grand.tafwak.presentation.userFlow.viewModel

import android.view.View
import android.view.inputmethod.EditorInfo
import android.widget.TextView
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel
import androidx.navigation.findNavController
import app.grand.tafwak.core.extensions.showErrorToast
import app.grand.tafwak.core.extensions.switchMapMultiple
import app.grand.tafwak.core.extensions.toJson
import app.grand.tafwak.data.local.preferences.PrefsUser
import app.grand.tafwak.data.user.repository.UserRepositoryImpl
import app.grand.tafwak.domain.user.entity.model.ProviderMiniDetails
import app.grand.tafwak.domain.userFlow.model.SearchFilter
import app.grand.tafwak.domain.utils.toSuccessOrNull
import app.grand.tafwak.presentation.userFlow.SearchResultsProvidersFragmentArgs
import app.grand.tafwak.presentation.userFlow.SearchResultsProvidersFragmentDirections
import app.grand.tafwak.presentation.userFlow.adapters.RVItemSearchResultProvider
import com.google.gson.Gson
import com.structure.base_mvvm.R
import dagger.hilt.android.lifecycle.HiltViewModel
import timber.log.Timber
import javax.inject.Inject

@HiltViewModel
class SearchResultsProvidersViewModel @Inject constructor(
	val args: SearchResultsProvidersFragmentArgs,
	repository: UserRepositoryImpl,
	private val gson: Gson
) : ViewModel(), RVItemSearchResultProvider.Listener {
	
	val showMapRedirectionButton = args.categoryName != null
	
	/** represents category_name search */
	val query = MutableLiveData("")
	
	private val lastSearchQuery = MutableLiveData("")
	
	val resultsShownFor = MutableLiveData(args.query)

	val filter = MutableLiveData(SearchFilter.LATEST)
	
	val latLongToken: MutableLiveData<PrefsUser.LatLongToken> = MutableLiveData()
	
	val response = switchMapMultiple(lastSearchQuery, filter, latLongToken) {
		val latLongToken = latLongToken.value
		
		if (latLongToken == null) {
			MutableLiveData()
		}else {
			repository.searchProvidersOfSpecificCategory(
				args.categoryId,
				lastSearchQuery.value.orEmpty(),
				latLongToken.latitude,
				latLongToken.longitude,
				filter.value ?: SearchFilter.LATEST,
				latLongToken.token,
			)
		}
	}
	
	val showData = MutableLiveData<Boolean>()
	
	val adapter = RVItemSearchResultProvider(this)
	
	fun onSearchTextSubmit() = TextView.OnEditorActionListener { textView, actionId, _ ->
		if (actionId == EditorInfo.IME_ACTION_SEARCH) {
			val query = textView.text?.toString().orEmpty()
			
			lastSearchQuery.value = query
			
			resultsShownFor.value = if (query.isEmpty()) args.query else query
			
			return@OnEditorActionListener true
		}
		
		false
	}
	
	fun searchAllData() {
		lastSearchQuery.value = ""
		
		resultsShownFor.value = args.query
		
		this.query.postValue("")
	}
	
	fun onFilterClick(view: View) {
		view.findNavController().navigate(
			SearchResultsProvidersFragmentDirections.actionDestSearchResultsProvidersToDestFilterPickerDialog(
				filter.value ?: SearchFilter.LATEST
			)
		)
	}
	
	fun handleDataResult(list: List<ProviderMiniDetails>) {
		showData.value = list.isNotEmpty()
		
		adapter.submitList(list)
	}
	
	fun onSurfOnMapClick(view: View) {
		val responseSearch = response.value?.toSuccessOrNull()?.value?.data
		
		if (responseSearch == null || responseSearch.providers.isEmpty()) {
			return view.context.showErrorToast(view.context.getString(R.string.no_data_found))
		}
		
		view.findNavController().navigate(
			SearchResultsProvidersFragmentDirections.actionDestSearchResultsProvidersToDestSelectProviderOnMap(
				responseSearch.toJson(gson),
				latLongToken.value?.latitude,
				latLongToken.value?.longitude,
			)
		)
	}
	
	override fun onProviderClick(view: View, id: Int) {
		view.findNavController().navigate(
			SearchResultsProvidersFragmentDirections.actionDestSearchResultsProvidersToDestProviderDetails(id)
		)
	}
	
}
