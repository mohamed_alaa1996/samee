package app.grand.tafwak.presentation.base

import android.graphics.drawable.InsetDrawable
import android.os.Bundle
import android.view.Gravity
import android.view.View
import android.view.Window
import androidx.annotation.CallSuper
import androidx.appcompat.content.res.AppCompatResources
import androidx.core.view.isVisible
import app.grand.tafwak.core.extensions.dpToPx
import app.grand.tafwak.presentation.base.extensions.getMyDrawable
import com.structure.base_mvvm.R
import com.structure.base_mvvm.databinding.DialogFragmentBaseSingleButtonBinding
import com.structure.base_mvvm.databinding.DialogFragmentBaseSingleButtonBottomSheetBinding
import kotlin.math.roundToInt

abstract class BaseSingleButtonBottomSheetDialogFragment : MADialogFragment<DialogFragmentBaseSingleButtonBottomSheetBinding>() {
	
	final override fun getLayoutId(): Int = R.layout.dialog_fragment_base_single_button_bottom_sheet
	
	override val windowGravity: Int = Gravity.BOTTOM
	
	@CallSuper
	override fun onCreateDialogWindowChanges(window: Window) {
		window.setBackgroundDrawable(getMyDrawable(R.drawable.dr_top_round_white))
		
		window.attributes?.windowAnimations = R.style.ScaleDialogAnim
	}
	
	final override fun initializeBindingVariables() {}
	
	override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
		binding.imageView.isVisible = drawableRes != 0
		if (drawableRes != 0) {
			binding.imageView.setImageResource(drawableRes)
		}
		
		binding.textView.text = message
		
		binding.materialButton.text = buttonText
		binding.materialButton.setOnClickListener(::onButtonClick)
	}
	
	open val drawableRes: Int = 0
	
	abstract val buttonText: String
	
	abstract val message: String
	
	abstract fun onButtonClick(view: View)
	
}
