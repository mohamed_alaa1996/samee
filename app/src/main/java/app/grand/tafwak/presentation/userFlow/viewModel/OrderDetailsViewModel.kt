package app.grand.tafwak.presentation.userFlow.viewModel

import android.app.Application
import android.net.Uri
import android.util.Log
import android.view.View
import androidx.fragment.app.findFragment
import androidx.lifecycle.AndroidViewModel
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.asFlow
import androidx.lifecycle.lifecycleScope
import androidx.navigation.NavDeepLinkRequest
import androidx.navigation.NavOptions
import androidx.navigation.findNavController
import app.grand.tafwak.core.customTypes.RetryAbleFlow
import app.grand.tafwak.core.extensions.*
import app.grand.tafwak.data.local.preferences.PrefsUser
import app.grand.tafwak.data.user.repository.UserRepositoryImpl
import app.grand.tafwak.domain.userFlow.response.OrderDetails
import app.grand.tafwak.domain.userFlow.response.OrderStatus
import app.grand.tafwak.domain.utils.MABaseResponse
import app.grand.tafwak.domain.utils.MAResult
import app.grand.tafwak.domain.utils.toSuccessOrNull
import app.grand.tafwak.presentation.userFlow.OrderDetailsFragment
import app.grand.tafwak.presentation.userFlow.OrderDetailsFragmentArgs
import app.grand.tafwak.presentation.userFlow.OrderDetailsFragmentDirections
import app.grand.tafwak.presentation.userFlow.adapters.RVItemRequiredService
import com.structure.base_mvvm.R
import dagger.hilt.android.lifecycle.HiltViewModel
import kotlinx.coroutines.delay
import kotlinx.coroutines.flow.firstOrNull
import kotlinx.coroutines.launch
import timber.log.Timber
import java.math.BigDecimal
import java.util.logging.Logger
import javax.inject.Inject
import kotlin.math.roundToInt

@HiltViewModel
class OrderDetailsViewModel @Inject constructor(
	application: Application,
	repository: UserRepositoryImpl,
	private val args: OrderDetailsFragmentArgs,
	private val prefsUser: PrefsUser,
) : AndroidViewModel(application) {
	
	val retryAbleFlowDetails = RetryAbleFlow {
		repository.getOrderDetails(args.id).asFlow()
	}
	
	val details = MutableLiveData<MAResult<MABaseResponse<OrderDetails>>>()
	
	val adapter = RVItemRequiredService()
	
	val showCallAndChatButtons = details.mapNullable {
		it?.toSuccessOrNull()?.value?.data?.orderStatus?.let { status ->
			status == OrderStatus.ACCEPTED || status == OrderStatus.ON_THE_WAY
		}
	}
	
	val showPayment = details.mapNullable {
		it?.toSuccessOrNull()?.value?.data?.orderStatus?.let { status ->
			status == OrderStatus.ON_THE_WAY || status == OrderStatus.FINISHED
		}
	}
	
	val address = details.mapNullable {
		it?.toSuccessOrNull()?.value?.data?.address
	}
	
	val showTrackingButton = details.mapNullable {
		it?.toSuccessOrNull()?.value?.data?.orderStatus == OrderStatus.ON_THE_WAY
	}
	
	val showReview = details.mapNullable {
		it?.toSuccessOrNull()?.value?.data?.let { details ->
			details.orderStatus == OrderStatus.FINISHED && details.provider.review != null
		}
	}
	
	val orderStatus = details.mapNullable {
		it?.toSuccessOrNull()?.value?.data?.orderStatus
	}
	
	val reviewImageUrl = details.mapNullable {
		it?.toSuccessOrNull()?.value?.data?.user?.review?.image
	}
	val reviewName = details.mapNullable {
		it?.toSuccessOrNull()?.value?.data?.user?.review?.name
	}
	val reviewComment = details.mapNullable {
		it?.toSuccessOrNull()?.value?.data?.user?.review?.review
	}
	val reviewRate = details.mapNullable {
		it?.toSuccessOrNull()?.value?.data?.user?.review?.rate?.let { rate ->
      Log.i("myOwnLogs" , rate.toString())
      (rate * 20f).roundToInt()
		}
	}
	
	val buttonText = details.mapNullable {
		val review = it?.toSuccessOrNull()?.value?.data?.orderStatus == OrderStatus.FINISHED
		
		val res = if (review) R.string.review_provider else R.string.cancel_order
		
		app.getString(res)
	}
	
	val paymentMethod = details.mapNullable { response ->
		response?.toSuccessOrNull()?.value?.data?.paymentMethod?.let {
			val res = if (it == 0) R.string.cash else R.string.online
			
			app.getString(res)
		}
	}

	val progressOfOrderIsVisible = details.mapNullable {
		when (it?.toSuccessOrNull()?.value?.data?.orderStatus) {
			OrderStatus.CANCELLED, OrderStatus.REJECTED -> false
			else -> true
		}
	}
	
	val progressBarDrawableRes = details.mapNullable {
		when (it?.toSuccessOrNull()?.value?.data?.orderStatus) {
			OrderStatus.ACCEPTED -> R.drawable.ic_horz_bar_2
			OrderStatus.ON_THE_WAY -> R.drawable.ic_horz_bar_3
			OrderStatus.FINISHED -> R.drawable.ic_horz_bar_4
			else -> R.drawable.ic_horz_bar
		}
	}
	
	val imageUrl = details.mapNullable {
		it?.toSuccessOrNull()?.value?.data?.provider?.imageUrl
	}
	val name = details.mapNullable {
		it?.toSuccessOrNull()?.value?.data?.provider?.name
	}
	val ratingProgress = details.mapNullable {
		it?.toSuccessOrNull()?.value?.data?.provider?.let { details ->
			(details.averageRate).roundToInt()
		}
	}
	val ratingText = details.mapNullable {
		it?.toSuccessOrNull()?.value?.data?.provider?.let { details ->
//      "(${String.format("%.2f",  details.averageRate)})"
//      "(${details.averageRate.roundHalfUp(scale = 2)})"

      app.getString(R.string.between_brackets_str, details.averageRate.roundHalfUp(scale = 2).toString())
    }
	}
	val description = details.mapNullable {
		it?.toSuccessOrNull()?.value?.data?.provider?.description
	}
	val distance = details.mapNullable {
		it?.toSuccessOrNull()?.value?.data?.provider?.let { details ->
			val kmDistance = (details.distanceInMeters/* / 1000f*/)
			val scaledDistance = kmDistance.toBigDecimal().setScale(1, BigDecimal.ROUND_HALF_UP).toFloat().coerceAtLeast(0.1f)
			app.getString(R.string.away_by) + " " + scaledDistance.toIntOrFloat().toString() + " " + app.getString(R.string.km_notation)
		}
	}
	
	val numOfPeople = details.mapNullable {
		it?.toSuccessOrNull()?.value?.data?.let { details ->
			app.resources.getQuantityString(R.plurals.num_people, details.numOfPeople, details.numOfPeople)
		}
	}
	
	val dateAndTime = details.mapNullable {
		it?.toSuccessOrNull()?.value?.data?.let { details ->
			buildString {
				append(app.getString(R.string.execution_time))
				append(" : ")
				append("(${details.time})")
				append(" - ")
				append("(${details.date})")
			}
		}
	}
	
	private val _servicesTotalCost = details.mapNullable {
		it?.toSuccessOrNull()?.value?.data?.subTotal?.roundHalfUp(1)
	}
	val servicesTotalCost = _servicesTotalCost.mapNullable { services ->
		services?.let {
			app.getString(
				R.string.float_sar,
				it
			)
		}
	}
	
	private val _delivery = details.mapNullable { response ->
		if (response?.toSuccessOrNull()?.value?.data?.provider?.deliveryIsForFree == true) {
			null
		}else {
			response?.toSuccessOrNull()?.value?.data?.deliveryCost
		}
	}
	val delivery = _delivery.mapNullable { deliveryCost ->
		deliveryCost?.let { app.getString(R.string.float_sar, it) } ?: app.getString(R.string.free)
	}
	
	private val _tax = details.mapNullable { response ->
		response?.toSuccessOrNull()?.value?.data?.tax?.roundHalfUp(1)
	}
	val tax = _tax.mapNullable { tax ->
		tax?.let { app.getString(R.string.float_sar, it) }
	}
	
	val total = details.mapNullable { response ->
		response?.toSuccessOrNull()?.value?.data?.let { details ->
			val total = details.total.roundHalfUp(1)

			app.getString(R.string.float_sar, total)
		}
	}
	
	fun call(view: View) {
		view.context.launchDialNumber(details.value?.toSuccessOrNull()?.value?.data?.provider?.phone.orEmpty())
	}
	
	fun chat(view: View) {
		val details = details.value?.toSuccessOrNull()?.value?.data ?: return
		
		val uri = Uri.Builder()
			.scheme("fragment-dest")
			.authority("app.grand.tafwak.dest.chat.details")
			.appendPath(details.providerId.toString())
			.appendPath(details.id.toString())
			.build()
		val request = NavDeepLinkRequest.Builder.fromUri(uri).build()
		
		view.findNavController().navigate(request)
	}
	
	fun onMapSelectionClick() { /* should do nothing */ }
	
	fun trackOnMap(view: View) {
		val details = details.value?.toSuccessOrNull()?.value?.data ?: return
		
		val fragment = view.findFragment<OrderDetailsFragment>()
		
		fragment.lifecycleScope.launch {
			fragment.activityViewModel?.globalLoading?.value = true
			
			val image = prefsUser.getImageUrl().firstOrNull().orEmpty()
			
			fragment.activityViewModel?.globalLoading?.value = false
			
			delay(150)

			val uri = Uri.Builder()
				.scheme("fragment-dest")
				.authority("app.grand.tafwak.global.location.tracking")
				.appendPath(details.userId.toString())
				.appendPath(details.providerId.toString())
				.appendPath(details.id.toString())
				.let {
					if (image.isEmpty()) it else it.appendPath(image)
				}
				.build()
			val request = NavDeepLinkRequest.Builder.fromUri(uri).build()
			val options = NavOptions.Builder()
				.setEnterAnim(R.anim.anim_slide_in_right)
				.setExitAnim(R.anim.anim_slide_out_left)
				.setPopEnterAnim(R.anim.anim_slide_in_left)
				.setPopExitAnim(R.anim.anim_slide_out_right)
				.build()
			view.findNavController().navigate(request, options)
		}
	}
	
	fun cancelOrReviewOrder(view: View) {
		val details = details.value?.toSuccessOrNull()?.value?.data ?: return
		
		if (details.orderStatus != OrderStatus.FINISHED) {
			// cancel order
			val directions = if (details.past24hours.not()) {
				// No fees
				OrderDetailsFragmentDirections.actionDestOrderDetailsToDestCancelOrderDialog(
					args.id,
					body = app.getString(R.string.are_you_sure_you_want_to_cancel_order)
				)
			}else {
				// With fees
				val body = buildString {
					append(app.getString(R.string.cancel_order_warning_part_1))
					
					append(" ${(details.cancellationFees * 100f).roundHalfUp(1)}% ")
					
					append(app.getString(R.string.cancel_order_warning_part_2))
				}
				
				OrderDetailsFragmentDirections.actionDestOrderDetailsToDestCancelOrderDialog(
					args.id,
					title = app.getString(R.string.are_you_sure_you_want_to_cancel_order),
					body = body
				)
			}
			
			view.findNavController().navigate(directions)
		}else {
			// review provider
			view.findNavController().navigate(
				OrderDetailsFragmentDirections.actionDestOrderDetailsToDestRateDialog(details.providerId, details.id)
			)
		}
	}

}
