package app.grand.tafwak.presentation.auth2.logIn

import android.graphics.drawable.InsetDrawable
import android.os.Bundle
import android.view.View
import android.view.Window
import androidx.annotation.CallSuper
import androidx.appcompat.content.res.AppCompatResources
import androidx.core.view.isVisible
import androidx.navigation.fragment.findNavController
import app.grand.tafwak.core.extensions.dpToPx
import app.grand.tafwak.core.extensions.showErrorToast
import app.grand.tafwak.presentation.base.GlobalBackToUserHomeDialogFragment
import app.grand.tafwak.presentation.base.MADialogFragment
import com.structure.base_mvvm.R
import com.structure.base_mvvm.databinding.DialogFragmentBaseSingleButtonBinding
import com.structure.base_mvvm.databinding.DialogFragmentFetchPhoneBinding
import kotlin.math.roundToInt

class FetchPhoneDialogFragment : MADialogFragment<DialogFragmentFetchPhoneBinding>() {
	
	companion object {
		const val KEY_FRAGMENT_RESULT_PHONE = "KEY_FRAGMENT_RESULT_PHONE"
	}
	
	override fun getLayoutId(): Int = R.layout.dialog_fragment_fetch_phone
	
	@CallSuper
	override fun onCreateDialogWindowChanges(window: Window) {
		val drawable = InsetDrawable(
			AppCompatResources.getDrawable(requireContext(), R.drawable.border_white_16),
			requireContext().dpToPx(16f).roundToInt()
		)
		window.setBackgroundDrawable(drawable)
	}
	
	override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
		binding.materialButton.setOnClickListener {
			val text = binding.editText.text?.toString().orEmpty()
			
			if (text.isEmpty()) {
				requireContext().showErrorToast(getString(R.string.field_required))
				
				return@setOnClickListener
			}
			
			val navController = findNavController()
			
			navController.navigateUp()
			
			navController.currentBackStackEntry?.savedStateHandle?.set(
				KEY_FRAGMENT_RESULT_PHONE,
				text
			)
		}
	}
	
}
