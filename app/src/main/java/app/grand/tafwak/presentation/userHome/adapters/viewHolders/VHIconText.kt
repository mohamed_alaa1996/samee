package app.grand.tafwak.presentation.userHome.adapters.viewHolders

import android.view.ViewGroup
import androidx.core.view.isVisible
import androidx.navigation.findNavController
import androidx.recyclerview.widget.RecyclerView
import app.grand.tafwak.core.extensions.inflateLayout
import app.grand.tafwak.domain.base.entity.IconText
import app.grand.tafwak.domain.user.entity.model.ServiceCategory
import app.grand.tafwak.presentation.userHome.adapters.RVIconText
import com.bumptech.glide.Glide
import com.bumptech.glide.request.RequestOptions
import com.structure.base_mvvm.R
import com.structure.base_mvvm.databinding.ItemHomeUserServicesBinding
import com.structure.base_mvvm.databinding.ItemIconTextBinding

class VHIconText(parent: ViewGroup, private val listener: RVIconText.Listener) : RecyclerView.ViewHolder(
	parent.context.inflateLayout(R.layout.item_icon_text, parent)
) {
	
	private val binding = ItemIconTextBinding.bind(itemView)
	
	init {
		binding.linearLayout.setOnClickListener {
			val text = binding.linearLayout.tag as? String ?: return@setOnClickListener
			
			listener.onItemIconTextClick(it, text)
		}
	}
	
	fun bind(iconText: IconText) {
		binding.linearLayout.tag = iconText.text
		
		binding.textView.text = iconText.text
		
		binding.imageView.isVisible = iconText.icon != null
		iconText.icon?.also { binding.imageView.setImageResource(it) }
	}
	
}
