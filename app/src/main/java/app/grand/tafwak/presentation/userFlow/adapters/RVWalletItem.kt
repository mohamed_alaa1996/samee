package app.grand.tafwak.presentation.userFlow.adapters

import android.view.View
import android.view.ViewGroup
import androidx.recyclerview.widget.DiffUtil
import androidx.recyclerview.widget.ListAdapter
import app.grand.tafwak.domain.userFlow.response.WalletItem
import app.grand.tafwak.presentation.userFlow.adapters.viewHolders.VHItemTextSearch
import app.grand.tafwak.presentation.userFlow.adapters.viewHolders.VHWalletItem

class RVWalletItem : ListAdapter<WalletItem, VHWalletItem>(COMPARATOR) {
	
	companion object {
		val COMPARATOR = object : DiffUtil.ItemCallback<WalletItem>() {
			override fun areItemsTheSame(
				oldItem: WalletItem,
				newItem: WalletItem
			): Boolean = oldItem.id == newItem.id
			
			override fun areContentsTheSame(
				oldItem: WalletItem,
				newItem: WalletItem
			): Boolean = oldItem == newItem
		}
	}
	
	override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): VHWalletItem {
		return VHWalletItem(parent)
	}
	
	override fun onBindViewHolder(holder: VHWalletItem, position: Int) {
		holder.bind(getItem(position))
	}
	
}
