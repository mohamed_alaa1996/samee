package app.grand.tafwak.presentation.auth2.location

import android.content.Context
import android.location.Location
import android.os.Bundle
import android.view.View
import androidx.appcompat.content.res.AppCompatResources
import androidx.core.graphics.drawable.toBitmap
import androidx.core.view.postDelayed
import androidx.fragment.app.viewModels
import androidx.lifecycle.lifecycleScope
import app.grand.tafwak.core.customTypes.LocationHandler
import app.grand.tafwak.core.extensions.*
import app.grand.tafwak.domain.auth.entity.model.LocationLatLng
import app.grand.tafwak.presentation.auth2.location.viewModels.LocationTrackingViewModel
import app.grand.tafwak.presentation.base.MABaseFragment
import app.grand.tafwak.presentation.base.extensions.executeShowingErrorOnce
import com.bumptech.glide.Glide
import com.bumptech.glide.request.RequestOptions
import com.google.android.gms.location.*
import com.google.android.gms.maps.CameraUpdateFactory
import com.google.android.gms.maps.GoogleMap
import com.google.android.gms.maps.GoogleMap.InfoWindowAdapter
import com.google.android.gms.maps.OnMapReadyCallback
import com.google.android.gms.maps.SupportMapFragment
import com.google.android.gms.maps.model.BitmapDescriptorFactory
import com.google.android.gms.maps.model.LatLng
import com.google.android.gms.maps.model.Marker
import com.google.android.gms.maps.model.MarkerOptions
import com.google.firebase.database.DataSnapshot
import com.google.firebase.database.DatabaseError
import com.google.firebase.database.ValueEventListener
import com.google.firebase.database.ktx.database
import com.google.firebase.ktx.Firebase
import com.google.maps.android.SphericalUtil
import com.structure.base_mvvm.R
import com.structure.base_mvvm.databinding.FragmentLocationTrackingBinding
import com.structure.base_mvvm.databinding.MarkerTrackingMineBinding
import com.structure.base_mvvm.databinding.MarkerTrackingOtherBinding
import dagger.hilt.android.AndroidEntryPoint
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.launch
import kotlinx.coroutines.withContext
import timber.log.Timber
import kotlin.math.min
import kotlin.math.roundToInt

@AndroidEntryPoint
class LocationTrackingFragment : MABaseFragment<FragmentLocationTrackingBinding>(),
	OnMapReadyCallback, ValueEventListener, LocationHandler.Listener {
	
	companion object {
		const val KEY_DATABASE_REFERENCE_BASE = "location"
	}
	
	private val viewModel by viewModels<LocationTrackingViewModel>()
	
	private val database by lazy { Firebase.database }
	private val referenceOtherCurrentLocation by lazy {
		database.getReference(KEY_DATABASE_REFERENCE_BASE)
			.child("${viewModel.arg.orderId}").child("${viewModel.arg.otherUserId}")
	}
	private val referenceMyCurrentLocation by lazy {
		database.getReference(KEY_DATABASE_REFERENCE_BASE)
			.child("${viewModel.arg.orderId}").child("${viewModel.arg.myId}")
	}
	
	private var googleMap: GoogleMap? = null
	
	/** Zoom levels https://developers.google.com/maps/documentation/android-sdk/views#zoom */
	private val zoom get() = min(googleMap?.maxZoomLevel ?: 5f, 15f)
	
	private var myMarker: Marker? = null
	
	private var otherUserMarker: Marker? = null

	private lateinit var locationHandler: LocationHandler

	override fun onCreate(savedInstanceState: Bundle?) {
		locationHandler = LocationHandler(
			this,
			lifecycle,
			requireContext(),
			this
		)

		super.onCreate(savedInstanceState)
		
		referenceOtherCurrentLocation.addValueEventListener(this)
	}
	
	override fun getLayoutId(): Int = R.layout.fragment_location_tracking
	
	override fun initializeBindingVariables() {
		binding?.viewModel = viewModel
	}
	
	override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
		locationHandler.requestCurrentLocation(true)
	}
	
	override fun onMapReady(googleMap: GoogleMap) {
		this.googleMap = googleMap
		
		val location = LatLng(
			viewModel.myLatitude.value?.toDouble() ?: 0.0,
			viewModel.myLongitude.value?.toDouble() ?: 0.0
		)
		
		drawUserOnMap(location)
		viewModel.otherUserLatitude.value?.toDoubleOrNull()?.also { latitude ->
			viewModel.otherUserLongitude.value?.toDoubleOrNull()?.also { longitude ->
				drawOtherOnMap(LatLng(latitude, longitude))

				googleMap.animateCameraIncludingAllPositions(
					context?.dpToPx(56f)?.roundToInt() ?: 56,
					listOf(location, LatLng(latitude, longitude))
				)
			}
		}

		googleMap.animateCamera(CameraUpdateFactory.newLatLngZoom(location, zoom))
	}
	
	override fun onDataChange(snapshot: DataSnapshot) {
		Timber.e("1324 -> ${!snapshot.exists()}")

		if (!snapshot.exists()) {
			return
		}
		
		snapshot.getValue(LocationLatLng::class.java)?.also { location ->
			viewModel.otherUserLatitude.value = location.latitude
			viewModel.otherUserLongitude.value = location.longitude
			
			drawOtherOnMap(LatLng(location.latitude.toDouble(), location.longitude.toDouble()))

			/*googleMap?.animateCameraIncludingAllPositions(
				context?.dpToPx(56f)?.roundToInt() ?: 56,
				listOf(LatLng(location.latitude.toDouble(), location.longitude.toDouble()))
			)*/
		}
	}
	
	override fun onCancelled(error: DatabaseError) {
		requireContext().showErrorToast(getString(R.string.something_went_wrong))
	}

	override fun onCurrentLocationResultSuccess(location: Location) {
		updateMyLocation(location.latitude.toString(), location.longitude.toString())

		(childFragmentManager.findFragmentById(R.id.mapFragmentContainerView) as? SupportMapFragment)
			?.getMapAsync(this)

		locationHandler.requestLocationUpdates()
	}

	override fun onCurrentLocationResultFailure(context: Context?, exception: Exception?) {
		executeShowingErrorOnce(
			false,
			getString(R.string.there_is_an_error_in_detecting_your_location)
		) {
			locationHandler.requestCurrentLocation(true)
		}
	}

	override fun onChangeLocationSuccess(location: Location) {
		val latLng = LatLng(location.latitude, location.longitude)

		updateMyLocation(latLng.latitude.toString(), latLng.longitude.toString())

		drawUserOnMap(latLng)
	}

	override fun onChangeLocationFailure(context: Context?, exception: Exception?) {
		executeShowingErrorOnce(
			false,
			getString(R.string.there_is_an_error_in_detecting_your_location)
		) {
			locationHandler.requestLocationUpdates()
		}
	}

	override fun onDestroyView() {
		locationHandler.stopLocationUpdates()

		super.onDestroyView()
	}
	
	/**
	 * - Updates on view model
	 *
	 * - Updates on firebase
	 */
	private fun updateMyLocation(latitude: String, longitude: String) {
		val performUpdate = if (viewModel.myLatitude.value.isNullOrEmpty() || viewModel.myLongitude.value.isNullOrEmpty()) {
			true
		}else {
			val distanceInMeters = SphericalUtil.computeDistanceBetween(
				LatLng(latitude.toDouble(), longitude.toDouble()),
				LatLng(viewModel.myLatitude.value.orEmpty().toDouble(), viewModel.myLongitude.value.orEmpty().toDouble()),
			)
			
			distanceInMeters > 10
		}
		
		if (!performUpdate) {
			return
		}
		
		viewModel.myLatitude.value = latitude
		viewModel.myLongitude.value = longitude
		
		referenceMyCurrentLocation.setValue(
			LocationLatLng(latitude, longitude)
		) { error, _ ->
			if (error != null) {
				requireContext().showErrorToast(error.message)
			}
		}
	}
	
	@Synchronized
	private fun drawUserOnMap(location: LatLng) {
		val googleMap = googleMap ?: return
		
		lifecycleScope.launch {
			val bitmap = viewModel.bitmapMine ?: withContext(Dispatchers.Main) {
				val size = requireContext().dpToPx(63f).roundToInt()

				Timber.e("sssssssss ${viewModel.arg.myImageUrl}")

				val bitmap = Glide.with(this@LocationTrackingFragment)
					.asBitmap()
					.load(viewModel.arg.myImageUrl)
					.placeholder(R.drawable.ic_dummy_user)
					.apply(RequestOptions().override(size, size).circleCrop())
					.intoBitmap()
				
				val binding = MarkerTrackingMineBinding.bind(requireContext().inflateLayout(R.layout.marker_tracking_mine))
				
				bitmap?.also {
					binding.shapeableImageView.setImageBitmap(bitmap)
				} ?: binding.shapeableImageView.setImageResource(R.drawable.ic_dummy_user)
				
				binding.root.toBitmapV3() ?: AppCompatResources.getDrawable(
					requireContext(), R.drawable.ic_location_wrapper_of_image
				)!!.toBitmap()
			}.also {
				viewModel.bitmapMine = it
			}
			
			myMarker?.remove() // todo see other point as well isa. and check firebase realtime database to be sure
			myMarker = googleMap.addMarker(MarkerOptions()
				.position(location)
				.anchor(0.5f, 0.5f)
				.icon(BitmapDescriptorFactory.fromBitmap(bitmap))
			)
		}
	}
	
	@Synchronized
	private fun drawOtherOnMap(location: LatLng) {
		Timber.e("drawOtherOnMap ${location.latitude} ${location.longitude}")

		val googleMap = googleMap ?: return
		
		lifecycleScope.launch {
			val bitmap = viewModel.bitmapOther ?: withContext(Dispatchers.Main) {
				//val size = requireContext().dpToPx(28f).roundToInt()

				val binding = MarkerTrackingOtherBinding.bind(requireContext().inflateLayout(R.layout.marker_tracking_other))
				
				binding.root.toBitmapV3(/*size*/) ?: AppCompatResources.getDrawable(
					requireContext(), R.drawable.ic_location_wrapper_of_image
				)!!.toBitmap()
			}.also {
				viewModel.bitmapOther = it
			}
			
			otherUserMarker?.remove()
			otherUserMarker = googleMap.addMarker(MarkerOptions()
				.position(location)
				.icon(BitmapDescriptorFactory.fromBitmap(bitmap))
				.zIndex(5f)
			)
		}
	}
	
}
