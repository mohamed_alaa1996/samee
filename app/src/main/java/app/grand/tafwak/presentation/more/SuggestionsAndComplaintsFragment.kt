package app.grand.tafwak.presentation.more

import android.os.Bundle
import android.view.View
import androidx.fragment.app.activityViewModels
import androidx.fragment.app.viewModels
import androidx.navigation.fragment.findNavController
import app.grand.tafwak.core.extensions.showErrorToast
import app.grand.tafwak.core.extensions.showSuccessToast
import app.grand.tafwak.data.local.preferences.PrefsSplash
import app.grand.tafwak.domain.utils.MAResult
import app.grand.tafwak.presentation.base.MABaseFragment
import app.grand.tafwak.presentation.base.extensions.handleRetryAbleFlowWithMustHaveResultWithNullability
import app.grand.tafwak.presentation.more.viewModels.SuggestionsAndComplaintsViewModel
import app.grand.tafwak.presentation.project.viewModel.ProjectViewModel
import com.structure.base_mvvm.R
import com.structure.base_mvvm.databinding.FragmentSuggestionsAndComplaintsBinding
import dagger.hilt.android.AndroidEntryPoint
import javax.inject.Inject

@AndroidEntryPoint
class SuggestionsAndComplaintsFragment : MABaseFragment<FragmentSuggestionsAndComplaintsBinding>() {
	
	private val viewModel by viewModels<SuggestionsAndComplaintsViewModel>()
	
	override fun getLayoutId(): Int = R.layout.fragment_suggestions_and_complaints
	
	override fun initializeBindingVariables() {
		binding?.viewModel = viewModel
	}
	
	override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
		viewModel.response.observe(viewLifecycleOwner) {
			when (it) {
				is MAResult.Loading -> {
					activityViewModel?.globalLoading?.value = true
				}
				is MAResult.Failure -> {
					activityViewModel?.globalLoading?.value = false
					
					requireContext().showErrorToast(it.message ?: getString(R.string.something_went_wrong))
					
					viewModel.receivedResponse.value = false
				}
				is MAResult.Success -> {
					activityViewModel?.globalLoading?.value = false
					
					viewModel.receivedResponse.value = false
					
					requireContext().showSuccessToast(getString(R.string.sent_done_successfully))
					
					findNavController().navigateUp()
				}
			}
		}
	}
	
}
