package app.grand.tafwak.presentation.userFlow

import android.os.Bundle
import android.view.View
import androidx.fragment.app.activityViewModels
import androidx.fragment.app.viewModels
import androidx.lifecycle.Lifecycle
import androidx.lifecycle.lifecycleScope
import androidx.lifecycle.repeatOnLifecycle
import androidx.navigation.fragment.findNavController
import androidx.paging.Pager
import androidx.recyclerview.widget.GridLayoutManager
import androidx.recyclerview.widget.LinearLayoutManager
import app.grand.tafwak.core.extensions.withCustomAdapters
import app.grand.tafwak.data.local.preferences.PrefsSplash
import app.grand.tafwak.presentation.base.GlobalYesNoBottomSheetDialogFragment
import app.grand.tafwak.presentation.base.MABaseFragment
import app.grand.tafwak.presentation.project.viewModel.ProjectViewModel
import app.grand.tafwak.presentation.providerFlow.CreateOrEditServiceDialogFragment
import app.grand.tafwak.presentation.userFlow.viewModel.ShowAllPreviousWorksViewModel
import app.grand.tafwak.presentation.userHome.adapters.LSAdapterLoadingErrorEmpty
import com.google.gson.Gson
import com.structure.base_mvvm.R
import com.structure.base_mvvm.databinding.FragmentShowAllPreviousWorksBinding
import dagger.hilt.android.AndroidEntryPoint
import kotlinx.coroutines.GlobalScope
import kotlinx.coroutines.flow.collectLatest
import kotlinx.coroutines.launch
import javax.inject.Inject

@AndroidEntryPoint
class ShowAllPreviousWorksFragment : MABaseFragment<FragmentShowAllPreviousWorksBinding>() {
	
	private val viewModel by viewModels<ShowAllPreviousWorksViewModel>()
	
	@Inject
	protected lateinit var gson: Gson
	
	override fun getLayoutId(): Int = R.layout.fragment_show_all_previous_works
	
	override fun initializeBindingVariables() {
		binding?.viewModel = viewModel
	}
	
	override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
		binding?.categoriesRecyclerView?.layoutManager = LinearLayoutManager(requireContext(), LinearLayoutManager.HORIZONTAL, false)
		val headerAdapterCurrent2 = LSAdapterLoadingErrorEmpty(viewModel.adapterCategories, false)
		val footerAdapterFinished2 = LSAdapterLoadingErrorEmpty(viewModel.adapterCategories, true)
		binding?.categoriesRecyclerView?.adapter = viewModel.adapterCategories.withCustomAdapters(
			headerAdapterCurrent2,
			footerAdapterFinished2
		)
		
		binding?.previousWorksRecyclerView?.layoutManager = GridLayoutManager(requireContext(), 3, GridLayoutManager.VERTICAL, false)
		binding?.previousWorksRecyclerView?.adapter = viewModel.adapterPreviousWorks
		
		viewLifecycleOwner.lifecycleScope.launch {
			viewLifecycleOwner.repeatOnLifecycle(Lifecycle.State.STARTED) {
				viewModel.response.collectLatest { pagingData ->
					viewModel.adapterCategories.addOnPagesUpdatedListener(oneTimeDataListener)

					viewModel.adapterCategories.submitData(pagingData)
				}
			}
		}
		
		findNavController().currentBackStackEntry?.savedStateHandle?.getLiveData(
			CreateOrEditServiceDialogFragment.KEY_FRAGMENT_RESULT_CREATION_OR_EDITION_IS_DONE,
			false
		)?.observe(viewLifecycleOwner) {
			if (it == true) {
				findNavController().currentBackStackEntry?.savedStateHandle?.set(
					CreateOrEditServiceDialogFragment.KEY_FRAGMENT_RESULT_CREATION_OR_EDITION_IS_DONE,
					false
				)

				viewModel.adapterCategories.refresh()

				//viewModel.adapterCategories.addOnPagesUpdatedListener(oneTimeDataListener)
			}
		}

		findNavController().currentBackStackEntry?.savedStateHandle?.getLiveData(
			GlobalYesNoBottomSheetDialogFragment.KEY_FRAGMENT_RESULT_YES_BUTTON_CLICKED,
			false
		)?.observe(viewLifecycleOwner) {
			if (it == true) {
				findNavController().currentBackStackEntry?.savedStateHandle?.set(
					GlobalYesNoBottomSheetDialogFragment.KEY_FRAGMENT_RESULT_YES_BUTTON_CLICKED,
					false
				)

				viewModel.adapterCategories.refresh()

				//viewModel.adapterCategories.addOnPagesUpdatedListener(oneTimeDataListener)
			}
		}
	}
	
	private val oneTimeDataListener: () -> Unit = {
		val list = viewModel.adapterCategories.snapshot().getOrNull(viewModel.selectedIndex)
			?.workings.orEmpty()

		viewModel.adapterPreviousWorks.submitList(list)
		/*if (list.isNotEmpty()) {
			viewModel.adapterPreviousWorks.submitList(list)
			
			unregisterOneTimeDataListener()
		}*/
	}
	
	private fun unregisterOneTimeDataListener() {
		viewModel.adapterCategories.removeOnPagesUpdatedListener(oneTimeDataListener)
	}
	
}
