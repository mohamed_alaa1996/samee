package app.grand.tafwak.presentation.userFlow

import android.os.Bundle
import android.view.Gravity
import android.view.View
import android.view.Window
import androidx.fragment.app.activityViewModels
import androidx.fragment.app.viewModels
import androidx.navigation.fragment.findNavController
import app.grand.tafwak.presentation.base.GlobalBackToUserHomeDialogFragment
import app.grand.tafwak.presentation.base.MADialogFragment
import app.grand.tafwak.presentation.base.extensions.getMyDrawable
import app.grand.tafwak.presentation.project.viewModel.ProjectViewModel
import app.grand.tafwak.presentation.providerHome.ProviderFragment
import app.grand.tafwak.presentation.userFlow.viewModel.CancelOrderViewModel
import app.grand.tafwak.presentation.userFlow.viewModel.RateViewModel
import com.structure.base_mvvm.R
import com.structure.base_mvvm.databinding.DialogFragmentCancelOrderBinding
import com.structure.base_mvvm.databinding.DialogFragmentRateBinding
import dagger.hilt.android.AndroidEntryPoint

@AndroidEntryPoint
class RateDialogFragment : MADialogFragment<DialogFragmentRateBinding>() {
	
	private val viewModel by viewModels<RateViewModel>()
	
	override fun getLayoutId(): Int = R.layout.dialog_fragment_rate
	
	override val windowGravity: Int = Gravity.BOTTOM
	
	override fun onCreateDialogWindowChanges(window: Window) {
		window.setBackgroundDrawable(getMyDrawable(R.drawable.dr_top_round_white))
		
		window.attributes?.windowAnimations = R.style.ScaleDialogAnim
	}
	
	override fun initializeBindingVariables() {
		binding.viewModel = viewModel
	}
	
	override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
		findNavController().currentBackStackEntry?.savedStateHandle?.getLiveData(
			GlobalBackToUserHomeDialogFragment.KEY_FRAGMENT_RESULT_BUTTON_CLICKED,
			false
		)?.observe(viewLifecycleOwner) {
			if (it == true) {
				val navController = findNavController()

				navController.currentBackStackEntry?.savedStateHandle?.set(
					GlobalBackToUserHomeDialogFragment.KEY_FRAGMENT_RESULT_BUTTON_CLICKED,
					false
				)

				if (navController.previousBackStackEntry?.destination?.id == R.id.dest_order_details_seen_by_provider) {
					navController.popBackStack(R.id.dest_provider, false)
				} else {
					navController.popBackStack(R.id.dest_user, false)
				}

				navController.currentBackStackEntry?.savedStateHandle?.set(
					ProviderFragment.KEY_FRAGMENT_RESULT_GO_TO_MENU_ITEM_ID,
					R.id.action_home
				)
			}
		}
	}
	
}
