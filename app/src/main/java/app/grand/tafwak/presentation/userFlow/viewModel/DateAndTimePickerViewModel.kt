package app.grand.tafwak.presentation.userFlow.viewModel

import android.view.View
import androidx.fragment.app.Fragment
import androidx.fragment.app.findFragment
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel
import androidx.navigation.findNavController
import app.grand.tafwak.core.customTypes.MADateAndTime
import app.grand.tafwak.core.extensions.findNavControllerOfProject
import app.grand.tafwak.core.extensions.showErrorToast
import app.grand.tafwak.core.extensions.toJson
import app.grand.tafwak.presentation.userFlow.DateAndTimePickerDialogFragment
import com.google.android.material.datepicker.CalendarConstraints
import com.google.android.material.datepicker.DateValidatorPointForward
import com.google.android.material.datepicker.MaterialDatePicker
import com.google.android.material.timepicker.MaterialTimePicker
import com.google.android.material.timepicker.TimeFormat
import com.google.gson.Gson
import com.structure.base_mvvm.R
import dagger.hilt.android.lifecycle.HiltViewModel
import java.time.Instant
import java.time.LocalDateTime
import java.time.ZoneId
import javax.inject.Inject

@HiltViewModel
class DateAndTimePickerViewModel @Inject constructor(
	private val gson: Gson
) : ViewModel() {
	
	companion object {
		private const val DIALOG_DATE_PICKER_KEY = "DIALOG_DATE_PICKER_KEY"
		
		private const val DIALOG_TIME_PICKER_KEY = "DIALOG_TIME_PICKER_KEY"
		
		private const val NO_SELECTION = ""
	}
	
	val date = MutableLiveData(NO_SELECTION)
	
	val time = MutableLiveData(NO_SELECTION)
	
	// Below formats used same as api ones isa.
	
	var year: Int? = null
	var month: Int? = null
	var day: Int? = null
	
	var hour24: Int? = null
	var minute: Int? = null
	
	fun showDatePicker(view: View) {
		// Makes only dates from today forward selectable.
		val constraints = CalendarConstraints.Builder()
			.setValidator(DateValidatorPointForward.now())
			.build()
		
		val datePicker = MaterialDatePicker.Builder.datePicker()
			.setTitleText(view.context.getString(R.string.determine_the_date))
			.setSelection(MaterialDatePicker.todayInUtcMilliseconds())
			.setCalendarConstraints(constraints)
			.build()
		
		datePicker.addOnPositiveButtonClickListener { millis ->
			val localDateTime = LocalDateTime.ofInstant(Instant.ofEpochMilli(millis), ZoneId.systemDefault())
			
			val day = localDateTime.dayOfMonth
			val month = view.context.resources
				.getStringArray(R.array.year_months)[localDateTime.monthValue.dec()]
			val year = localDateTime.year
			
			this.day = day
			this.month = localDateTime.monthValue
			this.year = year
			
			date.postValue("$day $month $year")
		}
		
		datePicker.show(view.findFragment<Fragment>().childFragmentManager, DIALOG_DATE_PICKER_KEY)
	}
	
	fun showTimePicker(view: View) {
		val now = LocalDateTime.now()
		
		val timePicker = MaterialTimePicker.Builder()
			.setTitleText(view.context.getString(R.string.determine_the_time))
			.setTimeFormat(TimeFormat.CLOCK_12H)
			.setHour(now.hour)
			.setMinute(now.minute)
			.build()
		
		timePicker.addOnPositiveButtonClickListener {
			val isAm = timePicker.hour < 12
			val hour = when {
				timePicker.hour == 0 -> 12
				timePicker.hour > 12 -> (timePicker.hour - 12)
				else -> timePicker.hour
			}
			
			this.hour24 = timePicker.hour
			this.minute = timePicker.minute
			
			val toggleText = if (isAm) {
				view.context.getString(R.string.am_full)
			}else {
				view.context.getString(R.string.pm_full)
			}
			time.postValue("${timePicker.minute} : $hour $toggleText")
		}
		
		timePicker.show(view.findFragment<Fragment>().childFragmentManager, DIALOG_TIME_PICKER_KEY)
	}
	
	fun confirm(view: View) {
		val year = year
		val month = month
		val day = day
		
		val hour24 = hour24
		val minute = minute
		
		if (year != null && month != null && day != null && hour24 != null && minute != null) {
			val activityViewModel = view.findFragment<DateAndTimePickerDialogFragment>().activityViewModel
			
			view.findNavControllerOfProject().navigateUp()
			
			val maDateAndTime = MADateAndTime(year, month, day, hour24, minute)
			view.findNavControllerOfProject().currentBackStackEntry?.savedStateHandle?.set(
				DateAndTimePickerDialogFragment.KEY_FRAGMENT_RESULT_MA_DATE_AND_TIME_AS_JSON,
				maDateAndTime.toJson(gson)
			)
			
			activityViewModel?.globalLoading?.value = true
		}else {
			val msg = when {
				year == null && hour24 == null -> view.context.getString(R.string.you_must_select_date_and_time)
				year == null -> view.context.getString(R.string.you_must_select_date)
				else -> view.context.getString(R.string.you_must_select_time)
			}
			
			view.context.showErrorToast(msg)
		}
	}
	
}
