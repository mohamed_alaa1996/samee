package app.grand.tafwak.presentation.providerFlow

import android.view.View
import androidx.fragment.app.activityViewModels
import androidx.lifecycle.lifecycleScope
import androidx.navigation.fragment.navArgs
import app.grand.tafwak.core.extensions.showErrorToast
import app.grand.tafwak.data.provider.repository.ProviderRepositoryImpl
import app.grand.tafwak.domain.utils.MAResult
import app.grand.tafwak.presentation.base.GlobalDeleteServiceDialogFragmentArgs
import app.grand.tafwak.presentation.base.GlobalYesNoBottomSheetDialogFragment
import app.grand.tafwak.presentation.project.viewModel.ProjectViewModel
import com.structure.base_mvvm.R
import dagger.hilt.android.AndroidEntryPoint
import kotlinx.coroutines.delay
import kotlinx.coroutines.launch
import javax.inject.Inject

@AndroidEntryPoint
class DeleteWorkDialogFragment : GlobalYesNoBottomSheetDialogFragment() {
	
	private val args by navArgs<DeleteWorkDialogFragmentArgs>()
	
	@Inject
	protected lateinit var repository: ProviderRepositoryImpl
	
	override fun yesButtonClick(view: View) {
		lifecycleScope.launch {
			activityViewModel?.globalLoading?.value = true
			
			when (val result = repository.deleteWorking(args.id)) {
				is MAResult.Failure -> {
					requireContext().showErrorToast(result.message ?: getString(R.string.something_went_wrong))
					
					activityViewModel?.globalLoading?.value = false
				}
				is MAResult.Success -> {
					activityViewModel?.globalLoading?.value = false
					
					delay(150)
					
					super.yesButtonClick(view)
				}
			}
		}
	}
	
}
