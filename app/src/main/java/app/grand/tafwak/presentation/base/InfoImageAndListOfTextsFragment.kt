package app.grand.tafwak.presentation.base

import android.graphics.drawable.Drawable
import android.os.Bundle
import android.view.View
import android.widget.ImageView
import androidx.core.text.HtmlCompat
import androidx.core.view.postDelayed
import androidx.core.view.setPadding
import androidx.fragment.app.activityViewModels
import androidx.fragment.app.viewModels
import androidx.navigation.fragment.findNavController
import androidx.navigation.fragment.navArgs
import app.grand.tafwak.core.extensions.dpToPx
import app.grand.tafwak.core.extensions.listenerOnResourceReady
import app.grand.tafwak.core.extensions.orZero
import app.grand.tafwak.data.local.preferences.PrefsSplash
import app.grand.tafwak.presentation.base.customTypes.DataOfInfoImageAndListOfTexts.*
import app.grand.tafwak.presentation.base.extensions.handleRetryAbleFlowWithMustHaveResultWithNullability
import app.grand.tafwak.presentation.base.viewModel.InfoImageAndListOfTextsViewModel
import app.grand.tafwak.presentation.project.viewModel.ProjectViewModel
import com.bumptech.glide.Glide
import com.bumptech.glide.load.DataSource
import com.bumptech.glide.load.engine.GlideException
import com.bumptech.glide.request.RequestListener
import com.bumptech.glide.request.RequestOptions
import com.bumptech.glide.request.target.Target
import com.structure.base_mvvm.R
import com.structure.base_mvvm.databinding.FragmentInfoImageAndListOfTextsBinding
import dagger.hilt.android.AndroidEntryPoint
import timber.log.Timber
import javax.inject.Inject
import kotlin.math.roundToInt

@AndroidEntryPoint
class InfoImageAndListOfTextsFragment : MABaseFragment<FragmentInfoImageAndListOfTextsBinding>() {
	
	private val viewModel by viewModels<InfoImageAndListOfTextsViewModel>()

	private val args by navArgs<InfoImageAndListOfTextsFragmentArgs>()
	
	override fun getLayoutId(): Int = R.layout.fragment_info_image_and_list_of_texts
	
	override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
		val title = when (args.data) {
			USER_ABOUT, PROVIDER_ABOUT -> getString(R.string.about)
			USER_TERMS_AND_CONDITIONS, PROVIDER_TERMS_AND_CONDITIONS -> getString(R.string.terms_and_conditions)
			USER_PRIVACY_POLICY, PROVIDER_PRIVACY_POLICY -> getString(R.string.privacy)
		}
		activityViewModel?.titleToolbar?.postValue(title)
		
		handleRetryAbleFlowWithMustHaveResultWithNullability(viewModel.response) { response ->
			binding?.textView?.text = HtmlCompat.fromHtml(response.description.orEmpty(), HtmlCompat.FROM_HTML_MODE_COMPACT)
			
			binding?.shapeableImageView?.post {
				Timber.e("response.image ${response.image}")

				Glide.with(this)
					.load(response.image)
					//.apply(RequestOptions().fitCenter())
					.listenerOnResourceReady {
						if (it != null) {
							if (args.data == USER_ABOUT || args.data == PROVIDER_ABOUT) {
								binding?.shapeableImageView?.setPadding(
									context?.dpToPx(16f)?.roundToInt().orZero()
								)
							}else {
								binding?.shapeableImageView?.scaleType = ImageView.ScaleType.FIT_XY
							}
						}
					}
					.into(binding?.shapeableImageView ?: return@post)
			}
		}
	}

}
