package app.grand.tafwak.presentation.base.customTypes

enum class DataOfInfoImageAndListOfTexts {
	USER_ABOUT,
	USER_TERMS_AND_CONDITIONS,
	USER_PRIVACY_POLICY,
	
	PROVIDER_ABOUT,
	PROVIDER_TERMS_AND_CONDITIONS,
	PROVIDER_PRIVACY_POLICY,
	
	// todo lessa about and other stuff as well isa.
}