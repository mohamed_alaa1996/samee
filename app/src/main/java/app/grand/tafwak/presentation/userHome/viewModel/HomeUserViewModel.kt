package app.grand.tafwak.presentation.userHome.viewModel

import android.net.Uri
import android.view.View
import androidx.fragment.app.findFragment
import androidx.lifecycle.ViewModel
import androidx.lifecycle.asLiveData
import androidx.lifecycle.switchMap
import androidx.lifecycle.viewModelScope
import androidx.navigation.NavDeepLinkRequest
import androidx.navigation.NavOptions
import androidx.navigation.findNavController
import androidx.navigation.fragment.findNavController
import androidx.paging.PagingData
import app.grand.tafwak.core.customTypes.RetryAbleFlow
import app.grand.tafwak.core.extensions.findNavControllerOfProject
import app.grand.tafwak.core.extensions.map
import app.grand.tafwak.core.extensions.showErrorToast
import app.grand.tafwak.data.local.preferences.PrefsUser
import app.grand.tafwak.data.user.dataSource.remote.UserRemoteDataSource
import app.grand.tafwak.data.user.repository.UserRepositoryImpl
import app.grand.tafwak.domain.auth.entity.model.LocationData
import app.grand.tafwak.domain.user.entity.model.ResponseUserHome
import app.grand.tafwak.domain.utils.MABaseResponse
import app.grand.tafwak.domain.utils.MAResult
import app.grand.tafwak.presentation.auth2.location.LocationUserRegisterFragment
import app.grand.tafwak.presentation.userHome.HomeUserFragment
import app.grand.tafwak.presentation.userHome.UserFragmentArgs
import app.grand.tafwak.presentation.userHome.UserFragmentDirections
import app.grand.tafwak.presentation.userHome.adapters.RVItemHomeUserServices
import app.grand.tafwak.presentation.userHome.adapters.SliderRVSliders
import com.google.gson.Gson
import com.structure.base_mvvm.R
import dagger.hilt.android.lifecycle.HiltViewModel
import kotlinx.coroutines.flow.*
import kotlinx.coroutines.launch
import javax.inject.Inject

@HiltViewModel
class HomeUserViewModel @Inject constructor(
	private val prefsUser: PrefsUser,
	private val repository: UserRepositoryImpl,
	gson: Gson,
) : ViewModel() {

	private val isGuest = prefsUser.getIsGuest().asLiveData()
	private val locationData = isGuest.switchMap { prefsUser.getLocation().asLiveData() }
	val address = locationData.map { it?.address }
	
	val response = RetryAbleFlow(repository::getUserHome)
	
	val categories = repository.getUserHomeCategories()
	
	val adapterSliders = SliderRVSliders(gson)
	
	val adapterCategories = RVItemHomeUserServices()
	
	fun editAddress(view: View) {
		viewModelScope.launch {
			adapterCategories.submitData(PagingData.empty())
		}
		
		val uri = Uri.Builder()
			.scheme("fragment-dest")
			.authority("app.grand.tafwak.global.location.selection")
			.appendPath(locationData.value!!.latitude)
			.appendPath(locationData.value!!.longitude)
			.build()
		val request = NavDeepLinkRequest.Builder.fromUri(uri).build()
		val options = NavOptions.Builder()
			.setEnterAnim(R.anim.anim_slide_in_right)
			.setExitAnim(R.anim.anim_slide_out_left)
			.setPopEnterAnim(R.anim.anim_slide_in_left)
			.setPopExitAnim(R.anim.anim_slide_out_right)
			.build()
		view.findNavControllerOfProject().navigate(request, options)
	}
	
	fun toSearchServices(view: View) {
		viewModelScope.launch {
			adapterCategories.submitData(PagingData.empty())
		}
		
		view.findFragment<HomeUserFragment>().findNavControllerOfProject().navigate(
			UserFragmentDirections.actionDestUserToDestSearchQuery()
		)
	}
	
	fun onSuccessSendLocationToApi(fragment: HomeUserFragment, locationData: LocationData) {
		viewModelScope.launch {
			if (fragment.activityViewModel?.globalLoading?.value != true) {
				fragment.activityViewModel?.globalLoading?.value = true
			}
			
			val isGuest = isGuest.value ?: true
			
			if (!isGuest) {
				val maResult = repository.updateLocation(
					locationData.latitude,
					locationData.longitude,
					locationData.address
				)
				
				if (maResult is MAResult.Failure) {
					fragment.activityViewModel?.globalLoading?.value = false
					
					fragment.requireContext().showErrorToast(maResult.message ?: fragment.getString(R.string.something_went_wrong))
					
					return@launch
				}
			}
			
			prefsUser.setLocation(locationData)
			
			address.value = locationData.address
			
			fragment.activityViewModel?.globalLoading?.value = false
		}
	}
	
}
