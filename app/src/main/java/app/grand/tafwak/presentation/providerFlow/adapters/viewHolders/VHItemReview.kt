package app.grand.tafwak.presentation.providerFlow.adapters.viewHolders

import android.text.style.ForegroundColorSpan
import android.view.ViewGroup
import androidx.core.text.buildSpannedString
import androidx.core.text.set
import androidx.recyclerview.widget.RecyclerView
import app.grand.tafwak.core.extensions.inflateLayout
import app.grand.tafwak.domain.providerFlow.ItemReview
import app.grand.tafwak.domain.userFlow.response.WalletItem
import com.bumptech.glide.Glide
import com.bumptech.glide.request.RequestOptions
import com.structure.base_mvvm.R
import com.structure.base_mvvm.databinding.ItemClientReviewBinding
import com.structure.base_mvvm.databinding.ItemWalletBinding
import kotlin.math.roundToInt

class VHItemReview(parent: ViewGroup) : RecyclerView.ViewHolder(
	parent.context.inflateLayout(R.layout.item_client_review, parent)
) {
	
	private val binding = ItemClientReviewBinding.bind(itemView)
	
	fun bind(item: ItemReview) {
		Glide.with(binding.imageView.context)
			.load(item.imageUrl)
			.apply(RequestOptions().centerCrop())
			.placeholder(R.drawable.ic_logo_samee_placeholder)
			.error(R.drawable.ic_logo_samee_placeholder)
			.into(binding.imageView)
		
		binding.nameTextView.text = item.name.orEmpty()
		
		binding.dateTextView.text = item.createdAt
		
		binding.descriptionTextView.text = item.review
		
		binding.ratingBar.progress = (item.rate * 20f).roundToInt()
	}
	
}
