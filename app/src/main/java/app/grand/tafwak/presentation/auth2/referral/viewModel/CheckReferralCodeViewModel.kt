package app.grand.tafwak.presentation.auth2.referral.viewModel

import android.view.View
import androidx.fragment.app.findFragment
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel
import androidx.navigation.findNavController
import androidx.navigation.fragment.findNavController
import app.grand.tafwak.core.extensions.orZero
import app.grand.tafwak.core.extensions.showErrorToast
import app.grand.tafwak.core.extensions.showSuccessToast
import app.grand.tafwak.data.auth.repository.MAAuthRepositoryImpl
import app.grand.tafwak.presentation.auth2.logIn.LoginFragmentDirections
import app.grand.tafwak.presentation.auth2.referral.CheckReferralCodeDialogFragment
import app.grand.tafwak.presentation.auth2.referral.CheckReferralCodeDialogFragmentArgs
import app.grand.tafwak.presentation.base.extensions.executeOnGlobalLoadingAndAutoHandleRetryCancellable
import app.grand.tafwak.presentation.providerFlow.CreateOrEditServiceDialogFragment
import com.structure.base_mvvm.R
import dagger.hilt.android.lifecycle.HiltViewModel
import javax.inject.Inject

@HiltViewModel
class CheckReferralCodeViewModel @Inject constructor(
	private val repository: MAAuthRepositoryImpl,
	private val args: CheckReferralCodeDialogFragmentArgs
) : ViewModel() {

	val code = MutableLiveData("")

	fun dismissDialog(view: View) {
		view.findFragment<CreateOrEditServiceDialogFragment>().findNavController().navigateUp()
	}

	fun checkReferralCode(view: View) {
		val fragment = view.findFragment<CheckReferralCodeDialogFragment>()

		val context = fragment.context ?: return

		if (code.value.isNullOrEmpty()) {
			return context.showErrorToast(context.getString(R.string.field_required))
		}else if (code.value == args.yourOwnReferCode) {
			return context.showErrorToast(context.getString(R.string.you_can_t_use_your_own_refer_code))
		}

		fragment.executeOnGlobalLoadingAndAutoHandleRetryCancellable(
			afterShowingLoading = {
				repository.checkReferralCode(code.value.orEmpty())
			},
			afterHidingLoading = {
				// code to check 300555
				fragment.context?.apply {
					showSuccessToast(getString(R.string.correct_referal_code))
				}

				val navController = fragment.findNavController()

				navController.navigateUp()

				navController.currentBackStackEntry?.savedStateHandle?.set(
					CheckReferralCodeDialogFragment.KEY_FRAGMENT_RESULT_REFERRAL_CODE,
					code.value.orEmpty()
				)
				navController.currentBackStackEntry?.savedStateHandle?.set(
					CheckReferralCodeDialogFragment.KEY_FRAGMENT_RESULT_REFERRED_BY_ID,
					it?.referredById.orZero()
				)
			}
		)
	}

}
