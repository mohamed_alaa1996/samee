package app.grand.tafwak.presentation.userHome

import android.os.Bundle
import android.view.View
import androidx.core.view.postDelayed
import androidx.fragment.app.activityViewModels
import androidx.fragment.app.viewModels
import androidx.lifecycle.Lifecycle
import androidx.lifecycle.lifecycleScope
import androidx.lifecycle.repeatOnLifecycle
import androidx.navigation.Navigation
import androidx.navigation.fragment.findNavController
import androidx.recyclerview.widget.GridLayoutManager
import app.grand.tafwak.core.extensions.*
import app.grand.tafwak.data.local.preferences.PrefsSplash
import app.grand.tafwak.domain.auth.entity.model.LocationData
import app.grand.tafwak.presentation.auth2.location.LocationSelectionFragment
import app.grand.tafwak.presentation.base.MABaseFragment
import app.grand.tafwak.presentation.base.extensions.handleRetryAbleFlowWithMustHaveResultWithNullability
import app.grand.tafwak.presentation.project.viewModel.ProjectViewModel
import app.grand.tafwak.presentation.userHome.adapters.LSAdapterLoadingErrorEmpty
import app.grand.tafwak.presentation.userHome.viewModel.HomeUserViewModel
import com.google.gson.Gson
import com.structure.base_mvvm.R
import com.structure.base_mvvm.databinding.FragmentHomeUserBinding
import dagger.hilt.android.AndroidEntryPoint
import kotlinx.coroutines.flow.collectLatest
import kotlinx.coroutines.launch
import timber.log.Timber
import javax.inject.Inject

@AndroidEntryPoint
class HomeUserFragment : MABaseFragment<FragmentHomeUserBinding>() {
	
	val viewModel by viewModels<HomeUserViewModel>()
	
	@Inject
	protected lateinit var gson: Gson
	
	override fun getLayoutId(): Int = R.layout.fragment_home_user
	
	override fun initializeBindingVariables() {
		binding?.viewModel = viewModel
	}
	
	override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
		binding?.sliderView?.setSliderAdapter(viewModel.adapterSliders)
		binding?.sliderView?.post {
			binding?.sliderView?.setIndicatorEnabled(true)
			binding?.sliderView?.indicatorSelectedColor = requireContext().getColor(R.color.indicator_selected_color)
			binding?.sliderView?.indicatorUnselectedColor = requireContext().getColor(R.color.indicator_unselected_color)

			binding?.sliderView?.isAutoCycle = true

			binding?.sliderView?.startAutoCycle()
		}
		
		val layoutManager = GridLayoutManager(
			requireContext(), 2, GridLayoutManager.VERTICAL, false
		)
		layoutManager.spanSizeLookup = object : GridLayoutManager.SpanSizeLookup() {
			private var prevCountInLastItem = 0
			
			override fun getSpanSize(position: Int): Int {
				val count = binding?.recyclerView?.adapter?.itemCount ?: return 2
				
				var forceNotify = false
				if (count - 1 == position) {
					forceNotify = prevCountInLastItem != 0 && prevCountInLastItem != count
					prevCountInLastItem = count
				}
				
				return if (count - 1 == position && count % 2 != 0) {
					if (forceNotify) {
						binding?.recyclerView?.post {
							kotlin.runCatching {
								binding?.recyclerView?.adapter?.notifyItemChanged(position)
							}
						}
					}
					
					2
				}else {
					1
				}
			}
		}
		binding?.recyclerView?.layoutManager = layoutManager
		val headerAdapterCurrent2 = LSAdapterLoadingErrorEmpty(viewModel.adapterCategories, false)
		val footerAdapterFinished2 = LSAdapterLoadingErrorEmpty(viewModel.adapterCategories, true)
		binding?.recyclerView?.adapter = viewModel.adapterCategories.withCustomAdapters(
			headerAdapterCurrent2,
			footerAdapterFinished2
		)
		
		handleRetryAbleFlowWithMustHaveResultWithNullability(viewModel.response) { response ->
			viewModel.adapterSliders.updateList(response.sliders)
		}
		
		viewLifecycleOwner.lifecycleScope.launch {
			viewLifecycleOwner.repeatOnLifecycle(Lifecycle.State.STARTED) {
				viewModel.categories.collectLatest {
					viewModel.adapterCategories.submitData(it)
				}
			}
		}
		
		findNavControllerOfProject().currentBackStackEntry?.savedStateHandle?.getLiveData(
			LocationSelectionFragment.KEY_FRAGMENT_RESULT_LOCATION_DATA_AS_JSON,
			""
		)?.observe(viewLifecycleOwner) {
			if (!it.isNullOrEmpty()) {
				findNavControllerOfProject().currentBackStackEntry?.savedStateHandle?.set(
					LocationSelectionFragment.KEY_FRAGMENT_RESULT_LOCATION_DATA_AS_JSON,
					""
				)
				
				viewModel.onSuccessSendLocationToApi(this, it.fromJson(gson))
			}
		}
	}
	
}