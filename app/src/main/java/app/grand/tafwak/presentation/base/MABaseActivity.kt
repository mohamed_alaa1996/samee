package app.grand.tafwak.presentation.base

import android.content.Context
import android.content.res.Configuration
import android.os.Build
import android.os.Bundle
import android.os.PersistableBundle
import android.util.Log
import androidx.activity.ComponentActivity
import androidx.annotation.CallSuper
import androidx.annotation.LayoutRes
import androidx.appcompat.app.AppCompatActivity
import androidx.databinding.DataBindingUtil
import androidx.databinding.ViewDataBinding
import app.grand.tafwak.core.extensions.getProjectCurrentLocale
import app.grand.tafwak.presentation.base.utils.LanguagesHelper
import app.grand.tafwak.presentation.base.utils.MyContextWrapper
import com.zeugmasolutions.localehelper.LocaleHelper
import com.zeugmasolutions.localehelper.LocaleHelperActivityDelegate
import com.zeugmasolutions.localehelper.LocaleHelperActivityDelegateImpl
import java.util.*

abstract class MABaseActivity<VDB : ViewDataBinding> : AppCompatActivity() {
	
	private var _binding: VDB? = null
	protected val binding get() = _binding!!

	override fun attachBaseContext(newBase: Context?) {
		if (Build.VERSION.SDK_INT > Build.VERSION_CODES.N_MR1 && newBase != null) {
			super.attachBaseContext(MyContextWrapper.wrap(newBase, newBase.getProjectCurrentLocale()))
		}else {
			super.attachBaseContext(newBase)
		}
	}

	@CallSuper
	override fun onCreate(savedInstanceState: Bundle?) {
		super.onCreate(savedInstanceState)

		val locale = getProjectCurrentLocale()
		LanguagesHelper.changeLanguage(this, locale)
		LanguagesHelper.changeLanguage(applicationContext, locale)
		
		_binding = DataBindingUtil.setContentView(this, getLayoutId())
		initializeBindingVariables()
		binding.lifecycleOwner = this
		
		setupsInOnCreate()
	}
	
	protected open fun initializeBindingVariables() {}
	
	/** Called inside [onCreate] after initializing [binding] isa. */
	protected open fun setupsInOnCreate() {}
	
	override fun onResume() {
		super.onResume()
		
		registerListeners()
	}
	
	override fun onPause() {
		unRegisterListeners()
		
		super.onPause()
	}
	
	@LayoutRes
	abstract fun getLayoutId(): Int
	
	open fun registerListeners() {}
	
	open fun unRegisterListeners() {}

}
