package app.grand.tafwak.presentation.auth2.location

import android.location.Location
import android.os.Bundle
import android.view.View
import androidx.activity.result.contract.ActivityResultContracts
import androidx.fragment.app.activityViewModels
import androidx.fragment.app.viewModels
import androidx.lifecycle.Lifecycle
import androidx.lifecycle.viewModelScope
import androidx.navigation.fragment.findNavController
import app.grand.tafwak.core.customTypes.LocationHandler
import app.grand.tafwak.core.extensions.fromJson
import app.grand.tafwak.core.extensions.getAddressFromLatitudeAndLongitude
import app.grand.tafwak.core.extensions.showErrorToast
import app.grand.tafwak.core.extensions.showNormalToast
import app.grand.tafwak.data.local.preferences.PrefsSplash
import app.grand.tafwak.domain.auth.entity.model.LocationData
import app.grand.tafwak.domain.utils.MAResult
import app.grand.tafwak.presentation.auth2.location.viewModels.LocationUserRegisterViewModel
import app.grand.tafwak.presentation.base.MABaseFragment
import app.grand.tafwak.presentation.base.extensions.handleRetryAbleFlowWithMustHaveResultWithNullability
import app.grand.tafwak.presentation.project.viewModel.ProjectViewModel
import com.google.android.gms.location.FusedLocationProviderClient
import com.google.android.gms.location.LocationServices
import com.google.android.gms.maps.CameraUpdateFactory
import com.google.android.gms.maps.model.LatLng
import com.structure.base_mvvm.R
import com.structure.base_mvvm.databinding.FragmentLocationUserRegisterBinding
import dagger.hilt.android.AndroidEntryPoint
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.launch
import kotlinx.coroutines.withContext
import javax.inject.Inject

@AndroidEntryPoint
class LocationUserRegisterFragment : MABaseFragment<FragmentLocationUserRegisterBinding>(), LocationHandler.Listener {
	
	private val viewModel by viewModels<LocationUserRegisterViewModel>()
	
	lateinit var locationHandler: LocationHandler
		private set
	
	override fun getLayoutId(): Int = R.layout.fragment_location_user_register
	
	override fun onCreate(savedInstanceState: Bundle?) {
		locationHandler = LocationHandler(
			this,
			lifecycle,
			requireContext(),
			this
		)

		super.onCreate(savedInstanceState)
	}
	
	override fun initializeBindingVariables() {
		binding?.viewModel = viewModel
	}
	
	override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
		viewModel.sentLocation.observe(viewLifecycleOwner) {
			when (it) {
				is MAResult.Failure -> {
					activityViewModel?.globalLoading?.value = false
					
					requireContext().showErrorToast(it.message ?: getString(R.string.something_went_wrong))
					
					viewModel.performSendLocation.value = false
				}
				is MAResult.Success -> {
					viewModel.onSuccessSendLocationToApi(this)
				}
				else -> { /* whether loading or null ignore (loading is already done now isa) */ }
			}
		}
		
		findNavController().currentBackStackEntry?.savedStateHandle?.getLiveData(
			LocationSelectionFragment.KEY_FRAGMENT_RESULT_LOCATION_DATA_AS_JSON,
			""
		)?.observe(viewLifecycleOwner) {
			if (!it.isNullOrEmpty()) {
				findNavController().currentBackStackEntry?.savedStateHandle?.set(
					LocationSelectionFragment.KEY_FRAGMENT_RESULT_LOCATION_DATA_AS_JSON,
					""
				)

				viewModel.locationData = it.fromJson<LocationData>()

				viewModel.onSuccessSendLocationToApi(this)
			}
		}
	}

	override fun onCurrentLocationResultSuccess(location: Location) {
		viewModel.viewModelScope.launch {
			val address = withContext(Dispatchers.IO) {
				context?.getAddressFromLatitudeAndLongitude(
					location.latitude,
					location.longitude
				)
			} ?: return@launch

			viewModel.locationData = LocationData(
				location.latitude.toString(), location.longitude.toString(), address
			)

			viewModel.onSuccessSendLocationToApi(this@LocationUserRegisterFragment)
		}
	}
}
