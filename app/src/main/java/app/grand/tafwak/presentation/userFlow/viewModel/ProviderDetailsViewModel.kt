package app.grand.tafwak.presentation.userFlow.viewModel

import android.app.Application
import android.view.View
import android.view.inputmethod.EditorInfo
import android.view.inputmethod.InputMethodManager
import android.widget.TextView
import androidx.core.content.ContextCompat.getSystemService
import androidx.core.content.getSystemService
import androidx.fragment.app.findFragment
import androidx.lifecycle.*
import androidx.navigation.findNavController
import app.grand.tafwak.core.customTypes.MADateAndTime
import app.grand.tafwak.core.customTypes.RetryAbleFlow
import app.grand.tafwak.core.customTypes.RetryAbleFlow2
import app.grand.tafwak.core.extensions.*
import app.grand.tafwak.core.extensions.map
import app.grand.tafwak.data.local.preferences.PrefsUser
import app.grand.tafwak.data.user.repository.UserRepositoryImpl
import app.grand.tafwak.domain.auth.entity.model.LocationData
import app.grand.tafwak.domain.user.entity.model.ProviderDetails
import app.grand.tafwak.domain.user.entity.model.ProviderService
import app.grand.tafwak.presentation.userFlow.ProviderDetailsFragment
import app.grand.tafwak.presentation.userFlow.ProviderDetailsFragmentArgs
import app.grand.tafwak.presentation.userFlow.ProviderDetailsFragmentDirections
import app.grand.tafwak.presentation.userFlow.adapters.RVItemImageRoundedInCard
import app.grand.tafwak.presentation.userFlow.adapters.RVItemSelectableService
import com.structure.base_mvvm.R
import dagger.hilt.android.lifecycle.HiltViewModel
import kotlinx.coroutines.delay
import kotlinx.coroutines.flow.first
import kotlinx.coroutines.launch
import javax.inject.Inject

@HiltViewModel
class ProviderDetailsViewModel @Inject constructor(
	application: Application,
	args: ProviderDetailsFragmentArgs,
	repository: UserRepositoryImpl,
	private val prefsUser: PrefsUser
) : AndroidViewModel(application), RVItemSelectableService.Listener {
	
	val adapterPreviousWorks = RVItemImageRoundedInCard()
	
	val adapterServices = RVItemSelectableService(this)
	
	val response = RetryAbleFlow2 {
		repository.getProviderDetailsWithLocation(args.id)
	}
	
	var details = ProviderDetails()
	
	var locationData = LocationData("0.0", "0.0", "")
	
	val showSelectTimeAndDate = MutableLiveData(false)
	
	val numberOfPeople = MutableLiveData("1")
	
	val totalCost = numberOfPeople.map { calculateTotalCost() }
	
	val search = MutableLiveData("")
	
	fun onDecrementNumberOfPeople() {
		numberOfPeople.value = numberOfPeople.value!!.toInt().dec().coerceAtLeast(0).toString()
	}
	fun onIncrementNumberOfPeople() {
		numberOfPeople.value = numberOfPeople.value!!.toInt().inc().toString()
	}
	
	/*fun onSearchTextSubmit() = TextView.OnEditorActionListener { textView, actionId, _ ->
		if (actionId == EditorInfo.IME_ACTION_SEARCH) {
			val query = textView.text?.toString().orEmpty()
			
			search.value = query
			
			val imm = textView.context.getSystemService<InputMethodManager>()
			imm?.hideSoftInputFromWindow(textView.windowToken, 0)
			
			return@OnEditorActionListener true
		}
		
		false
	}*/
	
	fun selectTimeAndDate(view: View) {
		if (numberOfPeople.value.toIntOrZero() <= 0) {
			view.context.showErrorToast(view.context.getString(R.string.min_number_of_people_must_be_1))
			
			return
		}
		
		val fragment = view.findFragment<ProviderDetailsFragment>()
		fragment.lifecycleScope.launch {
			fragment.activityViewModel?.globalLoading?.value = true
			
			val isGuest = prefsUser.getIsGuest().first()
			
			fragment.activityViewModel?.globalLoading?.postValue(false)
			
			delay(300)
			
			if (isGuest) {
				view.context.showErrorToast(view.context.getString(R.string.you_must_sign_in_tocreate_an_order))
			}else {
				view.findNavController().navigate(
					ProviderDetailsFragmentDirections.actionDestProviderDetailsToDestDateAndTimePickerDialog()
				)
			}
		}
	}
	
	fun showAllPreviousWorks(view: View) {
		view.findNavController().navigate(
			ProviderDetailsFragmentDirections.actionDestProviderDetailsToDestShowAllPreviousWorks(details.id, false)
		)
	}
	
	override fun afterChangeSelection(newlySelectedItems: List<ProviderService>) {
		showSelectTimeAndDate.value = newlySelectedItems.isNotEmpty()
		
		totalCost.value = calculateTotalCost()
	}
	
	private fun calculateTotalCost(): String {
		val prefix = (numberOfPeople.value.toIntOrZero() *
			adapterServices.getSelectedServices().sumOf { it.price.orZero() }).toString()

		return "$prefix ${app.getString(R.string.sar)}"
	}
	
}
