package app.grand.tafwak.presentation.userFlow.adapters

import android.view.View
import android.view.ViewGroup
import androidx.recyclerview.widget.DiffUtil
import androidx.recyclerview.widget.ListAdapter
import app.grand.tafwak.domain.user.entity.model.ProviderMiniDetails
import app.grand.tafwak.presentation.userFlow.adapters.viewHolders.VHItemSearchResultProvider

class RVItemSearchResultProvider(private val listener: Listener) : ListAdapter<ProviderMiniDetails, VHItemSearchResultProvider>(
	COMPARATOR
) {
	
	companion object {
		val COMPARATOR = object : DiffUtil.ItemCallback<ProviderMiniDetails>() {
			override fun areItemsTheSame(
				oldItem: ProviderMiniDetails,
				newItem: ProviderMiniDetails
			): Boolean = oldItem.id == newItem.id
			
			override fun areContentsTheSame(
				oldItem: ProviderMiniDetails,
				newItem: ProviderMiniDetails
			): Boolean = oldItem == newItem
		}
	}
	
	override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): VHItemSearchResultProvider {
		return VHItemSearchResultProvider(parent, listener)
	}
	
	override fun onBindViewHolder(holder: VHItemSearchResultProvider, position: Int) {
		holder.bind(getItem(position))
	}
	
	interface Listener {
		fun onProviderClick(view: View, id: Int)
	}
	
}
