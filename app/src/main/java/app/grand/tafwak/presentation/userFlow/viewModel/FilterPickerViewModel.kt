package app.grand.tafwak.presentation.userFlow.viewModel

import android.view.View
import android.widget.TextView
import androidx.fragment.app.findFragment
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel
import androidx.navigation.findNavController
import androidx.navigation.fragment.findNavController
import app.grand.tafwak.domain.userFlow.model.SearchFilter
import app.grand.tafwak.presentation.userFlow.FilterPickerDialogFragment
import app.grand.tafwak.presentation.userFlow.FilterPickerDialogFragmentArgs
import app.grand.tafwak.presentation.userFlow.SearchResultsProvidersFragment
import com.structure.base_mvvm.R
import dagger.hilt.android.lifecycle.HiltViewModel
import timber.log.Timber
import javax.inject.Inject

@HiltViewModel
class FilterPickerViewModel @Inject constructor(
	args: FilterPickerDialogFragmentArgs
) : ViewModel() {
	
	val filter = MutableLiveData(args.filter)
	
	fun changeFilter(view: View) {
		when ((view as? TextView)?.text) {
			view.context.getString(R.string.latest) -> filter.value = SearchFilter.LATEST
			view.context.getString(R.string.less_price_first) -> filter.value = SearchFilter.LESS_PRICE
			view.context.getString(R.string.nearest_distance_to_you) -> filter.value = SearchFilter.CLOSEST
			view.context.getString(R.string.best_rating) -> filter.value = SearchFilter.MOST_RATED
		}
	}
	
	// https://developer.android.com/guide/navigation/navigation-programmatic#returning_a_result
	fun confirm(view: View) {
		// navController.getBackStackEntry(R.id.dest_search_results_providers)
		val navController = view.findFragment<FilterPickerDialogFragment>().findNavController()
		
		navController.navigateUp()
		
		navController.currentBackStackEntry?.savedStateHandle?.set(
			SearchResultsProvidersFragment.KEY_FRAGMENT_RESULT_FILTER_AS_STRING,
			filter.value?.name
		)
	}
	
}
