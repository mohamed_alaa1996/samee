package app.grand.tafwak.presentation.auth2.confirm.viewModel

import android.os.CountDownTimer
import android.view.View
import androidx.fragment.app.findFragment
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel
import androidx.lifecycle.viewModelScope
import androidx.navigation.NavController
import androidx.navigation.NavOptions
import androidx.navigation.findNavController
import androidx.navigation.fragment.findNavController
import app.grand.tafwak.core.extensions.*
import app.grand.tafwak.data.auth.repository.MAAuthRepositoryImpl
import app.grand.tafwak.data.local.preferences.PrefsSplash
import app.grand.tafwak.data.local.preferences.PrefsUser
import app.grand.tafwak.domain.auth.entity.model.LocationData
import app.grand.tafwak.domain.auth.entity.response.ResponseVerifyPhone
import app.grand.tafwak.domain.splash.entity.SplashInitialLaunch
import app.grand.tafwak.presentation.auth2.confirm.ConfirmPhoneFragment
import app.grand.tafwak.presentation.auth2.confirm.ConfirmPhoneFragmentArgs
import app.grand.tafwak.presentation.auth2.logIn.LoginFragmentDirections
import app.grand.tafwak.presentation.base.extensions.executeOnGlobalLoadingAndAutoHandleRetryCancellable
import app.grand.tafwak.presentation.base.extensions.executeOnGlobalLoadingAndAutoHandleRetryCancellable2
import app.grand.tafwak.presentation.project.viewModel.ProjectViewModel
import com.structure.base_mvvm.R
import dagger.hilt.android.lifecycle.HiltViewModel
import kotlinx.coroutines.flow.first
import kotlinx.coroutines.launch
import timber.log.Timber
import javax.inject.Inject

@HiltViewModel
class ConfirmPhoneViewModel @Inject constructor(
	private val repository: MAAuthRepositoryImpl,
	private val args: ConfirmPhoneFragmentArgs,
	val prefsUser: PrefsUser,
	private val prefsSplash: PrefsSplash,
) : ViewModel() {

	var token = ""
	
	private val timer = object : CountDownTimer(60_000L, 1_000L) {
		override fun onTick(millisUntilFinished: Long) {
			val seconds = millisUntilFinished / 1000
			
			val suffixString = if (seconds < 10) "0$seconds" else "$seconds"
			
			countDownText.value = "00 : $suffixString"
		}
		
		override fun onFinish() {
			canResend.value = true
			countDownText.value = "00 : 00"
		}
	}
	
	val canResend = MutableLiveData(false)
	
	val countDownText = MutableLiveData("00 : 60")
	
	val firstConfirmText = MutableLiveData("")
	val secondConfirmText = MutableLiveData("")
	val thirdConfirmText = MutableLiveData("")
	val fourthConfirmText = MutableLiveData("")
	
	//val code = MutableLiveData("")

	/** `null` means ignore else handle action with loading dialog, success or error as toast */
	/*val responseVerify = code.switchMap {
		if (it != null && it.length == 4) {
			if (args.isUser) {
				repository.verifyUserPhone(it, args.phone)
			}else {
				repository.verifyProviderPhone(it, args.phone)
			}
		}else{
			MutableLiveData(null)
		}
	}*/
	
	init {
		timer.start()
	}

	fun onResendClick(view: View) {
		view.findFragment<ConfirmPhoneFragment>().executeOnGlobalLoadingAndAutoHandleRetryCancellable2(
			afterShowingLoading = {
				repository.resendUserVerificationCode2(args.phone)
			},
			afterHidingLoading = {
				view.context.showSuccessToast(it.message)

				canResend.value = false

				timer.start()
			}
		)
	}
	
	fun onConfirmClick(view: View) {
		if (firstConfirmText.value.isNullOrEmpty() || secondConfirmText.value.isNullOrEmpty()
			|| thirdConfirmText.value.isNullOrEmpty() || fourthConfirmText.value.isNullOrEmpty()) {
			return view.context.showErrorToast(view.context.getString(R.string.all_fields_required))
		}

		val code = firstConfirmText.value.orEmpty() +
				secondConfirmText.value.orEmpty() +
				thirdConfirmText.value.orEmpty() +
				fourthConfirmText.value.orEmpty()

		if (code.length != 4) {
			return view.context.showErrorToast(view.context.getString(R.string.all_fields_required))
		}

		val fragment = view.findFragment<ConfirmPhoneFragment>()

		fragment.executeOnGlobalLoadingAndAutoHandleRetryCancellable(
			afterShowingLoading = {
				if (args.isUser) {
					repository.verifyUserPhone2(code, args.phone)
				}else {
					repository.verifyProviderPhone2(code, args.phone)
				}
			},
			afterHidingLoading = {
				it?.also { data ->
					onVerifySuccess(
						fragment,
						data,
						data.id.orZero(),
						data.token.orEmpty(),
						data.imageUrl,
						fragment.findNavControllerOfProject(),
						fragment.activityViewModel,
					)
				}
			}
		)
	}
	
	private fun onVerifySuccess(fragment: ConfirmPhoneFragment, response: ResponseVerifyPhone, id: Int, token: String, imageUrl: String?, navController: NavController, activityViewModel: ProjectViewModel?) {
		timer.cancel()
		
		viewModelScope.launch {
			prefsUser.setId(id)

			prefsUser.setReferCode(response.referCode.orEmpty())

			Timber.v("referred by id -> response ${response.referredById}")

			prefsUser.setReferredById(response.referredById)

			Timber.e("hmmmmm $imageUrl --- $response")
			prefsUser.setImageUrl(imageUrl.orEmpty())
		
			prefsUser.setApiToken(token)
			
			prefsUser.setIsUserNotServiceProvider(args.isUser)
			
			prefsUser.setPhone(args.phone)
			
			if (args.isUser) {
				prefsUser.setName(response.name.orEmpty())
				response.email.orEmpty().also {
					if (it.isNotEmpty()) {
						prefsUser.setEMail(it)
					}
				}
			}else if (response.completeRegister == 1) {
				// Step 1
				prefsUser.setLocation(LocationData(response.latitude!!, response.longitude!!, response.address.orEmpty()))
				
				// Step 2
				prefsUser.setName(response.name.orEmpty())
				response.email.orEmpty().also {
					if (it.isNotEmpty()) {
						prefsUser.setEMail(it)
					}
				}
				prefsUser.setDescription(response.description.orEmpty())
				
				// Step 3
				prefsUser.setBankName(response.bankName.orEmpty())
				prefsUser.setBankAccountNumber(response.bankNumber.orEmpty())
				prefsUser.setBankIBANNumber(response.iban.orEmpty())
				prefsUser.setBankClientName(response.bankAccountHolder.orEmpty())
				
				// Step 4
				// image is already saved el7
				
				prefsSplash.setInitialLaunch(SplashInitialLaunch.SERVICE_PROVIDER)
			}

			if (args.isUser) {
				this@ConfirmPhoneViewModel.token = token

				fragment.locationHandler.performAccordingToGPSIsTurnedOnOrOffSuspended(
					offBlock = {
						fragment.activityViewModel?.globalLoading?.value = false

						navController.popBackStack(R.id.dest_log_in, false)
						navController.navigate(LoginFragmentDirections.actionDestLogInToDestLocationUserRegister(token))
					},
					onBlock = {
						fragment.locationHandler.requestCurrentLocation(true)
					}
				)
			}else {
				activityViewModel?.globalLoading?.value = false

				if (response.completeRegister == 1) {
					while (navController.popBackStack()) {
						continue
					}
					
					val options = NavOptions.Builder()
						.setEnterAnim(R.anim.anim_slide_in_right)
						.setExitAnim(R.anim.anim_slide_out_left)
						.setPopEnterAnim(R.anim.anim_slide_in_left)
						.setPopExitAnim(R.anim.anim_slide_out_right)
						.build()
					
					navController.navigate(
						R.id.nav_provider,
						null,
						options
					)
				}else {
					navController.popBackStack(R.id.dest_log_in, false)
					navController.navigate(LoginFragmentDirections.actionDestLogInToDestProviderRegistration(token))
				}
			}
		}
	}
	
	override fun onCleared() {
		timer.cancel()
		
		super.onCleared()
	}
	
}
