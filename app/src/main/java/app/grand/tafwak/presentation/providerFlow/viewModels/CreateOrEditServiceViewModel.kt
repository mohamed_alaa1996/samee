package app.grand.tafwak.presentation.providerFlow.viewModels

import android.app.Application
import android.net.Uri
import android.view.View
import android.widget.TextView
import androidx.fragment.app.findFragment
import androidx.lifecycle.AndroidViewModel
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel
import androidx.lifecycle.lifecycleScope
import androidx.navigation.NavDeepLinkRequest
import androidx.navigation.findNavController
import androidx.navigation.fragment.findNavController
import app.grand.tafwak.core.customTypes.TransformationsUtils
import app.grand.tafwak.core.extensions.*
import app.grand.tafwak.data.provider.repository.ProviderRepositoryImpl
import app.grand.tafwak.domain.user.entity.model.FileType
import app.grand.tafwak.domain.user.entity.model.ProviderService
import app.grand.tafwak.domain.userFlow.model.SearchFilter
import app.grand.tafwak.domain.utils.MAResult
import app.grand.tafwak.presentation.providerFlow.CreateOrEditServiceDialogFragment
import app.grand.tafwak.presentation.providerFlow.CreateOrEditServiceDialogFragmentArgs
import app.grand.tafwak.presentation.providerFlow.ServicesSelectionFragment
import app.grand.tafwak.presentation.providerHome.ServicesProviderFragment
import app.grand.tafwak.presentation.userFlow.FilterPickerDialogFragment
import app.grand.tafwak.presentation.userFlow.FilterPickerDialogFragmentArgs
import app.grand.tafwak.presentation.userFlow.SearchResultsProvidersFragment
import com.google.gson.Gson
import com.structure.base_mvvm.R
import dagger.hilt.android.lifecycle.HiltViewModel
import kotlinx.coroutines.delay
import kotlinx.coroutines.launch
import javax.inject.Inject

@HiltViewModel
class CreateOrEditServiceViewModel @Inject constructor(
	application: Application,
	private val args: CreateOrEditServiceDialogFragmentArgs,
	private val repository: ProviderRepositoryImpl,
	gson: Gson,
) : AndroidViewModel(application) {
	
	val isCreateServiceNotWorking = args.isCreateServiceNotWorking
	
	val providerService = args.providerServiceAsJson.fromJsonOrNull<ProviderService>(gson)
	
	private val isCreationNotEdit = providerService == null
	
	val nameOfService = MutableLiveData(providerService?.name.orEmpty())
	
	val priceOfService = MutableLiveData(providerService?.price?.toString().orEmpty())
	
	val imageUrl = MutableLiveData(providerService?.imageOrVideoUrl)
	
	val liveDataOfImageUri = MutableLiveData<Uri>()
	val liveDataOfVideoUri = MutableLiveData<Uri>()
	
	val showActualImage = switchMapMultiple(imageUrl, liveDataOfImageUri, liveDataOfVideoUri) {
		MutableLiveData(imageUrl.value != null || liveDataOfImageUri.value != null || liveDataOfVideoUri.value != null)
	}
	
	fun pickImageOrVideo(view: View) {
		view.findFragment<CreateOrEditServiceDialogFragment>().pickImageOrVideoOrRequestPermissions()
	}
	
	fun addOrEdit(view: View) {
		if (isCreateServiceNotWorking) {
			if (nameOfService.value.isNullOrEmpty() || priceOfService.value.isNullOrEmpty() || showActualImage.value != true) {
				return view.context.showErrorToast(view.context.getString(R.string.all_fields_required))
			}else if (!isCreationNotEdit && nameOfService.value == providerService?.name
				&& priceOfService.value == providerService?.price?.toString() && !imageUrl.value.isNullOrEmpty()) {
				return view.context.showErrorToast(view.context.getString(R.string.you_must_meke_at_least_1_change))
			}

			if (priceOfService.value.orEmpty().toIntOrNull() == null) {
				return view.context.showErrorToast(view.context.getString(R.string.number_is_too_big))
			}
		}else {
			if (showActualImage.value != true) {
				return view.context.showErrorToast(view.context.getString(R.string.all_fields_required))
			}
		}
		
		val fragment = view.findFragment<CreateOrEditServiceDialogFragment>()
		fragment.lifecycleScope.launch {
			fragment.activityViewModel?.globalLoading?.value = true
			
			val result = if (isCreateServiceNotWorking) {
				if (isCreationNotEdit) {
					val isImage = liveDataOfImageUri.value != null
					val fileType = if (isImage) FileType.IMAGE else FileType.VIDEO
					val uri = if (isImage) liveDataOfImageUri.value!! else liveDataOfVideoUri.value!!
					
					repository.addService(
						args.categoryId,
						nameOfService.value.orEmpty(),
						priceOfService.value.orEmpty().toInt(),
						fileType.apiKey,
						uri.createMultipartBodyPart(app, "file")!!
					)
				}else {
					val (fileType, uri) = when {
						liveDataOfImageUri.value != null -> FileType.IMAGE to liveDataOfImageUri.value!!
						liveDataOfVideoUri.value != null -> FileType.VIDEO to liveDataOfVideoUri.value!!
						else -> null to null
					}
					
					repository.editService(
						providerService?.id.orZero(),
						nameOfService.value.orEmpty(),
						priceOfService.value.orEmpty().toInt(),
						fileType?.apiKey,
						uri?.createMultipartBodyPart(app, "file")
					)
				}
			}else {
				val isImage = liveDataOfImageUri.value != null
				val fileType = if (isImage) FileType.IMAGE else FileType.VIDEO
				val uri = if (isImage) liveDataOfImageUri.value!! else liveDataOfVideoUri.value!!
				
				repository.createWorking(
					args.categoryId,
					fileType.apiKey,
					uri.createMultipartBodyPart(app, "file")!!
				)
			}
			
			when (result) {
				is MAResult.Failure -> {
					fragment.requireContext().showErrorToast(if (!result.message.isNullOrEmpty()) result.message else fragment.getString(R.string.something_went_wrong))
					
					fragment.activityViewModel?.globalLoading?.value = false
					
					delay(150)
					
					fragment.findNavController().navigateUp()
				}
				is MAResult.Success -> {
					fragment.activityViewModel?.globalLoading?.value = false
					
					delay(150)
					
					fragment.findNavController().navigateUp()
					
					fragment.findNavController().currentBackStackEntry?.savedStateHandle?.set(
						CreateOrEditServiceDialogFragment.KEY_FRAGMENT_RESULT_CREATION_OR_EDITION_IS_DONE,
						true
					)
				}
			}
		}
	}
	
}
