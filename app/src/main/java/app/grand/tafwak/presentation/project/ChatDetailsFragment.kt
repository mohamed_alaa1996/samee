package app.grand.tafwak.presentation.project

import android.Manifest
import android.app.Activity
import android.content.BroadcastReceiver
import android.content.Context
import android.content.Intent
import android.content.IntentFilter
import android.graphics.Bitmap
import android.graphics.BitmapFactory
import android.net.Uri
import android.os.Bundle
import android.provider.MediaStore
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.activity.result.contract.ActivityResultContracts
import androidx.fragment.app.activityViewModels
import androidx.fragment.app.viewModels
import androidx.lifecycle.Lifecycle
import androidx.lifecycle.lifecycleScope
import androidx.lifecycle.repeatOnLifecycle
import androidx.paging.LoadState
import androidx.paging.PagingData
import androidx.recyclerview.widget.LinearLayoutManager
import app.grand.tafwak.core.BaseNotificationModel
import app.grand.tafwak.core.customTypes.MADividerItemDecoration
import app.grand.tafwak.core.extensions.*
import app.grand.tafwak.data.local.preferences.PrefsSplash
import app.grand.tafwak.data.local.preferences.PrefsUser
import app.grand.tafwak.domain.providerFlow.ResponseChatMessage
import app.grand.tafwak.domain.user.entity.model.FileType
import app.grand.tafwak.presentation.base.MABaseFragment
import app.grand.tafwak.presentation.base.extensions.plusAssign
import app.grand.tafwak.presentation.project.viewModel.ChatDetailsViewModel
import app.grand.tafwak.presentation.project.viewModel.ConversationViewModel
import app.grand.tafwak.presentation.project.viewModel.ProjectViewModel
import app.grand.tafwak.presentation.userHome.adapters.LSAdapterLoadingErrorEmpty
import com.bumptech.glide.Glide
import com.bumptech.glide.request.RequestOptions
import com.google.gson.Gson
import com.structure.base_mvvm.R
import com.structure.base_mvvm.databinding.FragmentChatDetailsBinding
import com.structure.base_mvvm.databinding.FragmentConversationsBinding
import dagger.hilt.android.AndroidEntryPoint
import kotlinx.coroutines.flow.collectLatest
import kotlinx.coroutines.flow.first
import kotlinx.coroutines.launch
import timber.log.Timber
import java.io.ByteArrayOutputStream
import java.util.*
import javax.inject.Inject
import kotlin.math.roundToInt

@AndroidEntryPoint
class ChatDetailsFragment : MABaseFragment<FragmentChatDetailsBinding>() {

  private val viewModel by viewModels<ChatDetailsViewModel>()

  @Inject
  protected lateinit var prefsUser: PrefsUser

  @Inject
  protected lateinit var gson: Gson

  private val permissionLocationRequest = registerForActivityResult(
    ActivityResultContracts.RequestMultiplePermissions()
  ) { permissions ->
    when {
      permissions[Manifest.permission.READ_EXTERNAL_STORAGE] == true
        && permissions[Manifest.permission.WRITE_EXTERNAL_STORAGE] == true
        && permissions[Manifest.permission.CAMERA] == true -> {
        pickImageOrVideoViaChooser()
      }

      permissions[Manifest.permission.READ_EXTERNAL_STORAGE] == true
        && permissions[Manifest.permission.WRITE_EXTERNAL_STORAGE] == true -> {
        val image = getString(R.string.image)
        val video = getString(R.string.video)

        binding?.insertImageView?.showPopup(listOf(image, video)) {
          if (it.title?.toString() == image) {
            pickImage(false)
          } else {
            pickVideo(false)
          }
        }
      }

      permissions[Manifest.permission.CAMERA] == true -> {
        val image = getString(R.string.image)
        val video = getString(R.string.video)

        binding?.insertImageView?.showPopup(listOf(image, video)) {
          if (it.title?.toString() == image) {
            pickImage(true)
          } else {
            pickVideo(true)
          }
        }
      }

      else -> {
        requireContext().showNormalToast(getString(R.string.you_didn_t_accept_permission))
      }
    }
  }

  private val activityResultImageCamera = registerForActivityResult(
    ActivityResultContracts.StartActivityForResult()
  ) {
    if (it.resultCode == Activity.RESULT_OK) {
      val bitmap = it.data?.extras?.get("data") as? Bitmap ?: return@registerForActivityResult

      val uri = getUriFromBitmapRetrievedByCamera(bitmap)

      viewModel.sendMessageToApi(binding?.root, viewModel.msg.value.orEmpty(), FileType.IMAGE, uri)
    }
  }

  private val activityResultImageGallery = registerForActivityResult(
    ActivityResultContracts.StartActivityForResult()
  ) {
    if (it.resultCode == Activity.RESULT_OK) {
      val uri = it.data?.data ?: return@registerForActivityResult

      viewModel.sendMessageToApi(binding?.root, viewModel.msg.value.orEmpty(), FileType.IMAGE, uri)
    }
  }

  private val activityResultVideoCamera = registerForActivityResult(
    ActivityResultContracts.StartActivityForResult()
  ) {
    if (it.resultCode == Activity.RESULT_OK) {
      val uri = it.data?.data ?: return@registerForActivityResult

      if (!uri.checkSizeAndLengthOfVideo(context ?: return@registerForActivityResult)) {
        context?.showErrorToast(getString(R.string.max_video_hint))

        return@registerForActivityResult
      }

      viewModel.sendMessageToApi(binding?.root, viewModel.msg.value.orEmpty(), FileType.VIDEO, uri)
    }
  }

  private val activityResultVideoGallery = registerForActivityResult(
    ActivityResultContracts.StartActivityForResult()
  ) {
    if (it.resultCode == Activity.RESULT_OK) {
      val uri = it.data?.data ?: return@registerForActivityResult

      if (!uri.checkSizeAndLengthOfVideo(context ?: return@registerForActivityResult)) {
        context?.showErrorToast(getString(R.string.max_video_hint))

        return@registerForActivityResult
      }

      viewModel.sendMessageToApi(binding?.root, viewModel.msg.value.orEmpty(), FileType.VIDEO, uri)
    }
  }

  override fun getLayoutId(): Int = R.layout.fragment_chat_details

  override fun initializeBindingVariables() {
    binding?.viewModel = viewModel
  }

  var currentlyRefreshing = false

  override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
    binding?.recyclerView?.layoutManager =
      LinearLayoutManager(requireContext(), LinearLayoutManager.VERTICAL, false).also {
        it.stackFromEnd = true
      }

    val headerAdapterCurrent2 =
      LSAdapterLoadingErrorEmpty(viewModel.adapter, false, R.string.empty_string)
    val footerAdapterFinished2 =
      LSAdapterLoadingErrorEmpty(viewModel.adapter, true, R.string.empty_string)
    binding?.recyclerView?.adapter = viewModel.adapter.withCustomAdapters(
      headerAdapterCurrent2,
      footerAdapterFinished2
    )

    viewModel.adapter.addLoadStateListener { loadStates ->
      if (loadStates.refresh == LoadState.Loading || loadStates.append == LoadState.Loading) {
        currentlyRefreshing = true
      } else if (currentlyRefreshing) {
        currentlyRefreshing = false

        binding?.recyclerView?.post {
          binding?.recyclerView?.smoothScrollToPosition(binding?.recyclerView?.adapter!!.itemCount.dec())
        }
      }
    }

    viewLifecycleOwner.lifecycleScope.launch {
      viewLifecycleOwner.repeatOnLifecycle(Lifecycle.State.STARTED) {
        viewModel.chats.collectLatest { pagingData ->
          if (viewModel.adapter.myId == null) {
            viewModel.adapter.myId = prefsUser.getId().first()
          }

         viewModel.adapter.submitData(pagingData)


        }
      }
    }

  }

  fun pickImageOrVideoOrRequestPermissions() {
    when {
      requireContext().checkSelfPermissionGranted(Manifest.permission.READ_EXTERNAL_STORAGE)
        && requireContext().checkSelfPermissionGranted(Manifest.permission.WRITE_EXTERNAL_STORAGE)
        && requireContext().checkSelfPermissionGranted(Manifest.permission.CAMERA) -> {
        pickImageOrVideoViaChooser()
      }

      requireContext().checkSelfPermissionGranted(Manifest.permission.READ_EXTERNAL_STORAGE)
        && requireContext().checkSelfPermissionGranted(Manifest.permission.WRITE_EXTERNAL_STORAGE) -> {
        pickImage(false)
      }

      requireContext().checkSelfPermissionGranted(Manifest.permission.CAMERA) -> {
        pickImage(true)
      }

      else -> {
        permissionLocationRequest.launch(
          arrayOf(
            Manifest.permission.READ_EXTERNAL_STORAGE,
            Manifest.permission.WRITE_EXTERNAL_STORAGE,
            Manifest.permission.CAMERA,
          )
        )
      }
    }
  }

  private fun pickImage(fromCamera: Boolean) {
    if (fromCamera) {
      activityResultImageCamera.launch(Intent(MediaStore.ACTION_IMAGE_CAPTURE))
    } else {
      // From gallery
      activityResultImageGallery.launch(
        Intent(
          Intent.ACTION_PICK,
          MediaStore.Images.Media.EXTERNAL_CONTENT_URI
        )
      )
    }
  }

  private fun pickVideo(fromCamera: Boolean) {
    if (fromCamera) {
      activityResultVideoCamera.launch(Intent(MediaStore.ACTION_VIDEO_CAPTURE))
    } else {
      // From gallery
      activityResultVideoGallery.launch(
        Intent(
          Intent.ACTION_PICK,
          MediaStore.Video.Media.EXTERNAL_CONTENT_URI
        )
      )
    }
  }

  private fun pickImageOrVideoViaChooser() {
    val imageCamera = "${getString(R.string.image)} (${getString(R.string.camera)})"
    val imageGallery = "${getString(R.string.image)} (${getString(R.string.gallery)})"
    val videoCamera = "${getString(R.string.video)} (${getString(R.string.camera)})"
    val videoGallery = "${getString(R.string.video)} (${getString(R.string.gallery)})"

    binding?.insertImageView?.showPopup(
      listOf(
        imageCamera,
        imageGallery,
        videoCamera,
        videoGallery
      )
    ) {
      when (val title = it.title?.toString()) {
        imageCamera, imageGallery -> pickImage(title == imageCamera)
        videoCamera, videoGallery -> pickVideo(title == videoCamera)
      }
    }
  }

  private fun getUriFromBitmapRetrievedByCamera(bitmap: Bitmap): Uri {
    val stream = ByteArrayOutputStream()
    bitmap.compress(Bitmap.CompressFormat.JPEG, 70, stream)
    val byteArray = stream.toByteArray()
    val compressedBitmap = BitmapFactory.decodeByteArray(byteArray, 0, byteArray.size)

    val path = MediaStore.Images.Media.insertImage(
      requireContext().contentResolver,
      compressedBitmap,
      Date(System.currentTimeMillis()).toString() + "photo",
      null
    )
    return Uri.parse(path)
  }

  private val broadcastReceiver = object : BroadcastReceiver() {
    override fun onReceive(context: Context?, intent: Intent?) {
      if (intent?.action == BaseNotificationModel.Type.NEW_MESSAGE.apiKey) {
        lifecycleScope.launchWhenCreated {


          val responseMessage = Gson().fromJson(intent.extras?.get(BaseNotificationModel.Type.NEW_MESSAGE.apiKey) as String , ResponseChatMessage::class.java)
          Timber.e("Broadcast Catch A New Message :  ${responseMessage.toString()}")

          viewModel.adapter.addMessage(responseMessage)


          binding?.recyclerView?.post {
           binding?.recyclerView?.scrollToPosition(binding?.recyclerView?.adapter!!.itemCount.dec())
         }

        }
      }
    }
  }

  override fun onCreateView(
    inflater: LayoutInflater,
    container: ViewGroup?,
    savedInstanceState: Bundle?
  ): View? {

    val intentFilter = IntentFilter().apply {
      addAction(BaseNotificationModel.Type.NEW_MESSAGE.apiKey)

    }
    activity?.registerReceiver(broadcastReceiver, intentFilter)
    return super.onCreateView(inflater, container, savedInstanceState)
  }


  override fun onDestroyView() {
    activity?.unregisterReceiver(broadcastReceiver)
    super.onDestroyView()
  }


}
