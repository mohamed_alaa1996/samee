package app.grand.tafwak.presentation.providerHome

import android.os.Bundle
import android.os.Handler
import android.os.Looper
import android.view.View
import androidx.fragment.app.activityViewModels
import androidx.fragment.app.viewModels
import androidx.navigation.fragment.findNavController
import androidx.recyclerview.widget.GridLayoutManager
import androidx.recyclerview.widget.LinearLayoutManager
import app.grand.tafwak.core.extensions.orZero
import app.grand.tafwak.data.local.preferences.PrefsSplash
import app.grand.tafwak.domain.user.entity.model.ServiceCategory
import app.grand.tafwak.presentation.base.GlobalYesNoBottomSheetDialogFragment
import app.grand.tafwak.presentation.base.MABaseFragment
import app.grand.tafwak.presentation.base.extensions.handleRetryAbleFlowWithMustHaveResultWithNullability
import app.grand.tafwak.presentation.project.viewModel.ProjectViewModel
import app.grand.tafwak.presentation.providerFlow.CreateOrEditServiceDialogFragment
import app.grand.tafwak.presentation.providerHome.viewModels.ServicesProviderViewModel
import com.structure.base_mvvm.R
import com.structure.base_mvvm.databinding.FragmentServicesProviderBinding
import dagger.hilt.android.AndroidEntryPoint
import javax.inject.Inject

@AndroidEntryPoint
class ServicesProviderFragment : MABaseFragment<FragmentServicesProviderBinding>() {
	
	private val viewModel by viewModels<ServicesProviderViewModel>()

	override fun getLayoutId(): Int = R.layout.fragment_services_provider
	
	override fun initializeBindingVariables() {
		binding?.viewModel = viewModel
	}
	
	override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
		binding?.categoriesRecyclerView?.layoutManager = LinearLayoutManager(requireContext(), LinearLayoutManager.HORIZONTAL, false)
		binding?.categoriesRecyclerView?.adapter = viewModel.adapterCategories
		
		binding?.servicesRecyclerView?.layoutManager = GridLayoutManager(requireContext(), 2, GridLayoutManager.VERTICAL, false)
		binding?.servicesRecyclerView?.adapter = viewModel.adapterServices
		
		performHandleRetryAbleFlow()
		
		findNavController().currentBackStackEntry?.savedStateHandle?.getLiveData(
			CreateOrEditServiceDialogFragment.KEY_FRAGMENT_RESULT_CREATION_OR_EDITION_IS_DONE,
			false
		)?.observe(viewLifecycleOwner) {
			if (it == true) {
				findNavController().currentBackStackEntry?.savedStateHandle?.set(
					CreateOrEditServiceDialogFragment.KEY_FRAGMENT_RESULT_CREATION_OR_EDITION_IS_DONE,
					false
				)

				performHandleRetryAbleFlow(true)
			}
		}

		findNavController().currentBackStackEntry?.savedStateHandle?.getLiveData(
			GlobalYesNoBottomSheetDialogFragment.KEY_FRAGMENT_RESULT_YES_BUTTON_CLICKED,
			false
		)?.observe(viewLifecycleOwner) {
			if (it == true) {
				findNavController().currentBackStackEntry?.savedStateHandle?.set(
					GlobalYesNoBottomSheetDialogFragment.KEY_FRAGMENT_RESULT_YES_BUTTON_CLICKED,
					false
				)

				performHandleRetryAbleFlow(true)
			}
		}
	}
	
	private fun performHandleRetryAbleFlow(initialRetry: Boolean = false) {
		if (initialRetry) {
			viewModel.retryAbleFlow.retry()
			
			Handler(Looper.getMainLooper()).post {
				privatePerformHandleRetryAbleFlow()
			}
		}else {
			privatePerformHandleRetryAbleFlow()
		}
	}
	
	private fun privatePerformHandleRetryAbleFlow() {
		handleRetryAbleFlowWithMustHaveResultWithNullability(viewModel.retryAbleFlow) { list ->
			viewModel.listOfCategoryWithServices = list
			
			viewModel.adapterCategories.submitList(
				list.map { ServiceCategory(it.id.orZero(), it.imageUrl.orEmpty(), it.name.orEmpty()) }
			)
			
			viewModel.adapterServices.submitList(list.getOrNull(viewModel.selectedIndex)?.services.orEmpty())
		}
	}

}
