package app.grand.tafwak.presentation.project.adapters

import android.view.ViewGroup
import androidx.paging.PagingData
import androidx.paging.PagingDataAdapter
import androidx.recyclerview.widget.DiffUtil
import app.grand.tafwak.core.extensions.orZero
import app.grand.tafwak.domain.providerFlow.ResponseChatMessage
import app.grand.tafwak.domain.providerFlow.ResponseConversation
import app.grand.tafwak.presentation.project.adapters.viewHolders.VHItemChatDetails
import app.grand.tafwak.presentation.project.adapters.viewHolders.VHItemConversation

class RVItemChatDetails : PagingDataAdapter<ResponseChatMessage, VHItemChatDetails>(COMPARATOR) {
	
	companion object {
		val COMPARATOR = object : DiffUtil.ItemCallback<ResponseChatMessage>() {
			override fun areItemsTheSame(
				oldItem: ResponseChatMessage,
				newItem: ResponseChatMessage
			): Boolean = oldItem.id == newItem.id
			
			override fun areContentsTheSame(
				oldItem: ResponseChatMessage,
				newItem: ResponseChatMessage
			): Boolean  = oldItem == newItem
		}
		
		private const val MY_MSG = 1
		private const val OTHER_MSG = 2
	}
	
	var myId: Int? = null
	
	override fun getItemViewType(position: Int): Int {
		return if (getItem(position)?.receiverId == myId) OTHER_MSG else MY_MSG
	}
  suspend fun addMessage(responseChatMessage: ResponseChatMessage) {
    val currentList = snapshot().items // Get the current list of items
    val updatedList = currentList.toMutableList() // Convert the list to a mutable list
    updatedList.add(responseChatMessage) // Add the new message item to the list

    val newSnapshot = PagingData.from(updatedList) // Convert the updated list back to PagingData
    submitData(newSnapshot)
  }
	override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): VHItemChatDetails {
		return VHItemChatDetails(parent, viewType == MY_MSG)
	}
	
	override fun onBindViewHolder(holder: VHItemChatDetails, position: Int) {
		holder.bind(getItem(position) ?: return)
	}
	
}
