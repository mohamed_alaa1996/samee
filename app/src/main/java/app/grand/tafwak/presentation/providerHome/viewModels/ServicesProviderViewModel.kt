package app.grand.tafwak.presentation.providerHome.viewModels

import android.os.Handler
import android.os.Looper
import android.view.View
import androidx.fragment.app.findFragment
import androidx.lifecycle.ViewModel
import androidx.lifecycle.lifecycleScope
import androidx.navigation.findNavController
import app.grand.tafwak.core.customTypes.RetryAbleFlow
import app.grand.tafwak.core.extensions.orZero
import app.grand.tafwak.core.extensions.toJson
import app.grand.tafwak.core.extensions.toJsonOrNull
import app.grand.tafwak.data.provider.repository.ProviderRepositoryImpl
import app.grand.tafwak.data.user.repository.UserRepositoryImpl
import app.grand.tafwak.domain.providerFlow.CategoryWithServices
import app.grand.tafwak.presentation.base.extensions.handleRetryAbleFlowWithMustHaveResultWithNullability
import app.grand.tafwak.presentation.providerFlow.adapters.RVItemTextInCard2
import app.grand.tafwak.presentation.providerHome.ServicesProviderFragment
import app.grand.tafwak.presentation.providerHome.ServicesProviderFragmentDirections
import app.grand.tafwak.presentation.providerHome.adapters.RVItemProvidedService
import com.google.gson.Gson
import dagger.hilt.android.lifecycle.HiltViewModel
import kotlinx.coroutines.launch
import javax.inject.Inject

@HiltViewModel
class ServicesProviderViewModel @Inject constructor(
	repository: ProviderRepositoryImpl,
	private val gson: Gson
) : ViewModel(), RVItemTextInCard2.Listener, RVItemProvidedService.Listener {
	
	val adapterCategories = RVItemTextInCard2(this)
	
	val adapterServices = RVItemProvidedService(this)
	
	var selectedIndex = 0
		private set
	
	val retryAbleFlow = RetryAbleFlow(repository::getServices)
	
	var listOfCategoryWithServices = emptyList<CategoryWithServices>()
	
	fun addNewService(view: View) {
		view.findNavController().navigate(
			ServicesProviderFragmentDirections.actionDestServicesProviderToDestCreateOrEditServiceDialog(
				listOfCategoryWithServices[selectedIndex].id.orZero(), null, true
			)
		)
	}
	
	override fun changeCategoryWithWorking(index: Int) {
		if (index == selectedIndex) {
			return
		}
		
		val oldIndex = selectedIndex
		
		selectedIndex = index
		
		adapterCategories.notifyItemChanged(oldIndex)
		adapterCategories.notifyItemChanged(selectedIndex)
		
		adapterServices.submitList(listOfCategoryWithServices[selectedIndex].services)
	}
	
	override fun isSelected(index: Int): Boolean {
		return index == selectedIndex
	}
	
	override fun editService(view: View, id: Int) {
		view.findNavController().navigate(
			ServicesProviderFragmentDirections.actionDestServicesProviderToDestCreateOrEditServiceDialog(
				listOfCategoryWithServices[selectedIndex].id.orZero(),
				listOfCategoryWithServices[selectedIndex].services.orEmpty()
					.first { it.id == id }.toJson(gson),
				true
			)
		)
	}
	
	override fun deleteService(view: View, id: Int) {
		view.findNavController().navigate(
			ServicesProviderFragmentDirections.actionDestServicesProviderToDestGlobalDeleteServiceDialog(
				id
			)
		)
	}
	
}