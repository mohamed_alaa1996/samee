package app.grand.tafwak.presentation.base.extensions

import android.net.Uri
import android.os.Handler
import android.os.Looper
import androidx.core.os.postDelayed
import androidx.databinding.ViewDataBinding
import androidx.fragment.app.Fragment
import androidx.fragment.app.activityViewModels
import androidx.fragment.app.findFragment
import androidx.lifecycle.*
import androidx.navigation.NavDeepLinkRequest
import androidx.navigation.NavOptions
import androidx.navigation.Navigation
import androidx.navigation.findNavController
import app.grand.tafwak.core.customTypes.RetryAbleFlow
import app.grand.tafwak.core.customTypes.RetryAbleFlow2
import app.grand.tafwak.core.extensions.findNavControllerOfProject
import app.grand.tafwak.core.extensions.navigateDeepLinkWithoutOptions
import app.grand.tafwak.core.extensions.popAllBackStacks
import app.grand.tafwak.domain.splash.entity.SplashInitialLaunch
import app.grand.tafwak.domain.utils.MABaseResponse
import app.grand.tafwak.domain.utils.MAResult
import app.grand.tafwak.presentation.base.MABaseFragment
import app.grand.tafwak.presentation.base.MADialogFragment
import app.grand.tafwak.presentation.base.customTypes.GlobalError
import app.grand.tafwak.presentation.project.ProjectActivity
import app.grand.tafwak.presentation.project.viewModel.ProjectViewModel
import com.structure.base_mvvm.R
import kotlinx.coroutines.*
import kotlinx.coroutines.flow.Flow
import kotlinx.coroutines.flow.collectLatest
import kotlinx.coroutines.flow.retry
import timber.log.Timber

/**
 * - if [MABaseResponse.data] is not null then [onNotNullSuccess]
 */
fun <VDB : ViewDataBinding, F : MABaseFragment<VDB>, T> F.handleRetryAbleFlowWithMustHaveResultWithNullability(
	retryAbleFlow: RetryAbleFlow<T>,
	onNullableSuccess: (MABaseResponse<T>) -> Unit = {
		Timber.d("null success in handleRetryAbleFlowWithMustHaveResultWithNullability $it")
	},
	onNotNullSuccess: (T) -> Unit,
) {
	// Used to not keep repeating even after collection is done isa.
	var job: Job? = null
	job = viewLifecycleOwner.lifecycleScope.launch {
		viewLifecycleOwner.repeatOnLifecycle(Lifecycle.State.STARTED) {
			retryAbleFlow.value.collectLatest {
				when (it) {
					is MAResult.Loading -> {
						activityViewModel?.globalLoading?.value = true
					}
					is MAResult.Success -> {
						activityViewModel?.globalLoading?.value = false
						
						it.value.data?.also { data ->
							onNotNullSuccess(data)
						} ?: onNullableSuccess(it.value)
					}
					is MAResult.Failure -> {
						activityViewModel?.globalLoading?.value = false
						
						if (it.failureStatus == MAResult.Failure.Status.TOKEN_EXPIRED) {
							prefsSplash.setInitialLaunch(SplashInitialLaunch.LOGIN)
							
							prefsSplash.prefsUser.logOut()
							
							val uri = Uri.Builder()
								.scheme("fragment-dest-auth")
								.authority("app.grand.tafwak.global.log.in")
								.build()
							val request = NavDeepLinkRequest.Builder.fromUri(uri).build()
							val options = NavOptions.Builder()
								.setEnterAnim(R.anim.anim_slide_in_right)
								.setExitAnim(R.anim.anim_slide_out_left)
								.setPopEnterAnim(R.anim.anim_slide_in_left)
								.setPopExitAnim(R.anim.anim_slide_out_right)
								.setPopUpTo(R.id.dest_log_in, true/*todo check if required to be false isa.*/, saveState = false)
								.build()
							Navigation.findNavController(
								requireActivity(),
								R.id.mainNavHostFragment
							).navigate(request, options)
							
							return@collectLatest
						}
						/* else if -> MAResult.Failure.Status.ACTIVATION_NOT_VERIFIED ->
							not occurred in this app as it's a must to do it */
						/* else -> show retry able dialog as done below */
						
						activityViewModel?.globalError?.value = GlobalError.Show(it.message)

						activityViewModel?.globalError?.observe(viewLifecycleOwner, object : Observer<GlobalError> {
							override fun onChanged(globalError: GlobalError?) {
								if (globalError is GlobalError.Retry) {
									activityViewModel?.globalError?.removeObserver(this)

									activityViewModel?.globalError?.value = GlobalError.Cancel
									
									retryAbleFlow.retry()
									
									Handler(Looper.getMainLooper()).post {
										handleRetryAbleFlowWithMustHaveResultWithNullability(retryAbleFlow, onNullableSuccess, onNotNullSuccess)
									}
								}
							}
						})
					}
				}
			}
			job?.cancel()
		}
	}
}

fun <VDB : ViewDataBinding, F : MABaseFragment<VDB>, T> F.handleRetryAbleFlowWithMustHaveResultWithNullability(
	retryAbleFlow: RetryAbleFlow2<T>,
	onSuccess: (T) -> Unit,
) {
	// Used to not keep repeating even after collection is done isa.
	var job: Job? = null
	job = viewLifecycleOwner.lifecycleScope.launch {
		viewLifecycleOwner.repeatOnLifecycle(Lifecycle.State.STARTED) {
			retryAbleFlow.value.collectLatest {
				when (it) {
					is MAResult.Loading -> {
						activityViewModel?.globalLoading?.value = true
					}
					is MAResult.Success -> {
						activityViewModel?.globalLoading?.value = false
						
						onSuccess(it.value)
					}
					is MAResult.Failure -> {
						activityViewModel?.globalLoading?.value = false
						
						if (it.failureStatus == MAResult.Failure.Status.TOKEN_EXPIRED) {
							prefsSplash.setInitialLaunch(SplashInitialLaunch.LOGIN)
							
							prefsSplash.prefsUser.logOut()
							
							val uri = Uri.Builder()
								.scheme("fragment-dest-auth")
								.authority("app.grand.tafwak.global.log.in")
								.build()
							val request = NavDeepLinkRequest.Builder.fromUri(uri).build()
							val options = NavOptions.Builder()
								.setEnterAnim(R.anim.anim_slide_in_right)
								.setExitAnim(R.anim.anim_slide_out_left)
								.setPopEnterAnim(R.anim.anim_slide_in_left)
								.setPopExitAnim(R.anim.anim_slide_out_right)
								.setPopUpTo(R.id.dest_log_in, true/*todo check if required to be false isa.*/, saveState = false)
								.build()
							Navigation.findNavController(
								requireActivity(),
								R.id.mainNavHostFragment
							).navigate(request, options)
							
							return@collectLatest
						}
						/* else if -> MAResult.Failure.Status.ACTIVATION_NOT_VERIFIED ->
							not occurred in this app as it's a must to do it */
						/* else -> show retry able dialog as done below */

						activityViewModel?.globalError?.value = GlobalError.Show(it.message)

						activityViewModel?.globalError?.observe(viewLifecycleOwner, object : Observer<GlobalError> {
							override fun onChanged(globalError: GlobalError?) {
								if (globalError is GlobalError.Retry) {
									activityViewModel?.globalError?.removeObserver(this)

									activityViewModel?.globalError?.value = GlobalError.Cancel
									
									retryAbleFlow.retry()
									
									Handler(Looper.getMainLooper()).post {
										handleRetryAbleFlowWithMustHaveResultWithNullability(retryAbleFlow, onSuccess)
									}
								}
							}
						})
					}
				}
			}
			job?.cancel()
		}
	}
}

fun MABaseFragment<*>.executeShowingErrorOnce(
	canCancelDialog: Boolean,
	errorMessage: String,
	onRetryClick: () -> Unit,
) {
	val activityViewModel = activityViewModels<ProjectViewModel>().value

	activityViewModel.globalError.value = GlobalError.Show(errorMessage, canCancelDialog)

	activityViewModel.globalError.observe(viewLifecycleOwner, object :
		Observer<GlobalError> {
		override fun onChanged(globalError: GlobalError?) {
			if (globalError is GlobalError.Retry) {
				activityViewModel.globalError.removeObserver(this)

				activityViewModel.globalError.value = GlobalError.Cancel

				Handler(Looper.getMainLooper()).post {
					onRetryClick()
				}
			}
		}
	})
}

fun <T> MABaseFragment<*>.executeOnGlobalLoadingAndAutoHandleRetryCancellable(
	afterShowingLoading: suspend () -> MAResult.Immediate<MABaseResponse<T>>,
	afterHidingLoading: (T?) -> Unit,
	canCancelDialog: Boolean = true,
) {
	lifecycleScope.launch {
		activityViewModel?.globalLoading?.value = true

		delay(150)

		when (val result = afterShowingLoading()) {
			is MAResult.Failure -> {
				Timber.e("failure is $result")

				activityViewModel?.globalLoading?.value = false

				delay(150)

				if (result.failureStatus == MAResult.Failure.Status.TOKEN_EXPIRED) {
					prefsSplash.prefsUser.logOut()

					val navController = findNavControllerOfProject()

					navController.popAllBackStacks()

					navController.navigateDeepLinkWithoutOptions(
						"fragment-dest",
						"app.grand.tafwak.global.log.in",
					)

					return@launch
				}

				activityViewModel?.globalError?.value = GlobalError.Show(result.message, canCancelDialog)

				activityViewModel?.globalError?.observe(viewLifecycleOwner, object :
					Observer<GlobalError> {
					override fun onChanged(globalError: GlobalError?) {
						if (globalError is GlobalError.Retry) {
							activityViewModel?.globalError?.removeObserver(this)

							activityViewModel?.globalError?.value = GlobalError.Cancel

							Handler(Looper.getMainLooper()).post {
								executeOnGlobalLoadingAndAutoHandleRetryCancellable(afterShowingLoading, afterHidingLoading, canCancelDialog)
							}
						}
					}
				})
			}
			is MAResult.Success -> {
				activityViewModel?.globalLoading?.value = false

				delay(150)

				afterHidingLoading(result.value.data)
			}
		}
	}
}

fun <T> MABaseFragment<*>.executeOnGlobalLoadingAndAutoHandleRetryCancellable2(
	afterShowingLoading: suspend () -> MAResult.Immediate<MABaseResponse<T>>,
	afterHidingLoading: (MABaseResponse<T>) -> Unit,
	canCancelDialog: Boolean = true,
) {
	lifecycleScope.launch {
		activityViewModel?.globalLoading?.value = true

		delay(150)

		when (val result = afterShowingLoading()) {
			is MAResult.Failure -> {
				Timber.e("failure is $result")

				activityViewModel?.globalLoading?.value = false

				delay(150)

				if (result.failureStatus == MAResult.Failure.Status.TOKEN_EXPIRED) {
					prefsSplash.prefsUser.logOut()

					val navController = findNavControllerOfProject()

					navController.popAllBackStacks()

					navController.navigateDeepLinkWithoutOptions(
						"fragment-dest",
						"app.grand.tafwak.global.log.in",
					)

					return@launch
				}

				activityViewModel?.globalError?.value = GlobalError.Show(result.message, canCancelDialog)

				activityViewModel?.globalError?.observe(viewLifecycleOwner, object :
					Observer<GlobalError> {
					override fun onChanged(globalError: GlobalError?) {
						if (globalError is GlobalError.Retry) {
							activityViewModel?.globalError?.removeObserver(this)

							activityViewModel?.globalError?.value = GlobalError.Cancel

							Handler(Looper.getMainLooper()).post {
								executeOnGlobalLoadingAndAutoHandleRetryCancellable2(afterShowingLoading, afterHidingLoading, canCancelDialog)
							}
						}
					}
				})
			}
			is MAResult.Success -> {
				activityViewModel?.globalLoading?.value = false

				delay(150)

				afterHidingLoading(result.value)
			}
		}
	}
}

fun ProjectActivity.executeShowingErrorOnce(
	canCancelDialog: Boolean,
	errorMessage: String,
	onRetryClick: () -> Unit,
) {
	viewModel.globalError.value = GlobalError.Show(errorMessage, canCancelDialog)

	viewModel.globalError.observe(this, object :
		Observer<GlobalError> {
		override fun onChanged(globalError: GlobalError?) {
			if (globalError is GlobalError.Retry) {
				viewModel.globalError.removeObserver(this)

				viewModel.globalError.value = GlobalError.Cancel

				Handler(Looper.getMainLooper()).post {
					onRetryClick()
				}
			}
		}
	})
}

fun <T> MADialogFragment<*>.executeOnGlobalLoadingAndAutoHandleRetryCancellable(
	afterShowingLoading: suspend () -> MAResult.Immediate<MABaseResponse<T>>,
	afterHidingLoading: (T?) -> Unit,
	canCancelDialog: Boolean = true,
) {
	lifecycleScope.launch {
		activityViewModel?.globalLoading?.value = true

		delay(150)

		when (val result = afterShowingLoading()) {
			is MAResult.Failure -> {
				Timber.e("failure is $result")

				activityViewModel?.globalLoading?.value = false

				delay(150)

				if (result.failureStatus == MAResult.Failure.Status.TOKEN_EXPIRED) {
					prefsSplash.prefsUser.logOut()

					val navController = findNavControllerOfProject()

					navController.popAllBackStacks()

					navController.navigateDeepLinkWithoutOptions(
						"fragment-dest",
						"app.grand.tafwak.global.log.in",
					)

					return@launch
				}

				activityViewModel?.globalError?.value = GlobalError.Show(result.message, canCancelDialog)

				activityViewModel?.globalError?.observe(viewLifecycleOwner, object :
					Observer<GlobalError> {
					override fun onChanged(globalError: GlobalError?) {
						if (globalError is GlobalError.Retry) {
							activityViewModel?.globalError?.removeObserver(this)

							activityViewModel?.globalError?.value = GlobalError.Cancel

							Handler(Looper.getMainLooper()).post {
								executeOnGlobalLoadingAndAutoHandleRetryCancellable(afterShowingLoading, afterHidingLoading, canCancelDialog)
							}
						}
					}
				})
			}
			is MAResult.Success -> {
				activityViewModel?.globalLoading?.value = false

				delay(150)

				afterHidingLoading(result.value.data)
			}
		}
	}
}
