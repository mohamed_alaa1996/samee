package app.grand.tafwak.presentation.providerFlow.viewModels

import android.app.Application
import android.net.Uri
import android.view.View
import androidx.fragment.app.findFragment
import androidx.lifecycle.AndroidViewModel
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel
import androidx.lifecycle.viewModelScope
import androidx.navigation.NavDeepLinkRequest
import androidx.navigation.fragment.findNavController
import app.grand.tafwak.core.customTypes.TransformationsUtils
import app.grand.tafwak.core.extensions.*
import app.grand.tafwak.data.auth.repository.MAAuthRepositoryImpl
import app.grand.tafwak.data.local.preferences.PrefsSplash
import app.grand.tafwak.domain.splash.entity.SplashInitialLaunch
import app.grand.tafwak.domain.user.entity.model.FileType
import app.grand.tafwak.domain.user.entity.model.ServiceCategory
import app.grand.tafwak.domain.user.entity.model.ServiceWithUri
import app.grand.tafwak.domain.utils.MAResult
import app.grand.tafwak.presentation.providerFlow.ServicesSelectionFragment
import app.grand.tafwak.presentation.providerFlow.ServicesSelectionFragmentArgs
import app.grand.tafwak.presentation.providerFlow.adapters.RVItemTextInCard2
import app.grand.tafwak.presentation.userFlow.PersonalDataFragment
import com.google.gson.Gson
import com.structure.base_mvvm.R
import dagger.hilt.android.lifecycle.HiltViewModel
import kotlinx.coroutines.delay
import kotlinx.coroutines.launch
import okhttp3.MultipartBody
import timber.log.Timber
import javax.inject.Inject

@HiltViewModel
class ServicesSelectionViewModel @Inject constructor(
	application: Application,
	args: ServicesSelectionFragmentArgs,
	gson: Gson,
	private val repository: MAAuthRepositoryImpl,
	private val prefsSplash: PrefsSplash,
) : AndroidViewModel(application), RVItemTextInCard2.Listener {
	
	private val listOfServiceCategory = args.listOfServiceCategoryAsJson.fromJson<List<ServiceCategory>>(gson)
	
	private val correspondingList = listOfServiceCategory.map {
		ServiceWithUri(it.id, "", "", null, FileType.IMAGE)
	}
	
	val nameOfService = MutableLiveData("")
	
	val priceOfService = MutableLiveData("")
	
	val liveDataOfImageUri = MutableLiveData<Uri>()
	val liveDataOfVideoUri = MutableLiveData<Uri>()
	
	val enableButton = switchMapMultiple(nameOfService, priceOfService, liveDataOfImageUri, liveDataOfVideoUri) {
		val disable = nameOfService.value.isNullOrEmpty() || priceOfService.value.isNullOrEmpty()
			|| (liveDataOfImageUri.value == null && liveDataOfVideoUri.value == null)
		
		MutableLiveData(!disable)
	}
	
	val adapter = RVItemTextInCard2(this)
	
	private var selectedIndex = 0
	
	init {
		adapter.submitList(listOfServiceCategory)
	}
	
	fun pickImageOrVideo(view: View) {
		view.findFragment<ServicesSelectionFragment>().pickImageOrVideoOrRequestPermissions()
	}
	
	fun createAccount(view: View) {
		saveInCorrespondingList(selectedIndex)

		val filteredList = correspondingList.filter {
			it.name.isNotEmpty() && it.price.isNotEmpty() && it.uri != null
		}

		if (filteredList.size != correspondingList.size) {
			return view.context.showErrorToast(view.context.getString(R.string.fill_the_rest_of_data_of_other_services))
		}
		
		val fragment = view.findFragment<ServicesSelectionFragment>()
		viewModelScope.launch {
			fragment.activityViewModel?.globalLoading?.value = true
			
			val categoriesIds = mutableListOf<Int>()
			val servicesNames = mutableListOf<String>()
			val prices = mutableListOf<Int>()
			val files = mutableListOf<MultipartBody.Part>()
			val typesOfFiles = mutableListOf<String>()
			
			for (item in filteredList) {
				categoriesIds.add(item.id)
				servicesNames.add(item.name)
				prices.add(item.price.toInt())
				files.add(item.uri?.createMultipartBodyPart(app, "file[]")!!)
				typesOfFiles.add(item.type.apiKey)
			}
			
			val result = repository.registerProviderStepFinalStep(
				categoriesIds, servicesNames, prices, files, typesOfFiles
			)
			
			when (result) {
				is MAResult.Failure -> {
					fragment.requireContext().showErrorToast(result.message ?: fragment.getString(R.string.something_went_wrong))
					
					fragment.activityViewModel?.globalLoading?.value = false
				}
				is MAResult.Success -> {
					fragment.prefsUser.setImageUrl(result.value.data?.imageUrl.orEmpty())
					
					prefsSplash.setInitialLaunch(SplashInitialLaunch.SERVICE_PROVIDER)
					
					fragment.activityViewModel?.globalLoading?.value = false
					
					delay(300)
					
					val uri = Uri.Builder()
						.scheme("dialog-dest")
						.authority("app.grand.tafwak.global.single.button.with.img.dialog")
						.appendPath(result.value.message)
						.appendPath(fragment.getString(R.string.done_2))
						.appendPath(R.drawable.ic_success_in_a_circle.toString())
						.build()
					val request = NavDeepLinkRequest.Builder.fromUri(uri).build()
					
					fragment.findNavController().navigate(request)
				}
			}
		}
	}
	
	private fun saveInCorrespondingList(index: Int) {
		correspondingList[index].name = nameOfService.value.orEmpty()
		correspondingList[index].price = priceOfService.value.orEmpty()
		correspondingList[index].uri = liveDataOfImageUri.value ?: liveDataOfVideoUri.value
		correspondingList[index].type = if (liveDataOfImageUri.value != null) FileType.IMAGE else FileType.VIDEO
	}
	
	override fun changeCategoryWithWorking(index: Int) {
		if (index == selectedIndex) {
			return
		}
		
		saveInCorrespondingList(selectedIndex)
		
		val oldIndex = selectedIndex
		
		selectedIndex = index
		
		adapter.notifyItemChanged(oldIndex)
		adapter.notifyItemChanged(selectedIndex)
		
		val serviceWithUri = correspondingList[selectedIndex]
		
		nameOfService.value = serviceWithUri.name
		priceOfService.value = serviceWithUri.price
		liveDataOfImageUri.value = serviceWithUri.imageUri
		liveDataOfVideoUri.value = serviceWithUri.videoUri
	}
	
	override fun isSelected(index: Int): Boolean {
		return index == selectedIndex
	}
}
