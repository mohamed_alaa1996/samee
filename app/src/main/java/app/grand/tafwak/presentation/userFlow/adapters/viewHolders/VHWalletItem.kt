package app.grand.tafwak.presentation.userFlow.adapters.viewHolders

import android.text.Spanned
import android.text.style.ForegroundColorSpan
import android.text.style.TypefaceSpan
import android.view.ViewGroup
import androidx.core.text.buildSpannedString
import androidx.core.text.set
import androidx.recyclerview.widget.RecyclerView
import app.grand.tafwak.core.extensions.inflateLayout
import app.grand.tafwak.domain.userFlow.response.WalletItem
import app.grand.tafwak.presentation.userFlow.adapters.RVItemTextSearch
import com.structure.base_mvvm.R
import com.structure.base_mvvm.databinding.ItemTextSearchBinding
import com.structure.base_mvvm.databinding.ItemWalletBinding

class VHWalletItem(parent: ViewGroup) : RecyclerView.ViewHolder(
	parent.context.inflateLayout(R.layout.item_wallet, parent)
) {
	
	private val binding = ItemWalletBinding.bind(itemView)
	
	fun bind(item: WalletItem) {
		val stringRes = if (item.walletType == 0) {
			R.string.float_has_been_added_negatively_in_wallet_due_to_cancelling_order
		}else {
			R.string.float_has_been_added_in_wallet_due_to_cancelling_order
		}
		
		binding.dateTextView.text = item.date
		
		binding.descriptionTextView.text = buildSpannedString {
			append(binding.root.context.getString(stringRes, item.amount.toString()))
			
			val start = length
			append(" ${item.date}")
			this[start..length] = ForegroundColorSpan(binding.root.context.getColor(R.color.almostAccent))
		}
	}
	
}
