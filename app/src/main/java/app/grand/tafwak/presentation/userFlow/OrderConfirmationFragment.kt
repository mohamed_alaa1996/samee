package app.grand.tafwak.presentation.userFlow

import android.net.Uri
import android.os.Bundle
import android.view.View
import androidx.fragment.app.activityViewModels
import androidx.fragment.app.viewModels
import androidx.navigation.NavDeepLinkRequest
import androidx.navigation.findNavController
import androidx.navigation.fragment.findNavController
import androidx.recyclerview.widget.LinearLayoutManager
import app.grand.tafwak.core.extensions.*
import app.grand.tafwak.domain.auth.entity.model.ManualLocation
import app.grand.tafwak.domain.utils.MAResult
import app.grand.tafwak.presentation.auth2.location.LocationSelectionFragment
import app.grand.tafwak.presentation.base.GlobalBackToUserHomeDialogFragment
import app.grand.tafwak.presentation.base.GlobalBackToUserHomeDialogFragmentArgs
import app.grand.tafwak.presentation.base.MABaseFragment
import app.grand.tafwak.presentation.project.viewModel.ProjectViewModel
import app.grand.tafwak.presentation.providerHome.ProviderFragment
import app.grand.tafwak.presentation.userFlow.viewModel.OrderConfirmationViewModel
import com.google.android.gms.maps.*
import com.google.android.gms.maps.model.LatLng
import com.google.android.gms.maps.model.Marker
import com.google.android.gms.maps.model.MarkerOptions
import com.google.gson.Gson
import com.structure.base_mvvm.R
import com.structure.base_mvvm.databinding.FragmentOrderConfirmationBinding
import dagger.hilt.android.AndroidEntryPoint
import javax.inject.Inject
import kotlin.math.min

@AndroidEntryPoint
class OrderConfirmationFragment : MABaseFragment<FragmentOrderConfirmationBinding>(), OnMapReadyCallback {
	
	val viewModel by viewModels<OrderConfirmationViewModel>()
	
	@Inject
	protected lateinit var gson: Gson
	
	var marker: Marker? = null
	
	override fun getLayoutId(): Int = R.layout.fragment_order_confirmation
	
	override fun initializeBindingVariables() {
		binding?.viewModel = viewModel
	}
	
	override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
		activityViewModel?.titleToolbar?.postValue(viewModel.providerDetails.name)
		
		// RV setups
		viewModel.adapter.submitList(viewModel.providerDetails.services)
		binding?.servicesRecyclerView?.layoutManager = LinearLayoutManager(requireContext(), LinearLayoutManager.HORIZONTAL, false)
		binding?.servicesRecyclerView?.adapter = viewModel.adapter
		
		// Setup map for the image
		(childFragmentManager.findFragmentById(R.id.mapFragmentContainerView) as? SupportMapFragment)
			?.getMapAsync(this)
		
		// Observe Confirm order
		viewModel.response.observe(viewLifecycleOwner) {
			when (it) {
				is MAResult.Loading -> {
					activityViewModel?.globalLoading?.value = true
				}
				is MAResult.Failure -> {
					activityViewModel?.globalLoading?.value = false
					
					requireContext().showErrorToast(it.message ?: getString(R.string.something_went_wrong))
					
					viewModel.performResponse.value = false
				}
				is MAResult.Success -> {
					activityViewModel?.globalLoading?.value = false
					
					viewModel.performResponse.value = false
					
					val uri = Uri.Builder()
						.scheme("dialog-dest")
						.authority("app.grand.tafwak.global.back.to.user.home.dialog.with.image")
						.appendPath(getString(R.string.your_request_to_verify_your_account_has_been_delivered_successfully_3))
						.appendPath(R.drawable.ic_success_in_a_circle.toString())
						.build()
					val request = NavDeepLinkRequest.Builder.fromUri(uri).build()
					
					findNavController().navigate(request)
				}
			}
		}
		
		// Observe fragment results
		findNavController().currentBackStackEntry?.savedStateHandle?.getLiveData(
			ManualLocationFragment.KEY_FRAGMENT_RESULT_MANUAL_LOCATION_AS_JSON,
			""
		)?.observe(viewLifecycleOwner) {
			if (!it.isNullOrEmpty()) {
				val manualLocation = it.fromJson<ManualLocation>(gson)

				findNavController().currentBackStackEntry?.savedStateHandle?.set(
					ManualLocationFragment.KEY_FRAGMENT_RESULT_MANUAL_LOCATION_AS_JSON,
					""
				)

				viewModel.manualLocation = manualLocation
			}
		}

		findNavController().currentBackStackEntry?.savedStateHandle?.getLiveData(
			GlobalBackToUserHomeDialogFragment.KEY_FRAGMENT_RESULT_BUTTON_CLICKED,
			false
		)?.observe(viewLifecycleOwner) {
			if (it == true) {
				val navController = findNavController()

				navController.currentBackStackEntry?.savedStateHandle?.set(
					GlobalBackToUserHomeDialogFragment.KEY_FRAGMENT_RESULT_BUTTON_CLICKED,
					false
				)

				navController.popBackStack(R.id.dest_user, false)

				navController.currentBackStackEntry?.savedStateHandle?.set(
					ProviderFragment.KEY_FRAGMENT_RESULT_GO_TO_MENU_ITEM_ID,
					R.id.action_home
				)
			}
		}

		findNavControllerOfProject().currentBackStackEntry?.savedStateHandle?.getLiveData(
			LocationSelectionFragment.KEY_FRAGMENT_RESULT_LOCATION_DATA_AS_JSON,
			""
		)?.observe(viewLifecycleOwner) {
			if (!it.isNullOrEmpty()) {
				findNavControllerOfProject().currentBackStackEntry?.savedStateHandle?.set(
					LocationSelectionFragment.KEY_FRAGMENT_RESULT_LOCATION_DATA_AS_JSON,
					""
				)

				viewModel.locationData.value = it.fromJson(gson)
			}
		}
	}
	
	override fun onMapReady(googleMap: GoogleMap) {
		val location = LatLng(viewModel.locationData.value!!.latitude.toDouble(), viewModel.locationData.value!!.longitude.toDouble())

		// Add marker
		marker?.remove()
		marker = googleMap.addMarker(MarkerOptions().position(location))
		
		// Center map on the marker
		val zoom = min(googleMap.maxZoomLevel, 15f /* or 4f */)
		googleMap.moveCamera(CameraUpdateFactory.newLatLngZoom(location, zoom))
		
		binding?.mapImageView?.visibility = View.INVISIBLE
	}
	
}