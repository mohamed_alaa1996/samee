package app.grand.tafwak.presentation.userFlow.adapters

import android.view.ViewGroup
import androidx.paging.PagingDataAdapter
import androidx.recyclerview.widget.DiffUtil
import app.grand.tafwak.domain.userFlow.response.CategoryWithWorkings
import app.grand.tafwak.presentation.userFlow.adapters.viewHolders.VHItemTextInCard

class RVItemTextInCard(private val listener: Listener) : PagingDataAdapter<CategoryWithWorkings, VHItemTextInCard>(COMPARATOR) {
	
	companion object {
		val COMPARATOR = object : DiffUtil.ItemCallback<CategoryWithWorkings>() {
			override fun areItemsTheSame(
				oldItem: CategoryWithWorkings,
				newItem: CategoryWithWorkings
			): Boolean = oldItem.id == newItem.id
			
			override fun areContentsTheSame(
				oldItem: CategoryWithWorkings,
				newItem: CategoryWithWorkings
			): Boolean  = oldItem == newItem
		}
	}
	
	override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): VHItemTextInCard {
		return VHItemTextInCard(parent, listener)
	}
	
	override fun onBindViewHolder(holder: VHItemTextInCard, position: Int) {
		val item = getItem(position) ?: return
		
		holder.bind(item, listener.isSelected(position), position)
	}
	
	interface Listener {
		fun changeCategoryWithWorking(index: Int)
		
		fun isSelected(index: Int): Boolean
	}
	
}
