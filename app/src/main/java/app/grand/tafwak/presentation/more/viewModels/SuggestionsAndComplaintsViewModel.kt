package app.grand.tafwak.presentation.more.viewModels

import android.view.View
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel
import androidx.lifecycle.asLiveData
import app.grand.tafwak.core.extensions.showErrorToast
import app.grand.tafwak.core.extensions.switchMap
import app.grand.tafwak.data.auth.repository.MAAuthRepositoryImpl
import app.grand.tafwak.data.maSettings.repository.SettingsRepositoryImpl
import app.grand.tafwak.domain.settings.request.RequestSuggestionsAndComplains
import com.structure.base_mvvm.R
import dagger.hilt.android.lifecycle.HiltViewModel
import javax.inject.Inject

@HiltViewModel
class SuggestionsAndComplaintsViewModel @Inject constructor(
	private val repository: SettingsRepositoryImpl
) : ViewModel() {
	
	val name = MutableLiveData("")
	
	val email = MutableLiveData("")
	
	val phone = MutableLiveData("")
	
	val message = MutableLiveData("")
	
	val receivedResponse = MutableLiveData(false)
	
	val response = receivedResponse.switchMap {
		if (it != true) {
			MutableLiveData()
		}else {
			repository.sendSuggestionsAndComplains(RequestSuggestionsAndComplains(
				name.value.orEmpty(),
				email.value.orEmpty(),
				phone.value.orEmpty(),
				message.value.orEmpty(),
			))
		}
	}
	
	fun send(view: View) {
		if (name.value.isNullOrEmpty() || email.value.isNullOrEmpty()
			|| phone.value.isNullOrEmpty() || message.value.isNullOrEmpty()) {
			view.context.showErrorToast(view.context.getString(R.string.all_fields_required))
		}else{
			receivedResponse.value = true
		}
	}
	
}
