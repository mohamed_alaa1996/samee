package app.grand.tafwak.presentation.userFlow.adapters

import android.view.View
import android.view.ViewGroup
import androidx.recyclerview.widget.DiffUtil
import androidx.recyclerview.widget.ListAdapter
import app.grand.tafwak.domain.user.entity.model.PreviousWork
import app.grand.tafwak.domain.userFlow.response.Working
import app.grand.tafwak.presentation.userFlow.adapters.viewHolders.VHItemImageRoundedInCard
import app.grand.tafwak.presentation.userFlow.adapters.viewHolders.VHItemImageRoundedInCard2

class RVItemImageRoundedInCard2(private val listener: Listener, private val enableDeletion: Boolean) : ListAdapter<Working, VHItemImageRoundedInCard2>(COMPARATOR) {
	
	companion object {
		val COMPARATOR = object : DiffUtil.ItemCallback<Working>() {
			override fun areItemsTheSame(
				oldItem: Working,
				newItem: Working
			): Boolean = oldItem.id == newItem.id
			
			override fun areContentsTheSame(
				oldItem: Working,
				newItem: Working
			): Boolean = oldItem == newItem
		}
	}
	
	override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): VHItemImageRoundedInCard2 {
		return VHItemImageRoundedInCard2(parent, listener, enableDeletion)
	}
	
	override fun onBindViewHolder(holder: VHItemImageRoundedInCard2, position: Int) {
		holder.bind(getItem(position) ?: return)
	}
	
	interface Listener {
		fun onDeleteClick(view: View, id: Int)
	}
	
}
