package app.grand.tafwak.presentation.splash.viewModel

import android.app.Application
import androidx.lifecycle.AndroidViewModel
import app.grand.tafwak.core.customTypes.RetryAbleFlow
import app.grand.tafwak.core.extensions.app
import app.grand.tafwak.core.extensions.flowInitialLoadingWithMinExecutionTime
import app.grand.tafwak.core.extensions.getDeviceIdWithoutPermission
import app.grand.tafwak.data.auth.repository.MAAuthRepositoryImpl
import app.grand.tafwak.data.provider.repository.ProviderRepositoryImpl
import app.grand.tafwak.domain.utils.MABaseResponse
import app.grand.tafwak.domain.utils.MAResult
import com.google.firebase.messaging.FirebaseMessaging
import dagger.hilt.android.lifecycle.HiltViewModel
import timber.log.Timber
import javax.inject.Inject
import kotlin.coroutines.resume
import kotlin.coroutines.suspendCoroutine

@HiltViewModel
class SplashViewModel @Inject constructor(
	application: Application,
	private val repository: MAAuthRepositoryImpl
) : AndroidViewModel(application) {
	
	val retryAbleFlow = RetryAbleFlow {
		getCurrentFirebaseTokenAsFlow()
	}
	
	private fun getCurrentFirebaseTokenAsFlow() = flowInitialLoadingWithMinExecutionTime<MABaseResponse<String>> {
		val (exception, token) = getCurrentFirebaseToken()
		
		if (!token.isNullOrEmpty()) {
			val result = repository.setFirebaseTokenSuspend(token, app.getDeviceIdWithoutPermission())
			
			if (result is MAResult.Failure) {
				emit(MAResult.Failure(result.failureStatus, result.code, result.message))
				
				return@flowInitialLoadingWithMinExecutionTime
			}
		}
		
		emit(
			if (token.isNullOrEmpty()) {
				MAResult.Failure(MAResult.Failure.Status.OTHER, null, exception?.message)
			}else {
				MAResult.Success(MABaseResponse(token, "", 0))
			}
		)
	}
	
	private suspend fun getCurrentFirebaseToken(): Pair<Exception?, String?> {
		return suspendCoroutine { continuation ->
			FirebaseMessaging.getInstance().token.addOnCompleteListener { task ->
				if (!task.isSuccessful) {
					Timber.d("Firebase Token exception -> ${task.exception}")
					continuation.resume(task.exception to null)
				}else {
					Timber.d("Firebase Token -> ${task.result}")
					continuation.resume(null to task.result)
				}
			}
		}
	}
	
}
