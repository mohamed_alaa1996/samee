package app.grand.tafwak.presentation.userFlow.adapters

import android.view.View
import android.view.ViewGroup
import androidx.recyclerview.widget.DiffUtil
import androidx.recyclerview.widget.ListAdapter
import app.grand.tafwak.domain.user.entity.model.ProviderMiniDetails
import app.grand.tafwak.presentation.userFlow.adapters.viewHolders.VHItemProviderOnMap
import app.grand.tafwak.presentation.userFlow.adapters.viewHolders.VHItemTextSearch

class RVItemProviderOnMap(private val listener: Listener) : ListAdapter<ProviderMiniDetails, VHItemProviderOnMap>(COMPARATOR) {
	
	companion object {
		val COMPARATOR = object : DiffUtil.ItemCallback<ProviderMiniDetails>() {
			override fun areItemsTheSame(
				oldItem: ProviderMiniDetails,
				newItem: ProviderMiniDetails
			): Boolean = oldItem.id == newItem.id
			
			override fun areContentsTheSame(
				oldItem: ProviderMiniDetails,
				newItem: ProviderMiniDetails
			): Boolean = oldItem == newItem
		}
	}
	
	override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): VHItemProviderOnMap {
		return VHItemProviderOnMap(parent, listener)
	}
	
	override fun onBindViewHolder(holder: VHItemProviderOnMap, position: Int) {
		holder.bind(getItem(position))
	}
	
	interface Listener {
		fun goToProviderDetails(view: View, id: Int)
	}
	
}
