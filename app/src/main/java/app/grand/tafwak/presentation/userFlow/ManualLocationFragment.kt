package app.grand.tafwak.presentation.userFlow

import androidx.fragment.app.viewModels
import app.grand.tafwak.presentation.base.MABaseFragment
import app.grand.tafwak.presentation.userFlow.viewModel.ManualLocationViewModel
import com.structure.base_mvvm.R
import com.structure.base_mvvm.databinding.FragmentManualLocationBinding
import dagger.hilt.android.AndroidEntryPoint

@AndroidEntryPoint
class ManualLocationFragment : MABaseFragment<FragmentManualLocationBinding>() {
	
	companion object {
		const val KEY_FRAGMENT_RESULT_MANUAL_LOCATION_AS_JSON = "KEY_FRAGMENT_RESULT_MANUAL_LOCATION_AS_JSON"
	}
	
	private val viewModel by viewModels<ManualLocationViewModel>()
	
	override fun getLayoutId(): Int = R.layout.fragment_manual_location
	
	override fun initializeBindingVariables() {
		binding?.viewModel = viewModel
	}
	
}
