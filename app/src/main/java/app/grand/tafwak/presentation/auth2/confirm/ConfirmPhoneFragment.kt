package app.grand.tafwak.presentation.auth2.confirm

import android.location.Location
import android.os.Bundle
import android.text.Editable
import android.text.TextWatcher
import android.view.View
import android.view.inputmethod.InputMethodManager
import androidx.core.content.getSystemService
import androidx.core.widget.addTextChangedListener
import androidx.fragment.app.activityViewModels
import androidx.fragment.app.viewModels
import androidx.lifecycle.viewModelScope
import androidx.navigation.NavOptions
import androidx.navigation.fragment.findNavController
import app.grand.tafwak.core.customTypes.LocationHandler
import app.grand.tafwak.core.extensions.getAddressFromLatitudeAndLongitude
import app.grand.tafwak.core.extensions.popAllBackStacks
import app.grand.tafwak.core.extensions.showErrorToast
import app.grand.tafwak.core.extensions.showSuccessToast
import app.grand.tafwak.domain.auth.entity.model.LocationData
import app.grand.tafwak.domain.splash.entity.SplashInitialLaunch
import app.grand.tafwak.domain.utils.MAResult
import app.grand.tafwak.presentation.auth2.confirm.viewModel.ConfirmPhoneViewModel
import app.grand.tafwak.presentation.auth2.logIn.LoginFragmentDirections
import app.grand.tafwak.presentation.base.MABaseFragment
import app.grand.tafwak.presentation.project.viewModel.ProjectViewModel
import app.grand.tafwak.presentation.userHome.UserFragmentArgs
import com.structure.base_mvvm.R
import com.structure.base_mvvm.databinding.FragmentConfirmPhoneBinding
import dagger.hilt.android.AndroidEntryPoint
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.launch
import kotlinx.coroutines.withContext

@AndroidEntryPoint
class ConfirmPhoneFragment : MABaseFragment<FragmentConfirmPhoneBinding>(), LocationHandler.Listener {
	
	private val viewModel by viewModels<ConfirmPhoneViewModel>()

	lateinit var locationHandler: LocationHandler

	private var firstText = ""
	private var secondText = ""
	private var thirdText = ""
	private var fourthText = ""
	
	private val beforeTextChange: (text: CharSequence?, start: Int, count: Int, after: Int) -> Unit = { _, _, _, _ ->
		firstText = viewModel.firstConfirmText.value.orEmpty()
		secondText = viewModel.secondConfirmText.value.orEmpty()
		thirdText = viewModel.thirdConfirmText.value.orEmpty()
		fourthText = viewModel.fourthConfirmText.value.orEmpty()
	}
	private val watcher: (Editable?) -> Unit = { _ ->
		if (fourthText.isNotEmpty() && viewModel.fourthConfirmText.value.isNullOrEmpty()) {
			binding?.thirdTextInputEditText?.requestFocus()
			binding?.thirdTextInputEditText?.post {
				binding?.thirdTextInputEditText?.setSelection(0, viewModel.thirdConfirmText.value.orEmpty().length)
			}
		}else if (thirdText.isNotEmpty() && viewModel.thirdConfirmText.value.isNullOrEmpty()) {
			binding?.secondTextInputEditText?.requestFocus()
			binding?.secondTextInputEditText?.post {
				binding?.secondTextInputEditText?.setSelection(0, viewModel.secondConfirmText.value.orEmpty().length)
			}
		}else if (secondText.isNotEmpty() && viewModel.secondConfirmText.value.isNullOrEmpty()) {
			binding?.firstTextInputEditText?.requestFocus()
			binding?.firstTextInputEditText?.post {
				binding?.firstTextInputEditText?.setSelection(0, viewModel.firstConfirmText.value.orEmpty().length)
			}
		}else {
			if (!viewModel.firstConfirmText.value.isNullOrEmpty()) {
				if (!viewModel.secondConfirmText.value.isNullOrEmpty()) {
					if (!viewModel.thirdConfirmText.value.isNullOrEmpty()) {
						if (!viewModel.fourthConfirmText.value.isNullOrEmpty()) {
							// Do nothing.
						}else {
							binding?.fourthTextInputEditText?.requestFocus()
						}
					}else {
						binding?.thirdTextInputEditText?.requestFocus()
					}
				}else {
					binding?.secondTextInputEditText?.requestFocus()
				}
			}else {
				binding?.firstTextInputEditText?.requestFocus()
			}
		}
	}

	override fun onCreate(savedInstanceState: Bundle?) {
		locationHandler = LocationHandler(
			this,
			lifecycle,
			requireContext(),
			this
		)

		super.onCreate(savedInstanceState)
	}
	
	override fun getLayoutId(): Int = R.layout.fragment_confirm_phone
	
	override fun initializeBindingVariables() {
		binding?.viewModel = viewModel
	}
	
	override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
		binding?.firstTextInputEditText?.addTextChangedListener(beforeTextChange) { watcher(it) }
		binding?.secondTextInputEditText?.addTextChangedListener(beforeTextChange) { watcher(it) }
		binding?.thirdTextInputEditText?.addTextChangedListener(beforeTextChange) { watcher(it) }
		binding?.fourthTextInputEditText?.addTextChangedListener(beforeTextChange) { watcher(it) }

		binding?.firstTextInputEditText?.post {
			val textView = binding?.firstTextInputEditText ?: return@post

			textView.requestFocus()

			val imm = context?.getSystemService<InputMethodManager>() ?: return@post
			imm.showSoftInput(textView, InputMethodManager.SHOW_FORCED)
		}
	}

	override fun onCurrentLocationResultSuccess(location: Location) {
		viewModel.viewModelScope.launch {
			val address = withContext(Dispatchers.IO) {
				context?.getAddressFromLatitudeAndLongitude(
					location.latitude,
					location.longitude
				)
			} ?: return@launch

			viewModel.prefsUser.setLocation(
				LocationData(
					location.latitude.toString(),
					location.longitude.toString(),
					address,
				)
			)

      prefsSplash.setInitialLaunch(SplashInitialLaunch.USER)

      val options = NavOptions.Builder()
				.setEnterAnim(R.anim.anim_slide_in_right)
				.setExitAnim(R.anim.anim_slide_out_left)
				.setPopEnterAnim(R.anim.anim_slide_in_left)
				.setPopExitAnim(R.anim.anim_slide_out_right)
				.build()

			val navController = findNavController()

			navController.popAllBackStacks()
			navController.navigate(
				R.id.nav_user,
				UserFragmentArgs(false).toBundle(),
				options
			)
		}
	}

}