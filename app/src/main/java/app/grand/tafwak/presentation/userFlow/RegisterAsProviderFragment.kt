package app.grand.tafwak.presentation.userFlow

import android.net.Uri
import android.os.Bundle
import android.view.View
import androidx.fragment.app.activityViewModels
import androidx.fragment.app.viewModels
import androidx.lifecycle.Lifecycle
import androidx.lifecycle.lifecycleScope
import androidx.lifecycle.repeatOnLifecycle
import androidx.navigation.NavDeepLinkRequest
import androidx.navigation.fragment.findNavController
import androidx.recyclerview.widget.LinearLayoutManager
import app.grand.tafwak.core.extensions.showErrorToast
import app.grand.tafwak.core.extensions.withCustomAdapters
import app.grand.tafwak.data.local.preferences.PrefsSplash
import app.grand.tafwak.domain.utils.MAResult
import app.grand.tafwak.presentation.base.GlobalBackToUserHomeBottomSheetDialogFragment
import app.grand.tafwak.presentation.base.GlobalBackToUserHomeDialogFragment
import app.grand.tafwak.presentation.base.MABaseFragment
import app.grand.tafwak.presentation.base.extensions.handleRetryAbleFlowWithMustHaveResultWithNullability
import app.grand.tafwak.presentation.project.viewModel.ProjectViewModel
import app.grand.tafwak.presentation.providerHome.ProviderFragment
import app.grand.tafwak.presentation.userFlow.viewModel.RegisterAsProviderViewModel
import app.grand.tafwak.presentation.userHome.adapters.LSAdapterLoadingErrorEmpty
import com.structure.base_mvvm.R
import com.structure.base_mvvm.databinding.FragmentRegisterAsProviderBinding
import dagger.hilt.android.AndroidEntryPoint
import kotlinx.coroutines.flow.collectLatest
import kotlinx.coroutines.launch
import javax.inject.Inject

@AndroidEntryPoint
class RegisterAsProviderFragment : MABaseFragment<FragmentRegisterAsProviderBinding>() {
	
	private val viewModel by viewModels<RegisterAsProviderViewModel>()
	
	override fun getLayoutId(): Int = R.layout.fragment_register_as_provider
	
	override fun initializeBindingVariables() {
		binding?.viewModel = viewModel
	}
	
	override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
		binding?.recyclerView?.layoutManager = LinearLayoutManager(requireContext(), LinearLayoutManager.VERTICAL, false)
		val headerAdapterCurrent2 = LSAdapterLoadingErrorEmpty(viewModel.adapter, false)
		val footerAdapterFinished2 = LSAdapterLoadingErrorEmpty(viewModel.adapter, true)
		binding?.recyclerView?.adapter = viewModel.adapter.withCustomAdapters(
			headerAdapterCurrent2,
			footerAdapterFinished2
		)
		
		viewLifecycleOwner.lifecycleScope.launch {
			viewLifecycleOwner.repeatOnLifecycle(Lifecycle.State.STARTED) {
				viewModel.categories.collectLatest {
					viewModel.adapter.submitData(it)
				}
			}
		}
		
		viewModel.registrationResponse.observe(viewLifecycleOwner) { result ->
			when (result) {
				is MAResult.Loading -> activityViewModel?.globalLoading?.value = true
				is MAResult.Success -> {
					activityViewModel?.globalLoading?.value = false
					
					viewModel.performRegister.value = false
					
					val uri = Uri.Builder()
						.scheme("dialog-dest")
						.authority("app.grand.tafwak.global.back.to.user.home.dialog.with.image.bottom.sheet")
						.appendPath(getString(R.string.your_request_to_verify_your_account_has_been_delivered_successfully_2))
						.appendPath(R.drawable.ic_success_in_a_circle.toString())
						.build()
					val request = NavDeepLinkRequest.Builder.fromUri(uri).build()
					
					findNavController().navigate(request)
				}
				is MAResult.Failure -> {
					activityViewModel?.globalLoading?.value = false
					
					viewModel.performRegister.value = false
					
					requireContext().showErrorToast(result.message ?: getString(R.string.something_went_wrong))
				}
			}
		}
		
		findNavController().currentBackStackEntry?.savedStateHandle?.getLiveData(
			GlobalBackToUserHomeBottomSheetDialogFragment.KEY_FRAGMENT_RESULT_BUTTON_CLICKED,
			false
		)?.observe(viewLifecycleOwner) {
			if (it == true) {
				val navController = findNavController()

				navController.currentBackStackEntry?.savedStateHandle?.set(
					GlobalBackToUserHomeBottomSheetDialogFragment.KEY_FRAGMENT_RESULT_BUTTON_CLICKED,
					false
				)

				navController.popBackStack(R.id.dest_user, false)

				navController.currentBackStackEntry?.savedStateHandle?.set(
					ProviderFragment.KEY_FRAGMENT_RESULT_GO_TO_MENU_ITEM_ID,
					R.id.action_home
				)
			}
		}
	}
	
}
