package app.grand.tafwak.presentation.providerFlow.adapters

import android.view.ViewGroup
import androidx.paging.PagingDataAdapter
import androidx.recyclerview.widget.DiffUtil
import androidx.recyclerview.widget.ListAdapter
import app.grand.tafwak.domain.user.entity.model.ServiceCategory
import app.grand.tafwak.domain.userFlow.response.CategoryWithWorkings
import app.grand.tafwak.presentation.providerFlow.adapters.viewHolders.VHItemTextInCard2
import app.grand.tafwak.presentation.userFlow.adapters.RVItemTextInCard
import app.grand.tafwak.presentation.userFlow.adapters.viewHolders.VHItemTextInCard

class RVItemTextInCard2(private val listener: Listener) : ListAdapter<ServiceCategory, VHItemTextInCard2>(COMPARATOR) {
	
	companion object {
		val COMPARATOR = object : DiffUtil.ItemCallback<ServiceCategory>() {
			override fun areItemsTheSame(
				oldItem: ServiceCategory,
				newItem: ServiceCategory
			): Boolean = oldItem.id == newItem.id
			
			override fun areContentsTheSame(
				oldItem: ServiceCategory,
				newItem: ServiceCategory
			): Boolean  = oldItem == newItem
		}
	}
	
	override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): VHItemTextInCard2 {
		return VHItemTextInCard2(parent, listener)
	}
	
	override fun onBindViewHolder(holder: VHItemTextInCard2, position: Int) {
		val item = getItem(position) ?: return
		
		holder.bind(item, listener.isSelected(position), position)
	}
	
	interface Listener {
		fun changeCategoryWithWorking(index: Int)
		
		fun isSelected(index: Int): Boolean
	}
	
}
