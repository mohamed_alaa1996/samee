package app.grand.tafwak.presentation.providerFlow

import android.Manifest
import android.app.Activity
import android.content.Intent
import android.graphics.Bitmap
import android.graphics.BitmapFactory
import android.net.Uri
import android.provider.MediaStore
import android.view.Gravity
import android.view.Window
import androidx.activity.result.contract.ActivityResultContracts
import androidx.fragment.app.activityViewModels
import androidx.fragment.app.viewModels
import app.grand.tafwak.core.extensions.*
import app.grand.tafwak.presentation.base.MADialogFragment
import app.grand.tafwak.presentation.base.extensions.getMyDrawable
import app.grand.tafwak.presentation.project.viewModel.ProjectViewModel
import app.grand.tafwak.presentation.providerFlow.viewModels.CreateOrEditServiceViewModel
import app.grand.tafwak.presentation.userFlow.viewModel.FilterPickerViewModel
import com.bumptech.glide.Glide
import com.bumptech.glide.request.RequestOptions
import com.structure.base_mvvm.R
import com.structure.base_mvvm.databinding.DialogFragmentCreateOrEditServiceBinding
import com.structure.base_mvvm.databinding.DialogFragmentFilterPickerBinding
import dagger.hilt.android.AndroidEntryPoint
import java.io.ByteArrayOutputStream
import java.util.*

@AndroidEntryPoint
class CreateOrEditServiceDialogFragment : MADialogFragment<DialogFragmentCreateOrEditServiceBinding>() {

	companion object {
		const val KEY_FRAGMENT_RESULT_CREATION_OR_EDITION_IS_DONE = "KEY_FRAGMENT_RESULT_CREATION_OR_EDITION_IS_DONE"
	}

	private val viewModel by viewModels<CreateOrEditServiceViewModel>()

	private val permissionLocationRequest = registerForActivityResult(
		ActivityResultContracts.RequestMultiplePermissions()
	) { permissions ->
		when {
			permissions[Manifest.permission.READ_EXTERNAL_STORAGE] == true
				&& permissions[Manifest.permission.WRITE_EXTERNAL_STORAGE] == true
				&& permissions[Manifest.permission.CAMERA] == true -> {
				pickImageOrVideoViaChooser()
			}
			permissions[Manifest.permission.READ_EXTERNAL_STORAGE] == true
				&& permissions[Manifest.permission.WRITE_EXTERNAL_STORAGE] == true -> {
        if (viewModel.isCreateServiceNotWorking) {
	        pickImage(false)
        }else {
	        val image = getString(R.string.image)
					val video = getString(R.string.video)

					binding.bgView.showPopup(listOf(image, video)) {
						if (it.title?.toString() == image) {
							pickImage(false)
						}else {
							pickVideo(false)
						}
					}
        }
			}
			permissions[Manifest.permission.CAMERA] == true -> {
				if (viewModel.isCreateServiceNotWorking) {
					pickImage(true)
				}else {
					val image = getString(R.string.image)
					val video = getString(R.string.video)

					binding.bgView.showPopup(listOf(image, video)) {
						if (it.title?.toString() == image) {
							pickImage(true)
						}else {
							pickVideo(true)
						}
					}
				}
			}
			else -> {
				requireContext().showNormalToast(getString(R.string.you_didn_t_accept_permission))
			}
		}
	}

	private val activityResultImageCamera = registerForActivityResult(
		ActivityResultContracts.StartActivityForResult()
	) {
		if (it.resultCode == Activity.RESULT_OK) {
			val bitmap = it.data?.extras?.get("data") as? Bitmap ?: return@registerForActivityResult

			val uri = getUriFromBitmapRetrievedByCamera(bitmap)

			viewModel.liveDataOfImageUri.value = uri
			viewModel.liveDataOfVideoUri.value = null
			viewModel.imageUrl.value = null

			Glide.with(this)
				.load(uri)
				.apply(RequestOptions().centerCrop())
				.into(binding.fileAsImageImageView)
		}
	}

	private val activityResultVideoCamera = registerForActivityResult(
		ActivityResultContracts.StartActivityForResult()
	) {
		if (it.resultCode == Activity.RESULT_OK) {
			val uri = it.data?.data ?: return@registerForActivityResult

			if (!uri.checkSizeAndLengthOfVideo(context ?: return@registerForActivityResult)) {
				context?.showErrorToast(getString(R.string.max_video_hint))

				return@registerForActivityResult
			}

			viewModel.liveDataOfVideoUri.value = uri
			viewModel.liveDataOfImageUri.value = null
			viewModel.imageUrl.value = null

			Glide.with(this)
				.load(uri)
				.apply(RequestOptions().frame(1).centerCrop())
				.into(binding.fileAsImageImageView)
		}
	}

	private val activityResultImageGallery = registerForActivityResult(
		ActivityResultContracts.StartActivityForResult()
	) {
		if (it.resultCode == Activity.RESULT_OK) {
			val uri = it.data?.data ?: return@registerForActivityResult

			viewModel.liveDataOfImageUri.value = uri
			viewModel.liveDataOfVideoUri.value = null
			viewModel.imageUrl.value = null

			Glide.with(this)
				.load(uri)
				.apply(RequestOptions().centerCrop())
				.into(binding.fileAsImageImageView)
		}
	}

	private val activityResultVideoGallery = registerForActivityResult(
		ActivityResultContracts.StartActivityForResult()
	) {
		if (it.resultCode == Activity.RESULT_OK) {
			val uri = it.data?.data ?: return@registerForActivityResult

			if (!uri.checkSizeAndLengthOfVideo(context ?: return@registerForActivityResult)) {
				context?.showErrorToast(getString(R.string.max_video_hint))

				return@registerForActivityResult
			}

			viewModel.liveDataOfVideoUri.value = uri
			viewModel.liveDataOfImageUri.value = null
			viewModel.imageUrl.value = null

			Glide.with(this)
				.load(uri)
				.apply(RequestOptions().frame(1).centerCrop())
				.into(binding.fileAsImageImageView)
		}
	}

	override val windowGravity: Int = Gravity.BOTTOM

	override fun getLayoutId(): Int = R.layout.dialog_fragment_create_or_edit_service

	override fun onCreateDialogWindowChanges(window: Window) {
		window.setBackgroundDrawable(getMyDrawable(R.drawable.dr_top_round_white))

		window.attributes?.windowAnimations = R.style.ScaleDialogAnim
	}

	override fun initializeBindingVariables() {
		binding.viewModel = viewModel
	}

	fun pickImageOrVideoOrRequestPermissions() {
		when {
			requireContext().checkSelfPermissionGranted(Manifest.permission.READ_EXTERNAL_STORAGE)
				&& requireContext().checkSelfPermissionGranted(Manifest.permission.WRITE_EXTERNAL_STORAGE)
				&& requireContext().checkSelfPermissionGranted(Manifest.permission.CAMERA) -> {
				pickImageOrVideoViaChooser()
			}
			requireContext().checkSelfPermissionGranted(Manifest.permission.READ_EXTERNAL_STORAGE)
				&& requireContext().checkSelfPermissionGranted(Manifest.permission.WRITE_EXTERNAL_STORAGE) -> {
        if (viewModel.isCreateServiceNotWorking) {
	        pickImage(false)
        }else {
	        val image = getString(R.string.image)
					val video = getString(R.string.video)

					binding.bgView.showPopup(listOf(image, video)) {
						if (it.title?.toString() == image) {
							pickImage(false)
						}else {
							pickVideo(false)
						}
					}
        }
			}
			requireContext().checkSelfPermissionGranted(Manifest.permission.CAMERA) -> {
        if (viewModel.isCreateServiceNotWorking) {
	        pickImage(true)
        }else {
	        val image = getString(R.string.image)
					val video = getString(R.string.video)

					binding.bgView.showPopup(listOf(image, video)) {
						if (it.title?.toString() == image) {
							pickImage(true)
						}else {
							pickVideo(true)
						}
					}
        }
			}
			else -> {
				permissionLocationRequest.launch(arrayOf(
					Manifest.permission.READ_EXTERNAL_STORAGE,
					Manifest.permission.WRITE_EXTERNAL_STORAGE,
					Manifest.permission.CAMERA,
				))
			}
		}
	}

	private fun pickImageOrVideoViaChooser() {
		val imageCamera = "${getString(R.string.image)} (${getString(R.string.camera)})"
		val imageGallery = "${getString(R.string.image)} (${getString(R.string.gallery)})"

		val list = mutableListOf(imageCamera, imageGallery)
		val videoCamera = "${getString(R.string.video)} (${getString(R.string.camera)})"
		val videoGallery = "${getString(R.string.video)} (${getString(R.string.gallery)})"

		if (viewModel.isCreateServiceNotWorking.not()) {
			list += videoCamera
			list += videoGallery
		}

		binding.bgView.showPopup(list) {
			when (val title = it.title?.toString()) {
				imageCamera, imageGallery -> pickImage(title == imageCamera)
				videoCamera, videoGallery -> pickVideo(title == videoCamera)
			}
		}
	}

	private fun pickImage(fromCamera: Boolean) {
		if (fromCamera) {
			activityResultImageCamera.launch(Intent(MediaStore.ACTION_IMAGE_CAPTURE))
		}else {
			// From gallery
			activityResultImageGallery.launch(Intent(Intent.ACTION_PICK, MediaStore.Images.Media.EXTERNAL_CONTENT_URI))
		}
	}

	private fun pickVideo(fromCamera: Boolean) {
		if (fromCamera) {
			activityResultVideoCamera.launch(Intent(MediaStore.ACTION_VIDEO_CAPTURE).also {
				it.putExtra(MediaStore.EXTRA_DURATION_LIMIT,15)
			})
		}else {
			// From gallery
			activityResultVideoGallery.launch(Intent(Intent.ACTION_PICK, MediaStore.Video.Media.EXTERNAL_CONTENT_URI))
		}
	}

	private fun getUriFromBitmapRetrievedByCamera(bitmap: Bitmap): Uri {
		val stream = ByteArrayOutputStream()
		bitmap.compress(Bitmap.CompressFormat.JPEG, 70, stream)
		val byteArray = stream.toByteArray()
		val compressedBitmap = BitmapFactory.decodeByteArray(byteArray, 0, byteArray.size)

		val path = MediaStore.Images.Media.insertImage(
			requireContext().contentResolver, compressedBitmap, Date(System.currentTimeMillis()).toString() + "photo", null
		)
		return Uri.parse(path)
	}

}
