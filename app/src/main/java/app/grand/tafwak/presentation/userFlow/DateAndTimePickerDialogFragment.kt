package app.grand.tafwak.presentation.userFlow

import android.view.Gravity
import android.view.Window
import androidx.fragment.app.activityViewModels
import androidx.fragment.app.viewModels
import app.grand.tafwak.presentation.base.MADialogFragment
import app.grand.tafwak.presentation.base.extensions.getMyDrawable
import app.grand.tafwak.presentation.project.viewModel.ProjectViewModel
import app.grand.tafwak.presentation.userFlow.viewModel.DateAndTimePickerViewModel
import app.grand.tafwak.presentation.userFlow.viewModel.FilterPickerViewModel
import com.structure.base_mvvm.R
import com.structure.base_mvvm.databinding.DialogFragmentDateAndTimePickerBinding
import dagger.hilt.android.AndroidEntryPoint

@AndroidEntryPoint
class DateAndTimePickerDialogFragment : MADialogFragment<DialogFragmentDateAndTimePickerBinding>() {
	
	companion object {
		const val KEY_FRAGMENT_RESULT_MA_DATE_AND_TIME_AS_JSON = "KEY_FRAGMENT_RESULT_MA_DATE_AND_TIME_AS_JSON"
	}
	
	private val viewModel by viewModels<DateAndTimePickerViewModel>()
	
	override fun getLayoutId(): Int = R.layout.dialog_fragment_date_and_time_picker
	
	override val windowGravity: Int = Gravity.BOTTOM
	
	override fun onCreateDialogWindowChanges(window: Window) {
		window.setBackgroundDrawable(getMyDrawable(R.drawable.dr_top_round_white))
		
		window.attributes?.windowAnimations = R.style.ScaleDialogAnim
	}
	
	override fun initializeBindingVariables() {
		binding.viewModel = viewModel
	}
	
}
