package app.grand.tafwak.presentation.providerFlow

import android.Manifest
import android.app.Activity
import android.content.Intent
import android.graphics.Bitmap
import android.graphics.BitmapFactory
import android.net.Uri
import android.os.Bundle
import android.provider.MediaStore
import android.view.View
import androidx.activity.OnBackPressedCallback
import androidx.activity.addCallback
import androidx.activity.result.contract.ActivityResultContracts
import androidx.fragment.app.activityViewModels
import androidx.fragment.app.setFragmentResultListener
import androidx.fragment.app.viewModels
import androidx.lifecycle.Lifecycle
import androidx.lifecycle.lifecycleScope
import androidx.lifecycle.repeatOnLifecycle
import androidx.navigation.fragment.findNavController
import androidx.recyclerview.widget.LinearLayoutManager
import app.grand.tafwak.core.extensions.*
import app.grand.tafwak.data.local.preferences.PrefsUser
import app.grand.tafwak.domain.auth.entity.model.LocationData
import app.grand.tafwak.presentation.auth2.location.LocationSelectionFragment
import app.grand.tafwak.presentation.base.MABaseFragment
import app.grand.tafwak.presentation.project.viewModel.ProjectViewModel
import app.grand.tafwak.presentation.providerFlow.viewModels.ProviderRegistrationViewModel
import app.grand.tafwak.presentation.userHome.adapters.LSAdapterLoadingErrorEmpty
import com.bumptech.glide.Glide
import com.bumptech.glide.request.RequestOptions
import com.structure.base_mvvm.R
import com.structure.base_mvvm.databinding.FragmentProviderRegistrationBinding
import dagger.hilt.android.AndroidEntryPoint
import kotlinx.coroutines.flow.collectLatest
import kotlinx.coroutines.launch
import java.io.ByteArrayOutputStream
import java.util.*
import javax.inject.Inject

@AndroidEntryPoint
class ProviderRegistrationFragment : MABaseFragment<FragmentProviderRegistrationBinding>() {
	
	private val viewModel by viewModels<ProviderRegistrationViewModel>()
	
	@Inject
	lateinit var prefsUser: PrefsUser

	var onBackPressedCallback: OnBackPressedCallback? = null

	private val permissionLocationRequest = registerForActivityResult(
		ActivityResultContracts.RequestMultiplePermissions()
	) { permissions ->
		when {
			permissions[Manifest.permission.READ_EXTERNAL_STORAGE] == true
				&& permissions[Manifest.permission.WRITE_EXTERNAL_STORAGE] == true
				&& permissions[Manifest.permission.CAMERA] == true -> {
				pickImageViaChooser()
			}
			permissions[Manifest.permission.READ_EXTERNAL_STORAGE] == true
				&& permissions[Manifest.permission.WRITE_EXTERNAL_STORAGE] == true -> {
				pickImage(false)
			}
			permissions[Manifest.permission.CAMERA] == true -> {
				pickImage(true)
			}
			else -> {
				requireContext().showNormalToast(getString(R.string.you_didn_t_accept_permission))
			}
		}
	}
	
	private val activityResultImageCamera = registerForActivityResult(
		ActivityResultContracts.StartActivityForResult()
	) {
		if (it.resultCode == Activity.RESULT_OK) {
			val bitmap = it.data?.extras?.get("data") as? Bitmap ?: return@registerForActivityResult
			
			val uri = getUriFromBitmapRetrievedByCamera(bitmap)
			
			viewModel.imageUri = uri
			
			Glide.with(this)
				.load(uri)
				.apply(RequestOptions().centerCrop())
				.into(binding?.personalDataImageImageView ?: return@registerForActivityResult)
		}
	}
	
	private val activityResultImageGallery = registerForActivityResult(
		ActivityResultContracts.StartActivityForResult()
	) {
		if (it.resultCode == Activity.RESULT_OK) {
			val uri = it.data?.data ?: return@registerForActivityResult
			
			viewModel.imageUri = uri
			
			Glide.with(this)
				.load(uri)
				.apply(RequestOptions().centerCrop())
				.into(binding?.personalDataImageImageView ?: return@registerForActivityResult)
		}
	}
	
	override fun getLayoutId(): Int = R.layout.fragment_provider_registration
	
	override fun initializeBindingVariables() {
		binding?.viewModel = viewModel
	}

	override fun onCreate(savedInstanceState: Bundle?) {
		super.onCreate(savedInstanceState)

		onBackPressedCallback = requireActivity().onBackPressedDispatcher.addCallback(
			this,
			viewModel.registrationStep.value.orZero() > 1
		) {
			viewModel.registrationStep.value = viewModel.registrationStep.value.orZero().dec()
		}
	}
	
	override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
		viewModel.registrationStep.observe(viewLifecycleOwner) {
			onBackPressedCallback?.isEnabled = it.orZero() > 1
		}

		binding?.recyclerView?.layoutManager = LinearLayoutManager(requireContext(), LinearLayoutManager.VERTICAL, false)
		val headerAdapterCurrent2 = LSAdapterLoadingErrorEmpty(viewModel.adapter, false)
		val footerAdapterFinished2 = LSAdapterLoadingErrorEmpty(viewModel.adapter, true)
		binding?.recyclerView?.adapter = viewModel.adapter.withCustomAdapters(
			headerAdapterCurrent2,
			footerAdapterFinished2
		)
		
		viewLifecycleOwner.lifecycleScope.launch {
			viewLifecycleOwner.repeatOnLifecycle(Lifecycle.State.STARTED) {
				viewModel.categories.collectLatest {
					viewModel.adapter.submitData(it)
				}
			}
		}
		
		findNavController().currentBackStackEntry?.savedStateHandle?.getLiveData(
			LocationSelectionFragment.KEY_FRAGMENT_RESULT_LOCATION_DATA_AS_JSON,
			""
		)?.observe(viewLifecycleOwner) {
			if (!it.isNullOrEmpty()) {
				findNavController().currentBackStackEntry?.savedStateHandle?.set(
					LocationSelectionFragment.KEY_FRAGMENT_RESULT_LOCATION_DATA_AS_JSON,
					""
				)

				viewModel.onLocationSelected(this, it.fromJson())
			}
		}
	}
	
	fun pickImageOrRequestPermissions() {
		when {
			requireContext().checkSelfPermissionGranted(Manifest.permission.READ_EXTERNAL_STORAGE)
				&& requireContext().checkSelfPermissionGranted(Manifest.permission.WRITE_EXTERNAL_STORAGE)
				&& requireContext().checkSelfPermissionGranted(Manifest.permission.CAMERA) -> {
				pickImageViaChooser()
			}
			requireContext().checkSelfPermissionGranted(Manifest.permission.READ_EXTERNAL_STORAGE)
				&& requireContext().checkSelfPermissionGranted(Manifest.permission.WRITE_EXTERNAL_STORAGE) -> {
				pickImage(false)
			}
			requireContext().checkSelfPermissionGranted(Manifest.permission.CAMERA) -> {
				pickImage(true)
			}
			else -> {
				permissionLocationRequest.launch(arrayOf(
					Manifest.permission.READ_EXTERNAL_STORAGE,
					Manifest.permission.WRITE_EXTERNAL_STORAGE,
					Manifest.permission.CAMERA,
				))
			}
		}
	}
	
	private fun pickImage(fromCamera: Boolean) {
		if (fromCamera) {
			activityResultImageCamera.launch(Intent(MediaStore.ACTION_IMAGE_CAPTURE))
		}else {
			// From gallery
			activityResultImageGallery.launch(Intent(Intent.ACTION_PICK, MediaStore.Images.Media.EXTERNAL_CONTENT_URI))
		}
	}
	
	private fun pickImageViaChooser() {
		val camera = getString(R.string.camera)
		val gallery = getString(R.string.gallery)
		
		binding?.personalDataImageMaterialCardView?.showPopup(listOf(camera, gallery)) {
			pickImage(it.title?.toString() == camera)
		}
	}
	
	private fun getUriFromBitmapRetrievedByCamera(bitmap: Bitmap): Uri {
		val stream = ByteArrayOutputStream()
		bitmap.compress(Bitmap.CompressFormat.JPEG, 70, stream)
		val byteArray = stream.toByteArray()
		val compressedBitmap = BitmapFactory.decodeByteArray(byteArray, 0, byteArray.size)
		
		val path = MediaStore.Images.Media.insertImage(
			requireContext().contentResolver, compressedBitmap, Date(System.currentTimeMillis()).toString() + "photo", null
		)
		return Uri.parse(path)
	}
	
}
