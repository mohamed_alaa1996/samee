package app.grand.tafwak.presentation.project.adapters.viewHolders

import android.net.Uri
import android.view.ViewGroup
import androidx.appcompat.content.res.AppCompatResources
import androidx.core.view.isVisible
import androidx.navigation.NavDeepLinkRequest
import androidx.navigation.findNavController
import androidx.recyclerview.widget.RecyclerView
import app.grand.tafwak.core.extensions.inflateLayout
import app.grand.tafwak.domain.providerFlow.ResponseChatMessage
import app.grand.tafwak.domain.providerFlow.ResponseConversation
import app.grand.tafwak.presentation.project.ConversationsFragmentDirections
import com.bumptech.glide.Glide
import com.bumptech.glide.request.RequestOptions
import com.structure.base_mvvm.R
import com.structure.base_mvvm.databinding.ItemChatMsgOtherBinding
import com.structure.base_mvvm.databinding.ItemChatMsgUserBinding
import com.structure.base_mvvm.databinding.ItemConversationBinding

class VHItemChatDetails(parent: ViewGroup, private val isMyMsg: Boolean) : RecyclerView.ViewHolder(
	parent.context.inflateLayout(
		if (isMyMsg) R.layout.item_chat_msg_user else R.layout.item_chat_msg_other,
		parent
	)
) {
	
	private val userBinding = if (isMyMsg) ItemChatMsgUserBinding.bind(itemView) else null
	private val otherBinding = if (!isMyMsg) ItemChatMsgOtherBinding.bind(itemView) else null
	
	private val constraintLayout get() = userBinding?.constraintLayout ?: otherBinding!!.constraintLayout
	private val msgTextView get() = userBinding?.msgTextView ?: otherBinding!!.msgTextView
	private val imageView get() = userBinding?.imageView ?: otherBinding!!.imageView
	private val videoIndicatorImageView get() = userBinding?.videoIndicatorImageView ?: otherBinding!!.videoIndicatorImageView
	private val timeTextView get() = userBinding?.timeTextView ?: otherBinding!!.timeTextView
	
	init {
		imageView.setOnClickListener {
			val url = constraintLayout.tag as? String ?: return@setOnClickListener
			
			val isImage = msgTextView.tag as? Boolean ?: return@setOnClickListener
			
			if (isImage) {
				val uri = Uri.Builder()
					.scheme("dialog-dest")
					.authority("app.grand.tafwak.global.showing.image.dialog")
					.appendPath(url)
					.build()
				val request = NavDeepLinkRequest.Builder.fromUri(uri).build()
				
				it.findNavController().navigate(request)
			}else {
				val uri = Uri.Builder()
					.scheme("dialog-dest")
					.authority("app.grand.tafwak.global.video.player.dialog")
					.appendPath(url)
					.build()
				val request = NavDeepLinkRequest.Builder.fromUri(uri).build()
				
				it.findNavController().navigate(request)
			}
		}
	}
	
	fun bind(item: ResponseChatMessage) {
		constraintLayout.tag = item.media?.mediaUrl
		msgTextView.tag = item.isImage
		
		msgTextView.text = item.message.orEmpty()
		msgTextView.isVisible = !item.message.isNullOrEmpty()
		
		imageView.isVisible = item.media?.mediaUrl != null
		videoIndicatorImageView.isVisible = item.media?.mediaUrl != null && !item.isImage
		if (item.media?.mediaUrl != null) {
			val options = RequestOptions().let {
				if (item.isImage) it else it.frame(1)
			}.centerCrop()
			Glide.with(imageView.context)
				.load(item.media?.mediaUrl)
				.apply(options)
				.placeholder(R.drawable.ic_logo_samee_placeholder)
				.error(R.drawable.ic_logo_samee_placeholder)
				.into(imageView)
		}else {
			imageView.setImageDrawable(null)
		}
		
		timeTextView.text = item.createdAt
		timeTextView.setCompoundDrawablesRelativeWithIntrinsicBounds(
			if (isMyMsg) AppCompatResources.getDrawable(imageView.context, R.drawable.ic_done_white_2) else null,
			null,
			null,
			null,
		)
	}
	
}
