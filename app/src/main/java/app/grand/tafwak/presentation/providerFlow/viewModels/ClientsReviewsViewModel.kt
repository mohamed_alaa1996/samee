package app.grand.tafwak.presentation.providerFlow.viewModels

import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel
import app.grand.tafwak.core.customTypes.RetryAbleFlow
import app.grand.tafwak.core.extensions.mapNullable
import app.grand.tafwak.data.provider.repository.ProviderRepositoryImpl
import app.grand.tafwak.data.user.repository.UserRepositoryImpl
import app.grand.tafwak.domain.utils.toSuccessOrNull
import app.grand.tafwak.presentation.providerFlow.adapters.RVItemReview
import app.grand.tafwak.presentation.userFlow.adapters.RVWalletItem
import dagger.hilt.android.lifecycle.HiltViewModel
import javax.inject.Inject
import kotlin.math.roundToInt

@HiltViewModel
class ClientsReviewsViewModel @Inject constructor(
	repository: ProviderRepositoryImpl
) : ViewModel() {

	val response = RetryAbleFlow(repository::getReviews)
	
	val reviews = repository.getReviewsItems()
	
	val ratingText = MutableLiveData("")
	val ratingProgress = MutableLiveData(0)
	
	val adapter = RVItemReview()
	
}
