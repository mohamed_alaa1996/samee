package app.grand.tafwak.presentation.userFlow

import android.Manifest
import android.annotation.SuppressLint
import android.content.Context
import android.content.pm.PackageManager
import android.graphics.Bitmap
import android.location.Location
import android.os.Bundle
import android.view.View
import androidx.activity.result.contract.ActivityResultContracts
import androidx.annotation.RequiresPermission
import androidx.core.content.ContextCompat
import androidx.fragment.app.activityViewModels
import androidx.fragment.app.viewModels
import androidx.lifecycle.viewModelScope
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.RecyclerView
import app.grand.tafwak.core.customTypes.LocationHandler
import app.grand.tafwak.core.extensions.showErrorToast
import app.grand.tafwak.core.extensions.showNormalToast
import app.grand.tafwak.domain.user.entity.model.ProviderMiniDetails
import app.grand.tafwak.presentation.base.MABaseFragment
import app.grand.tafwak.presentation.project.viewModel.ProjectViewModel
import app.grand.tafwak.presentation.userFlow.viewModel.SelectProviderOnMapViewModel
import com.google.android.gms.location.FusedLocationProviderClient
import com.google.android.gms.location.LocationRequest
import com.google.android.gms.location.LocationServices
import com.google.android.gms.maps.CameraUpdateFactory
import com.google.android.gms.maps.GoogleMap
import com.google.android.gms.maps.OnMapReadyCallback
import com.google.android.gms.maps.SupportMapFragment
import com.google.android.gms.maps.model.BitmapDescriptorFactory
import com.google.android.gms.maps.model.LatLng
import com.google.android.gms.maps.model.MarkerOptions
import com.google.android.gms.tasks.CancellationToken
import com.google.android.gms.tasks.OnTokenCanceledListener
import com.structure.base_mvvm.R
import com.structure.base_mvvm.databinding.FragmentSelectProviderOnMapBinding
import dagger.hilt.android.AndroidEntryPoint
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.delay
import kotlinx.coroutines.launch
import kotlinx.coroutines.withContext
import timber.log.Timber
import java.lang.Exception
import kotlin.math.min
import kotlin.math.roundToInt

@AndroidEntryPoint
class SelectProviderOnMapFragment : MABaseFragment<FragmentSelectProviderOnMapBinding>(),
	OnMapReadyCallback, LocationHandler.Listener {
	
	private val viewModel by viewModels<SelectProviderOnMapViewModel>()
	
	lateinit var locationHandler: LocationHandler
		private set
	
	var googleMap: GoogleMap? = null
		private set
	
	/** Zoom levels https://developers.google.com/maps/documentation/android-sdk/views#zoom */
	val zoom get() = min(googleMap?.maxZoomLevel ?: 5f, 15f)
	
	override fun onCreate(savedInstanceState: Bundle?) {
		locationHandler = LocationHandler(
			this,
			lifecycle,
			requireContext(),
			this
		)

		super.onCreate(savedInstanceState)
	}
	
	override fun getLayoutId(): Int = R.layout.fragment_select_provider_on_map
	
	override fun initializeBindingVariables() {
		binding?.viewModel = viewModel
	}
	
	override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
		// Setup map
		(childFragmentManager.findFragmentById(R.id.mapFragmentContainerView) as? SupportMapFragment)
			?.getMapAsync(this)
		
		binding?.recyclerView?.layoutManager = object : LinearLayoutManager(requireContext(), HORIZONTAL, false) {
			override fun checkLayoutParams(layoutParams: RecyclerView.LayoutParams?): Boolean {
				if (layoutParams != null) {
					layoutParams.width = (width.toFloat() * 0.9f).roundToInt()
				}
				
				return super.checkLayoutParams(layoutParams)
			}
		}
		binding?.recyclerView?.adapter = viewModel.adapter
	}
	
	override fun onMapReady(googleMap: GoogleMap) {
		this.googleMap = googleMap
		
		if (viewModel.args.latitude == null || viewModel.args.longitude == null) {
			locationHandler.requestCurrentLocation(true)
		}
		
		val location = LatLng(
			viewModel.args.latitude?.toDouble() ?: 0.0,
			viewModel.args.longitude?.toDouble() ?: 0.0
		)
		
		googleMap.moveCamera(CameraUpdateFactory.newLatLngZoom(location, zoom))
		
		viewModel.viewModelScope.launch(Dispatchers.IO) {
			try {
				viewModel.loadBitmaps()
				
				withContext(Dispatchers.Main) {
					for ((details, bitmap) in viewModel.bitmaps) {
						drawMarker(details, bitmap)
					}
					
					delay(300)
				}
			}catch (throwable: Throwable) {
				Timber.e("error loading bitmaps $throwable")
			}
		}
	}

	override fun onCurrentLocationResultSuccess(location: Location) {
		val latLng = LatLng(location.latitude, location.longitude)

		viewModel.myCurrentLocation = latLng

		googleMap?.moveCamera(CameraUpdateFactory.newLatLngZoom(latLng, zoom))
	}
	
	private fun drawMarker(details: ProviderMiniDetails, bitmap: Bitmap) {
		val googleMap = googleMap ?: return
		
		googleMap.addMarker(
			MarkerOptions()
				.position(LatLng(details.latitude, details.longitude))
				.icon(BitmapDescriptorFactory.fromBitmap(bitmap))
				.title(details.name)
		)
	}
	
}