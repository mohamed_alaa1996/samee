package app.grand.tafwak.presentation.userFlow.adapters.viewHolders

import android.net.Uri
import android.view.ViewGroup
import androidx.core.view.isVisible
import androidx.navigation.NavDeepLinkRequest
import androidx.navigation.findNavController
import androidx.recyclerview.widget.RecyclerView
import app.grand.tafwak.core.extensions.inflateLayout
import app.grand.tafwak.domain.user.entity.model.FileType
import app.grand.tafwak.domain.user.entity.model.PreviousWork
import app.grand.tafwak.domain.userFlow.response.Working
import app.grand.tafwak.presentation.userFlow.adapters.RVItemImageRoundedInCard2
import com.bumptech.glide.Glide
import com.bumptech.glide.request.RequestOptions
import com.structure.base_mvvm.R
import com.structure.base_mvvm.databinding.ItemImageRoundedInCard2Binding

class VHItemImageRoundedInCard2(parent: ViewGroup, private val listener: RVItemImageRoundedInCard2.Listener, private val enableDeletion: Boolean) : RecyclerView.ViewHolder(
	parent.context.inflateLayout(R.layout.item_image_rounded_in_card_2, parent)
) {
	
	private val binding = ItemImageRoundedInCard2Binding.bind(itemView)
	
	init {
		binding.materialCardView.setOnClickListener {
			val isImage = binding.materialCardView.tag as? Boolean ?: return@setOnClickListener
			val url = binding.frameLayout.tag as? String ?: return@setOnClickListener
			
			if (isImage) {
				val uri = Uri.Builder()
					.scheme("dialog-dest")
					.authority("app.grand.tafwak.global.showing.image.dialog")
					.appendPath(url)
					.build()
				val request = NavDeepLinkRequest.Builder.fromUri(uri).build()
				
				it.findNavController().navigate(request)
			}else {
				val uri = Uri.Builder()
					.scheme("dialog-dest")
					.authority("app.grand.tafwak.global.video.player.dialog")
					.appendPath(url)
					.build()
				val request = NavDeepLinkRequest.Builder.fromUri(uri).build()
				
				it.findNavController().navigate(request)
			}
		}
		
		binding.deleteMaterialCardView.setOnClickListener {
			val id = binding.deleteMaterialCardView.tag as? Int ?: return@setOnClickListener
			
			listener.onDeleteClick(it, id)
		}
	}
	
	fun bind(working: Working) {
		binding.deleteMaterialCardView.tag = working.id
		
		val isImage = working.fileType == FileType.IMAGE.apiKey
		binding.materialCardView.tag = isImage
		binding.frameLayout.tag = working.imageOrVideoUrl
		
		val options = RequestOptions().let {
			if (isImage) it else it.frame(1)
		}.centerCrop()
		Glide.with(binding.imageView.context)
			.load(working.imageOrVideoUrl)
			.apply(options)
			.placeholder(R.drawable.ic_logo_samee_placeholder)
			.error(R.drawable.ic_logo_samee_placeholder)
			.into(binding.imageView)
		
		binding.videoIndicatorImageView.isVisible = !isImage
		
		binding.deleteMaterialCardView.isVisible = enableDeletion
	}
	
}
