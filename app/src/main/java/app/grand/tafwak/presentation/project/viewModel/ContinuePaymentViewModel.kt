package app.grand.tafwak.presentation.project.viewModel

import android.view.View
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel
import androidx.lifecycle.lifecycleScope
import app.grand.tafwak.core.extensions.showErrorToast
import app.grand.tafwak.data.provider.repository.ProviderRepositoryImpl
import app.grand.tafwak.data.user.repository.UserRepositoryImpl
import app.grand.tafwak.domain.user.entity.model.PaymentMethod
import app.grand.tafwak.domain.user.entity.model.PaymentMethod.*
import app.grand.tafwak.domain.utils.MAResult
import app.grand.tafwak.presentation.auth2.confirm.ConfirmPhoneFragmentArgs
import app.grand.tafwak.presentation.project.ContinuePaymentFragmentArgs
import app.grand.tafwak.presentation.project.NotificationsFragment
import app.grand.tafwak.presentation.project.adapters.RVItemNotification
import com.structure.base_mvvm.R
import dagger.hilt.android.lifecycle.HiltViewModel
import kotlinx.coroutines.launch
import javax.inject.Inject

@HiltViewModel
class ContinuePaymentViewModel @Inject constructor(
	repository: ProviderRepositoryImpl,
	private val userRepository: UserRepositoryImpl,
	args: ContinuePaymentFragmentArgs
) : ViewModel() {
	
	val paymentMethod = MutableLiveData(CASH)
	
	fun onConfirmClick(view: View) {
		when (paymentMethod.value) {
			CASH -> {
				// todo no api endpoint to select payment method isa ?!
			}
			VISA -> {
				// todo payment screen
			}
			PAY_PAL -> {
				// todo payment screen
			}
			AMAZON_PAY -> {
				// todo payment screen
			}
			null -> {}
		}
	}
	
	fun onCashClick() {
		paymentMethod.value = CASH
	}
	
	fun onVisaClick() {
		paymentMethod.value = VISA
	}
	
	fun onPayPalClick() {
		paymentMethod.value = PAY_PAL
	}
	
	fun onAmazonPayClick() {
		paymentMethod.value = AMAZON_PAY
	}
	
}
