package app.grand.tafwak.presentation.userFlow

import android.os.Bundle
import android.view.View
import androidx.fragment.app.activityViewModels
import androidx.fragment.app.viewModels
import androidx.lifecycle.Lifecycle
import androidx.lifecycle.lifecycleScope
import androidx.lifecycle.repeatOnLifecycle
import androidx.lifecycle.viewModelScope
import androidx.recyclerview.widget.LinearLayoutManager
import app.grand.tafwak.core.customTypes.MADividerItemDecoration
import app.grand.tafwak.core.extensions.dpToPx
import app.grand.tafwak.core.extensions.withCustomAdapters
import app.grand.tafwak.data.local.preferences.PrefsProvider
import app.grand.tafwak.presentation.base.MABaseFragment
import app.grand.tafwak.presentation.project.viewModel.ProjectViewModel
import app.grand.tafwak.presentation.userFlow.viewModel.SearchQueryViewModel
import app.grand.tafwak.presentation.userHome.adapters.LSAdapterLoadingErrorEmpty
import com.structure.base_mvvm.R
import com.structure.base_mvvm.databinding.FragmentSearchQueryBinding
import com.structure.base_mvvm.databinding.FragmentUserBinding
import dagger.hilt.android.AndroidEntryPoint
import kotlinx.coroutines.flow.collectLatest
import kotlinx.coroutines.flow.first
import kotlinx.coroutines.launch
import javax.inject.Inject
import kotlin.math.roundToInt

@AndroidEntryPoint
class SearchQueryFragment : MABaseFragment<FragmentSearchQueryBinding>()  {
	
	private val viewModel by viewModels<SearchQueryViewModel>()
	
	@Inject
	lateinit var prefsProvider: PrefsProvider

	override fun getLayoutId(): Int = R.layout.fragment_search_query
	
	override fun initializeBindingVariables() {
		binding?.viewModel = viewModel
	}
	
	override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
		binding?.recyclerView?.addItemDecoration(
			MADividerItemDecoration(
				requireContext().dpToPx(1f).roundToInt(),
				requireContext().getColor(R.color.divider_item_decor)
			)
		)
		binding?.recyclerView?.layoutManager = LinearLayoutManager(requireContext(), LinearLayoutManager.VERTICAL, false)
		val headerAdapterCurrent2 = LSAdapterLoadingErrorEmpty(viewModel.adapter, false)
		val footerAdapterFinished2 = LSAdapterLoadingErrorEmpty(viewModel.adapter, true)
		binding?.recyclerView?.adapter = viewModel.adapter.withCustomAdapters(
			headerAdapterCurrent2,
			footerAdapterFinished2
		)
		
		viewLifecycleOwner.lifecycleScope.launch {
			viewLifecycleOwner.repeatOnLifecycle(Lifecycle.State.STARTED) {
				viewModel.categories.collectLatest { pagingData ->
					viewModel.adapter.submitData(pagingData)
				}
			}
		}
	}
	
}
