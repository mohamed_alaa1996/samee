package app.grand.tafwak.presentation.userFlow

import android.os.Bundle
import android.view.Gravity
import android.view.View
import android.view.Window
import androidx.fragment.app.activityViewModels
import androidx.fragment.app.viewModels
import app.grand.tafwak.presentation.base.MADialogFragment
import app.grand.tafwak.presentation.base.extensions.getMyDrawable
import app.grand.tafwak.presentation.project.viewModel.ProjectViewModel
import app.grand.tafwak.presentation.userFlow.viewModel.CancelOrderViewModel
import app.grand.tafwak.presentation.userFlow.viewModel.FilterPickerViewModel
import com.structure.base_mvvm.R
import com.structure.base_mvvm.databinding.DialogFragmentCancelOrderBinding
import com.structure.base_mvvm.databinding.DialogFragmentFilterPickerBinding
import dagger.hilt.android.AndroidEntryPoint

@AndroidEntryPoint
class CancelOrderDialogFragment : MADialogFragment<DialogFragmentCancelOrderBinding>() {
	
	private val viewModel by viewModels<CancelOrderViewModel>()
	
	override fun getLayoutId(): Int = R.layout.dialog_fragment_cancel_order
	
	override val windowGravity: Int = Gravity.BOTTOM
	
	override fun onCreateDialogWindowChanges(window: Window) {
		window.setBackgroundDrawable(getMyDrawable(R.drawable.dr_top_round_white))
		
		window.attributes?.windowAnimations = R.style.ScaleDialogAnim
	}
	
	override fun initializeBindingVariables() {
		binding.viewModel = viewModel
	}
	
}
