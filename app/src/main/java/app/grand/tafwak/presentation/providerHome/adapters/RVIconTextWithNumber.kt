package app.grand.tafwak.presentation.providerHome.adapters

import android.view.View
import android.view.ViewGroup
import androidx.recyclerview.widget.DiffUtil
import androidx.recyclerview.widget.ListAdapter
import app.grand.tafwak.domain.base.entity.IconText
import app.grand.tafwak.domain.base.entity.IconTextWithNumber
import app.grand.tafwak.presentation.providerHome.adapters.viewHolders.VHIconTextWithNumber
import app.grand.tafwak.presentation.userHome.adapters.viewHolders.VHIconText

class RVIconTextWithNumber(private val listener: Listener) : ListAdapter<IconTextWithNumber, VHIconTextWithNumber>(COMPARATOR) {
	
	companion object {
		val COMPARATOR = object : DiffUtil.ItemCallback<IconTextWithNumber>() {
			override fun areItemsTheSame(
				oldItem: IconTextWithNumber,
				newItem: IconTextWithNumber
			): Boolean = oldItem.text == newItem.text
			
			override fun areContentsTheSame(
				oldItem: IconTextWithNumber,
				newItem: IconTextWithNumber
			): Boolean = oldItem == newItem
		}
	}
	
	override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): VHIconTextWithNumber {
		return VHIconTextWithNumber(parent, listener)
	}
	
	override fun onBindViewHolder(holder: VHIconTextWithNumber, position: Int) {
		holder.bind(getItem(position))
	}
	
	interface Listener {
		
		fun onItemIconTextClick(view: View, text: String)
		
	}
	
}
