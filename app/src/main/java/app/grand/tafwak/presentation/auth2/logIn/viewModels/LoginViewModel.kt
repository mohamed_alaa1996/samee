package app.grand.tafwak.presentation.auth2.logIn.viewModels

import android.content.Intent
import android.net.Uri
import android.provider.Contacts
import android.provider.Settings
import android.view.View
import androidx.activity.result.ActivityResultRegistryOwner
import androidx.fragment.app.Fragment
import androidx.fragment.app.FragmentManager
import androidx.fragment.app.findFragment
import androidx.lifecycle.*
import androidx.navigation.NavDeepLinkRequest
import androidx.navigation.NavOptions
import androidx.navigation.findNavController
import androidx.navigation.fragment.findNavController
import app.grand.tafwak.core.customTypes.RetryAbleFlow
import app.grand.tafwak.core.customTypes.isPhoneValid
import app.grand.tafwak.core.extensions.*
import app.grand.tafwak.core.extensions.map
import app.grand.tafwak.data.auth.repository.MAAuthRepositoryImpl
import app.grand.tafwak.data.auth.repository.TwitterRepositoryImpl
import app.grand.tafwak.data.local.preferences.PrefsUser
import app.grand.tafwak.domain.splash.entity.SplashInitialLaunch
import app.grand.tafwak.domain.utils.MAResult
import app.grand.tafwak.presentation.auth2.logIn.LoginFragment
import app.grand.tafwak.presentation.auth2.logIn.LoginFragmentDirections
import app.grand.tafwak.presentation.base.customTypes.DataOfInfoImageAndListOfTexts
import app.grand.tafwak.presentation.base.extensions.executeOnGlobalLoadingAndAutoHandleRetryCancellable
import app.grand.tafwak.presentation.project.ProjectActivity
import app.grand.tafwak.presentation.userHome.UserFragmentArgs
import com.facebook.*
import com.facebook.FacebookSdk.getApplicationSignature
import com.facebook.login.LoginBehavior
import com.facebook.login.LoginManager
import com.facebook.login.LoginResult
import com.google.android.gms.auth.api.signin.GoogleSignIn
import com.google.android.gms.auth.api.signin.GoogleSignInOptions
import com.structure.base_mvvm.R
import dagger.hilt.android.lifecycle.HiltViewModel
import kotlinx.coroutines.delay
import kotlinx.coroutines.launch
import timber.log.Timber
import javax.inject.Inject

@HiltViewModel
class LoginViewModel @Inject constructor(
	private val repository: MAAuthRepositoryImpl,
	private val twitterRepository: TwitterRepositoryImpl,
	val prefsUser: PrefsUser,
) : ViewModel() {

	var referralCode: String? = null

	var isGuest = false
	
	val showUser = MutableLiveData(true)
	
	val phone = MutableLiveData("")
	
	val errorPhone: MutableLiveData<String?> = phone.map { null }
	
	val termsAndConditionsIsAccepted = MutableLiveData(false)

	var socialId: String = ""

	fun onSkipClick(view: View) {
		val fragment = view.findFragment<LoginFragment>()

		viewModelScope.launch {
			isGuest = true

			fragment.activityViewModel?.globalLoading?.value = true

			fragment.locationHandler.performAccordingToGPSIsTurnedOnOrOffSuspended(
				offBlock = {
					fragment.activityViewModel?.globalLoading?.value = false

					view.findNavController().navigate(LoginFragmentDirections.actionDestLogInToDestLocationUserRegister(""))
				},
				onBlock = {
					fragment.locationHandler.requestCurrentLocation(true)
				}
			)
		}
	}
	
	fun toggleLanguage(view: View) {
		view.context.setProjectCurrentLocaleByToggling()
		
		val activity = FragmentManager.findFragment<Fragment>(view).requireActivity()
		
		activity.finishAffinity()
		activity.startActivity(Intent(activity, ProjectActivity::class.java))
	}
	
	fun toggleShowUser() {
		val value = showUser.value ?: false
		showUser.value = !value
	}
	
	fun onLoginClick(view: View) {
		if (termsAndConditionsIsAccepted.value != true) {
			return view.context.showErrorToast(
				view.context.getString(R.string.you_must_agree_terms_and_conditions)
			)
		}

		val res = phone.value.isPhoneValid()
		if (res != null) {
			errorPhone.value = view.context.getString(res)
		}else {
			val fragment = view.findFragment<LoginFragment>()

			fragment.executeOnGlobalLoadingAndAutoHandleRetryCancellable(
				afterShowingLoading = {
					Timber.v("Referral Code -> $referralCode")

					if (showUser.value == true) {
						repository.registerUserSendPhone2(phone.value.orEmpty(), referralCode)
					}else {
						repository.registerProviderSendPhone2(phone.value.orEmpty(), referralCode)
					}
				},
				afterHidingLoading = {
					fragment.findNavController().navigate(
						LoginFragmentDirections.actionDestLogInToDestConfirmPhone(
							phone.value.orEmpty(),
							showUser.value ?: false
						)
					)
				}
			)
		}
	}
	
	fun toggleAcceptingTermsAndConditions() {
		val value = termsAndConditionsIsAccepted.value ?: false
		termsAndConditionsIsAccepted.value = !value
	}
	
	fun showTermsAndConditions(view: View) {
		val data = if (showUser.value == true) {
			DataOfInfoImageAndListOfTexts.USER_TERMS_AND_CONDITIONS
		}else {
			DataOfInfoImageAndListOfTexts.PROVIDER_TERMS_AND_CONDITIONS
		}
		val uri = Uri.Builder()
			.scheme("fragment-dest")
			.authority("app.grand.tafwak.global.info.app")
			.appendPath(data.name)
			.build()
		val request = NavDeepLinkRequest.Builder.fromUri(uri).build()
		val options = NavOptions.Builder()
			.setEnterAnim(R.anim.anim_slide_in_right)
			.setExitAnim(R.anim.anim_slide_out_left)
			.setPopEnterAnim(R.anim.anim_slide_in_left)
			.setPopExitAnim(R.anim.anim_slide_out_right)
			.build()
		view.findNavController().navigate(request, options)
	}
	
	fun onTwitterClick(view: View) {
		if (termsAndConditionsIsAccepted.value != true) {
			return view.context.showErrorToast(
				view.context.getString(R.string.you_must_agree_terms_and_conditions)
			)
		}

		val fragment = view.findFragment<LoginFragment>()
		fragment.lifecycleScope.launch {
			fragment.activityViewModel?.globalLoading?.value = true
			
			val result = twitterRepository.obtainRequestToken()
			
			Timber.e("result $result")
			
			if (result != null && result.oauthCallbackConfirmed) {
				fragment.activityViewModel?.globalLoading?.value = false
				
				delay(150)
				
				val link = "https://api.twitter.com/oauth/authenticate?oauth_token=${result.oauthToken}"
				
				fragment.requireContext().launchBrowser(link)
			}else {
				fragment.activityViewModel?.globalLoading?.value = false
				
				fragment.requireContext().showErrorToast(fragment.getString(R.string.something_went_wrong))
			}
		}
	}
	
	fun onFacebookClick(view: View) {
		val registryOwner = view.findFragment<LoginFragment>().activity
			as? ActivityResultRegistryOwner ?: return

		if (termsAndConditionsIsAccepted.value != true) {
			return view.context.showErrorToast(
				view.context.getString(R.string.you_must_agree_terms_and_conditions)
			)
		}

		/*val accessToken = AccessToken.getCurrentAccessToken()
		val isLoggedIn = accessToken != null && !accessToken.isExpired
		Timber.e("dewjodj isLoggedIn $isLoggedIn")
		
		if (isLoggedIn) {
			// so actual log in -> LoginManager.getInstance().logInWithReadPermissions(this, Arrays.asList("public_profile"));
			return
		}
		*/
		// todo keeps getting error of not being published to live on facebook console which requires app review,
		//  and if need in debug mode u need to add a tester in Roles in console dunno why isa ?!
		val callbackManager = CallbackManager.Factory.create()
		
		val loginManager = LoginManager.getInstance()
		
		loginManager.setLoginBehavior(LoginBehavior.NATIVE_WITH_FALLBACK)
		loginManager.logIn(
			registryOwner,
			callbackManager,
			listOf("email", "public_profile"/*, "user_friends"*/),
		)
		
		loginManager.registerCallback(callbackManager, object : FacebookCallback<LoginResult> {
			override fun onSuccess(result: LoginResult) {
				Timber.e("hsiuaha facebook id ${result.accessToken.userId}, accessToken -> ${result.accessToken}")
				
				performSocialLoginWithApi(view.findFragment(), result.accessToken.userId)
			}
			
			override fun onError(error: FacebookException) {
				Timber.e("hsiuaha Facebook Exception 1 $error")
				Timber.e("hsiuaha Facebook Exception 1.5 ${error.message}")
				Timber.e("hsiuaha Facebook Exception 1.3 ${error.localizedMessage}")
				Timber.e("hsiuaha Facebook Exception 1.2 ${error.suppressed.toList()}")
				Timber.e("hsiuaha Facebook Exception 1.0 ${error.stackTrace.toList()}")
				Timber.e("hsiuaha Facebook Exception 1.1 ${error.cause}")
				view.context.showErrorToast(view.context.getString(R.string.something_went_wrong))
			}
			
			override fun onCancel() {
				Timber.e("hsiuaha facebook onCancel 0")
				view.context.showErrorToast(view.context.getString(R.string.something_went_wrong))
			}
		})
	}
	
	fun onGoogleClick(view: View) {
		if (termsAndConditionsIsAccepted.value != true) {
			return view.context.showErrorToast(
				view.context.getString(R.string.you_must_agree_terms_and_conditions)
			)
		}

		val fragment = view.findFragment<LoginFragment>()

		val options = GoogleSignInOptions.Builder(GoogleSignInOptions.DEFAULT_SIGN_IN)
			.requestId()
			.build()
		val client = GoogleSignIn.getClient(view.context, options)
		
		//val account = GoogleSignIn.getLastSignedInAccount(view.context)
		
		fragment.activityResultGoogleSignIn.launch(client.signInIntent)
	}
	
	fun handleTwitterCallbackSuccess(fragment: LoginFragment, oauthToken: String, oauthVerifier: String) {
		fragment.lifecycleScope.launch {
			fragment.activityViewModel?.globalLoading?.value = true
			
			val result = twitterRepository.obtainAccessToken(oauthToken, oauthVerifier)
			
			Timber.e("result $result")
			
			if (result != null) {
				fragment.activityViewModel?.globalLoading?.value = false

				val socialId = result.userId
				
				Timber.e("socialId $socialId")
				
				performSocialLoginWithApi(fragment, socialId)
			}else {
				fragment.activityViewModel?.globalLoading?.value = false
				
				fragment.requireContext().showErrorToast(fragment.getString(R.string.something_went_wrong))
			}
		}
	}
	
	fun onPhoneSuccessForSocialRegistration(fragment: LoginFragment, phone: String) {
		fragment.lifecycleScope.launch {
			fragment.activityViewModel?.globalLoading?.value = true
			
			when (val result = repository.socialRegister(socialId, phone)) {
				is MAResult.Failure -> {
					fragment.activityViewModel?.globalLoading?.value = false
					
					fragment.requireContext().showErrorToast(
						result.getMessageNotEmptyNorNullOr(fragment.getString(R.string.something_went_wrong))
					)
				}
				is MAResult.Success -> {
					fragment.activityViewModel?.globalLoading?.value = false
					
					delay(150)
					
					fragment.findNavController().navigate(
						LoginFragmentDirections.actionDestLogInToDestConfirmPhone(
							phone,
							true
						)
					)
				}
			}
		}
	}
	
	fun performSocialLoginWithApi(fragment: LoginFragment, socialId: String) {
		fragment.lifecycleScope.launch {
			delay(150)
			
			fragment.activityViewModel?.globalLoading?.value = true
			
			// Check if this social id is already logged in isa.
			when (val result = repository.socialLogin(socialId)) {
				is MAResult.Failure -> {
					this@LoginViewModel.socialId = socialId
					
					fragment.activityViewModel?.globalLoading?.value = false
					
					delay(150)
					
					fragment.findNavController().navigate(
						LoginFragmentDirections.actionDestLogInToDestFetchPhoneDialog()
					)
				}
				is MAResult.Success -> {
					val user = result.value.data!!
					
					prefsUser.setId(user.id.orZero())
					prefsUser.setPhone(user.phone.orEmpty())
					prefsUser.setApiToken(user.token.orEmpty())
					
					prefsUser.setIsUserNotServiceProvider(true)
					
					prefsUser.setName(user.name.orEmpty())
					prefsUser.setEMail(user.email.orEmpty())
					prefsUser.setImageUrl(user.imageUrl.orEmpty())

					isGuest = false

					fragment.locationHandler.performAccordingToGPSIsTurnedOnOrOffSuspended(
						offBlock = {
							fragment.activityViewModel?.globalLoading?.value = false

							fragment.findNavController().navigate(LoginFragmentDirections.actionDestLogInToDestLocationUserRegister(user.token.orEmpty()))
						},
						onBlock = {
							fragment.locationHandler.requestCurrentLocation(true)
						}
					)
				}
			}
		}
	}

	fun putReferralCode(view: View) {
		view.findNavController().navigate(
			LoginFragmentDirections.actionDestLogInToDestCheckReferralCodeDialog()
		)
	}
	
}