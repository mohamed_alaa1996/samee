package app.grand.tafwak.presentation.project.adapters

import android.view.ViewGroup
import androidx.paging.PagingDataAdapter
import androidx.recyclerview.widget.DiffUtil
import app.grand.tafwak.domain.providerFlow.ResponseConversation
import app.grand.tafwak.domain.providerFlow.ResponseNotification
import app.grand.tafwak.presentation.project.adapters.viewHolders.VHItemConversation
import app.grand.tafwak.presentation.project.adapters.viewHolders.VHItemNotification

class RVItemNotification : PagingDataAdapter<ResponseNotification, VHItemNotification>(COMPARATOR) {
	
	companion object {
		val COMPARATOR = object : DiffUtil.ItemCallback<ResponseNotification>() {
			override fun areItemsTheSame(
				oldItem: ResponseNotification,
				newItem: ResponseNotification
			): Boolean = oldItem.id == newItem.id
			
			override fun areContentsTheSame(
				oldItem: ResponseNotification,
				newItem: ResponseNotification
			): Boolean  = oldItem == newItem
		}
	}
	
	override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): VHItemNotification {
		return VHItemNotification(parent)
	}
	
	override fun onBindViewHolder(holder: VHItemNotification, position: Int) {
		holder.bind(getItem(position) ?: return)
	}
	
}
