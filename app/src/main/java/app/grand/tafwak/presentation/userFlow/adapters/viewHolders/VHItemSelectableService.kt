package app.grand.tafwak.presentation.userFlow.adapters.viewHolders

import android.net.Uri
import android.view.ViewGroup
import androidx.core.view.isVisible
import androidx.navigation.NavDeepLinkRequest
import androidx.navigation.findNavController
import androidx.recyclerview.widget.RecyclerView
import app.grand.tafwak.core.extensions.inflateLayout
import app.grand.tafwak.domain.user.entity.model.FileType
import app.grand.tafwak.domain.user.entity.model.PreviousWork
import app.grand.tafwak.domain.user.entity.model.ProviderService
import app.grand.tafwak.presentation.userFlow.adapters.RVItemSelectableService
import com.bumptech.glide.Glide
import com.bumptech.glide.request.RequestOptions
import com.structure.base_mvvm.R
import com.structure.base_mvvm.databinding.ItemImageRoundedInCardBinding
import com.structure.base_mvvm.databinding.ItemSelectableServiceBinding

class VHItemSelectableService(parent: ViewGroup, private val adapter: RVItemSelectableService) : RecyclerView.ViewHolder(
	parent.context.inflateLayout(R.layout.item_selectable_service, parent)
) {
	
	private val binding = ItemSelectableServiceBinding.bind(itemView)
	
	init {
		binding.materialCardView.setOnClickListener {
			val id = binding.materialCardView.tag as? Int ?: return@setOnClickListener
			val positionInAdapter = binding.checkImageView.tag as? Int ?: return@setOnClickListener
			
			adapter.toggleSelection(id, positionInAdapter)
		}
	}
	
	fun bind(details: ProviderService, isSelected: Boolean, positionInAdapter: Int) {
		binding.materialCardView.tag = details.id
		binding.checkImageView.tag = positionInAdapter
		
		binding.serviceNameTextView.text = details.name
		
		binding.priceTextView.text = binding.priceTextView.context.getString(R.string.num_sar, details.price)
		
		binding.checkImageView.setImageResource(
			if (isSelected) R.drawable.ic_checked else R.drawable.ic_not_checked
		)
		
		val isImage = details.fileType == FileType.IMAGE.name
		val options = RequestOptions().let {
			if (isImage) it else it.frame(1)
		}.centerCrop()
		Glide.with(binding.imageView.context)
			.load(details.imageOrVideoUrl)
			.apply(options)
			.placeholder(R.drawable.ic_logo_samee_placeholder)
			.error(R.drawable.ic_logo_samee_placeholder)
			.into(binding.imageView)
	}
	
}
