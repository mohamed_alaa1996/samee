package app.grand.tafwak.presentation.auth2

import android.os.Bundle
import android.view.View
import androidx.fragment.app.activityViewModels
import app.grand.tafwak.data.auth.repository.TwitterRepositoryImpl
import app.grand.tafwak.presentation.base.MABaseFragment
import app.grand.tafwak.presentation.project.viewModel.ProjectViewModel
import com.structure.base_mvvm.R
import com.structure.base_mvvm.databinding.FragmentTwitterCallbackBinding
import dagger.hilt.android.AndroidEntryPoint
import javax.inject.Inject

@AndroidEntryPoint
class TwitterCallbackFragment : MABaseFragment<FragmentTwitterCallbackBinding>() {
	
	//private val args by navArgs<TwitterCallbackFragmentArgs>()
	
	@Inject
	protected lateinit var twitterRepository: TwitterRepositoryImpl
	
	override fun getLayoutId(): Int = R.layout.fragment_twitter_callback
	
	override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
	}
	
}
