package app.grand.tafwak.presentation.base

import android.graphics.drawable.ColorDrawable
import android.os.Bundle
import android.view.View
import android.view.Window
import androidx.fragment.app.DialogFragment
import androidx.fragment.app.activityViewModels
import androidx.navigation.fragment.findNavController
import app.grand.tafwak.presentation.project.viewModel.ProjectViewModel
import com.structure.base_mvvm.R
import com.structure.base_mvvm.databinding.DialogFragmentLottieLoaderBinding
import dagger.hilt.android.AndroidEntryPoint
import timber.log.Timber

@AndroidEntryPoint
class LottieLoaderDialogFragment : MADialogFragment<DialogFragmentLottieLoaderBinding>() {
	
	override val heightIsMatchParent = true
	override val canceledOnTouchOutside = false
	
	override fun getLayoutId(): Int = R.layout.dialog_fragment_lottie_loader
	
	override fun onCreate(savedInstanceState: Bundle?) {
		super.onCreate(savedInstanceState)
		
		setStyle(STYLE_NO_FRAME, R.style.CustomDialogTheme2)
	}
	
	override fun onCreateDialogWindowChanges(window: Window) {
		window.setBackgroundDrawable(ColorDrawable(requireContext().getColor(android.R.color.transparent)))
		
		//window.attributes?.windowAnimations = R.style.ScaleDialogAnim
	}
	
	override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
		activityViewModel?.globalLoading?.observe(viewLifecycleOwner) {
			if (it == false) {
				findNavController().navigateUp()
			}
		}
	}

}
