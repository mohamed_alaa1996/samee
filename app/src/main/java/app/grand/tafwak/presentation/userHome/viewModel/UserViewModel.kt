package app.grand.tafwak.presentation.userHome.viewModel

import android.app.Application
import androidx.lifecycle.AndroidViewModel
import androidx.lifecycle.SavedStateHandle
import androidx.lifecycle.ViewModel
import app.grand.tafwak.core.customTypes.RetryAbleFlow
import app.grand.tafwak.core.extensions.app
import app.grand.tafwak.core.extensions.getDeviceIdWithoutPermission
import app.grand.tafwak.data.auth.repository.MAAuthRepositoryImpl
import app.grand.tafwak.domain.utils.MABaseResponse
import app.grand.tafwak.domain.utils.MAResult
import app.grand.tafwak.presentation.userHome.UserFragmentArgs
import dagger.hilt.android.lifecycle.HiltViewModel
import kotlinx.coroutines.flow.flowOf
import javax.inject.Inject

@HiltViewModel
class UserViewModel @Inject constructor(
	application: Application,
	repository: MAAuthRepositoryImpl,
	args: UserFragmentArgs
) : AndroidViewModel(application) {
	
	val isGuest = args.isGuest
	
	val retryAbleFlow = if (isGuest) {
		RetryAbleFlow {
			flowOf(MAResult.Success(MABaseResponse("", "", 0)))
		}
	}else {
		RetryAbleFlow {
			repository.getCurrentFirebaseTokenAsFlow(app.getDeviceIdWithoutPermission())
		}
	}
	
}
