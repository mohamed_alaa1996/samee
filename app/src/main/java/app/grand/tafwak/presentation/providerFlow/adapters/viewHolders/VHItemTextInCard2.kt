package app.grand.tafwak.presentation.providerFlow.adapters.viewHolders

import android.graphics.drawable.ColorDrawable
import android.view.ViewGroup
import androidx.appcompat.content.res.AppCompatResources
import androidx.recyclerview.widget.RecyclerView
import app.grand.tafwak.core.extensions.inflateLayout
import app.grand.tafwak.domain.user.entity.model.ServiceCategory
import app.grand.tafwak.domain.userFlow.response.CategoryWithWorkings
import app.grand.tafwak.presentation.providerFlow.adapters.RVItemTextInCard2
import app.grand.tafwak.presentation.userFlow.adapters.RVItemTextInCard
import com.structure.base_mvvm.R
import com.structure.base_mvvm.databinding.ItemTextInCardBinding

class VHItemTextInCard2(parent: ViewGroup, private val listener: RVItemTextInCard2.Listener) : RecyclerView.ViewHolder(
	parent.context.inflateLayout(R.layout.item_text_in_card, parent)
) {
	
	private val binding = ItemTextInCardBinding.bind(itemView)
	
	init {
		binding.materialCardView.setOnClickListener {
			val index = binding.materialCardView.tag as? Int ?: return@setOnClickListener
			
			listener.changeCategoryWithWorking(index)
		}
	}
	
	fun bind(item: ServiceCategory, isSelected: Boolean, index: Int) {
		binding.materialCardView.tag = index
		
		binding.textView.text = item.name
		
		if (isSelected) {
			binding.textView.background = AppCompatResources.getDrawable(binding.textView.context, R.drawable.dr_project_gradiect_rect)
			binding.textView.setTextColor(binding.textView.context.getColor(R.color.white))
		}else {
			binding.textView.background = ColorDrawable(binding.textView.context.getColor(R.color.white))
			binding.textView.setTextColor(binding.textView.context.getColor(R.color.black))
		}
	}
	
}
