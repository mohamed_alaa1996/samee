package app.grand.tafwak.presentation.userHome.adapters

import android.view.ViewGroup
import androidx.paging.PagingDataAdapter
import androidx.recyclerview.widget.DiffUtil
import androidx.recyclerview.widget.ListAdapter
import app.grand.tafwak.domain.user.entity.model.ServiceCategory
import app.grand.tafwak.presentation.userHome.adapters.viewHolders.VHItemHomeUserServices

class RVItemHomeUserServices : PagingDataAdapter<ServiceCategory, VHItemHomeUserServices>(COMPARATOR) {
	
	companion object {
		val COMPARATOR = object : DiffUtil.ItemCallback<ServiceCategory>() {
			override fun areItemsTheSame(
				oldItem: ServiceCategory,
				newItem: ServiceCategory
			): Boolean = oldItem.id == newItem.id
			
			override fun areContentsTheSame(
				oldItem: ServiceCategory,
				newItem: ServiceCategory
			): Boolean = oldItem == newItem
		}
	}
	
	override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): VHItemHomeUserServices {
		return VHItemHomeUserServices(parent)
	}
	
	override fun onBindViewHolder(holder: VHItemHomeUserServices, position: Int) {
		holder.bind(getItem(position) ?: return)
	}
}
