package app.grand.tafwak.presentation.userFlow.viewModel

import android.net.Uri
import android.view.View
import androidx.appcompat.widget.AppCompatRatingBar
import androidx.fragment.app.findFragment
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel
import androidx.lifecycle.lifecycleScope
import androidx.navigation.NavDeepLinkRequest
import androidx.navigation.fragment.findNavController
import app.grand.tafwak.core.extensions.roundHalfUp
import app.grand.tafwak.core.extensions.showErrorToast
import app.grand.tafwak.data.user.repository.UserRepositoryImpl
import app.grand.tafwak.domain.utils.MAResult
import app.grand.tafwak.presentation.userFlow.CancelOrderDialogFragment
import app.grand.tafwak.presentation.userFlow.CancelOrderDialogFragmentArgs
import app.grand.tafwak.presentation.userFlow.RateDialogFragment
import app.grand.tafwak.presentation.userFlow.RateDialogFragmentArgs
import com.structure.base_mvvm.R
import dagger.hilt.android.lifecycle.HiltViewModel
import kotlinx.coroutines.launch
import javax.inject.Inject

@HiltViewModel
class RateViewModel @Inject constructor(
	private val args: RateDialogFragmentArgs,
	private val repository: UserRepositoryImpl
) : ViewModel() {
	
	val comment = MutableLiveData("")
	
	fun rate(ratingBar: AppCompatRatingBar) {
		val rating = (ratingBar.progress.toFloat() / 20f).roundHalfUp(1)
		
		if (comment.value.isNullOrEmpty()) {
			return ratingBar.context.showErrorToast(ratingBar.context.getString(R.string.all_fields_required))
		}
		
		val fragment = ratingBar.findFragment<RateDialogFragment>()
		
		fragment.lifecycleScope.launch {
			fragment.activityViewModel?.globalLoading?.value = true
			
			val result = repository.addReview(
				args.orderId, args.reviewAbleId, rating, comment.value.orEmpty()
			)
			
			fragment.activityViewModel?.globalLoading?.value = false
			
			when (result) {
				is MAResult.Success -> {
					val navController = fragment.findNavController()
					
					val uri = Uri.Builder()
						.scheme("dialog-dest")
						.authority("app.grand.tafwak.global.back.to.user.home.dialog")
						.appendPath(fragment.getString(R.string.done_successfully))
						.build()
					val request = NavDeepLinkRequest.Builder.fromUri(uri).build()
					
					navController.navigate(request)
				}
				is MAResult.Failure -> {
					fragment.requireContext().showErrorToast(result.message ?: fragment.getString(R.string.something_went_wrong))
				}
			}
		}
	}
	
}
