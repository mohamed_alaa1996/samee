package app.grand.tafwak.presentation.providerFlow.viewModels

import android.app.Application
import android.net.Uri
import android.view.View
import androidx.fragment.app.findFragment
import androidx.lifecycle.AndroidViewModel
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel
import androidx.lifecycle.viewModelScope
import androidx.navigation.NavDeepLinkRequest
import androidx.navigation.NavOptions
import androidx.navigation.findNavController
import app.grand.tafwak.core.extensions.*
import app.grand.tafwak.data.auth.repository.MAAuthRepositoryImpl
import app.grand.tafwak.data.user.repository.UserRepositoryImpl
import app.grand.tafwak.domain.auth.entity.model.LocationData
import app.grand.tafwak.domain.utils.MAResult
import app.grand.tafwak.presentation.providerFlow.ProviderRegistrationFragment
import app.grand.tafwak.presentation.providerFlow.ProviderRegistrationFragmentDirections
import app.grand.tafwak.presentation.userFlow.PersonalDataFragment
import app.grand.tafwak.presentation.userFlow.adapters.RVItemCheckable
import com.google.gson.Gson
import com.structure.base_mvvm.R
import dagger.hilt.android.lifecycle.HiltViewModel
import kotlinx.coroutines.launch
import javax.inject.Inject

@HiltViewModel
class ProviderRegistrationViewModel @Inject constructor(
	application: Application,
	private val repository: MAAuthRepositoryImpl,
	userRepository: UserRepositoryImpl,
	private val gson: Gson,
) : AndroidViewModel(application), RVItemCheckable.Listener {
	
	/** 4 steps where 1 points to address, 2 -> personal data, 3 -> bank account, 4 -> services */
	val registrationStep = MutableLiveData(1)
	
	val personalDataName = MutableLiveData("")
	
	val personalDataEmail = MutableLiveData("")
	
	val personalDataMessage = MutableLiveData("")
	
	val bankAccountBankName = MutableLiveData("")
	val bankAccountAccountNumber = MutableLiveData("")
	val bankAccountClientName = MutableLiveData("")
	val bankAccountIBANNumber = MutableLiveData("")
	
	var imageUri: Uri? = null
	
	val categories = userRepository.getRegisterAsProviderCategories()
	
	val adapter = RVItemCheckable(this)
	
	private val selectedIds = mutableListOf<Int>()
	
	//region Step 1

	/** Step 1 */
	fun selectLocation(view: View) {
		val uri = Uri.Builder()
			.scheme("fragment-dest")
			.authority("app.grand.tafwak.global.location.selection.no.args")
			.build()
		val request = NavDeepLinkRequest.Builder.fromUri(uri).build()
		val options = NavOptions.Builder()
			.setEnterAnim(R.anim.anim_slide_in_right)
			.setExitAnim(R.anim.anim_slide_out_left)
			.setPopEnterAnim(R.anim.anim_slide_in_left)
			.setPopExitAnim(R.anim.anim_slide_out_right)
			.build()
		view.findNavController().navigate(request, options)
	}
	
	fun onLocationSelected(fragment: ProviderRegistrationFragment, locationData: LocationData) {
		viewModelScope.launch {
			fragment.activityViewModel?.globalLoading?.value = true
			
			when (val result = repository.registerProviderSendLocation(locationData.latitude, locationData.longitude, locationData.address)) {
				is MAResult.Failure -> {
					fragment.requireContext().showErrorToast(result.message ?: fragment.getString(R.string.something_went_wrong))
					
					fragment.activityViewModel?.globalLoading?.value = false
				}
				is MAResult.Success -> {
					fragment.prefsUser.setLocation(locationData)
					
					registrationStep.value = registrationStep.value.orZero().inc()
					
					fragment.activityViewModel?.globalLoading?.value = false
				}
			}
		}
	}
	
	//endregion
	
	//region Step 2
	
	fun changeImage(view: View) {
		view.findFragment<ProviderRegistrationFragment>().pickImageOrRequestPermissions()
	}

	fun nextStep2(view: View) {
		if (imageUri == null || personalDataName.value.isNullOrEmpty()
			|| personalDataMessage.value.isNullOrEmpty()) {
			val res = when {
				!personalDataName.value.isNullOrEmpty()
						&& !personalDataMessage.value.isNullOrEmpty() -> R.string.image_is_required
				imageUri != null && !personalDataMessage.value.isNullOrEmpty() -> R.string.name_is_required
				imageUri != null && !personalDataName.value.isNullOrEmpty() -> R.string.description_is_required
				else -> R.string.all_fields_required
			}

			return view.context.showErrorToast(view.context.getString(res))
		}
		
		val fragment = view.findFragment<ProviderRegistrationFragment>()
		viewModelScope.launch {
			fragment.activityViewModel?.globalLoading?.value = true
			
			val result = repository.registerProviderStep2PersonalData(
				imageUri?.createMultipartBodyPart(app, "image")
					?: return@launch view.context.showErrorToast(view.context.getString(R.string.something_went_wrong)),
				personalDataName.value.orEmpty(),
				personalDataEmail.value.orEmpty(),
				personalDataMessage.value.orEmpty()
			)
			
			when (result) {
				is MAResult.Failure -> {
					fragment.requireContext().showErrorToast(result.message ?: fragment.getString(R.string.something_went_wrong))
					
					fragment.activityViewModel?.globalLoading?.value = false
				}
				is MAResult.Success -> {
					fragment.prefsUser.setName(personalDataName.value.orEmpty())
					personalDataEmail.value.orEmpty().also {
						if (it.isNotEmpty()) {
							fragment.prefsUser.setEMail(it)
						}
					}
					fragment.prefsUser.setDescription(personalDataMessage.value.orEmpty())
					
					registrationStep.value = registrationStep.value.orZero().inc()
					
					fragment.activityViewModel?.globalLoading?.value = false
				}
			}
		}
	}
	
	//endregion
	
	//region Step 3

	fun nextStep3(view: View) {
		if (bankAccountBankName.value.isNullOrEmpty() || bankAccountAccountNumber.value.isNullOrEmpty()
			|| bankAccountClientName.value.isNullOrEmpty() || bankAccountIBANNumber.value.isNullOrEmpty()) {
			return view.context.showErrorToast(view.context.getString(R.string.either_skip_all_or_all_fields_required))
		}
		
		val fragment = view.findFragment<ProviderRegistrationFragment>()
		viewModelScope.launch {
			fragment.activityViewModel?.globalLoading?.value = true
			
			val result = repository.registerProviderStep3PersonalData(
				bankAccountBankName.value.orEmpty(),
				bankAccountAccountNumber.value.orEmpty(),
				bankAccountIBANNumber.value.orEmpty(),
				bankAccountClientName.value.orEmpty(),
			)
			
			when (result) {
				is MAResult.Failure -> {
					fragment.requireContext().showErrorToast(result.message ?: fragment.getString(R.string.something_went_wrong))
					
					fragment.activityViewModel?.globalLoading?.value = false
				}
				is MAResult.Success -> {
					fragment.prefsUser.setBankName(bankAccountBankName.value.orEmpty())
					fragment.prefsUser.setBankAccountNumber(bankAccountAccountNumber.value.orEmpty())
					fragment.prefsUser.setBankIBANNumber(bankAccountIBANNumber.value.orEmpty())
					fragment.prefsUser.setBankClientName(bankAccountClientName.value.orEmpty())
					
					registrationStep.value = registrationStep.value.orZero().inc()
					
					fragment.activityViewModel?.globalLoading?.value = false
				}
			}
		}
	}

	fun skip() {
		registrationStep.value = registrationStep.value.orZero().inc()
	}
	
	//endregion
	
	//region Step 4
	
	fun nextStep4(view: View) {
		val categories = adapter.snapshot().mapNotNull {
			if (it?.id in selectedIds) it else null
		}
		
		if (categories.isEmpty()) {
			return view.context.showErrorToast(view.context.getString(R.string.you_must_meke_at_least_1_selection))
		}
		
		view.findNavController().navigate(
			ProviderRegistrationFragmentDirections.actionDestProviderRegistrationToDestServicesSelection(
				categories.toJson(gson)
			)
		)
	}
	
	//endregion
	
	//region RVItemCheckable.Listener
	
	override fun isChecked(id: Int): Boolean = id in selectedIds
	
	override fun toggleCheckState(id: Int, adapterPosition: Int) {
		if (isChecked(id)) {
			selectedIds.remove(id)
		}else {
			selectedIds.add(id)
		}
		
		adapter.notifyItemChanged(adapterPosition)
	}
	
	//endregion

}
