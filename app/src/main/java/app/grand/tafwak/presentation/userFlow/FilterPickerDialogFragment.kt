package app.grand.tafwak.presentation.userFlow

import android.view.Gravity
import android.view.Window
import androidx.fragment.app.viewModels
import app.grand.tafwak.presentation.base.MADialogFragment
import app.grand.tafwak.presentation.base.extensions.getMyDrawable
import app.grand.tafwak.presentation.userFlow.viewModel.FilterPickerViewModel
import com.structure.base_mvvm.R
import com.structure.base_mvvm.databinding.DialogFragmentFilterPickerBinding
import dagger.hilt.android.AndroidEntryPoint

@AndroidEntryPoint
class FilterPickerDialogFragment : MADialogFragment<DialogFragmentFilterPickerBinding>() {
	
	private val viewModel by viewModels<FilterPickerViewModel>()
	
	override fun getLayoutId(): Int = R.layout.dialog_fragment_filter_picker
	
	override val windowGravity: Int = Gravity.BOTTOM
	
	override fun onCreateDialogWindowChanges(window: Window) {
		window.setBackgroundDrawable(getMyDrawable(R.drawable.dr_top_round_white))
		
		window.attributes?.windowAnimations = R.style.ScaleDialogAnim
	}
	
	override fun initializeBindingVariables() {
		binding.viewModel = viewModel
	}
	
}
