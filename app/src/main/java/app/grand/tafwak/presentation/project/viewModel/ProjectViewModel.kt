package app.grand.tafwak.presentation.project.viewModel

import android.location.Location
import android.net.Uri
import android.view.View
import androidx.databinding.DataBindingUtil
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel
import androidx.navigation.NavDeepLinkRequest
import androidx.navigation.NavOptions
import androidx.navigation.findNavController
import androidx.navigation.fragment.NavHostFragment
import app.grand.tafwak.core.customTypes.LocationHandler
import app.grand.tafwak.core.extensions.findNavControllerOfProject
import app.grand.tafwak.core.extensions.orZero
import app.grand.tafwak.data.local.preferences.PrefsUser
import app.grand.tafwak.domain.auth.entity.model.LocationLatLng
import app.grand.tafwak.domain.userFlow.model.MenuItemVisibility
import app.grand.tafwak.domain.userFlow.response.OrderDetails
import app.grand.tafwak.presentation.base.customTypes.GlobalError
import app.grand.tafwak.presentation.project.ProjectActivity
import com.google.firebase.database.ktx.database
import com.google.firebase.ktx.Firebase
import com.structure.base_mvvm.R
import com.structure.base_mvvm.databinding.ActivityProjectBinding
import dagger.hilt.android.lifecycle.HiltViewModel
import kotlinx.coroutines.flow.firstOrNull
import kotlinx.coroutines.runBlocking
import timber.log.Timber
import javax.inject.Inject

@HiltViewModel
class ProjectViewModel @Inject constructor(
	prefsUser: PrefsUser
) : ViewModel() {

	companion object {
		private const val KEY_DATABASE_REFERENCE_BASE = "location"
	}

	private val database by lazy { Firebase.database }
	private val referenceLocation by lazy {
		database.getReference(KEY_DATABASE_REFERENCE_BASE)
	}
	
	val showToolbar = MutableLiveData(false)
	
	val titleToolbar = MutableLiveData("")
	
	val globalLoading = MutableLiveData(false)
	
	val globalError = MutableLiveData<GlobalError>(GlobalError.Cancel)
	
	val menuItemNotificationsVisibility = MutableLiveData(MenuItemVisibility.HIDE)
	
	val menuItemConversationsVisibility = MutableLiveData(MenuItemVisibility.HIDE)

	val showPrintingOrderDetails = MutableLiveData<OrderDetails>()

	private val listOfOrdersIdsForTracking = mutableSetOf<Int>()

	var locationIsBeingTracked = false

	private val myId by lazy {
		runBlocking {
			prefsUser.getId().firstOrNull().orZero().also {
				Timber.e("my id is $it")
			}
		}
	}

	fun onNotificationsClick(view: View) {
		val uri = Uri.Builder()
			.scheme("fragment-dest")
			.authority("app.grand.tafwak.dest.notifications")
			.build()
		val request = NavDeepLinkRequest.Builder.fromUri(uri).build()
		val options = NavOptions.Builder()
			.setEnterAnim(R.anim.anim_slide_in_right)
			.setExitAnim(R.anim.anim_slide_out_left)
			.setPopEnterAnim(R.anim.anim_slide_in_left)
			.setPopExitAnim(R.anim.anim_slide_out_right)
			.build()
		
		val activity = DataBindingUtil.findBinding<ActivityProjectBinding>(view)
			?.lifecycleOwner as? ProjectActivity ?: return
		val navHostFragment = activity.supportFragmentManager
			.findFragmentById(R.id.mainNavHostFragment) as? NavHostFragment ?: return
		
		navHostFragment.navController.navigate(request, options)
	}
	
	fun onConversationsClick(view: View) {
		val uri = Uri.Builder()
			.scheme("fragment-dest")
			.authority("app.grand.tafwak.dest.conversations")
			.build()
		val request = NavDeepLinkRequest.Builder.fromUri(uri).build()
		val options = NavOptions.Builder()
			.setEnterAnim(R.anim.anim_slide_in_right)
			.setExitAnim(R.anim.anim_slide_out_left)
			.setPopEnterAnim(R.anim.anim_slide_in_left)
			.setPopExitAnim(R.anim.anim_slide_out_right)
			.build()

		val activity = DataBindingUtil.findBinding<ActivityProjectBinding>(view)
			?.lifecycleOwner as? ProjectActivity ?: return
		val navHostFragment = activity.supportFragmentManager
			.findFragmentById(R.id.mainNavHostFragment) as? NavHostFragment ?: return
		
		navHostFragment.navController.navigate(request, options)
	}

	fun onPrinterClick(view: View) {
		// todo not done as waiting for backend to say functionality for it as last bug report said
		//  so as well isa.
	}

	fun startTrackingOrders(ordersIds: List<Int>, listener: Listener) {
		Timber.e("abc333 ordersIds $ordersIds")

		val oldSize = listOfOrdersIdsForTracking.size
		listOfOrdersIdsForTracking.addAll(ordersIds)

		Timber.e("abc333 listOfOrdersIdsForTracking from ordersIds $listOfOrdersIdsForTracking")

		if (listOfOrdersIdsForTracking.size != oldSize) {
			checkIfShouldRequestLocationUpdates(listener)
		}
	}

	fun changeTrackingForOrder(orderId: Int, startTrackingElseStopIt: Boolean, listener: Listener) {
		Timber.e("abc333 orderId $orderId $startTrackingElseStopIt")

		val oldSize = listOfOrdersIdsForTracking.size
		if (startTrackingElseStopIt) {
			listOfOrdersIdsForTracking += orderId
		}else {
			listOfOrdersIdsForTracking -= orderId
		}

		Timber.e("abc333 listOfOrdersIdsForTracking $listOfOrdersIdsForTracking")

		if (listOfOrdersIdsForTracking.size != oldSize) {
			checkIfShouldRequestLocationUpdates(listener)
		}
	}

	fun checkIfShouldRequestLocationUpdates(listener: Listener) {
		if (listOfOrdersIdsForTracking.size > 0) {
			listener.startLocationTrackingAfterCheckingPermissions()
		}else {
			listener.stopLocationTracking()
		}
	}

	fun afterLocationChange(location: Location) {
		val copiedList = listOfOrdersIdsForTracking.toList()

		for (orderId in copiedList) {
			referenceLocation.child(orderId.toString()).child("$myId").setValue(
				LocationLatLng(
					location.latitude.toString(),
					location.longitude.toString()
				)
			) { error, _ ->
				Timber.e("Periodic Location error $error\n msg -> ${error?.message}")
			}
		}
	}

	interface Listener {
		fun startLocationTrackingAfterCheckingPermissions()

		fun stopLocationTracking()
	}
	
}
