package app.grand.tafwak.presentation.splash

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.databinding.DataBindingUtil
import androidx.fragment.app.activityViewModels
import androidx.fragment.app.viewModels
import androidx.lifecycle.Lifecycle
import androidx.lifecycle.lifecycleScope
import androidx.lifecycle.repeatOnLifecycle
import androidx.navigation.fragment.findNavController
import app.grand.tafwak.data.local.preferences.PrefsSplash
import app.grand.tafwak.data.local.preferences.PrefsUser
import app.grand.tafwak.domain.splash.entity.SplashInitialLaunch
import app.grand.tafwak.presentation.base.BaseFragment
import app.grand.tafwak.presentation.base.MABaseFragment
import app.grand.tafwak.presentation.base.extensions.handleRetryAbleFlowWithMustHaveResultWithNullability
import app.grand.tafwak.presentation.project.viewModel.ProjectViewModel
import app.grand.tafwak.presentation.providerFlow.viewModels.ProfitsViewModel
import app.grand.tafwak.presentation.splash.viewModel.SplashViewModel
import com.structure.base_mvvm.BuildConfig
import com.structure.base_mvvm.NavUserBottomNavDirections
import com.structure.base_mvvm.R
import com.structure.base_mvvm.databinding.ActivitySplashBinding
import com.structure.base_mvvm.databinding.FragmentSplashBinding
import dagger.hilt.android.AndroidEntryPoint
import kotlinx.coroutines.delay
import kotlinx.coroutines.flow.first
import kotlinx.coroutines.flow.firstOrNull
import kotlinx.coroutines.launch
import timber.log.Timber
import javax.inject.Inject

@AndroidEntryPoint
class SplashFragment : MABaseFragment<FragmentSplashBinding>() {
	
	private val viewModel by viewModels<SplashViewModel>()

	@Inject
	lateinit var prefsUser: PrefsUser

	override fun getLayoutId(): Int = R.layout.fragment_splash
	
	override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
		// https://www.youtube.com/watch?v=fSB6_KE95bU&t=746s
		viewLifecycleOwner.lifecycleScope.launch {
			viewLifecycleOwner.repeatOnLifecycle(Lifecycle.State.STARTED) {
				delay(1_000)
				
				when (prefsSplash.getInitialLaunch().first()) {
					SplashInitialLaunch.USER -> {
						if (BuildConfig.DEBUG) {
							kotlin.runCatching {
								Timber.d("user -> ${prefsUser.getId().firstOrNull()} ${prefsUser.getToken().firstOrNull()}")
							}
						}
						findNavController().navigate(SplashFragmentDirections.actionDestSplashToNavUser())
					}
					SplashInitialLaunch.SERVICE_PROVIDER -> {
						if (BuildConfig.DEBUG) {
							kotlin.runCatching {
								Timber.d("provider -> ${prefsUser.getId().firstOrNull()} ${prefsUser.getToken().firstOrNull()}")
							}
						}
						findNavController().navigate(SplashFragmentDirections.actionDestSplashToNavProvider())
					}
					else /* SplashInitialLaunch.LOGIN */ -> {
						findNavController().navigate(R.id.action_dest_splash_to_nav_auth_2)
					}
				}
			}
		}
	}
	
}