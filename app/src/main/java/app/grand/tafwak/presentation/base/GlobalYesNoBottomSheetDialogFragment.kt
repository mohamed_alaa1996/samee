package app.grand.tafwak.presentation.base

import android.graphics.drawable.InsetDrawable
import android.os.Bundle
import android.view.Gravity
import android.view.View
import android.view.Window
import androidx.annotation.CallSuper
import androidx.appcompat.content.res.AppCompatResources
import androidx.core.view.isVisible
import androidx.navigation.fragment.findNavController
import app.grand.tafwak.core.extensions.dpToPx
import app.grand.tafwak.presentation.base.extensions.getMyDrawable
import com.structure.base_mvvm.R
import com.structure.base_mvvm.databinding.DialogFragmentBaseSingleButtonBinding
import com.structure.base_mvvm.databinding.DialogFragmentYesNoBottomSheetBinding
import dagger.hilt.android.AndroidEntryPoint
import kotlin.math.roundToInt

@AndroidEntryPoint
open class GlobalYesNoBottomSheetDialogFragment : MADialogFragment<DialogFragmentYesNoBottomSheetBinding>() {
	
	companion object {
		const val KEY_FRAGMENT_RESULT_YES_BUTTON_CLICKED = "KEY_FRAGMENT_RESULT_YES_BUTTON_CLICKED"
		const val KEY_FRAGMENT_RESULT_NO_BUTTON_CLICKED = "KEY_FRAGMENT_RESULT_NO_BUTTON_CLICKED"
	}
	
	final override fun getLayoutId(): Int = R.layout.dialog_fragment_yes_no_bottom_sheet
	
	override val windowGravity: Int = Gravity.BOTTOM
	
	@CallSuper
	override fun onCreateDialogWindowChanges(window: Window) {
		window.setBackgroundDrawable(getMyDrawable(R.drawable.dr_top_round_white))
		
		window.attributes?.windowAnimations = R.style.ScaleDialogAnim
	}
	
	final override fun initializeBindingVariables() {}
	
	override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
		binding.imageView.isVisible = drawableRes != 0
		if (drawableRes != 0) {
			binding.imageView.setImageResource(drawableRes)
		}
		
		binding.textView.text = message
		
		binding.yesMaterialButton.text = yesButtonText
		binding.yesMaterialButton.setOnClickListener(::yesButtonClick)
		
		binding.noMaterialButton.text = noButtonText
		binding.noMaterialButton.setOnClickListener(::noButtonClick)
	}
	
	open val drawableRes: Int by lazy {
		R.drawable.ic_special_warning
	}
	
	open val message: String by lazy {
		getString(R.string.are_you_sure_about_service_deletion)
	}
	
	open val yesButtonText: String by lazy {
		getString(R.string.yes_del)
	}
	
	open val noButtonText: String by lazy {
		getString(R.string.no_back)
	}
	
	open fun yesButtonClick(view: View) {
		val navController = findNavController()
		
		navController.navigateUp()
		
		navController.currentBackStackEntry?.savedStateHandle?.set(
			KEY_FRAGMENT_RESULT_YES_BUTTON_CLICKED,
			true
		)
	}
	
	open fun noButtonClick(view: View) {
		val navController = findNavController()
		
		navController.navigateUp()
		
		navController.currentBackStackEntry?.savedStateHandle?.set(
			KEY_FRAGMENT_RESULT_NO_BUTTON_CLICKED,
			true
		)
	}
	
}
