package app.grand.tafwak.presentation.base.extensions

import android.net.Uri
import android.view.View
import android.widget.ImageView
import androidx.annotation.DrawableRes
import androidx.appcompat.content.res.AppCompatResources
import androidx.databinding.BindingAdapter
import app.grand.tafwak.core.extensions.listenerOnResourceReadyFitXY
import com.bumptech.glide.Glide
import com.bumptech.glide.request.RequestOptions
import com.google.android.material.card.MaterialCardView
import com.structure.base_mvvm.R

@BindingAdapter("imageView_setUrlViaGlide")
fun ImageView.setUrlViaGlide(url: String?) {
	if (url.isNullOrEmpty()) {
		setImageDrawable(null)
	}else {
		Glide.with(context)
			.load(url)
			.apply(RequestOptions().centerCrop())
			.placeholder(R.drawable.ic_logo_samee_placeholder)
			.error(R.drawable.ic_logo_samee_placeholder)
			.into(this)
	}
}

@BindingAdapter("imageView_setUrlViaGlideOrIgnore")
fun ImageView.setUrlViaGlideOrIgnore(url: String?) {
	if (!url.isNullOrEmpty()) {
		Glide.with(context)
			.load(url)
			.apply(RequestOptions().centerCrop())
			.placeholder(R.drawable.ic_logo_samee_placeholder)
			.error(R.drawable.ic_logo_samee_placeholder)
			.into(this)
	}
}

@BindingAdapter("imageView_setLinkViaGlideOrIgnore_link", "imageView_setLinkViaGlideOrIgnore_isImageNotVideo")
fun ImageView.setLinkViaGlideOrIgnore(link: String?, isImageNotVideo: Boolean?) {
	if (!link.isNullOrEmpty()) {
		val options = RequestOptions().let {
			if (isImageNotVideo == true) it else it.frame(1)
		}.centerCrop()
		Glide.with(context)
			.load(link)
			.apply(options)
			.placeholder(R.drawable.ic_logo_samee_placeholder)
			.error(R.drawable.ic_logo_samee_placeholder)
			.into(this)
	}
}

@BindingAdapter("imageView_setUriViaGlideOrPlaceholder")
fun ImageView.setUriViaGlideOrPlaceholder(uri: Uri?) {
	if (uri != null) {
		Glide.with(context)
			.load(uri)
			.apply(RequestOptions().centerCrop())
			.placeholder(R.drawable.ic_logo_samee_placeholder)
			.error(R.drawable.ic_logo_samee_placeholder)
			.into(this)
	}else {
		setImageResource(R.drawable.ic_logo_samee_placeholder)
	}
}

@BindingAdapter("imageView_setUrlViaGlideOrIgnoreWithFitXY")
fun ImageView.setUrlViaGlideOrIgnoreWithFitXY(url: String?) {
	if (!url.isNullOrEmpty()) {
		/*
        Glide.with(binding.imageView.context)
            .load(slider.imageUrl)
            //.apply(RequestOptions().centerCrop())
            .placeholder(R.drawable.ic_logo_samee_placeholder)
            .error(R.drawable.ic_logo_samee_placeholder)
            .listenerOnResourceReadyFitXY(binding.imageView)
            .into(binding.imageView)
         */
		Glide.with(context)
			.load(url)
			//.apply(RequestOptions().centerCrop())
			.placeholder(R.drawable.ic_logo_samee_placeholder)
			.error(R.drawable.ic_logo_samee_placeholder)
			.listenerOnResourceReadyFitXY(this)
			.into(this)
	}
}

@BindingAdapter("imageView_setUriViaGlideOrPlaceholderWithFitXY")
fun ImageView.setUriViaGlideOrPlaceholderWithFitXY(uri: Uri?) {
	if (uri != null) {
		Glide.with(context)
			.load(uri)
			.frame(1)
			//.apply(RequestOptions().centerCrop())
			.placeholder(R.drawable.ic_logo_samee_placeholder)
			.error(R.drawable.ic_logo_samee_placeholder)
			.listenerOnResourceReadyFitXY(this)
			.into(this)
	}else {
		setImageResource(R.drawable.ic_logo_samee_placeholder)
	}
}
