package app.grand.tafwak.presentation.userHome

import android.os.Bundle
import android.view.View
import androidx.fragment.app.activityViewModels
import androidx.fragment.app.viewModels
import androidx.lifecycle.Lifecycle
import androidx.lifecycle.lifecycleScope
import androidx.lifecycle.repeatOnLifecycle
import androidx.paging.filter
import androidx.recyclerview.widget.LinearLayoutManager
import app.grand.tafwak.core.extensions.withCustomAdapters
import app.grand.tafwak.data.local.preferences.PrefsUser
import app.grand.tafwak.presentation.base.MABaseFragment
import app.grand.tafwak.presentation.project.viewModel.ProjectViewModel
import app.grand.tafwak.presentation.userHome.adapters.LSAdapterLoadingErrorEmpty
import app.grand.tafwak.presentation.userHome.viewModel.OrdersUserViewModel
import com.structure.base_mvvm.R
import com.structure.base_mvvm.databinding.FragmentOrdersUserBinding
import dagger.hilt.android.AndroidEntryPoint
import kotlinx.coroutines.flow.collectLatest
import kotlinx.coroutines.launch
import timber.log.Timber
import javax.inject.Inject

@AndroidEntryPoint
class OrdersUserFragment : MABaseFragment<FragmentOrdersUserBinding>() {
	
	private val viewModel by viewModels<OrdersUserViewModel>()
	
	@Inject
	lateinit var prefsUser: PrefsUser
	
	override fun getLayoutId(): Int = R.layout.fragment_orders_user
	
	override fun initializeBindingVariables() {
		binding?.viewModel = viewModel
	}
	
	override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
		binding?.currentRecyclerView?.layoutManager = LinearLayoutManager(requireContext(), LinearLayoutManager.VERTICAL, false)
		val headerAdapterCurrent = LSAdapterLoadingErrorEmpty(viewModel.adapterCurrentOrders, false)
		val footerAdapterFinished = LSAdapterLoadingErrorEmpty(viewModel.adapterCurrentOrders, true)
		binding?.currentRecyclerView?.adapter = viewModel.adapterCurrentOrders.withCustomAdapters(
			headerAdapterCurrent,
			footerAdapterFinished
		)
		binding?.previousRecyclerView?.layoutManager = LinearLayoutManager(requireContext(), LinearLayoutManager.VERTICAL, false)
		val headerAdapterCurrent2 = LSAdapterLoadingErrorEmpty(viewModel.adapterPreviousOrders, false)
		val footerAdapterFinished2 = LSAdapterLoadingErrorEmpty(viewModel.adapterPreviousOrders, true)
		binding?.previousRecyclerView?.adapter = viewModel.adapterPreviousOrders.withCustomAdapters(
			headerAdapterCurrent2,
			footerAdapterFinished2
		)
		
		viewLifecycleOwner.lifecycleScope.launch {
			viewLifecycleOwner.repeatOnLifecycle(Lifecycle.State.STARTED) {
				launch {
					viewModel.currentOrders.collectLatest {
						viewModel.adapterCurrentOrders.submitData(it)
					}
				}
				
				launch {
					viewModel.finishedOrders.collectLatest {
						viewModel.adapterPreviousOrders.submitData(it)
					}
				}
			}
		}
	}
	
}
