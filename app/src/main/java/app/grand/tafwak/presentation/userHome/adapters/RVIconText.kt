package app.grand.tafwak.presentation.userHome.adapters

import android.view.View
import android.view.ViewGroup
import androidx.recyclerview.widget.DiffUtil
import androidx.recyclerview.widget.ListAdapter
import app.grand.tafwak.domain.base.entity.IconText
import app.grand.tafwak.domain.user.entity.model.ServiceCategory
import app.grand.tafwak.presentation.userHome.adapters.viewHolders.VHIconText
import app.grand.tafwak.presentation.userHome.adapters.viewHolders.VHItemHomeUserServices

class RVIconText(private val listener: Listener) : ListAdapter<IconText, VHIconText>(COMPARATOR) {
	
	companion object {
		val COMPARATOR = object : DiffUtil.ItemCallback<IconText>() {
			override fun areItemsTheSame(
				oldItem: IconText,
				newItem: IconText
			): Boolean = oldItem.text == newItem.text
			
			override fun areContentsTheSame(
				oldItem: IconText,
				newItem: IconText
			): Boolean = oldItem == newItem
		}
	}
	
	override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): VHIconText {
		return VHIconText(parent, listener)
	}
	
	override fun onBindViewHolder(holder: VHIconText, position: Int) {
		holder.bind(getItem(position))
	}
	
	interface Listener {
		
		fun onItemIconTextClick(view: View, text: String)
		
	}
	
}
