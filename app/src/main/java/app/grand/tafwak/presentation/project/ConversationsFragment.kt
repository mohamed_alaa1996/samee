package app.grand.tafwak.presentation.project

import android.os.Bundle
import android.view.View
import androidx.fragment.app.activityViewModels
import androidx.fragment.app.viewModels
import androidx.lifecycle.Lifecycle
import androidx.lifecycle.lifecycleScope
import androidx.lifecycle.repeatOnLifecycle
import androidx.navigation.fragment.findNavController
import androidx.recyclerview.widget.GridLayoutManager
import androidx.recyclerview.widget.LinearLayoutManager
import app.grand.tafwak.core.customTypes.MADividerItemDecoration
import app.grand.tafwak.core.extensions.dpToPx
import app.grand.tafwak.core.extensions.withCustomAdapters
import app.grand.tafwak.data.local.preferences.PrefsSplash
import app.grand.tafwak.presentation.base.GlobalYesNoBottomSheetDialogFragment
import app.grand.tafwak.presentation.base.MABaseFragment
import app.grand.tafwak.presentation.project.adapters.RVItemConversation
import app.grand.tafwak.presentation.project.viewModel.ConversationViewModel
import app.grand.tafwak.presentation.project.viewModel.ProjectViewModel
import app.grand.tafwak.presentation.providerFlow.CreateOrEditServiceDialogFragment
import app.grand.tafwak.presentation.userFlow.viewModel.ShowAllPreviousWorksViewModel
import app.grand.tafwak.presentation.userHome.adapters.LSAdapterLoadingErrorEmpty
import com.google.gson.Gson
import com.structure.base_mvvm.R
import com.structure.base_mvvm.databinding.FragmentConversationsBinding
import com.structure.base_mvvm.databinding.FragmentShowAllPreviousWorksBinding
import dagger.hilt.android.AndroidEntryPoint
import kotlinx.coroutines.flow.collectLatest
import kotlinx.coroutines.flow.first
import kotlinx.coroutines.launch
import javax.inject.Inject
import kotlin.math.roundToInt

@AndroidEntryPoint
class ConversationsFragment : MABaseFragment<FragmentConversationsBinding>() {
	
	private val viewModel by viewModels<ConversationViewModel>()

  private lateinit var adapter : RVItemConversation

	@Inject
	protected lateinit var gson: Gson
	
	override fun getLayoutId(): Int = R.layout.fragment_conversations


	
	override fun initializeBindingVariables() {
		binding?.viewModel = viewModel
	}


  override fun onCreate(savedInstanceState: Bundle?) {
    lifecycleScope.launchWhenCreated {
      adapter = RVItemConversation(viewModel.prefUser.getId().first() ?: 0)
    }
    super.onCreate(savedInstanceState)
  }
	
	override fun onViewCreated(view: View, savedInstanceState: Bundle?) {

		binding?.recyclerView?.addItemDecoration(
			MADividerItemDecoration(
				requireContext().dpToPx(1f).roundToInt(),
				requireContext().getColor(R.color.divider_item_decor)
			)
		)
		binding?.recyclerView?.layoutManager = LinearLayoutManager(requireContext(), LinearLayoutManager.HORIZONTAL, false)
		val headerAdapterCurrent2 = LSAdapterLoadingErrorEmpty(adapter, false)
		val footerAdapterFinished2 = LSAdapterLoadingErrorEmpty(adapter, true)
		binding?.recyclerView?.adapter = adapter.withCustomAdapters(
			headerAdapterCurrent2,
			footerAdapterFinished2
		)
		
		viewLifecycleOwner.lifecycleScope.launch {
			viewLifecycleOwner.repeatOnLifecycle(Lifecycle.State.STARTED) {
				viewModel.conversations.collectLatest { pagingData ->
					adapter.submitData(pagingData)
				}
			}
		}
	}
	
}
