package app.grand.tafwak.presentation.providerFlow

import android.Manifest
import android.app.Activity
import android.content.Intent
import android.graphics.Bitmap
import android.graphics.BitmapFactory
import android.net.Uri
import android.os.Bundle
import android.provider.MediaStore
import android.view.View
import android.view.WindowInsets
import android.view.WindowManager
import androidx.activity.result.contract.ActivityResultContracts
import androidx.core.view.WindowCompat
import androidx.core.view.WindowInsetsCompat
import androidx.fragment.app.activityViewModels
import androidx.fragment.app.viewModels
import androidx.navigation.NavOptions
import androidx.navigation.fragment.findNavController
import androidx.recyclerview.widget.LinearLayoutManager
import app.grand.tafwak.core.extensions.*
import app.grand.tafwak.data.local.preferences.PrefsUser
import app.grand.tafwak.presentation.base.GlobalBackToUserHomeDialogFragment
import app.grand.tafwak.presentation.base.MABaseFragment
import app.grand.tafwak.presentation.project.viewModel.ProjectViewModel
import app.grand.tafwak.presentation.providerFlow.viewModels.ServicesSelectionViewModel
import app.grand.tafwak.presentation.providerHome.ProviderFragment
import com.bumptech.glide.Glide
import com.bumptech.glide.request.RequestOptions
import com.structure.base_mvvm.R
import com.structure.base_mvvm.databinding.FragmentServicesSelectionBinding
import dagger.hilt.android.AndroidEntryPoint
import java.io.ByteArrayOutputStream
import java.util.*
import javax.inject.Inject

@AndroidEntryPoint
class ServicesSelectionFragment : MABaseFragment<FragmentServicesSelectionBinding>()  {

	private val viewModel by viewModels<ServicesSelectionViewModel>()

	@Inject
	lateinit var prefsUser: PrefsUser

	private val permissionLocationRequest = registerForActivityResult(
		ActivityResultContracts.RequestMultiplePermissions()
	) { permissions ->
		when {
			permissions[Manifest.permission.READ_EXTERNAL_STORAGE] == true
					&& permissions[Manifest.permission.WRITE_EXTERNAL_STORAGE] == true
					&& permissions[Manifest.permission.CAMERA] == true -> {
				pickImageOrVideoViaChooser()
			}
			permissions[Manifest.permission.READ_EXTERNAL_STORAGE] == true
					&& permissions[Manifest.permission.WRITE_EXTERNAL_STORAGE] == true -> {
				pickImage(false)
				/*val image = getString(R.string.image)
				val video = getString(R.string.video)

				binding?.linearLayout?.showPopup(listOf(image, video)) {
					if (it.title?.toString() == image) {
						pickImage(false)
					}else {
						pickVideo(false)
					}
				}*/
			}
			permissions[Manifest.permission.CAMERA] == true -> {
				pickImage(true)
				/*val image = getString(R.string.image)
				val video = getString(R.string.video)

				binding?.linearLayout?.showPopup(listOf(image, video)) {
					if (it.title?.toString() == image) {
						pickImage(true)
					}else {
						pickVideo(true)
					}
				}*/
			}
			else -> {
				requireContext().showNormalToast(getString(R.string.you_didn_t_accept_permission))
			}
		}
	}

	private val activityResultImageCamera = registerForActivityResult(
		ActivityResultContracts.StartActivityForResult()
	) {
		if (it.resultCode == Activity.RESULT_OK) {
			val bitmap = it.data?.extras?.get("data") as? Bitmap ?: return@registerForActivityResult

			val uri = getUriFromBitmapRetrievedByCamera(bitmap)

			viewModel.liveDataOfImageUri.value = uri
			viewModel.liveDataOfVideoUri.value = null
		}
	}

	private val activityResultVideoCamera = registerForActivityResult(
		ActivityResultContracts.StartActivityForResult()
	) {
		if (it.resultCode == Activity.RESULT_OK) {
			val uri = it.data?.data ?: return@registerForActivityResult

			if (uri.checkSizeAndLengthOfVideo(context ?: return@registerForActivityResult)) {
				viewModel.liveDataOfVideoUri.value = uri
				viewModel.liveDataOfImageUri.value = null
			}else {
				context?.showErrorToast(getString(R.string.max_video_hint))
			}
		}
	}

	private val activityResultImageGallery = registerForActivityResult(
		ActivityResultContracts.StartActivityForResult()
	) {
		if (it.resultCode == Activity.RESULT_OK) {
			val uri = it.data?.data ?: return@registerForActivityResult

			viewModel.liveDataOfImageUri.value = uri
			viewModel.liveDataOfVideoUri.value = null
		}
	}

	private val activityResultVideoGallery = registerForActivityResult(
		ActivityResultContracts.StartActivityForResult()
	) {
		if (it.resultCode == Activity.RESULT_OK) {
			val uri = it.data?.data ?: return@registerForActivityResult

			if (uri.checkSizeAndLengthOfVideo(context ?: return@registerForActivityResult)) {
				viewModel.liveDataOfVideoUri.value = uri
				viewModel.liveDataOfImageUri.value = null
			}else {
				context?.showErrorToast(getString(R.string.max_video_hint))
			}
		}
	}

	override fun getLayoutId(): Int = R.layout.fragment_services_selection

	override fun initializeBindingVariables() {
		binding?.viewModel = viewModel
	}

	override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
		binding?.recyclerView?.layoutManager = LinearLayoutManager(requireContext(), LinearLayoutManager.HORIZONTAL, false)
		binding?.recyclerView?.adapter = viewModel.adapter

		findNavController().currentBackStackEntry?.savedStateHandle?.getLiveData(
			GlobalBackToUserHomeDialogFragment.KEY_FRAGMENT_RESULT_BUTTON_CLICKED,
			false
		)?.observe(viewLifecycleOwner) {
			if (it == true) {
				val navController = findNavController()

				navController.currentBackStackEntry?.savedStateHandle?.set(
					GlobalBackToUserHomeDialogFragment.KEY_FRAGMENT_RESULT_BUTTON_CLICKED,
					false
				)

				val options = NavOptions.Builder()
					.setEnterAnim(R.anim.anim_slide_in_right)
					.setExitAnim(R.anim.anim_slide_out_left)
					.setPopEnterAnim(R.anim.anim_slide_in_left)
					.setPopExitAnim(R.anim.anim_slide_out_right)
					.setPopUpTo(R.id.dest_services_selection, true)
					.build()

				navController.navigate(
					R.id.nav_provider,
					null,
					options
				)

				navController.currentBackStackEntry?.savedStateHandle?.set(
					ProviderFragment.KEY_FRAGMENT_RESULT_GO_TO_MENU_ITEM_ID,
					R.id.action_home
				)
			}
		}
	}

	fun pickImageOrVideoOrRequestPermissions() {
		when {
			requireContext().checkSelfPermissionGranted(Manifest.permission.READ_EXTERNAL_STORAGE)
					&& requireContext().checkSelfPermissionGranted(Manifest.permission.WRITE_EXTERNAL_STORAGE)
					&& requireContext().checkSelfPermissionGranted(Manifest.permission.CAMERA) -> {
				pickImageOrVideoViaChooser()
			}
			requireContext().checkSelfPermissionGranted(Manifest.permission.READ_EXTERNAL_STORAGE)
					&& requireContext().checkSelfPermissionGranted(Manifest.permission.WRITE_EXTERNAL_STORAGE) -> {
				pickImage(false)
				/*val image = getString(R.string.image)
				val video = getString(R.string.video)

				binding?.linearLayout?.showPopup(listOf(image, video)) {
					if (it.title?.toString() == image) {
						pickImage(false)
					}else {
						pickVideo(false)
					}
				}*/
			}
			requireContext().checkSelfPermissionGranted(Manifest.permission.CAMERA) -> {
				pickImage(true)
				/*val image = getString(R.string.image)
				val video = getString(R.string.video)

				binding?.linearLayout?.showPopup(listOf(image, video)) {
					if (it.title?.toString() == image) {
						pickImage(true)
					}else {
						pickVideo(true)
					}
				}*/
			}
			else -> {
				permissionLocationRequest.launch(arrayOf(
					Manifest.permission.READ_EXTERNAL_STORAGE,
					Manifest.permission.WRITE_EXTERNAL_STORAGE,
					Manifest.permission.CAMERA,
				))
			}
		}
	}

	private fun pickImageOrVideoViaChooser() {
		val imageCamera = "${getString(R.string.image)} (${getString(R.string.camera)})"
		val imageGallery = "${getString(R.string.image)} (${getString(R.string.gallery)})"
		/*val videoCamera = "${getString(R.string.video)} (${getString(R.string.camera)})"
		val videoGallery = "${getString(R.string.video)} (${getString(R.string.gallery)})"*/

		binding?.linearLayout?.showPopup(listOf(imageCamera, imageGallery/*, videoCamera, videoGallery*/)) {
			when (val title = it.title?.toString()) {
				imageCamera, imageGallery -> pickImage(title == imageCamera)
				//videoCamera, videoGallery -> pickVideo(title == videoCamera)
			}
		}
	}

	private fun pickImage(fromCamera: Boolean) {
		if (fromCamera) {
			activityResultImageCamera.launch(Intent(MediaStore.ACTION_IMAGE_CAPTURE))
		}else {
			// From gallery
			activityResultImageGallery.launch(Intent(Intent.ACTION_PICK, MediaStore.Images.Media.EXTERNAL_CONTENT_URI))
		}
	}

	private fun pickVideo(fromCamera: Boolean) {
		if (fromCamera) {
			activityResultVideoCamera.launch(Intent(MediaStore.ACTION_VIDEO_CAPTURE))
		}else {
			// From gallery
			activityResultVideoGallery.launch(Intent(Intent.ACTION_PICK, MediaStore.Video.Media.EXTERNAL_CONTENT_URI))
		}
	}

	private fun getUriFromBitmapRetrievedByCamera(bitmap: Bitmap): Uri {
		val stream = ByteArrayOutputStream()
		bitmap.compress(Bitmap.CompressFormat.JPEG, 70, stream)
		val byteArray = stream.toByteArray()
		val compressedBitmap = BitmapFactory.decodeByteArray(byteArray, 0, byteArray.size)

		val path = MediaStore.Images.Media.insertImage(
			requireContext().contentResolver, compressedBitmap, Date(System.currentTimeMillis()).toString() + "photo", null
		)
		return Uri.parse(path)
	}

}
