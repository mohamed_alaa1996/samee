package app.grand.tafwak.presentation.providerFlow.adapters

import android.view.ViewGroup
import androidx.paging.PagingDataAdapter
import androidx.recyclerview.widget.DiffUtil
import androidx.recyclerview.widget.ListAdapter
import app.grand.tafwak.domain.providerFlow.ItemReview
import app.grand.tafwak.domain.userFlow.response.WalletItem
import app.grand.tafwak.presentation.providerFlow.adapters.viewHolders.VHItemReview
import app.grand.tafwak.presentation.userFlow.adapters.viewHolders.VHWalletItem

class RVItemReview : PagingDataAdapter<ItemReview, VHItemReview>(COMPARATOR) {
	
	companion object {
		val COMPARATOR = object : DiffUtil.ItemCallback<ItemReview>() {
			override fun areItemsTheSame(
				oldItem: ItemReview,
				newItem: ItemReview
			): Boolean = oldItem.id == newItem.id
			
			override fun areContentsTheSame(
				oldItem: ItemReview,
				newItem: ItemReview
			): Boolean = oldItem == newItem
		}
	}
	
	override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): VHItemReview {
		return VHItemReview(parent)
	}
	
	override fun onBindViewHolder(holder: VHItemReview, position: Int) {
		holder.bind(getItem(position) ?: return)
	}
	
}
