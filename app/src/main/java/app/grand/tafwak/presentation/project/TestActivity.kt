package app.grand.tafwak.presentation.project

import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import app.grand.tafwak.presentation.base.BaseActivity
import app.grand.tafwak.presentation.base.MABaseActivity
import com.structure.base_mvvm.R
import com.structure.base_mvvm.databinding.ActivityTestBinding
import dagger.hilt.android.AndroidEntryPoint

@AndroidEntryPoint
class TestActivity : MABaseActivity<ActivityTestBinding>() {
	override fun onCreate(savedInstanceState: Bundle?) {
		super.onCreate(savedInstanceState)
		//setContentView()
	}
	
	override fun getLayoutId(): Int = R.layout.activity_test
}