package app.grand.tafwak.presentation.userFlow

import android.os.Bundle
import android.view.View
import androidx.core.view.isVisible
import androidx.fragment.app.activityViewModels
import androidx.fragment.app.viewModels
import androidx.lifecycle.lifecycleScope
import androidx.navigation.fragment.findNavController
import androidx.recyclerview.widget.LinearLayoutManager
import app.grand.tafwak.core.customTypes.MADateAndTime
import app.grand.tafwak.core.extensions.fromJson
import app.grand.tafwak.core.extensions.roundHalfUp
import app.grand.tafwak.core.extensions.toIntOrFloat
import app.grand.tafwak.core.extensions.toJson
import app.grand.tafwak.data.local.preferences.PrefsSplash
import app.grand.tafwak.presentation.base.MABaseFragment
import app.grand.tafwak.presentation.base.extensions.handleRetryAbleFlowWithMustHaveResultWithNullability
import app.grand.tafwak.presentation.base.extensions.setLinkViaGlideOrIgnore
import app.grand.tafwak.presentation.base.extensions.setUrlViaGlideOrIgnore
import app.grand.tafwak.presentation.project.viewModel.ProjectViewModel
import app.grand.tafwak.presentation.userFlow.viewModel.ProviderDetailsViewModel
import com.bumptech.glide.Glide
import com.bumptech.glide.request.RequestOptions
import com.google.gson.Gson
import com.structure.base_mvvm.R
import com.structure.base_mvvm.databinding.FragmentProviderDetailsBinding
import dagger.hilt.android.AndroidEntryPoint
import kotlinx.coroutines.delay
import kotlinx.coroutines.launch
import timber.log.Timber
import java.math.BigDecimal
import javax.inject.Inject
import kotlin.math.roundToInt

@AndroidEntryPoint
class ProviderDetailsFragment : MABaseFragment<FragmentProviderDetailsBinding>() {
	
	private val viewModel by viewModels<ProviderDetailsViewModel>()
	
	@Inject
	protected lateinit var gson: Gson
	
	override fun getLayoutId(): Int = R.layout.fragment_provider_details
	
	override fun initializeBindingVariables() {
		binding?.viewModel = viewModel
	}
	
	override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
		binding?.previousWorksRecyclerView?.layoutManager = LinearLayoutManager(requireContext(), LinearLayoutManager.HORIZONTAL, false)
		binding?.previousWorksRecyclerView?.adapter = viewModel.adapterPreviousWorks
		binding?.servicesRecyclerView?.layoutManager = LinearLayoutManager(requireContext(), LinearLayoutManager.VERTICAL, false)
		binding?.servicesRecyclerView?.adapter = viewModel.adapterServices
		
		handleRetryAbleFlowWithMustHaveResultWithNullability(viewModel.response) { (locationData, response) ->
			val details = response.data ?: return@handleRetryAbleFlowWithMustHaveResultWithNullability
			
			activityViewModel?.titleToolbar?.postValue(details.name)
			
			viewModel.details = details
			
			viewModel.locationData = locationData

			binding?.imageView?.setUrlViaGlideOrIgnore(details.imageUrl)

			binding?.nameTextView?.text = details.name
			
			binding?.ratingBar?.progress = (details.averageRate ).roundToInt()
			
			binding?.ratingTextView?.text = getString(R.string.between_brackets_str, details.averageRate.roundHalfUp(2).toString())
			
			Timber.e("DISTANCE 1 -> ${details.distanceInMeters}")
			val kmDistance = (details.distanceInMeters/* / 1000f*/)
			val scaledDistance = kmDistance.toBigDecimal().setScale(1, BigDecimal.ROUND_HALF_UP).toFloat().coerceAtLeast(0.1f)
			val distance = getString(R.string.away_by) + " " + scaledDistance.toIntOrFloat().toString() + " " + getString(R.string.km_notation)
			binding?.distanceTextView?.text = distance
			
			binding?.descriptionTextView?.text = details.description
			
			val showWorks = details.previousWorks.isNotEmpty()
			binding?.previousWorksTitleTextView?.isVisible = showWorks
			binding?.viewAllPreviousWorksTitleTextView?.isVisible = showWorks
			binding?.previousWorksRecyclerView?.isVisible = showWorks
			viewModel.adapterPreviousWorks.submitList(details.previousWorks)
			
			val showServices = details.services.isNotEmpty()
			binding?.servicesTitleTextView?.isVisible = showServices
			binding?.searchMaterialCardView?.isVisible = showServices
			binding?.servicesRecyclerView?.isVisible = showServices
			viewModel.adapterServices.submitList(details.services)
			
			viewModel.search.observe(viewLifecycleOwner) { search ->
				val list = if (search.isNullOrEmpty()) {
					viewModel.details.services
				}else{
					viewModel.details.services.filter { service -> search in service.name.orEmpty() }
				}
				viewModel.adapterServices.submitList(list)
			}
		}
		
		findNavController().currentBackStackEntry?.savedStateHandle?.getLiveData(
			DateAndTimePickerDialogFragment.KEY_FRAGMENT_RESULT_MA_DATE_AND_TIME_AS_JSON,
			""
		)?.observe(viewLifecycleOwner) {
			if (!it.isNullOrEmpty()) {
				findNavController().currentBackStackEntry?.savedStateHandle?.set(
					DateAndTimePickerDialogFragment.KEY_FRAGMENT_RESULT_MA_DATE_AND_TIME_AS_JSON,
					""
				)
				
				val details = viewModel.details.copy(services = viewModel.adapterServices.getSelectedServices())
				val maDateAndTime = it.fromJson<MADateAndTime>(gson)
				val locationData = viewModel.locationData
				
				lifecycleScope.launch {
					delay(300)
					
					activityViewModel?.globalLoading?.value = false
					
					findNavController().navigate(
						ProviderDetailsFragmentDirections.actionDestProviderDetailsToDestOrderConfirmation(
							details.toJson(gson),
							maDateAndTime.toJson(gson),
							locationData.toJson(gson),
							viewModel.numberOfPeople.value.orEmpty().toInt()
						)
					)
				}
			}
		}
	}

}
