package app.grand.tafwak.presentation.userFlow.viewModel

import android.view.View
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel
import androidx.navigation.findNavController
import app.grand.tafwak.core.extensions.fromJsonOrNull
import app.grand.tafwak.core.extensions.showErrorToast
import app.grand.tafwak.core.extensions.toJson
import app.grand.tafwak.domain.auth.entity.model.ManualLocation
import app.grand.tafwak.presentation.userFlow.ManualLocationFragment
import app.grand.tafwak.presentation.userFlow.ManualLocationFragmentArgs
import com.google.gson.Gson
import com.structure.base_mvvm.R
import dagger.hilt.android.lifecycle.HiltViewModel
import javax.inject.Inject

@HiltViewModel
class ManualLocationViewModel @Inject constructor(
	private val gson: Gson,
	args: ManualLocationFragmentArgs
) : ViewModel() {
	
	private val manualLocation = args.manualLocationAsJson.fromJsonOrNull<ManualLocation>(gson)
	
	val city = MutableLiveData(manualLocation?.city.orEmpty())
	
	val area = MutableLiveData(manualLocation?.area.orEmpty())
	
	val street = MutableLiveData(manualLocation?.street.orEmpty())
	
	val floorNumber = MutableLiveData(manualLocation?.floorNumber?.toString().orEmpty())
	
	val specialSign = MutableLiveData(manualLocation?.specialSign.orEmpty())
	
	fun onSelectClick(view: View) {
		val city = city.value
		val area = area.value
		val street = street.value
		val floorNumber = floorNumber.value?.toIntOrNull()
		val specialSign = specialSign.value
		
		if (city == null || area == null || street == null || floorNumber == null) {
			return view.context.showErrorToast(view.context.getString(R.string.all_fields_required_except_special_sign))
		}
		
		val navController = view.findNavController()
		
		navController.navigateUp()
		
		navController.currentBackStackEntry?.savedStateHandle?.set(
			ManualLocationFragment.KEY_FRAGMENT_RESULT_MANUAL_LOCATION_AS_JSON,
			ManualLocation(city, area, street, floorNumber, specialSign.orEmpty()).toJson(gson)
		)
	}
	
}
