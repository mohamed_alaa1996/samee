package app.grand.tafwak.presentation.userHome

import android.os.Bundle
import android.view.View
import androidx.fragment.app.activityViewModels
import androidx.fragment.app.viewModels
import androidx.lifecycle.lifecycleScope
import androidx.recyclerview.widget.LinearLayoutManager
import app.grand.tafwak.core.customTypes.MADividerItemDecoration
import app.grand.tafwak.core.extensions.dpToPx
import app.grand.tafwak.core.extensions.findNavControllerOfProject
import app.grand.tafwak.core.extensions.showErrorToast
import app.grand.tafwak.data.local.preferences.PrefsSplash
import app.grand.tafwak.data.local.preferences.PrefsUser
import app.grand.tafwak.domain.base.entity.IconText
import app.grand.tafwak.domain.utils.MAResult
import app.grand.tafwak.presentation.base.MABaseFragment
import app.grand.tafwak.presentation.project.viewModel.ProjectViewModel
import app.grand.tafwak.presentation.userHome.viewModel.HomeUserViewModel
import app.grand.tafwak.presentation.userHome.viewModel.MoreUserViewModel
import com.structure.base_mvvm.R
import com.structure.base_mvvm.databinding.FragmentMoreUserBinding
import dagger.hilt.android.AndroidEntryPoint
import kotlinx.coroutines.flow.first
import kotlinx.coroutines.launch
import javax.inject.Inject
import kotlin.math.roundToInt

@AndroidEntryPoint
class MoreUserFragment : MABaseFragment<FragmentMoreUserBinding>() {
	
	private val viewModel by viewModels<MoreUserViewModel>()

	@Inject
	protected lateinit var prefsUser: PrefsUser
	
	override fun getLayoutId(): Int = R.layout.fragment_more_user
	
	override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
		viewModel.adapter.submitList(listOfNotNull(
			IconText(R.drawable.ic_about, getString(R.string.about)),
			IconText(R.drawable.ic_terms_and_conditions, getString(R.string.terms_and_conditions)),
			IconText(R.drawable.ic_privacy_policy, getString(R.string.privacy)),
			IconText(R.drawable.ic_suggestions_and_complains, getString(R.string.suggestions_and_complaints)),
			IconText(R.drawable.ic_share, getString(R.string.share_app)),
			IconText(R.drawable.ic_rating, getString(R.string.rate_app)),
			IconText(R.drawable.ic_telegram, getString(R.string.contact_us_on_telegram)),
			if (viewModel.args.isUserNotProvider) null else IconText(R.drawable.ic_log_out_5, getString(R.string.log_out))
		))
		
		binding?.recyclerView?.addItemDecoration(
			MADividerItemDecoration(
				requireContext().dpToPx(1f).roundToInt(),
				requireContext().getColor(R.color.divider_item_decor)
			)
		)
		binding?.recyclerView?.layoutManager = LinearLayoutManager(requireContext(), LinearLayoutManager.VERTICAL, false)
		binding?.recyclerView?.adapter = viewModel.adapter
		
		viewModel.responseLogOut.observe(viewLifecycleOwner) { result ->
			when (result) {
				is MAResult.Loading -> activityViewModel?.globalLoading?.value = true
				is MAResult.Success -> {
					activityViewModel?.globalLoading?.value = false
					
					viewModel.performLogOut.value = false
					
					lifecycleScope.launch {
						viewModel.onLogOutSuccess(findNavControllerOfProject())
					}
				}
				is MAResult.Failure -> {
					activityViewModel?.globalLoading?.value = false
					
					viewModel.performLogOut.value = false
					
					requireContext().showErrorToast(result.message ?: getString(R.string.something_went_wrong))
				}
			}
		}
	}
	
}
