package app.grand.tafwak.presentation.userFlow

import android.Manifest
import android.annotation.SuppressLint
import android.app.Activity
import android.content.Intent
import android.graphics.Bitmap
import android.graphics.BitmapFactory
import android.net.Uri
import android.os.Bundle
import android.provider.MediaStore
import android.view.View
import androidx.activity.result.contract.ActivityResultContracts
import androidx.core.text.buildSpannedString
import androidx.fragment.app.activityViewModels
import androidx.fragment.app.viewModels
import androidx.lifecycle.lifecycleScope
import androidx.lifecycle.viewModelScope
import androidx.navigation.findNavController
import androidx.navigation.fragment.findNavController
import app.grand.tafwak.core.extensions.*
import app.grand.tafwak.data.local.preferences.PrefsUser
import app.grand.tafwak.domain.auth.entity.model.LocationData
import app.grand.tafwak.presentation.auth2.location.LocationSelectionFragment
import app.grand.tafwak.presentation.auth2.referral.CheckReferralCodeDialogFragment
import app.grand.tafwak.presentation.base.MABaseFragment
import app.grand.tafwak.presentation.project.viewModel.ProjectViewModel
import app.grand.tafwak.presentation.userFlow.viewModel.PersonalDataViewModel
import com.bumptech.glide.Glide
import com.bumptech.glide.request.RequestOptions
import com.google.gson.Gson
import com.structure.base_mvvm.R
import com.structure.base_mvvm.databinding.FragmentPersonalDataBinding
import dagger.hilt.android.AndroidEntryPoint
import kotlinx.coroutines.delay
import kotlinx.coroutines.flow.first
import kotlinx.coroutines.flow.map
import kotlinx.coroutines.launch
import timber.log.Timber
import java.io.ByteArrayOutputStream
import java.util.*
import javax.inject.Inject

@AndroidEntryPoint
class PersonalDataFragment : MABaseFragment<FragmentPersonalDataBinding>() {
	
	private val viewModel by viewModels<PersonalDataViewModel>()
	
	@Inject
	lateinit var prefsUser: PrefsUser
	
	@Inject
	protected lateinit var gson: Gson
	
	private val permissionLocationRequest = registerForActivityResult(
		ActivityResultContracts.RequestMultiplePermissions()
	) { permissions ->
		when {
			permissions[Manifest.permission.READ_EXTERNAL_STORAGE] == true
				&& permissions[Manifest.permission.WRITE_EXTERNAL_STORAGE] == true
				&& permissions[Manifest.permission.CAMERA] == true -> {
				pickImageViaChooser()
			}
			permissions[Manifest.permission.READ_EXTERNAL_STORAGE] == true
				&& permissions[Manifest.permission.WRITE_EXTERNAL_STORAGE] == true -> {
				pickImage(false)
			}
			permissions[Manifest.permission.CAMERA] == true -> {
				pickImage(true)
			}
			else -> {
				requireContext().showNormalToast(getString(R.string.you_didn_t_accept_permission))
			}
		}
	}
	
	private val activityResultImageCamera = registerForActivityResult(
		ActivityResultContracts.StartActivityForResult()
	) {
		if (it.resultCode == Activity.RESULT_OK) {
			val bitmap = it.data?.extras?.get("data") as? Bitmap ?: return@registerForActivityResult
			
			val uri = getUriFromBitmapRetrievedByCamera(bitmap)
			
			viewModel.imageUri = uri
			
			Glide.with(this)
				.load(uri)
				.apply(RequestOptions().centerCrop())
				.into(binding?.imageView ?: return@registerForActivityResult)
		}
	}
	
	private val activityResultImageGallery = registerForActivityResult(
		ActivityResultContracts.StartActivityForResult()
	) {
		if (it.resultCode == Activity.RESULT_OK) {
			val uri = it.data?.data ?: return@registerForActivityResult
			
			viewModel.imageUri = uri
			
			Glide.with(this)
				.load(uri)
				.apply(RequestOptions().centerCrop())
				.into(binding?.imageView ?: return@registerForActivityResult)
		}
	}
	
	override fun getLayoutId(): Int = R.layout.fragment_personal_data
	
	override fun initializeBindingVariables() {
		binding?.viewModel = viewModel
	}
	
	override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
		lifecycleScope.launch {
			activityViewModel?.globalLoading?.value = true
			
			viewModel.imageUrl.value = prefsUser.getImageUrl().first().also {
				Timber.e("imaaaaaaaaaage $it")
			}
			viewModel.name.value = prefsUser.getName().first()
			viewModel.phone.value = prefsUser.getPhone().first()
			viewModel.referCode.value = prefsUser.getReferCode().first()
			Timber.v("referred by id -> ${prefsUser.getReferredById().first()} ${prefsUser.getReferredById().first() != null}")
			viewModel.usedCodeBefore.value = prefsUser.getReferredById().first() != null
			viewModel.locationData.value = prefsUser.getLocation().first()
			viewModel.email.value = prefsUser.getEMail().first()
			viewModel.description.value = prefsUser.getDescription().first()
			
			// For dialog anim
			delay(300)
			
			activityViewModel?.globalLoading?.value = false
			
			findNavController().currentBackStackEntry?.savedStateHandle?.getLiveData(
				LocationSelectionFragment.KEY_FRAGMENT_RESULT_LOCATION_DATA_AS_JSON,
				""
			)?.observe(viewLifecycleOwner) {
				Timber.e("it $it")
				if (!it.isNullOrEmpty()) {
					Timber.e("2 it $it")
					findNavController().currentBackStackEntry?.savedStateHandle?.set(
						LocationSelectionFragment.KEY_FRAGMENT_RESULT_LOCATION_DATA_AS_JSON,
						""
					)

					Timber.e("3 ${it.fromJson<LocationData>(gson)}")
					viewModel.locationData.value = it.fromJson(gson)
				}
			}

			observeSavedStateHandleResult<Int?>(
				CheckReferralCodeDialogFragment.KEY_FRAGMENT_RESULT_REFERRED_BY_ID,
				null
			) {
				Timber.v("referred by id -> $it")

				viewModel.usedCodeBefore.value = true

				viewModel.viewModelScope.launch {
					prefsUser.setReferredById(it.orZero())
				}
			}
		}
		
		binding?.root?.post {
			activityViewModel?.titleToolbar?.postValue(viewModel.args.toolbarTitle)
		}
	}
	
	fun pickImageOrRequestPermissions() {
		when {
			requireContext().checkSelfPermissionGranted(Manifest.permission.READ_EXTERNAL_STORAGE)
				&& requireContext().checkSelfPermissionGranted(Manifest.permission.WRITE_EXTERNAL_STORAGE)
				&& requireContext().checkSelfPermissionGranted(Manifest.permission.CAMERA) -> {
				pickImageViaChooser()
			}
			requireContext().checkSelfPermissionGranted(Manifest.permission.READ_EXTERNAL_STORAGE)
				&& requireContext().checkSelfPermissionGranted(Manifest.permission.WRITE_EXTERNAL_STORAGE) -> {
				pickImage(false)
			}
			requireContext().checkSelfPermissionGranted(Manifest.permission.CAMERA) -> {
				pickImage(true)
			}
			else -> {
				permissionLocationRequest.launch(arrayOf(
					Manifest.permission.READ_EXTERNAL_STORAGE,
					Manifest.permission.WRITE_EXTERNAL_STORAGE,
					Manifest.permission.CAMERA,
				))
			}
		}
	}
	
	private fun pickImage(fromCamera: Boolean) {
		if (fromCamera) {
			activityResultImageCamera.launch(Intent(MediaStore.ACTION_IMAGE_CAPTURE))
		}else {
			// From gallery
			activityResultImageGallery.launch(Intent(Intent.ACTION_PICK, MediaStore.Images.Media.EXTERNAL_CONTENT_URI))
		}
	}
	
	private fun pickImageViaChooser() {
		val camera = getString(R.string.camera)
		val gallery = getString(R.string.gallery)
		
		binding?.materialCardView?.showPopup(listOf(camera, gallery)) {
			pickImage(it.title?.toString() == camera)
		}
	}
	
	private fun getUriFromBitmapRetrievedByCamera(bitmap: Bitmap): Uri {
		val stream = ByteArrayOutputStream()
		bitmap.compress(Bitmap.CompressFormat.JPEG, 70, stream)
		val byteArray = stream.toByteArray()
		val compressedBitmap = BitmapFactory.decodeByteArray(byteArray, 0, byteArray.size)
		
		val path = MediaStore.Images.Media.insertImage(
			requireContext().contentResolver, compressedBitmap, Date(System.currentTimeMillis()).toString() + "photo", null
		)
		return Uri.parse(path)
	}
	
}
