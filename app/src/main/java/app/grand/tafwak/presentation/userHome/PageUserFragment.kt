package app.grand.tafwak.presentation.userHome

import android.net.Uri
import android.os.Bundle
import android.view.View
import androidx.fragment.app.activityViewModels
import androidx.fragment.app.viewModels
import androidx.lifecycle.lifecycleScope
import androidx.navigation.NavDeepLinkRequest
import androidx.navigation.fragment.findNavController
import app.grand.tafwak.core.extensions.findNavControllerOfProject
import app.grand.tafwak.core.extensions.showErrorToast
import app.grand.tafwak.data.local.preferences.PrefsSplash
import app.grand.tafwak.domain.userFlow.model.MenuItemVisibility
import app.grand.tafwak.domain.utils.MAResult
import app.grand.tafwak.presentation.base.MABaseFragment
import app.grand.tafwak.presentation.base.extensions.handleRetryAbleFlowWithMustHaveResultWithNullability
import app.grand.tafwak.presentation.project.viewModel.ProjectViewModel
import app.grand.tafwak.presentation.userHome.viewModel.PageUserViewModel
import com.structure.base_mvvm.R
import com.structure.base_mvvm.databinding.FragmentPageUserBinding
import dagger.hilt.android.AndroidEntryPoint
import kotlinx.coroutines.launch
import javax.inject.Inject

@AndroidEntryPoint
class PageUserFragment : MABaseFragment<FragmentPageUserBinding>() {
	
	private val viewModel by viewModels<PageUserViewModel>()
	
	override fun getLayoutId(): Int = R.layout.fragment_page_user
	
	override fun initializeBindingVariables() {
		binding?.viewModel = viewModel
	}
	
	override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
		handleRetryAbleFlowWithMustHaveResultWithNullability(viewModel.myPageData) {
			activityViewModel?.menuItemNotificationsVisibility?.postValue(
				if (it.notificationsCount > 0) MenuItemVisibility.SHOW_HAVING_NEW_DATA else MenuItemVisibility.SHOW
			)
			activityViewModel?.menuItemConversationsVisibility?.postValue(
				if (it.messagesCount > 0) MenuItemVisibility.SHOW_HAVING_NEW_DATA else MenuItemVisibility.SHOW
			)
		}
		
		viewModel.responseLogOut.observe(viewLifecycleOwner) { result ->
			when (result) {
				is MAResult.Loading -> activityViewModel?.globalLoading?.value = true
				is MAResult.Success -> {
					activityViewModel?.globalLoading?.value = false
					
					viewModel.performLogOut.value = false
					
					lifecycleScope.launch {
						viewModel.onLogOutSuccess(findNavControllerOfProject())
					}
				}
				is MAResult.Failure -> {
					activityViewModel?.globalLoading?.value = false
					
					viewModel.performLogOut.value = false
					
					requireContext().showErrorToast(result.message ?: getString(R.string.something_went_wrong))
				}
			}
		}
	}
	
}
