package app.grand.tafwak.presentation.project.adapters.viewHolders

import android.graphics.drawable.ColorDrawable
import android.view.ViewGroup
import androidx.appcompat.content.res.AppCompatResources
import androidx.navigation.findNavController
import androidx.recyclerview.widget.RecyclerView
import app.grand.tafwak.core.extensions.inflateLayout
import app.grand.tafwak.domain.providerFlow.ResponseConversation
import app.grand.tafwak.domain.userFlow.response.CategoryWithWorkings
import app.grand.tafwak.presentation.project.ConversationsFragmentDirections
import app.grand.tafwak.presentation.userFlow.adapters.RVItemTextInCard
import com.bumptech.glide.Glide
import com.bumptech.glide.request.RequestOptions
import com.structure.base_mvvm.R
import com.structure.base_mvvm.databinding.ItemConversationBinding
import com.structure.base_mvvm.databinding.ItemTextInCardBinding

class VHItemConversation(parent: ViewGroup) : RecyclerView.ViewHolder(
	parent.context.inflateLayout(R.layout.item_conversation, parent)
) {
	
	private val binding = ItemConversationBinding.bind(itemView)
	
	init {
		binding.constraintLayout.setOnClickListener {
			val otherUserId = binding.constraintLayout.tag as? Int ?: return@setOnClickListener
			
			val orderId = binding.titleTextView.tag as? Int ?: return@setOnClickListener
			
			it.findNavController().navigate(
				ConversationsFragmentDirections.actionDestConversationsToDestChatDetails(otherUserId, orderId)
			)
		}
	}
	
	fun bind(item: ResponseConversation) {
		binding.constraintLayout.tag = item.otherUserId
		binding.titleTextView.tag = item.message.orderId
		binding.dateTextView.text = item.message.createdAt
		binding.descTextView.text = item.message.message
		binding.titleTextView.text = item.message.user.name
		Glide.with(binding.imageView.context)
			.load(item.message.user.imageUrl)
			.apply(RequestOptions().centerCrop())
			.placeholder(R.drawable.ic_logo_samee_placeholder)
			.error(R.drawable.ic_logo_samee_placeholder)
			.into(binding.imageView)
	}
	
}
