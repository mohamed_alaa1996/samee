package app.grand.tafwak.presentation.providerHome.adapters

import android.view.View
import android.view.ViewGroup
import androidx.recyclerview.widget.DiffUtil
import androidx.recyclerview.widget.ListAdapter
import app.grand.tafwak.domain.user.entity.model.ProviderService
import app.grand.tafwak.domain.user.entity.model.ServiceCategory
import app.grand.tafwak.presentation.providerFlow.adapters.viewHolders.VHItemTextInCard2
import app.grand.tafwak.presentation.providerHome.adapters.viewHolders.VHItemProvidedService

class RVItemProvidedService(private val listener: Listener) : ListAdapter<ProviderService, VHItemProvidedService>(COMPARATOR) {
	
	companion object {
		val COMPARATOR = object : DiffUtil.ItemCallback<ProviderService>() {
			override fun areItemsTheSame(
				oldItem: ProviderService,
				newItem: ProviderService
			): Boolean = oldItem.id == newItem.id
			
			override fun areContentsTheSame(
				oldItem: ProviderService,
				newItem: ProviderService
			): Boolean  = oldItem == newItem
		}
	}
	
	override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): VHItemProvidedService {
		return VHItemProvidedService(parent, listener)
	}
	
	override fun onBindViewHolder(holder: VHItemProvidedService, position: Int) {
		holder.bind(getItem(position))
	}
	
	interface Listener {
		fun editService(view: View, id: Int)
		
		fun deleteService(view: View, id: Int)
	}
	
}
