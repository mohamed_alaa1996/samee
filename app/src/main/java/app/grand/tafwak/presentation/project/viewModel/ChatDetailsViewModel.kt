package app.grand.tafwak.presentation.project.viewModel

import android.net.Uri
import android.os.Build
import android.view.View
import android.view.inputmethod.EditorInfo
import android.widget.TextView
import androidx.annotation.RequiresApi
import androidx.fragment.app.findFragment
import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel
import androidx.lifecycle.viewModelScope
import androidx.paging.PagingData
import app.grand.tafwak.core.extensions.createMultipartBodyPart
import app.grand.tafwak.core.extensions.showErrorToast
import app.grand.tafwak.data.local.preferences.PrefsUser
import app.grand.tafwak.data.provider.repository.ProviderRepositoryImpl
import app.grand.tafwak.domain.providerFlow.ResponseChatMessage
import app.grand.tafwak.domain.user.entity.model.FileType
import app.grand.tafwak.domain.utils.MAResult
import app.grand.tafwak.presentation.project.ChatDetailsFragment
import app.grand.tafwak.presentation.project.ChatDetailsFragmentArgs
import app.grand.tafwak.presentation.project.adapters.RVItemChatDetails
import com.structure.base_mvvm.R
import dagger.hilt.android.lifecycle.HiltViewModel
import kotlinx.coroutines.launch
import java.util.concurrent.Flow
import javax.inject.Inject

@HiltViewModel
class ChatDetailsViewModel @Inject constructor(
	private val repository: ProviderRepositoryImpl,
	private val args: ChatDetailsFragmentArgs,
  private val prefUser : PrefsUser
) : ViewModel() {
	
	val msg = MutableLiveData("")
	
	val chats = repository.getChatMessages(args.otherUserId)
	
	val adapter = RVItemChatDetails()


	
	/*
	                    editText_setOnEditorActionListenerBA="@{viewModel.onSearchTextSubmit()}"

	 */

	fun onSubmitTextByClick(textView: TextView) {
		val query = textView.text?.toString().orEmpty()

		if (query.isEmpty()) {
			textView.context.showErrorToast(textView.context.getString(R.string.msg_must_not_be_empty))
		}else {
			sendMessageToApi(textView, query)
		}
	}

	fun onSearchTextSubmit() = TextView.OnEditorActionListener { textView, actionId, _ ->
		if (actionId == EditorInfo.IME_ACTION_SEND) {
			val query = textView.text?.toString().orEmpty()
			
			if (query.isEmpty()) {
				textView.context.showErrorToast(textView.context.getString(R.string.msg_must_not_be_empty))
			}else {
				sendMessageToApi(textView, query)
			}
			
			return@OnEditorActionListener true
		}
		
		false
	}
	
	fun sendMessageToApi(view: View?, msg: String, fileType: FileType = FileType.IMAGE, uri: Uri? = null) {
		val fragment = view?.findFragment<ChatDetailsFragment>() ?: return
		
		viewModelScope.launch {
			fragment.activityViewModel?.globalLoading?.value = true
			
			val result = if (uri != null) {
				repository.sendChatMessage(args.orderId, args.otherUserId, msg, fileType, uri.createMultipartBodyPart(view.context, "media")!!)
			}else {
				repository.sendChatMessage(args.orderId, args.otherUserId, msg)
			}
			
			when (result) {
				is MAResult.Failure -> {
					fragment.requireContext().showErrorToast(result.message ?: fragment.getString(R.string.something_went_wrong))
					
					fragment.activityViewModel?.globalLoading?.value = false
				}
				is MAResult.Success -> {
					this@ChatDetailsViewModel.msg.value = ""
					
					adapter.refresh()
					
					fragment.activityViewModel?.globalLoading?.value = false
					
					//fragment.binding.recyclerView.scrollToPosition(fragment.binding.recyclerView.adapter!!.itemCount.dec())
					/*fragment.binding.recyclerView.post {
						Timber.e("count s1 -> ${fragment.binding.recyclerView.adapter!!.itemCount.dec()}")
						fragment.binding.recyclerView.scrollToPosition(fragment.binding.recyclerView.adapter!!.itemCount.dec())
					}
					fragment.binding.recyclerView.postDelayed(1_000) {
						Timber.e("count s2 -> ${fragment.binding.recyclerView.adapter!!.itemCount.dec()}")
						fragment.binding.recyclerView.scrollToPosition(fragment.binding.recyclerView.adapter!!.itemCount.dec())
					}*/
					//fragment.binding.recyclerView.scrollBy(0, Int.MAX_VALUE)
					//(fragment.binding.recyclerView.layoutManager as? LinearLayoutManager)?.computeHorizontalScrollOffset()
				}
			}
		}
	}
	
	fun pickImageOrVideo(view: View) {
		view.findFragment<ChatDetailsFragment>().pickImageOrVideoOrRequestPermissions()
	}
	
}
