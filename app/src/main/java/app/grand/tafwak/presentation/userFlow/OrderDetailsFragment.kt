package app.grand.tafwak.presentation.userFlow

import android.content.BroadcastReceiver
import android.content.Context
import android.content.Intent
import android.content.IntentFilter
import android.os.Bundle
import android.view.View
import androidx.fragment.app.viewModels
import androidx.recyclerview.widget.LinearLayoutManager
import app.grand.tafwak.core.BaseNotificationModel
import app.grand.tafwak.data.local.preferences.PrefsUser
import app.grand.tafwak.domain.auth.entity.model.LocationData
import app.grand.tafwak.domain.userFlow.response.OrderStatus
import app.grand.tafwak.domain.utils.MABaseResponse
import app.grand.tafwak.domain.utils.MAResult
import app.grand.tafwak.domain.utils.toSuccessOrNull
import app.grand.tafwak.presentation.base.MABaseFragment
import app.grand.tafwak.presentation.base.extensions.handleRetryAbleFlowWithMustHaveResultWithNullability
import app.grand.tafwak.presentation.userFlow.viewModel.OrderDetailsViewModel
import com.google.android.gms.maps.CameraUpdateFactory
import com.google.android.gms.maps.GoogleMap
import com.google.android.gms.maps.OnMapReadyCallback
import com.google.android.gms.maps.SupportMapFragment
import com.google.android.gms.maps.model.LatLng
import com.google.android.gms.maps.model.MarkerOptions
import com.structure.base_mvvm.R
import com.structure.base_mvvm.databinding.FragmentOrderDetailsBinding
import dagger.hilt.android.AndroidEntryPoint
import timber.log.Timber
import javax.inject.Inject
import kotlin.math.min

@AndroidEntryPoint
class OrderDetailsFragment : MABaseFragment<FragmentOrderDetailsBinding>(), OnMapReadyCallback {
	
	private val viewModel by viewModels<OrderDetailsViewModel>()
	
	@Inject
	protected lateinit var prefsUser: PrefsUser

	private val broadcastReceiver = object : BroadcastReceiver() {
		override fun onReceive(context: Context, intent: Intent) {
			Timber.e("1234 -> onReceive")
			fetchOrderDetailsAndShowData()
		}
	}

	override fun onCreate(savedInstanceState: Bundle?) {
		super.onCreate(savedInstanceState)

		activity?.registerReceiver(
			broadcastReceiver,
			IntentFilter(BaseNotificationModel.Type.NEW_ORDER.apiKey).also {
				it.addAction(BaseNotificationModel.Type.ORDER_PAYMENT.apiKey)
			}
		)
	}

	override fun onDestroy() {
		activity?.unregisterReceiver(broadcastReceiver)

		super.onDestroy()
	}
	
	override fun getLayoutId(): Int = R.layout.fragment_order_details
	
	override fun initializeBindingVariables() {
		binding?.viewModel = viewModel
	}

	override fun onPause() {
		activityViewModel?.showPrintingOrderDetails?.value = null

		super.onPause()
	}
	
	override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
		// RV setups
		binding?.servicesRecyclerView?.layoutManager = LinearLayoutManager(requireContext(), LinearLayoutManager.HORIZONTAL, false)
		binding?.servicesRecyclerView?.adapter = viewModel.adapter
		
		// Observe to show loading
		fetchOrderDetailsAndShowData(true)
	}

	private fun fetchOrderDetailsAndShowData(requestMap: Boolean = false) {
		viewModel.retryAbleFlowDetails.retry()
		handleRetryAbleFlowWithMustHaveResultWithNullability(viewModel.retryAbleFlowDetails) {
			activityViewModel?.showPrintingOrderDetails?.value = if (it.orderStatus == OrderStatus.FINISHED) {
				it
			}else {
				null
			}

			if (requestMap) {
				// Setup map for the image
				(childFragmentManager.findFragmentById(R.id.mapFragmentContainerView) as? SupportMapFragment)
					?.getMapAsync(this)
			}

			viewModel.adapter.submitList(it.services)

			viewModel.details.value = MAResult.Success(MABaseResponse(it, "", 200))
		}
	}
	
	override fun onMapReady(googleMap: GoogleMap) {
		val details = viewModel.details.value?.toSuccessOrNull()?.value?.data ?: return
		val locationData = LocationData(
			details.latitude.toString(),
			details.longitude.toString(),
			details.address
		)
		
		val location = LatLng(locationData.latitude.toDouble(), locationData.longitude.toDouble())
		
		// Add marker
		googleMap.addMarker(MarkerOptions().position(location))
		
		// Center map on the marker
		val zoom = min(googleMap.maxZoomLevel, 15f /* or 4f */)
		googleMap.moveCamera(CameraUpdateFactory.newLatLngZoom(location, zoom))
		
		binding?.mapImageView?.visibility = View.INVISIBLE
	}

}
