package app.grand.tafwak.presentation.userFlow.viewModel

import android.app.Application
import android.graphics.Bitmap
import android.view.View
import androidx.appcompat.content.res.AppCompatResources
import androidx.core.graphics.drawable.toBitmap
import androidx.fragment.app.findFragment
import androidx.lifecycle.*
import androidx.navigation.findNavController
import app.grand.tafwak.core.extensions.*
import app.grand.tafwak.domain.user.entity.model.ProviderMiniDetails
import app.grand.tafwak.domain.user.entity.model.ResponseSearch
import app.grand.tafwak.presentation.userFlow.SearchResultsProvidersFragmentDirections
import app.grand.tafwak.presentation.userFlow.SelectProviderOnMapFragment
import app.grand.tafwak.presentation.userFlow.SelectProviderOnMapFragmentArgs
import app.grand.tafwak.presentation.userFlow.SelectProviderOnMapFragmentDirections
import app.grand.tafwak.presentation.userFlow.adapters.RVItemProviderOnMap
import com.bumptech.glide.Glide
import com.bumptech.glide.request.RequestOptions
import com.google.android.gms.common.api.Status
import com.google.android.gms.maps.CameraUpdateFactory
import com.google.android.gms.maps.model.LatLng
import com.google.android.libraries.places.api.Places
import com.google.android.libraries.places.api.model.Place
import com.google.android.libraries.places.widget.AutocompleteSupportFragment
import com.google.android.libraries.places.widget.listener.PlaceSelectionListener
import com.google.gson.Gson
import com.structure.base_mvvm.R
import com.structure.base_mvvm.databinding.CustomMarkerBinding
import dagger.hilt.android.lifecycle.HiltViewModel
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.launch
import kotlinx.coroutines.withContext
import timber.log.Timber
import javax.inject.Inject
import kotlin.math.roundToInt

@HiltViewModel
class SelectProviderOnMapViewModel @Inject constructor(
	application: Application,
	val args: SelectProviderOnMapFragmentArgs,
	gson: Gson,
) : AndroidViewModel(application), RVItemProviderOnMap.Listener {
	
	private val responseSearch = args.responseSearchAsJson.fromJson<ResponseSearch>(gson)
	
	val title = responseSearch.name
	
	val search = MutableLiveData("")
	
	val showMapNotSearchResults = MutableLiveData(true)
	
	val adapter = RVItemProviderOnMap(this)
	
	var myCurrentLocation: LatLng? = null
	
	/**
	 * [Map.Entry.key] represents [ProviderMiniDetails.id] in [ResponseSearch.providers] & [Bitmap]
	 * represents final bitmap with the custom layout
	 */
	private val _bitmaps = mutableMapOf<ProviderMiniDetails, Bitmap>()
	val bitmaps: Map<ProviderMiniDetails, Bitmap> get() = _bitmaps
	
	init {
		adapter.submitList(responseSearch.providers)
	}
	
	fun goBack(view: View) {
		view.findNavController().navigateUp()
	}
	
	fun toSearchPlace(view: View) {
		showMapNotSearchResults.value = false
		
		val fragment = view.findFragment<SelectProviderOnMapFragment>()
		
		if (!Places.isInitialized()) {
			Places.initialize(view.context.applicationContext, view.context.getString(R.string.google_geo_api_key))
		}
		
		val autocompleteSupportFragment = (fragment.childFragmentManager
			.findFragmentById(R.id.placesFragmentContainerView) as AutocompleteSupportFragment)
		
		autocompleteSupportFragment.setPlaceFields(listOf(
			Place.Field.ID, Place.Field.NAME, Place.Field.ADDRESS, Place.Field.LAT_LNG
		))
		
		autocompleteSupportFragment.setOnPlaceSelectedListener(object : PlaceSelectionListener {
			override fun onPlaceSelected(place: Place) {
				val latLng = LatLng(place.latLng?.latitude ?: 0.0, place.latLng?.longitude ?: 0.0)
				
				fragment.googleMap?.moveCamera(CameraUpdateFactory.newLatLngZoom(latLng, fragment.zoom))
				
				showMapNotSearchResults.postValue(true)
			}
			
			override fun onError(status: Status) {
				Timber.e("Places API error with status $status")
				
				fragment.requireContext().showErrorToast(fragment.getString(R.string.something_went_wrong))
				
				showMapNotSearchResults.postValue(true)
			}
		})
	}
	
	fun moveToCurrentLocation(view: View) {
		val fragment = view.findFragment<SelectProviderOnMapFragment>()

		myCurrentLocation?.also { myCurrentLocation ->
			fragment.googleMap?.moveCamera(
				CameraUpdateFactory.newLatLngZoom(myCurrentLocation, fragment.zoom)
			)
		} ?: fragment.locationHandler.requestCurrentLocation(true)
	}
	
	override fun goToProviderDetails(view: View, id: Int) {
		view.findNavController().navigate(
			SelectProviderOnMapFragmentDirections.actionDestSelectProviderOnMapToDestProviderDetails(id)
		)
	}
	
	suspend fun loadBitmaps() {
		val size = withContext(Dispatchers.Main) {
			app.dpToPx(22f).roundToInt()
		}
		
		for (details in responseSearch.providers) {
			val bitmap = Glide.with(app)
				.asBitmap()
				.load(details.imageUrl)
				.apply(RequestOptions().override(size, size))
				.intoBitmap()
			
			val newBitmap = withContext(Dispatchers.Main) {
				val binding = CustomMarkerBinding.bind(app.inflateLayout(R.layout.custom_marker))
				
				bitmap?.also { binding.imageView.setImageBitmap(bitmap) }
				
				binding.root.toBitmap(app) ?: AppCompatResources.getDrawable(
					app, R.drawable.ic_location_wrapper_of_image
				)!!.toBitmap()
			}
			
			_bitmaps[details] = newBitmap
		}
	}
	
}
