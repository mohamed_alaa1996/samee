package app.grand.tafwak.presentation.base

import android.view.View
import androidx.navigation.fragment.findNavController
import androidx.navigation.fragment.navArgs
import com.structure.base_mvvm.R
import dagger.hilt.android.AndroidEntryPoint

@AndroidEntryPoint
class GlobalBackToUserHomeDialogFragment : BaseSingleButtonDialogFragment() {
	
	companion object {
		const val KEY_FRAGMENT_RESULT_BUTTON_CLICKED = "KEY_FRAGMENT_RESULT_BUTTON_CLICKED"
	}
	
	private val args by navArgs<GlobalBackToUserHomeDialogFragmentArgs>()
	
	override val canceledOnTouchOutside = false
	
	override val message: String by lazy {
		args.message
	}
	
	override val buttonText: String by lazy {
		args.buttonText ?: getString(R.string.back_to_main_page)
	}
	
	override val drawableRes: Int by lazy {
		args.drawableRes
	}
	
	override fun onButtonClick(view: View) {
		val navController = findNavController()
		
		navController.navigateUp()
		
		navController.currentBackStackEntry?.savedStateHandle?.set(
			KEY_FRAGMENT_RESULT_BUTTON_CLICKED,
			true
		)
	}
}
