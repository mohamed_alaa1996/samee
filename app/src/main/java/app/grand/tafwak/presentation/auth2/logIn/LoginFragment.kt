package app.grand.tafwak.presentation.auth2.logIn

import android.app.Activity
import android.location.Location
import android.os.Bundle
import android.view.View
import androidx.activity.result.contract.ActivityResultContracts
import androidx.fragment.app.activityViewModels
import androidx.fragment.app.viewModels
import androidx.lifecycle.viewModelScope
import androidx.navigation.NavOptions
import androidx.navigation.fragment.findNavController
import app.grand.tafwak.core.customTypes.LocationHandler
import app.grand.tafwak.core.extensions.getAddressFromLatitudeAndLongitude
import app.grand.tafwak.core.extensions.observeSavedStateHandleResult
import app.grand.tafwak.core.extensions.observeSavedStateHandleResultForStringEmptyOrNull
import app.grand.tafwak.core.extensions.showErrorToast
import app.grand.tafwak.data.local.preferences.PrefsSplash
import app.grand.tafwak.data.samee.SameeAPI
import app.grand.tafwak.domain.auth.entity.model.LocationData
import app.grand.tafwak.domain.splash.entity.SplashInitialLaunch
import app.grand.tafwak.presentation.auth2.logIn.viewModels.LoginViewModel
import app.grand.tafwak.presentation.auth2.referral.CheckReferralCodeDialogFragment
import app.grand.tafwak.presentation.base.MABaseFragment
import app.grand.tafwak.presentation.base.extensions.handleRetryAbleFlowWithMustHaveResultWithNullability
import app.grand.tafwak.presentation.project.viewModel.ProjectViewModel
import app.grand.tafwak.presentation.userHome.UserFragmentArgs
import com.google.android.gms.auth.api.signin.GoogleSignIn
import com.google.android.gms.common.api.ApiException
import com.structure.base_mvvm.R
import com.structure.base_mvvm.databinding.FragmentLogIn2Binding
import dagger.hilt.android.AndroidEntryPoint
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.launch
import kotlinx.coroutines.withContext
import timber.log.Timber
import javax.inject.Inject

@AndroidEntryPoint
class LoginFragment : MABaseFragment<FragmentLogIn2Binding>(), LocationHandler.Listener {
	
	companion object {
		const val REQUEST_CODE_GOOGLE_SIGN_IN = 920
	}

	lateinit var locationHandler: LocationHandler

	private val viewModel by viewModels<LoginViewModel>()
	
	val activityResultGoogleSignIn = registerForActivityResult(
		ActivityResultContracts.StartActivityForResult()
	) {
		Timber.e("it.resultCode == Activity.RESULT_OK ${it.resultCode} == ${Activity.RESULT_OK}")

		try {
			val task = GoogleSignIn.getSignedInAccountFromIntent(it.data)

			val account = task.getResult(ApiException::class.java)

			val id = account.id!!

			Timber.e("id is $id")

			viewModel.performSocialLoginWithApi(this, id)
		}catch (throwable: Throwable) {
			Timber.e("Can't login with google -> $throwable")

			requireContext().showErrorToast(getString(R.string.something_went_wrong))
		}
	}

	override fun onCreate(savedInstanceState: Bundle?) {
		locationHandler = LocationHandler(
			this,
			lifecycle,
			requireContext(),
			this
		)

		super.onCreate(savedInstanceState)
	}

	override fun getLayoutId(): Int = R.layout.fragment_log_in_2
	
	override fun initializeBindingVariables() {
		binding?.viewModel = viewModel
	}
	
	override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
		findNavController().currentBackStackEntry?.savedStateHandle?.getLiveData(
			FetchPhoneDialogFragment.KEY_FRAGMENT_RESULT_PHONE,
			""
		)?.observe(viewLifecycleOwner) { phone ->
			if (!phone.isNullOrEmpty()) {
				findNavController().currentBackStackEntry?.savedStateHandle?.set(
					FetchPhoneDialogFragment.KEY_FRAGMENT_RESULT_PHONE,
					""
				)
				
				viewModel.onPhoneSuccessForSocialRegistration(this, phone)
			}
		}

		observeSavedStateHandleResultForStringEmptyOrNull(
			CheckReferralCodeDialogFragment.KEY_FRAGMENT_RESULT_REFERRAL_CODE,
		) { code ->
			viewModel.referralCode = code
		}
	}
	
	override fun onResume() {
		super.onResume()
		
		val uri = activity?.intent?.data
		val oauthToken = uri?.getQueryParameter(SameeAPI.Query.OAUTH_TOKEN)
		val oauthVerifier = uri?.getQueryParameter(SameeAPI.Query.OAUTH_VERIFIER)
		
		if (!oauthToken.isNullOrEmpty() && !oauthVerifier.isNullOrEmpty()) {
			binding?.root?.post {
				activity?.intent?.data = null
				
				viewModel.handleTwitterCallbackSuccess(this, oauthToken, oauthVerifier)
			}
		}
	}

	override fun onCurrentLocationResultSuccess(location: Location) {
		viewModel.viewModelScope.launch {
			val address = withContext(Dispatchers.IO) {
				context?.getAddressFromLatitudeAndLongitude(
					location.latitude,
					location.longitude
				)
			} ?: return@launch

			viewModel.prefsUser.setLocation(LocationData(
				location.latitude.toString(),
				location.longitude.toString(),
				address,
			))

      prefsSplash.setInitialLaunch(SplashInitialLaunch.USER)

      val options = NavOptions.Builder()
				.setEnterAnim(R.anim.anim_slide_in_right)
				.setExitAnim(R.anim.anim_slide_out_left)
				.setPopEnterAnim(R.anim.anim_slide_in_left)
				.setPopExitAnim(R.anim.anim_slide_out_right)
				.build()

			findNavController().navigate(
				R.id.nav_user,
				UserFragmentArgs(viewModel.isGuest).toBundle(),
				options
			)
		}
	}

}
