package app.grand.tafwak.presentation.project.viewModel

import android.view.inputmethod.EditorInfo
import android.widget.TextView
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel
import androidx.lifecycle.lifecycleScope
import androidx.navigation.fragment.findNavController
import app.grand.tafwak.core.extensions.showErrorToast
import app.grand.tafwak.data.provider.repository.ProviderRepositoryImpl
import app.grand.tafwak.data.user.repository.UserRepositoryImpl
import app.grand.tafwak.domain.utils.MAResult
import app.grand.tafwak.presentation.project.NotificationsFragment
import app.grand.tafwak.presentation.project.adapters.RVItemConversation
import app.grand.tafwak.presentation.project.adapters.RVItemNotification
import com.structure.base_mvvm.R
import dagger.hilt.android.AndroidEntryPoint
import dagger.hilt.android.lifecycle.HiltViewModel
import kotlinx.coroutines.ExperimentalCoroutinesApi
import kotlinx.coroutines.flow.MutableStateFlow
import kotlinx.coroutines.flow.flatMapLatest
import kotlinx.coroutines.launch
import javax.inject.Inject

@HiltViewModel
class NotificationsViewModel @Inject constructor(
	repository: ProviderRepositoryImpl,
	private val userRepository: UserRepositoryImpl
) : ViewModel() {
	
	val notifications = repository.getNotifications()
	
	val adapter = RVItemNotification()
	
	fun cancelOrder(fragment: NotificationsFragment, id: Int) {
		fragment.lifecycleScope.launch {
			fragment.activityViewModel?.globalLoading?.value = true
			
			val result = userRepository.cancelOrder(id)
			
			fragment.activityViewModel?.globalLoading?.value = false
			
			when (result) {
				is MAResult.Success -> {
					adapter.refresh()
				}
				is MAResult.Failure -> {
					fragment.requireContext().showErrorToast(result.message ?: fragment.getString(R.string.something_went_wrong))
				}
			}
		}
	}
	
}
