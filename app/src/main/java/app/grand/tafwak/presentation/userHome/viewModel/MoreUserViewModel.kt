package app.grand.tafwak.presentation.userHome.viewModel

import android.app.Application
import android.net.Uri
import android.view.View
import androidx.fragment.app.findFragment
import androidx.lifecycle.AndroidViewModel
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel
import androidx.navigation.*
import app.grand.tafwak.core.extensions.*
import app.grand.tafwak.data.auth.repository.MAAuthRepositoryImpl
import app.grand.tafwak.data.local.preferences.PrefsSplash
import app.grand.tafwak.data.local.preferences.PrefsUser
import app.grand.tafwak.domain.splash.entity.SplashInitialLaunch
import app.grand.tafwak.presentation.base.customTypes.DataOfInfoImageAndListOfTexts
import app.grand.tafwak.presentation.userHome.MoreUserFragment
import app.grand.tafwak.presentation.userHome.MoreUserFragmentArgs
import app.grand.tafwak.presentation.userHome.UserFragmentDirections
import app.grand.tafwak.presentation.userHome.adapters.RVIconText
import com.structure.base_mvvm.R
import dagger.hilt.android.lifecycle.HiltViewModel
import javax.inject.Inject

@HiltViewModel
class MoreUserViewModel @Inject constructor(
	application: Application,
	val args: MoreUserFragmentArgs,
	private val authRepository: MAAuthRepositoryImpl,
	private val prefsSplash: PrefsSplash,
) : AndroidViewModel(application), RVIconText.Listener {
	
	val adapter = RVIconText(this)
	
	val performLogOut = MutableLiveData(false)
	val responseLogOut = performLogOut.switchMap {
		if (it == true) {
			// todo tell api backend to make it empty - able isa. + make it in the handle api extension function
			//  also note kda in the cases where u don't uses handle ext fun u need to check token expired ya zaki isa.
			authRepository.setFirebaseToken("null", app.getDeviceIdWithoutPermission())
		}else {
			MutableLiveData()
		}
	}
	
	override fun onItemIconTextClick(view: View, text: String) {
		val fragment = view.findFragment<MoreUserFragment>()
		val isUser = args.isUserNotProvider
		
		val data = view.context.run {
			when (text) {
				getString(R.string.about) -> {
					if (isUser) {
						DataOfInfoImageAndListOfTexts.USER_ABOUT
					}else {
						DataOfInfoImageAndListOfTexts.PROVIDER_ABOUT
					}
				}
				getString(R.string.terms_and_conditions) -> {
					if (isUser) {
						DataOfInfoImageAndListOfTexts.USER_TERMS_AND_CONDITIONS
					}else {
						DataOfInfoImageAndListOfTexts.PROVIDER_TERMS_AND_CONDITIONS
					}
				}
				getString(R.string.privacy) -> {
					if (isUser) {
						DataOfInfoImageAndListOfTexts.USER_PRIVACY_POLICY
					}else {
						DataOfInfoImageAndListOfTexts.PROVIDER_PRIVACY_POLICY
					}
				}
				getString(R.string.suggestions_and_complaints) -> {
					val uri = Uri.Builder()
						.scheme("fragment-dest")
						.authority("app.grand.tafwak.dest.suggestions.and.complaints")
						.build()
					val request = NavDeepLinkRequest.Builder.fromUri(uri).build()
					val options = NavOptions.Builder()
						.setEnterAnim(R.anim.anim_slide_in_right)
						.setExitAnim(R.anim.anim_slide_out_left)
						.setPopEnterAnim(R.anim.anim_slide_in_left)
						.setPopExitAnim(R.anim.anim_slide_out_right)
						.build()
					fragment.findNavControllerOfProject().navigate(request, options)
					
					return
				}
				getString(R.string.share_app) -> {
					launchShareText(
						"${getString(R.string.app_name)}\n${getAppWebLinkOnGooglePay()}"
					)
					
					return
				}
				getString(R.string.rate_app) -> {
					launchAppOnGooglePlay()
					
					return
				}
				getString(R.string.contact_us_on_telegram) -> {
					// todo el mafrod byedek number on telegram lel tawasol aw channel isa.
					launchTelegram()
					
					return
				}
				getString(R.string.log_out) -> {
					performLogOut.value = true
					
					return
				}
				else -> return
			}
		}
		
		val uri = Uri.Builder()
			.scheme("fragment-dest")
			.authority("app.grand.tafwak.global.info.app")
			.appendPath(data.name)
			.build()
		val request = NavDeepLinkRequest.Builder.fromUri(uri).build()
		val options = NavOptions.Builder()
			.setEnterAnim(R.anim.anim_slide_in_right)
			.setExitAnim(R.anim.anim_slide_out_left)
			.setPopEnterAnim(R.anim.anim_slide_in_left)
			.setPopExitAnim(R.anim.anim_slide_out_right)
			.build()
		fragment.findNavControllerOfProject().navigate(request, options)
	}
	
	suspend fun onLogOutSuccess(navController: NavController) {
		prefsSplash.setInitialLaunch(SplashInitialLaunch.LOGIN)
		
		prefsSplash.prefsUser.logOut()
		
		while (navController.popBackStack()) {
			continue
		}
		
		val uri = Uri.Builder()
			.scheme("fragment-dest-auth")
			.authority("app.grand.tafwak.global.log.in")
			.build()
		val request = NavDeepLinkRequest.Builder.fromUri(uri).build()
		val options = NavOptions.Builder()
			.setEnterAnim(R.anim.anim_slide_in_right)
			.setExitAnim(R.anim.anim_slide_out_left)
			.setPopEnterAnim(R.anim.anim_slide_in_left)
			.setPopExitAnim(R.anim.anim_slide_out_right)
			.build()
		navController.navigate(request, options)
	}
	
}
