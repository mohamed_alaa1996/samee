package app.grand.tafwak.presentation.base

import android.graphics.drawable.ColorDrawable
import android.view.View
import android.view.Window
import androidx.fragment.app.activityViewModels
import androidx.navigation.fragment.findNavController
import androidx.navigation.fragment.navArgs
import app.grand.tafwak.presentation.base.customTypes.GlobalError
import app.grand.tafwak.presentation.project.viewModel.ProjectViewModel
import com.structure.base_mvvm.R
import dagger.hilt.android.AndroidEntryPoint

@AndroidEntryPoint
class GlobalErrorDialogFragment : BaseOkCancelDialogFragment() {
	
	override val canceledOnTouchOutside = false
	
	private val args by navArgs<GlobalErrorDialogFragmentArgs>()
	
	override val okButtonText: String by lazy {
		requireContext().getString(R.string.retry)
	}
	
	override val cancelButtonText: String by lazy {
		if (args.cancellable) requireContext().getString(R.string.cancel) else requireContext().getString(R.string.back)
	}
	
	override val message: String by lazy {
		args.message
	}
	
	override fun onCreateDialogWindowChanges(window: Window) {
		super.onCreateDialogWindowChanges(window)
		
		window.attributes?.windowAnimations = R.style.ScaleDialogAnim
	}
	
	override fun onOkClick(view: View) {
		activityViewModel?.globalError?.value = GlobalError.Retry
		
		findNavController().navigateUp()
	}
	
	override fun onCancelClick(view: View) {
		activityViewModel?.globalError?.value = GlobalError.Cancel

		findNavController().navigateUp()
		if (!args.cancellable) {
			findNavController().navigateUp()
		}
	}
	
	override fun onDismissListener() {
		activityViewModel?.globalError?.value = GlobalError.Cancel
	}
	
}
