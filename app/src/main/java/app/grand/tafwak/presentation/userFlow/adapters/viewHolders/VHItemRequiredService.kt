package app.grand.tafwak.presentation.userFlow.adapters.viewHolders

import android.view.ViewGroup
import androidx.recyclerview.widget.RecyclerView
import app.grand.tafwak.core.extensions.inflateLayout
import app.grand.tafwak.domain.user.entity.model.FileType
import app.grand.tafwak.domain.user.entity.model.ProviderService
import app.grand.tafwak.presentation.userFlow.adapters.RVItemSelectableService
import com.bumptech.glide.Glide
import com.bumptech.glide.request.RequestOptions
import com.structure.base_mvvm.R
import com.structure.base_mvvm.databinding.ItemRequiredServiceBinding

class VHItemRequiredService(parent: ViewGroup) : RecyclerView.ViewHolder(
	parent.context.inflateLayout(R.layout.item_required_service, parent)
) {
	
	private val binding = ItemRequiredServiceBinding.bind(itemView)
	
	fun bind(details: ProviderService) {
		binding.serviceTextView.text = details.name
		
		binding.priceTextView.text = binding.priceTextView.context.getString(R.string.num_sar, details.price)
		
		val options = RequestOptions().let {
			if (details.isImage) it else it.frame(1)
		}.centerCrop()
		Glide.with(binding.imageView.context)
			.load(details.imageOrVideoUrl)
			.apply(options)
			.placeholder(R.drawable.ic_logo_samee_placeholder)
			.error(R.drawable.ic_logo_samee_placeholder)
			.into(binding.imageView)
	}
	
}
