package app.grand.tafwak.presentation.userFlow.adapters.viewHolders

import android.net.Uri
import android.view.ViewGroup
import androidx.core.view.isVisible
import androidx.navigation.NavDeepLinkRequest
import androidx.navigation.findNavController
import androidx.recyclerview.widget.RecyclerView
import app.grand.tafwak.core.extensions.inflateLayout
import app.grand.tafwak.domain.user.entity.model.FileType
import app.grand.tafwak.domain.user.entity.model.PreviousWork
import com.bumptech.glide.Glide
import com.bumptech.glide.request.RequestOptions
import com.structure.base_mvvm.R
import com.structure.base_mvvm.databinding.ItemImageRoundedInCardBinding
import timber.log.Timber

class VHItemImageRoundedInCard(parent: ViewGroup) : RecyclerView.ViewHolder(
	parent.context.inflateLayout(R.layout.item_image_rounded_in_card, parent)
) {
	
	private val binding = ItemImageRoundedInCardBinding.bind(itemView)
	
	init {
		binding.materialCardView.setOnClickListener {
			val isImage = binding.materialCardView.tag as? Boolean ?: return@setOnClickListener
			val url = binding.frameLayout.tag as? String ?: return@setOnClickListener
			
			if (isImage) {
				val uri = Uri.Builder()
					.scheme("dialog-dest")
					.authority("app.grand.tafwak.global.showing.image.dialog")
					.appendPath(url)
					.build()
				val request = NavDeepLinkRequest.Builder.fromUri(uri).build()
				
				it.findNavController().navigate(request)
			}else {
				val uri = Uri.Builder()
					.scheme("dialog-dest")
					.authority("app.grand.tafwak.global.video.player.dialog")
					.appendPath(url)
					.build()
				val request = NavDeepLinkRequest.Builder.fromUri(uri).build()
				
				it.findNavController().navigate(request)
			}
		}
	}
	
	fun bind(details: PreviousWork) {
		val isImage = details.fileType == FileType.IMAGE.apiKey
		binding.materialCardView.tag = isImage
		binding.frameLayout.tag = details.imageOrVideoUrl
		
		val options = RequestOptions().let {
			if (isImage) it else it.frame(1)
		}.centerCrop()
		Glide.with(binding.imageView.context)
			.load(details.imageOrVideoUrl)
			.apply(options)
			.placeholder(R.drawable.ic_logo_samee_placeholder)
			.error(R.drawable.ic_logo_samee_placeholder)
			.into(binding.imageView)
		
		binding.videoIndicatorImageView.isVisible = !isImage
	}
	
}
