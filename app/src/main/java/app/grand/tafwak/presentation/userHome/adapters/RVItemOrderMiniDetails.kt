package app.grand.tafwak.presentation.userHome.adapters

import android.view.ViewGroup
import androidx.paging.PagingDataAdapter
import androidx.recyclerview.widget.DiffUtil
import app.grand.tafwak.domain.userFlow.response.OrderMiniDetails
import app.grand.tafwak.presentation.userHome.adapters.viewHolders.VHItemOrderMiniDetails
import com.google.gson.Gson

class RVItemOrderMiniDetails(private val gson: Gson) : PagingDataAdapter<OrderMiniDetails, VHItemOrderMiniDetails>(COMPARATOR) {
	
	companion object {
		val COMPARATOR = object : DiffUtil.ItemCallback<OrderMiniDetails>() {
			override fun areItemsTheSame(
				oldItem: OrderMiniDetails,
				newItem: OrderMiniDetails
			): Boolean = oldItem.id == newItem.id
			
			override fun areContentsTheSame(
				oldItem: OrderMiniDetails,
				newItem: OrderMiniDetails
			): Boolean = oldItem == newItem
		}
	}
	
	override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): VHItemOrderMiniDetails {
		return VHItemOrderMiniDetails(parent, gson)
	}
	
	override fun onBindViewHolder(holder: VHItemOrderMiniDetails, position: Int) {
		// Ignore null value as I will not use placeholders isa.
		holder.bind(getItem(position) ?: return)
	}

}
