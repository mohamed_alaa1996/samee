package app.grand.tafwak.presentation.project

import android.app.NotificationManager
import android.content.Context
import android.content.Intent
import android.graphics.Color
import android.graphics.drawable.ColorDrawable
import android.location.Location
import android.net.Uri
import android.view.View
import androidx.activity.viewModels
import androidx.appcompat.content.res.AppCompatResources
import androidx.appcompat.graphics.drawable.DrawerArrowDrawable
import androidx.core.content.getSystemService
import androidx.fragment.app.findFragment
import androidx.lifecycle.distinctUntilChanged
import androidx.navigation.NavDeepLinkRequest
import androidx.navigation.fragment.NavHostFragment
import androidx.navigation.ui.AppBarConfiguration
import app.grand.tafwak.core.customTypes.LocationHandler
import app.grand.tafwak.core.extensions.*
import app.grand.tafwak.domain.userFlow.model.MenuItemVisibility
import app.grand.tafwak.presentation.base.MABaseActivity
import app.grand.tafwak.presentation.base.customTypes.GlobalError
import app.grand.tafwak.presentation.base.extensions.executeShowingErrorOnce
import app.grand.tafwak.presentation.project.viewModel.ProjectViewModel
import app.grand.tafwak.presentation.userFlow.SearchResultsProvidersFragmentArgs
import com.facebook.FacebookCallback
import com.facebook.FacebookException
import com.facebook.FacebookSdk
import com.facebook.login.LoginManager
import com.facebook.login.LoginResult
import com.structure.base_mvvm.R
import com.structure.base_mvvm.databinding.ActivityProjectBinding
import dagger.hilt.android.AndroidEntryPoint
import timber.log.Timber
import java.lang.Exception

@AndroidEntryPoint
class ProjectActivity : MABaseActivity<ActivityProjectBinding>(), LocationHandler.Listener, ProjectViewModel.Listener {

	companion object {
		fun onGoingNotificationExists(context: Context): Boolean {
			val count = context.getSystemService<NotificationManager>()?.activeNotifications
				?.count { it.isOngoing }.orZero()

			return count > 0
		}
	}
	
	val viewModel by viewModels<ProjectViewModel>()
	
	private val destinationsHideToolbar by lazy {
		setOf(
			R.id.dest_splash,
			R.id.dest_log_in,
			R.id.dest_location_selection,
			R.id.dest_select_provider_on_map,
			R.id.dest_location_tracking,
			R.id.dest_check_referral_code_dialog,
		)
	}
	
	private val destinationsIgnoreToolbarVisibility by lazy {
		setOf(
			R.id.dest_user,
			R.id.dest_provider,

			R.id.dest_global_error_dialog,
			R.id.dest_lottie_loader_dialog,
			R.id.dest_showing_image_dialog,
			
			R.id.dest_filter_picker_dialog,
		)
	}
	
	private val topLevelDestinations by lazy {
		setOf(
			R.id.dest_splash,
			R.id.dest_log_in,
			R.id.dest_user,
			R.id.dest_provider,
		)
	}
	
	private val darkToolbarDestinations by lazy {
		setOf(
			R.id.dest_search_query,
			R.id.dest_search_results_providers,
			R.id.dest_filter_picker_dialog,
		)
	}
	
	val mainContainer: View
		get() = binding.constraintLayout

	private lateinit var locationHandler: LocationHandler

	override fun getLayoutId(): Int = R.layout.activity_project
	
	override fun initializeBindingVariables() {
		binding.viewModel = viewModel
	}
	
	override fun setupsInOnCreate() {
		locationHandler = LocationHandler(
			this,
			lifecycle,
			this,
			this
		)

		// Support action bar so that fragments have easier access for toolbar menu items.
		setSupportActionBar(binding.materialToolbar)
		supportActionBar?.setDisplayHomeAsUpEnabled(false)
		supportActionBar?.setDisplayShowTitleEnabled(false)
		
		// Setups
		val navHostFragment = supportFragmentManager
			.findFragmentById(R.id.mainNavHostFragment) as NavHostFragment
		val navController = navHostFragment.navController
		binding.materialToolbar.setupWithNavControllerMA(
			navController,
			AppBarConfiguration(topLevelDestinations)
		)
		
		navController.addOnDestinationChangedListener { _, destination, arguments ->
			run {
				if (destination.id == R.id.dest_provider_registration) {
					binding.materialToolbar.setNavigationOnClickListener {
						super.onBackPressed()
					}
				}else {
					binding.materialToolbar.setupWithNavControllerMA(
						navController,
						AppBarConfiguration(topLevelDestinations)
					)
				}

				if (destination.id !in destinationsIgnoreToolbarVisibility) {
					viewModel.showToolbar.value = destination.id !in destinationsHideToolbar
				}
				destination.label?.toString().orEmpty().also {
					if (it.isNotEmpty()) {
						viewModel.titleToolbar.postValue(it)
					}
				}
				
				if (destination.id !in topLevelDestinations) {
					val forceNormalToolbar = destination.id == R.id.dest_search_results_providers
						&& SearchResultsProvidersFragmentArgs.fromBundle(arguments.orEmpty()).categoryName != null
					val (color, drawable) = if (destination.id in darkToolbarDestinations && !forceNormalToolbar) {
						getColor(R.color.black) to {
							ColorDrawable(getColor(R.color.white))
						}
					}else {
						getColor(R.color.white) to {
							AppCompatResources.getDrawable(this, R.drawable.dr_project_gradient)!!
						}
					}
					binding.materialToolbar.post {
						(binding.materialToolbar.navigationIcon as? DrawerArrowDrawable)?.color = color
						binding.materialToolbar.setTitleTextColor(color)
						binding.materialToolbar.background = drawable()
						binding.aboveToolbarView.background = drawable()
						binding.belowToolbarView.background = drawable()
					}
				}
				
				viewModel.menuItemNotificationsVisibility.value = MenuItemVisibility.HIDE
				viewModel.menuItemConversationsVisibility.value = MenuItemVisibility.HIDE
			}
		}
		
		viewModel.globalLoading.distinctUntilChanged().observe(this) {
			if (it == true) {
				val uri = Uri.Builder()
					.scheme("dialog-dest")
					.authority("app.grand.tafwak.lottie.loader.dialog")
					.build()
				val request = NavDeepLinkRequest.Builder.fromUri(uri).build()
				navController.navigate(request)
			}
		}
		
		viewModel.globalError.distinctUntilChanged().observe(this) {
			if (it is GlobalError.Show) {
				if (it.canCancelDialog) {
					navController.navigateDeepLinkWithoutOptions(
						"dialog-dest",
						"app.grand.tafwak.global.error.dialog.cancellable",
						if (it.errorMsg.isNullOrEmpty()) getString(R.string.something_went_wrong) else it.errorMsg,
						true.toString()
					)
				}else {
					val uri = Uri.Builder()
						.scheme("dialog-dest")
						.authority("app.grand.tafwak.global.error.dialog")
						.appendPath(if (it.errorMsg.isNullOrEmpty()) getString(R.string.something_went_wrong) else it.errorMsg)
						.build()
					val request = NavDeepLinkRequest.Builder.fromUri(uri).build()
					navController.navigate(request)
				}
			}
		}

		binding.root.post {
			viewModel.checkIfShouldRequestLocationUpdates(this)
		}
	}

	fun trackOrders(ordersIds: List<Int>) {
		if (ordersIds.isEmpty()) {
			return
		}

		viewModel.startTrackingOrders(ordersIds, this)
	}

	override fun onDestroy() {
		stopLocationTracking()

		super.onDestroy()
	}

	override fun startLocationTrackingAfterCheckingPermissions() {
		if (viewModel.locationIsBeingTracked) {
			return
		}else {
			viewModel.locationIsBeingTracked = true
		}

		if (::locationHandler.isInitialized) {
			locationHandler.requestLocationUpdates()
		}
	}

	override fun stopLocationTracking() {
		Timber.e("abc333444444444444 stopLocationTracking")

		viewModel.locationIsBeingTracked = false

		if ((::locationHandler.isInitialized)) {
			locationHandler.stopLocationUpdates()
		}
	}

	override fun onChangeLocationSuccess(location: Location) {
		Timber.e("abc333444444444444 your_location_is_being_tracked")
		//showSuccessToast(getString(com.maproductions.mohamedalaa.shared.R.string.your_location_is_being_tracked))

		viewModel.afterLocationChange(location)
	}

	override fun onChangeLocationFailure(context: Context?, exception: Exception?) {
		executeShowingErrorOnce(
			false,
			getString(R.string.there_is_an_error_in_detecting_your_location)
		) {
			if ((::locationHandler.isInitialized)) {
				locationHandler.requestLocationUpdates()
			}
		}
	}
	
}
