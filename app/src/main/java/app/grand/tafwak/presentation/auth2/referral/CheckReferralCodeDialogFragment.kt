package app.grand.tafwak.presentation.auth2.referral

import android.graphics.drawable.ColorDrawable
import android.os.Bundle
import android.view.View
import android.view.Window
import androidx.annotation.CallSuper
import androidx.core.content.ContextCompat
import androidx.fragment.app.viewModels
import androidx.navigation.fragment.findNavController
import app.grand.tafwak.core.extensions.showErrorToast
import app.grand.tafwak.presentation.auth2.referral.viewModel.CheckReferralCodeViewModel
import app.grand.tafwak.presentation.base.MADialogFragment
import com.structure.base_mvvm.R
import com.structure.base_mvvm.databinding.DialogFragmentCheckReferralCodeBinding
import dagger.hilt.android.AndroidEntryPoint

@AndroidEntryPoint
class CheckReferralCodeDialogFragment : MADialogFragment<DialogFragmentCheckReferralCodeBinding>() {

	companion object {
		const val KEY_FRAGMENT_RESULT_REFERRAL_CODE = "KEY_FRAGMENT_RESULT_REFERRAL_CODE"
		const val KEY_FRAGMENT_RESULT_REFERRED_BY_ID = "KEY_FRAGMENT_RESULT_REFERRED_BY_ID"
	}

	override fun getLayoutId(): Int = R.layout.dialog_fragment_check_referral_code

	override val heightIsMatchParent: Boolean = true

	private val viewModel by viewModels<CheckReferralCodeViewModel>()

	@CallSuper
	override fun onCreateDialogWindowChanges(window: Window) {
		window.setBackgroundDrawable(
			ColorDrawable(ContextCompat.getColor(requireContext(), R.color.dialog_background_window_primary))
		)
	}

	override fun initializeBindingVariables() {
		binding.viewModel = viewModel
	}

	override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
		binding.frameLayout.setOnClickListener {
			findNavController().navigateUp()
		}

		binding.materialButton.setOnClickListener {
			val text = binding.editText.text?.toString().orEmpty()

			if (text.isEmpty()) {
				requireContext().showErrorToast(getString(R.string.field_required))

				return@setOnClickListener
			}
		}
	}

}
