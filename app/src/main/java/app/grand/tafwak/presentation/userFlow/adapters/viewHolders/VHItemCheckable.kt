package app.grand.tafwak.presentation.userFlow.adapters.viewHolders

import android.text.Spanned
import android.text.style.ForegroundColorSpan
import android.view.ViewGroup
import androidx.core.text.buildSpannedString
import androidx.recyclerview.widget.RecyclerView
import app.grand.tafwak.core.extensions.inflateLayout
import app.grand.tafwak.domain.userFlow.response.WalletItem
import app.grand.tafwak.presentation.userFlow.adapters.RVItemCheckable
import com.structure.base_mvvm.R
import com.structure.base_mvvm.databinding.ItemCheckableBinding
import com.structure.base_mvvm.databinding.ItemWalletBinding

class VHItemCheckable(parent: ViewGroup, private val listener: RVItemCheckable.Listener) : RecyclerView.ViewHolder(
	parent.context.inflateLayout(R.layout.item_checkable, parent)
) {
	
	private val binding = ItemCheckableBinding.bind(itemView)
	
	init {
		binding.textView.setOnClickListener {
			val id = binding.textView.tag as? Int ?: return@setOnClickListener
			val adapterPosition = binding.textView.getTag(R.id.view_holder_id) as? Int ?: return@setOnClickListener
			
			listener.toggleCheckState(id, adapterPosition)
		}
	}
	
	fun bind(id: Int, text: String, adapterPosition: Int) {
		binding.textView.tag = id
		binding.textView.setTag(R.id.view_holder_id, adapterPosition)
	
		binding.textView.text = text
		
		val res = if (listener.isChecked(id)) R.drawable.ic_checked else R.drawable.ic_not_checked
		binding.textView.setCompoundDrawablesRelativeWithIntrinsicBounds(0, 0, res, 0)
	}
	
}
