package app.grand.tafwak.presentation.base

import android.graphics.drawable.InsetDrawable
import android.os.Bundle
import android.view.View
import android.view.Window
import androidx.annotation.CallSuper
import androidx.appcompat.content.res.AppCompatResources
import androidx.core.view.isVisible
import app.grand.tafwak.core.extensions.dpToPx
import com.structure.base_mvvm.R
import com.structure.base_mvvm.databinding.DialogFragmentBaseSingleButtonBinding
import kotlin.math.roundToInt

abstract class BaseSingleButtonDialogFragment : MADialogFragment<DialogFragmentBaseSingleButtonBinding>() {
	
	final override fun getLayoutId(): Int = R.layout.dialog_fragment_base_single_button
	
	@CallSuper
	override fun onCreateDialogWindowChanges(window: Window) {
		val drawable = InsetDrawable(
			AppCompatResources.getDrawable(requireContext(), R.drawable.border_white_16),
			requireContext().dpToPx(16f).roundToInt()
		)
		window.setBackgroundDrawable(drawable)
	}
	
	final override fun initializeBindingVariables() {}
	
	override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
		binding.imageView.isVisible = drawableRes != 0
		if (drawableRes != 0) {
			binding.imageView.setImageResource(drawableRes)
		}
		
		binding.messageTextView.text = message
		
		binding.materialButton.text = buttonText
		binding.materialButton.setOnClickListener(::onButtonClick)
	}
	
	open val drawableRes: Int = 0
	
	abstract val buttonText: String
	
	abstract val message: String
	
	abstract fun onButtonClick(view: View)
	
}
