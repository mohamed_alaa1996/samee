package app.grand.tafwak.presentation.project.viewModel

import android.view.inputmethod.EditorInfo
import android.widget.TextView
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel
import androidx.paging.map
import app.grand.tafwak.data.auth.repository.MAAuthRepositoryImpl
import app.grand.tafwak.data.local.preferences.PrefsUser
import app.grand.tafwak.data.provider.repository.ProviderRepositoryImpl
import app.grand.tafwak.presentation.project.adapters.RVItemConversation
import dagger.hilt.android.lifecycle.HiltViewModel
import kotlinx.coroutines.ExperimentalCoroutinesApi
import kotlinx.coroutines.flow.MutableStateFlow
import kotlinx.coroutines.flow.first
import kotlinx.coroutines.flow.flatMapLatest
import kotlinx.coroutines.flow.map
import javax.inject.Inject

@HiltViewModel
class ConversationViewModel @Inject constructor(
	repositoryImpl: ProviderRepositoryImpl ,
 val prefUser : PrefsUser
) : ViewModel() {

	val query = MutableLiveData("")
	
	private val apiSearch = MutableStateFlow("")
	
	@OptIn(ExperimentalCoroutinesApi::class)
	val conversations = apiSearch.flatMapLatest {query ->
		repositoryImpl.getConversations(query)
	}
	


	fun onSearchTextSubmit() = TextView.OnEditorActionListener { textView, actionId, _ ->
		if (actionId == EditorInfo.IME_ACTION_SEARCH) {
			val query = textView.text?.toString().orEmpty()
			
			apiSearch.value = query
			
			this.query.postValue("")
			
			return@OnEditorActionListener true
		}
		
		false
	}
	
}
