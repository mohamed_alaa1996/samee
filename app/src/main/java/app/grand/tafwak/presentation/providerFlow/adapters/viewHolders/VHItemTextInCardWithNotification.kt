package app.grand.tafwak.presentation.providerFlow.adapters.viewHolders

import android.graphics.drawable.ColorDrawable
import android.view.View
import android.view.ViewGroup
import androidx.appcompat.content.res.AppCompatResources
import androidx.recyclerview.widget.RecyclerView
import app.grand.tafwak.core.extensions.inflateLayout
import app.grand.tafwak.domain.providerFlow.CategoryWithOrders
import app.grand.tafwak.domain.userFlow.response.CategoryWithWorkings
import app.grand.tafwak.presentation.providerFlow.adapters.RVItemTextInCardWithNotification
import app.grand.tafwak.presentation.userFlow.adapters.RVItemTextInCard
import com.structure.base_mvvm.R
import com.structure.base_mvvm.databinding.ItemTextInCardBinding
import com.structure.base_mvvm.databinding.ItemTextInCardWithNotificationBinding

class VHItemTextInCardWithNotification(parent: ViewGroup, private val listener: RVItemTextInCardWithNotification.Listener) : RecyclerView.ViewHolder(
	parent.context.inflateLayout(R.layout.item_text_in_card_with_notification, parent)
) {
	
	private val binding = ItemTextInCardWithNotificationBinding.bind(itemView)
	
	init {
		binding.materialCardView.setOnClickListener {
			val index = binding.materialCardView.tag as? Int ?: return@setOnClickListener
			
			listener.changeCategoryWithWorking(index)
		}
	}
	
	fun bind(item: CategoryWithOrders, isSelected: Boolean, index: Int) {
		binding.materialCardView.tag = index
		
		binding.circleView.visibility = if (item.ordersCount > 0) View.VISIBLE else View.INVISIBLE
		
		binding.textView.text = item.name
		
		if (isSelected) {
			binding.textView.background = AppCompatResources.getDrawable(binding.textView.context, R.drawable.dr_project_gradiect_rect)
			binding.textView.setTextColor(binding.textView.context.getColor(R.color.white))
		}else {
			binding.textView.background = ColorDrawable(binding.textView.context.getColor(R.color.white))
			binding.textView.setTextColor(binding.textView.context.getColor(R.color.black))
		}
	}
	
}
