package app.grand.tafwak.presentation.base

import android.annotation.SuppressLint
import android.app.Dialog
import android.net.Uri
import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.annotation.LayoutRes
import androidx.databinding.DataBindingUtil
import androidx.databinding.ViewDataBinding
import androidx.fragment.app.Fragment
import androidx.fragment.app.FragmentActivity
import com.afollestad.assent.Permission
import com.afollestad.assent.askForPermissions
import com.afollestad.assent.isAllGranted
import com.afollestad.assent.showSystemAppDetailsPage
import app.grand.tafwak.presentation.base.utils.SingleLiveEvent
import app.grand.tafwak.presentation.base.utils.hideLoadingDialog
import app.grand.tafwak.presentation.base.utils.showLoadingDialog
import gun0912.tedbottompicker.TedRxBottomPicker
import java.util.Locale

abstract class BaseFragment<VDB : ViewDataBinding> : Fragment() {
	
	protected var _binding: VDB? = null
	
	protected val binding get() = _binding!!
	
	private var progressDialog: Dialog? = null
	val selectedImages = SingleLiveEvent<Uri>()
	
	override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View? {
		_binding = initializeBinding(inflater, container)
		binding.lifecycleOwner = viewLifecycleOwner
		
		return binding.root
	}
	
	/**
	 * - Ex. of setting variables is to use super fun then with .also set variables.
	 *
	 * - The [ViewDataBinding.setLifecycleOwner] will already be set in [onCreateView] isa.
	 */
	protected open fun initializeBinding(inflater: LayoutInflater, container: ViewGroup?): VDB {
		return DataBindingUtil.inflate(inflater, getLayoutId(), container, false)
	}
	
	override fun onResume() {
		super.onResume()
		
		registerListeners()
	}
	
	override fun onPause() {
		unRegisterListeners()
		
		super.onPause()
	}
	
	override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
		getFragmentArguments()
		setBindingVariables()
		setUpViews()
		observeAPICall()
		setupObservers()
	}
	
	@LayoutRes
	abstract fun getLayoutId(): Int
	
	open fun registerListeners() {}
	
	open fun unRegisterListeners() {}
	
	open fun getFragmentArguments() {}
	
	open fun setBindingVariables() {}
	
	open fun setUpViews() {}
	
	open fun observeAPICall() {}
	
	open fun setupObservers() {}
	
	fun showLoading() {
		hideLoading()
		progressDialog = showLoadingDialog(requireActivity(), null)
	}
	
	fun showLoading(hint: String?) {
		hideLoading()
		progressDialog = showLoadingDialog(requireActivity(), hint)
	}
	
	fun hideLoading() = hideLoadingDialog(progressDialog, requireActivity())
	
	fun setLanguage(language: String) {
		(requireActivity() as BaseActivity<*>).updateLocale(language)
	}
	
	val currentLanguage: Locale
		get() = Locale.getDefault()
	
	// check Permissions
	private fun checkGalleryPermissions(fragmentActivity: FragmentActivity): Boolean {
		fragmentActivity.askForPermissions(
				Permission.WRITE_EXTERNAL_STORAGE,
				Permission.READ_EXTERNAL_STORAGE,
				Permission.CAMERA
		) {}
		return if (fragmentActivity.isAllGranted(
						Permission.WRITE_EXTERNAL_STORAGE,
						Permission.READ_EXTERNAL_STORAGE,
						Permission.CAMERA
				)
		)
			true
		else {
			fragmentActivity.showSystemAppDetailsPage()
			false
		}
	}
	
	// Pick Single image
	@SuppressLint("CheckResult")
	fun singleTedBottomPicker(fragmentActivity: FragmentActivity) {
		if (checkGalleryPermissions(fragmentActivity)) {
			TedRxBottomPicker.with(fragmentActivity)
					.show()
					.subscribe({
						selectedImages.value = it
					}, Throwable::printStackTrace)
		}
	}
	
	@SuppressLint("CheckResult")
	fun multiTedBottomPicker(fragmentActivity: FragmentActivity) {
		if (checkGalleryPermissions(fragmentActivity)) {
			TedRxBottomPicker.with(fragmentActivity)
					.show()
					.subscribe({
						selectedImages.value = it
					}, Throwable::printStackTrace)
		}
	}
	
}