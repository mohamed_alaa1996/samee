@file:OptIn(ExperimentalCoroutinesApi::class)

package app.grand.tafwak.presentation.userFlow.viewModel

import android.view.View
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel
import androidx.lifecycle.asFlow
import androidx.navigation.findNavController
import app.grand.tafwak.data.user.repository.UserRepositoryImpl
import app.grand.tafwak.presentation.userFlow.ShowAllPreviousWorksFragmentArgs
import app.grand.tafwak.presentation.userFlow.adapters.RVItemImageRoundedInCard2
import app.grand.tafwak.presentation.userFlow.adapters.RVItemTextInCard
import com.structure.base_mvvm.NavProviderDirections
import dagger.hilt.android.lifecycle.HiltViewModel
import kotlinx.coroutines.ExperimentalCoroutinesApi
import kotlinx.coroutines.flow.emptyFlow
import kotlinx.coroutines.flow.flatMapLatest
import kotlinx.coroutines.flow.mapLatest
import timber.log.Timber
import javax.inject.Inject

@HiltViewModel
class ShowAllPreviousWorksViewModel @Inject constructor(
	repository: UserRepositoryImpl,
	args: ShowAllPreviousWorksFragmentArgs
) : ViewModel(), RVItemTextInCard.Listener, RVItemImageRoundedInCard2.Listener {
	
	val showEditableNotShowOnly = args.isEditableNotShowOnly
	
	val response = if (showEditableNotShowOnly) {
		repository.getAllWorkingsOfMe()
	}else {
		repository.getAllWorkingsOfProvider(args.id)
	}
	
	var selectedIndex = 0
	
	val adapterCategories = RVItemTextInCard(this)
	
	val adapterPreviousWorks = RVItemImageRoundedInCard2(this, showEditableNotShowOnly)
	
	fun addNewWorking(view: View) {
		view.findNavController().navigate(
			NavProviderDirections.actionGlobalDestCreateOrEditServiceDialog(
				adapterCategories.snapshot().getOrNull(selectedIndex)?.id ?: return,
				null,
				false
			)
		)
	}
	
	override fun changeCategoryWithWorking(index: Int) {
		if (index == selectedIndex) {
			return
		}
		
		val oldIndex = selectedIndex
		
		selectedIndex = index
		
		adapterCategories.notifyItemChanged(oldIndex)
		adapterCategories.notifyItemChanged(selectedIndex)
		
		adapterPreviousWorks.submitList(adapterCategories.snapshot()[selectedIndex]?.workings.orEmpty())
	}
	
	override fun isSelected(index: Int): Boolean {
		return index == selectedIndex
	}
	
	override fun onDeleteClick(view: View, id: Int) {
		view.findNavController().navigate(
			NavProviderDirections.actionGlobalDestDeleteWorkDialog(id)
		)
	}
	
}
