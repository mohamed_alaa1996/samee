package app.grand.tafwak.presentation.userHome.viewModel

import android.app.Application
import android.net.Uri
import android.view.View
import androidx.lifecycle.AndroidViewModel
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel
import androidx.navigation.NavController
import androidx.navigation.NavDeepLinkRequest
import androidx.navigation.NavOptions
import app.grand.tafwak.core.customTypes.RetryAbleFlow
import app.grand.tafwak.core.extensions.app
import app.grand.tafwak.core.extensions.findNavControllerOfProject
import app.grand.tafwak.core.extensions.getDeviceIdWithoutPermission
import app.grand.tafwak.core.extensions.switchMap
import app.grand.tafwak.data.auth.repository.MAAuthRepositoryImpl
import app.grand.tafwak.data.local.preferences.PrefsSplash
import app.grand.tafwak.data.user.repository.UserRepositoryImpl
import app.grand.tafwak.domain.splash.entity.SplashInitialLaunch
import app.grand.tafwak.presentation.userHome.UserFragmentDirections
import com.structure.base_mvvm.R
import dagger.hilt.android.lifecycle.HiltViewModel
import javax.inject.Inject

@HiltViewModel
class PageUserViewModel @Inject constructor(
	application: Application,
	repository: UserRepositoryImpl,
	private val prefsSplash: PrefsSplash,
	private val authRepository: MAAuthRepositoryImpl,
) : AndroidViewModel(application) {
	
	val myPageData = RetryAbleFlow(repository::getMyPageData)
	
	val performLogOut = MutableLiveData(false)
	val responseLogOut = performLogOut.switchMap {
		if (it == true) {
			// todo tell api backend to make it empty - able isa. + make it in the handle api extension function
			//  also note kda in the cases where u don't uses handle ext fun u need to check token expired ya zaki isa.
			authRepository.setFirebaseToken("null", app.getDeviceIdWithoutPermission())
		}else {
			MutableLiveData()
		}
	}
	
	fun goToPersonalData(view: View) {
		view.findNavControllerOfProject().navigate(
			UserFragmentDirections.actionDestUserToDestPersonalData(
				view.context.getString(R.string.update_your_profile_by_ensuring_account),
				true
			)
		)
	}
	
	fun goToWallet(view: View) {
		view.findNavControllerOfProject().navigate(
			UserFragmentDirections.actionDestUserToDestWallet()
		)
	}
	
	fun goToRegisterAsProvider(view: View) {
		view.findNavControllerOfProject().navigate(
			UserFragmentDirections.actionDestUserToDestRegisterAsProvider()
		)
	}
	
	fun logOut() {
		performLogOut.value = true
	}
	
	suspend fun onLogOutSuccess(navController: NavController) {
		prefsSplash.setInitialLaunch(SplashInitialLaunch.LOGIN)
		
		prefsSplash.prefsUser.logOut()
		
		val uri = Uri.Builder()
			.scheme("fragment-dest-auth")
			.authority("app.grand.tafwak.global.log.in")
			.build()
		val request = NavDeepLinkRequest.Builder.fromUri(uri).build()
		val options = NavOptions.Builder()
			.setEnterAnim(R.anim.anim_slide_in_right)
			.setExitAnim(R.anim.anim_slide_out_left)
			.setPopEnterAnim(R.anim.anim_slide_in_left)
			.setPopExitAnim(R.anim.anim_slide_out_right)
			.setPopUpTo(R.id.dest_log_in, true/*todo check if required to be false isa.*/, saveState = false)
			.build()
		navController.navigate(request, options)
	}
	
}
