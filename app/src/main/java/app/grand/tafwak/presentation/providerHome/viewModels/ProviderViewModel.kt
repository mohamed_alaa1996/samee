package app.grand.tafwak.presentation.providerHome.viewModels

import android.app.Application
import androidx.lifecycle.AndroidViewModel
import androidx.lifecycle.ViewModel
import app.grand.tafwak.core.customTypes.RetryAbleFlow
import app.grand.tafwak.core.extensions.app
import app.grand.tafwak.core.extensions.getDeviceIdWithoutPermission
import app.grand.tafwak.data.auth.repository.MAAuthRepositoryImpl
import app.grand.tafwak.data.user.repository.UserRepositoryImpl
import app.grand.tafwak.presentation.userHome.UserFragmentArgs
import dagger.hilt.android.lifecycle.HiltViewModel
import javax.inject.Inject

@HiltViewModel
class ProviderViewModel @Inject constructor(
	application: Application,
	repository: MAAuthRepositoryImpl
) : AndroidViewModel(application) {
	
	val retryAbleFlow = RetryAbleFlow {
		repository.getCurrentFirebaseTokenAsFlow(app.getDeviceIdWithoutPermission())
	}
	
}
