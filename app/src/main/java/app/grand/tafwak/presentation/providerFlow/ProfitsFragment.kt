package app.grand.tafwak.presentation.providerFlow

import android.os.Bundle
import android.view.View
import androidx.fragment.app.activityViewModels
import androidx.fragment.app.viewModels
import androidx.recyclerview.widget.LinearLayoutManager
import app.grand.tafwak.core.extensions.showErrorToast
import app.grand.tafwak.data.local.preferences.PrefsSplash
import app.grand.tafwak.domain.utils.MAResult
import app.grand.tafwak.presentation.base.MABaseFragment
import app.grand.tafwak.presentation.base.extensions.handleRetryAbleFlowWithMustHaveResultWithNullability
import app.grand.tafwak.presentation.project.viewModel.ProjectViewModel
import app.grand.tafwak.presentation.providerFlow.viewModels.ProfitsViewModel
import app.grand.tafwak.presentation.userFlow.viewModel.WalletViewModel
import com.structure.base_mvvm.R
import com.structure.base_mvvm.databinding.FragmentProfitsBinding
import com.structure.base_mvvm.databinding.FragmentProviderBinding
import com.structure.base_mvvm.databinding.FragmentWalletBinding
import dagger.hilt.android.AndroidEntryPoint
import javax.inject.Inject

@AndroidEntryPoint
class ProfitsFragment : MABaseFragment<FragmentProfitsBinding>() {
	
	private val viewModel by viewModels<ProfitsViewModel>()
	
	override fun getLayoutId(): Int = R.layout.fragment_profits
	
	override fun initializeBindingVariables() {
		binding?.viewModel = viewModel
	}
	
	override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
		viewModel.profits.observe(viewLifecycleOwner) {
			when (it) {
				is MAResult.Loading -> {
					activityViewModel?.globalLoading?.value = true
				}
				is MAResult.Failure -> {
					requireContext().showErrorToast(it.message ?: getString(R.string.something_went_wrong))
					
					activityViewModel?.globalLoading?.value = false
				}
				is MAResult.Success -> {
					activityViewModel?.globalLoading?.value = false
				}
			}
		}
	}
	
}
