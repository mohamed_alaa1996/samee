package app.grand.tafwak.presentation.base.customTypes

import android.view.View
import androidx.databinding.BindingConversion
import androidx.lifecycle.MutableLiveData

object BindingConversions {
	
	@JvmStatic
	@BindingConversion
	fun convertBooleanToVisibility(value: MutableLiveData<Boolean>): Int {
		return if (value.value == true) View.VISIBLE else View.GONE
	}
	
	@JvmStatic
	@BindingConversion
	fun convertBooleanToVisibility(value: Boolean): Int {
		return if (value) View.VISIBLE else View.GONE
	}
	
}
