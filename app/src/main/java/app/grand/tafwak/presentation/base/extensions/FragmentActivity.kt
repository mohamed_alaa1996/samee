package app.grand.tafwak.presentation.base.extensions

import app.grand.tafwak.presentation.base.MABaseFragment
import app.grand.tafwak.presentation.base.MADialogFragment
import app.grand.tafwak.presentation.project.ProjectActivity
import app.grand.tafwak.presentation.project.viewModel.ProjectViewModel

fun MABaseFragment<*>.changeOrderNotOnTheWay(orderId: Int) {
    activityViewModel?.changeTrackingForOrder(
        orderId, false, activity as? ProjectViewModel.Listener ?: return
    )
}

fun MABaseFragment<*>.trackOrdersInActivity(ordersIds: List<Int>) {
    (activity as? ProjectActivity)?.trackOrders(ordersIds)
}

/*fun MADialogFragment<*>.changeOrderNotOnTheWay(orderId: Int) {
    activityViewModel?.changeTrackingForOrder(
        orderId, false, activity as? MainViewModel.Listener ?: return
    )
}*/
