package app.grand.tafwak.presentation.base.extensions

import androidx.lifecycle.MutableLiveData
import com.google.gson.Gson

fun Any.toJsonString(): String = Gson().toJson(this)

fun <A : Any> String.toJsonModel(modelClass: Class<A>): A = Gson().fromJson(this, modelClass)

operator fun <T> MutableLiveData<MutableList<T>>.plusAssign(item: T) {
  val value = this.value ?: mutableListOf()
  value.add(item)
  this.value = value
}