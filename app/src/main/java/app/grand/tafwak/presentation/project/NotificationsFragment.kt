package app.grand.tafwak.presentation.project

import android.graphics.Canvas
import android.os.Bundle
import android.view.View
import androidx.fragment.app.activityViewModels
import androidx.fragment.app.viewModels
import androidx.lifecycle.Lifecycle
import androidx.lifecycle.lifecycleScope
import androidx.lifecycle.repeatOnLifecycle
import androidx.recyclerview.widget.ItemTouchHelper
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.RecyclerView
import app.grand.tafwak.core.customTypes.MADividerItemDecoration
import app.grand.tafwak.core.extensions.dpToPx
import app.grand.tafwak.core.extensions.withCustomAdapters
import app.grand.tafwak.data.local.preferences.PrefsSplash
import app.grand.tafwak.presentation.base.MABaseFragment
import app.grand.tafwak.presentation.project.adapters.viewHolders.VHItemNotification
import app.grand.tafwak.presentation.project.viewModel.ConversationViewModel
import app.grand.tafwak.presentation.project.viewModel.NotificationsViewModel
import app.grand.tafwak.presentation.project.viewModel.ProjectViewModel
import app.grand.tafwak.presentation.userHome.adapters.LSAdapterLoadingErrorEmpty
import com.google.gson.Gson
import com.structure.base_mvvm.R
import com.structure.base_mvvm.databinding.FragmentConversationsBinding
import com.structure.base_mvvm.databinding.FragmentNotificationsBinding
import dagger.hilt.android.AndroidEntryPoint
import kotlinx.coroutines.flow.collectLatest
import kotlinx.coroutines.launch
import javax.inject.Inject
import kotlin.math.roundToInt

@AndroidEntryPoint
class NotificationsFragment : MABaseFragment<FragmentNotificationsBinding>() {
	
	private val viewModel by viewModels<NotificationsViewModel>()
	
	@Inject
	protected lateinit var gson: Gson
	
	private val onTouchItemListener = object : ItemTouchHelper.SimpleCallback(0, ItemTouchHelper.END) {
		override fun onMove(
			recyclerView: RecyclerView,
			viewHolder: RecyclerView.ViewHolder,
			target: RecyclerView.ViewHolder
		): Boolean = true
		
		override fun onSelectedChanged(viewHolder: RecyclerView.ViewHolder?, actionState: Int) {
			if (viewHolder is VHItemNotification && viewHolder.canBeSwiped()) {
				getDefaultUIUtil().onSelected(viewHolder.binding.firstConstraintLayout)
			}
		}
		
		override fun onChildDrawOver(
			c: Canvas,
			recyclerView: RecyclerView,
			viewHolder: RecyclerView.ViewHolder?,
			dX: Float,
			dY: Float,
			actionState: Int,
			isCurrentlyActive: Boolean
		) {
			if (viewHolder is VHItemNotification && viewHolder.canBeSwiped()) {
				getDefaultUIUtil().onDrawOver(c, recyclerView, viewHolder.binding.firstConstraintLayout, dX, dY, actionState, isCurrentlyActive)
			}
		}
		
		override fun clearView(recyclerView: RecyclerView, viewHolder: RecyclerView.ViewHolder) {
			if (viewHolder is VHItemNotification && viewHolder.canBeSwiped()) {
				getDefaultUIUtil().clearView(viewHolder.binding.firstConstraintLayout)
			}
		}
		
		override fun onChildDraw(
			c: Canvas,
			recyclerView: RecyclerView,
			viewHolder: RecyclerView.ViewHolder,
			dX: Float,
			dY: Float,
			actionState: Int,
			isCurrentlyActive: Boolean
		) {
			if (viewHolder is VHItemNotification && viewHolder.canBeSwiped()) {
				val foregroundView = viewHolder.binding.firstConstraintLayout
				getDefaultUIUtil().onDraw(c, recyclerView, foregroundView, dX, dY, actionState, isCurrentlyActive);
			}
		}
		
		override fun onSwiped(viewHolder: RecyclerView.ViewHolder, direction: Int) {
			if (viewHolder is VHItemNotification && viewHolder.canBeSwiped()) {
				viewHolder.getTargetIdOrNull()?.also {
					viewModel.cancelOrder(this@NotificationsFragment, it)
				}
			}
		}
	}
	
	override fun getLayoutId(): Int = R.layout.fragment_notifications
	
	override fun initializeBindingVariables() {
		binding?.viewModel = viewModel
	}
	
	override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
		ItemTouchHelper(onTouchItemListener).attachToRecyclerView(binding?.recyclerView)
		binding?.recyclerView?.layoutManager = LinearLayoutManager(requireContext(), LinearLayoutManager.VERTICAL, false)
		val headerAdapterCurrent2 = LSAdapterLoadingErrorEmpty(viewModel.adapter, false)
		val footerAdapterFinished2 = LSAdapterLoadingErrorEmpty(viewModel.adapter, true)
		binding?.recyclerView?.adapter = viewModel.adapter.withCustomAdapters(
			headerAdapterCurrent2,
			footerAdapterFinished2
		)
		
		viewLifecycleOwner.lifecycleScope.launch {
			viewLifecycleOwner.repeatOnLifecycle(Lifecycle.State.STARTED) {
				viewModel.notifications.collectLatest { pagingData ->
					viewModel.adapter.submitData(pagingData)
				}
			}
		}
	}
	
}

