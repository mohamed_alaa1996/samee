package app.grand.tafwak.presentation.base.extensions

import android.graphics.Color
import android.os.Build
import android.view.View
import android.view.WindowManager
import androidx.annotation.ColorInt
import androidx.core.content.ContextCompat
import androidx.core.view.WindowInsetsCompat
import androidx.core.view.WindowInsetsControllerCompat
import androidx.fragment.app.Fragment
import com.structure.base_mvvm.R
import timber.log.Timber

fun Fragment.changeStatusBarToWhiteBackgroundDarkIcons() = changeStatusBarCustomColor(Color.WHITE, true)
fun Fragment.changeStatusBarToDefaultBackgroundAndIcons() {
    context?.also { context ->
        changeStatusBarCustomColor(ContextCompat.getColor(context, R.color.colorPrimaryDark), false)
    }
}

fun Fragment.changeStatusBarCustomColor(@ColorInt color: Int, useDarkIcons: Boolean) {
    activity?.window?.apply {
        kotlin.runCatching {
            //clearFlags(WindowManager.LayoutParams.FLAG_TRANSLUCENT_STATUS)
            addFlags(WindowManager.LayoutParams.FLAG_DRAWS_SYSTEM_BAR_BACKGROUNDS)
            statusBarColor = color

            if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.R && insetsController != null) {
                val compat = WindowInsetsControllerCompat.toWindowInsetsControllerCompat(insetsController!!)

                compat.isAppearanceLightStatusBars = useDarkIcons

                compat.show(WindowInsetsCompat.Type.statusBars())
            }else {
                if (useDarkIcons) {
                    decorView.systemUiVisibility = decorView.systemUiVisibility or
                            View.SYSTEM_UI_FLAG_LIGHT_STATUS_BAR
                }else {
                    decorView.systemUiVisibility = decorView.systemUiVisibility and
                            View.SYSTEM_UI_FLAG_LIGHT_STATUS_BAR.inv()
                }
            }
        }
    }
}

/*fun Fragment.clearStatusBarCustomColor(@ColorInt color: Int, useDarkIcons: Boolean) {
    activity?.window?.apply {
        //clearFlags(WindowManager.LayoutParams.FLAG_TRANSLUCENT_STATUS)
        clearFlags(WindowManager.LayoutParams.FLAG_DRAWS_SYSTEM_BAR_BACKGROUNDS)
        statusBarColor = color

        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.R && insetsController != null) {
            val compat = WindowInsetsControllerCompat.toWindowInsetsControllerCompat(insetsController!!)

            compat.isAppearanceLightStatusBars = true

            compat.show(WindowInsetsCompat.Type.statusBars())
        }else {
            decorView.systemUiVisibility = decorView.systemUiVisibility or
                    View.SYSTEM_UI_FLAG_LIGHT_STATUS_BAR
        }
    }
}*/
