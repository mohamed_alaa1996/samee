package app.grand.tafwak.presentation.userFlow

import android.os.Bundle
import android.view.View
import androidx.fragment.app.activityViewModels
import androidx.fragment.app.viewModels
import androidx.recyclerview.widget.LinearLayoutManager
import app.grand.tafwak.data.local.preferences.PrefsSplash
import app.grand.tafwak.presentation.base.MABaseFragment
import app.grand.tafwak.presentation.base.extensions.handleRetryAbleFlowWithMustHaveResultWithNullability
import app.grand.tafwak.presentation.project.viewModel.ProjectViewModel
import app.grand.tafwak.presentation.userFlow.viewModel.WalletViewModel
import com.structure.base_mvvm.R
import com.structure.base_mvvm.databinding.FragmentWalletBinding
import dagger.hilt.android.AndroidEntryPoint
import javax.inject.Inject

@AndroidEntryPoint
class WalletFragment : MABaseFragment<FragmentWalletBinding>() {
	
	private val viewModel by viewModels<WalletViewModel>()
	
	override fun getLayoutId(): Int = R.layout.fragment_wallet
	
	override fun initializeBindingVariables() {
		binding?.viewModel = viewModel
	}
	
	override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
		binding?.recyclerView?.layoutManager = LinearLayoutManager(requireContext(), LinearLayoutManager.VERTICAL, false)
		binding?.recyclerView?.adapter = viewModel.adapter
		
		handleRetryAbleFlowWithMustHaveResultWithNullability(viewModel.response) {
			viewModel.wallet.value = "${it.money} ${getString(R.string.sar)}"
			
			viewModel.adapter.submitList(it.wallets)
		}
	}
	
}
