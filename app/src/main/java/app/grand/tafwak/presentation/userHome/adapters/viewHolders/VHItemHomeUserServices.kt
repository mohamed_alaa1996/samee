package app.grand.tafwak.presentation.userHome.adapters.viewHolders

import android.view.ViewGroup
import androidx.fragment.app.findFragment
import androidx.lifecycle.viewModelScope
import androidx.navigation.findNavController
import androidx.paging.PagingData
import androidx.recyclerview.widget.RecyclerView
import app.grand.tafwak.core.extensions.findNavControllerOfProject
import app.grand.tafwak.core.extensions.inflateLayout
import app.grand.tafwak.domain.user.entity.model.ServiceCategory
import app.grand.tafwak.presentation.userFlow.SearchQueryFragmentDirections
import app.grand.tafwak.presentation.userHome.HomeUserFragment
import app.grand.tafwak.presentation.userHome.UserFragmentDirections
import com.bumptech.glide.Glide
import com.bumptech.glide.request.RequestOptions
import com.structure.base_mvvm.R
import com.structure.base_mvvm.databinding.ItemHomeUserServicesBinding
import kotlinx.coroutines.launch

class VHItemHomeUserServices(parent: ViewGroup) : RecyclerView.ViewHolder(
	parent.context.inflateLayout(R.layout.item_home_user_services, parent)
) {
	
	private val binding = ItemHomeUserServicesBinding.bind(itemView)
	
	init {
		binding.materialCardView.setOnClickListener { view ->
			val id = binding.materialCardView.tag as? Int ?: return@setOnClickListener
			
			view.findFragment<HomeUserFragment>().viewModel.apply {
				viewModelScope.launch {
					adapterCategories.submitData(PagingData.empty())
				}
			}
			
			view.findNavControllerOfProject().navigate(
				UserFragmentDirections.actionDestUserToDestSearchResultsProviders("", id, binding.textView.text?.toString())
			)
		}
	}
	
	fun bind(serviceCategory: ServiceCategory) {
		binding.materialCardView.tag = serviceCategory.id
		
		binding.textView.text = serviceCategory.name
		
		Glide.with(binding.imageView.context)
			.load(serviceCategory.imageUrl)
			.apply(RequestOptions().centerCrop())
			.placeholder(R.drawable.ic_logo_samee_placeholder)
			.error(R.drawable.ic_logo_samee_placeholder)
			.into(binding.imageView)
	}
	
}
