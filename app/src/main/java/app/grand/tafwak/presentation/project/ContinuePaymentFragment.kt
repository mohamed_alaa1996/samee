package app.grand.tafwak.presentation.project

import android.graphics.Canvas
import android.os.Bundle
import android.view.View
import androidx.fragment.app.activityViewModels
import androidx.fragment.app.viewModels
import androidx.lifecycle.Lifecycle
import androidx.lifecycle.lifecycleScope
import androidx.lifecycle.repeatOnLifecycle
import androidx.recyclerview.widget.ItemTouchHelper
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.RecyclerView
import app.grand.tafwak.core.extensions.withCustomAdapters
import app.grand.tafwak.data.local.preferences.PrefsSplash
import app.grand.tafwak.presentation.base.MABaseFragment
import app.grand.tafwak.presentation.project.adapters.viewHolders.VHItemNotification
import app.grand.tafwak.presentation.project.viewModel.ContinuePaymentViewModel
import app.grand.tafwak.presentation.project.viewModel.NotificationsViewModel
import app.grand.tafwak.presentation.project.viewModel.ProjectViewModel
import app.grand.tafwak.presentation.userHome.adapters.LSAdapterLoadingErrorEmpty
import com.google.gson.Gson
import com.structure.base_mvvm.R
import com.structure.base_mvvm.databinding.FragmentContinuePaymentBinding
import com.structure.base_mvvm.databinding.FragmentNotificationsBinding
import dagger.hilt.android.AndroidEntryPoint
import kotlinx.coroutines.flow.collectLatest
import kotlinx.coroutines.launch
import javax.inject.Inject

// todo require akid el target id isa. see how to select payment method on api isa.
@AndroidEntryPoint
class ContinuePaymentFragment : MABaseFragment<FragmentContinuePaymentBinding>() {
	
	private val viewModel by viewModels<ContinuePaymentViewModel>()
	
	@Inject
	protected lateinit var gson: Gson
	
	override fun getLayoutId(): Int = R.layout.fragment_continue_payment
	
	override fun initializeBindingVariables() {
		binding?.viewModel = viewModel
	}

}
