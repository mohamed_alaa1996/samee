package app.grand.tafwak.presentation.providerHome

import android.os.Bundle
import android.view.View
import androidx.fragment.app.activityViewModels
import androidx.fragment.app.viewModels
import androidx.lifecycle.lifecycleScope
import androidx.recyclerview.widget.GridLayoutManager
import app.grand.tafwak.core.extensions.findNavControllerOfProject
import app.grand.tafwak.core.extensions.showErrorToast
import app.grand.tafwak.data.local.preferences.PrefsSplash
import app.grand.tafwak.domain.base.entity.IconTextWithNumber
import app.grand.tafwak.domain.userFlow.model.MenuItemVisibility
import app.grand.tafwak.domain.utils.MAResult
import app.grand.tafwak.presentation.base.MABaseFragment
import app.grand.tafwak.presentation.base.extensions.handleRetryAbleFlowWithMustHaveResultWithNullability
import app.grand.tafwak.presentation.project.viewModel.ProjectViewModel
import app.grand.tafwak.presentation.providerHome.viewModels.PageProviderViewModel
import app.grand.tafwak.presentation.userHome.viewModel.PageUserViewModel
import com.structure.base_mvvm.R
import com.structure.base_mvvm.databinding.FragmentPageProviderBinding
import com.structure.base_mvvm.databinding.FragmentPageUserBinding
import dagger.hilt.android.AndroidEntryPoint
import kotlinx.coroutines.launch
import javax.inject.Inject

@AndroidEntryPoint
class PageProviderFragment : MABaseFragment<FragmentPageProviderBinding>() {
	
	private val viewModel by viewModels<PageProviderViewModel>()
	
	override fun getLayoutId(): Int = R.layout.fragment_page_provider
	
	override fun initializeBindingVariables() {
		binding?.viewModel = viewModel
	}
	
	override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
		binding?.recyclerView?.layoutManager = GridLayoutManager(requireContext(), 2, GridLayoutManager.VERTICAL, false)
		binding?.recyclerView?.adapter = viewModel.adapter
		
		handleRetryAbleFlowWithMustHaveResultWithNullability(viewModel.myPageData) {
			activityViewModel?.menuItemNotificationsVisibility?.postValue(
				if (it.notificationsCount > 0) MenuItemVisibility.SHOW_HAVING_NEW_DATA else MenuItemVisibility.SHOW
			)
			activityViewModel?.menuItemConversationsVisibility?.postValue(
				if (it.messagesCount > 0) MenuItemVisibility.SHOW_HAVING_NEW_DATA else MenuItemVisibility.SHOW
			)
			
			viewModel.list = listOf(
				IconTextWithNumber(R.drawable.ic_orders_themed, getString(R.string.orders_count), if (it.ordersCount > 0) it.ordersCount else null),
				IconTextWithNumber(R.drawable.ic_personal_data_themed, getString(R.string.personal_data)),
				IconTextWithNumber(R.drawable.ic_wallet_themed, getString(R.string.portfolio)),
				IconTextWithNumber(R.drawable.ic_sar_themed_2, getString(R.string.profits)),
				IconTextWithNumber(R.drawable.ic_rate_themed, getString(R.string.clients_reviews)),
				IconTextWithNumber(R.drawable.ic_my_works_themed, getString(R.string.from_my_works)),
				IconTextWithNumber(if (it.deliveryFlag == 0) R.drawable.ic_toggle_on_themed else R.drawable.ic_toggle_off, getString(R.string.going_to_client_for_free)),
				IconTextWithNumber(if (it.ordersFlag == 0) R.drawable.ic_toggle_on_themed else R.drawable.ic_toggle_off, getString(R.string.stop_receiving_orders)),
			)
			viewModel.adapter.submitList(viewModel.list)
		}
	}

}
