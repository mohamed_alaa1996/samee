package app.grand.tafwak.presentation.providerHome.adapters.viewHolders

import android.view.ViewGroup
import androidx.core.view.isVisible
import androidx.recyclerview.widget.RecyclerView
import app.grand.tafwak.core.extensions.inflateLayout
import app.grand.tafwak.domain.base.entity.IconText
import app.grand.tafwak.domain.base.entity.IconTextWithNumber
import app.grand.tafwak.presentation.providerHome.adapters.RVIconTextWithNumber
import app.grand.tafwak.presentation.userHome.adapters.RVIconText
import com.structure.base_mvvm.R
import com.structure.base_mvvm.databinding.ItemIconTextBinding
import com.structure.base_mvvm.databinding.ItemIconTextWithNumberBinding

class VHIconTextWithNumber(parent: ViewGroup, private val listener: RVIconTextWithNumber.Listener) : RecyclerView.ViewHolder(
	parent.context.inflateLayout(R.layout.item_icon_text_with_number, parent)
) {
	
	private val binding = ItemIconTextWithNumberBinding.bind(itemView)
	
	init {
		binding.materialCardView.setOnClickListener {
			val text = binding.materialCardView.tag as? String ?: return@setOnClickListener
			
			listener.onItemIconTextClick(it, text)
		}
	}
	
	fun bind(item: IconTextWithNumber) {
		binding.materialCardView.tag = item.text
		
		binding.nameTextView.text = item.text
		
		binding.imageView.setImageResource(item.icon)

		binding.numberTextView.isVisible = item.number != null
		binding.numberTextView.text = item.number?.toString().orEmpty()
	}
	
}
