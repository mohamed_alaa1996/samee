package app.grand.tafwak.presentation.base

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.activity.ComponentActivity
import androidx.annotation.CallSuper
import androidx.annotation.LayoutRes
import androidx.databinding.DataBindingUtil
import androidx.databinding.ViewDataBinding
import androidx.fragment.app.Fragment
import androidx.fragment.app.createViewModelLazy
import androidx.lifecycle.ViewModel
import androidx.lifecycle.ViewModelProvider
import androidx.navigation.fragment.findNavController
import app.grand.tafwak.data.local.preferences.PrefsSplash
import app.grand.tafwak.presentation.project.ProjectActivity
import app.grand.tafwak.presentation.project.viewModel.ProjectViewModel
import javax.inject.Inject

abstract class MABaseFragment<VDB : ViewDataBinding> : Fragment() {

	@Inject
	lateinit var prefsSplash: PrefsSplash
	
	var binding: VDB? = null
		private set

	private var _activityViewModel: ProjectViewModel? = null

	val sharedMainActivity: ProjectActivity?
		get() = activity as? ProjectActivity

	val activityViewModel: ProjectViewModel?
		get() {
			return try {
				_activityViewModel ?: synchronized(this) {
					_activityViewModel ?: safeActivityViewModels<ProjectViewModel>().value.also { _activityViewModel = it }
				}
			}catch (throwable: Throwable) {
				safeActivityViewModels<ProjectViewModel>().value.also { _activityViewModel = it }
			}
		}

	private inline fun <reified VM : ViewModel> Fragment.safeActivityViewModels(
		noinline factoryProducer: (() -> ViewModelProvider.Factory)? = null
	): Lazy<VM?> = if (isAdded) createViewModelLazy(
		VM::class, { requireActivity().viewModelStore },
		factoryProducer ?: { requireActivity().defaultViewModelProviderFactory }
	) else lazy<VM?> { null }

	@CallSuper
	override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View? {
		binding = DataBindingUtil.inflate(inflater, getLayoutId(), container, false)
		initializeBindingVariables()
		binding?.lifecycleOwner = viewLifecycleOwner
		
		return binding?.root
	}
	
	protected open fun initializeBindingVariables() {}
	
	override fun onDestroyView() {
		binding = null
		
		super.onDestroyView()
	}
	
	@LayoutRes
	abstract fun getLayoutId(): Int

}