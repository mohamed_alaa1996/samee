package app.grand.tafwak.presentation.userHome.adapters.viewHolders

import android.net.Uri
import android.view.ViewGroup
import androidx.core.view.isVisible
import androidx.fragment.app.findFragment
import androidx.lifecycle.lifecycleScope
import androidx.navigation.NavDeepLinkRequest
import androidx.navigation.NavOptions
import androidx.navigation.findNavController
import androidx.recyclerview.widget.RecyclerView
import app.grand.tafwak.core.extensions.*
import app.grand.tafwak.domain.userFlow.response.OrderMiniDetails
import app.grand.tafwak.domain.userFlow.response.OrderStatus
import app.grand.tafwak.presentation.userHome.OrdersUserFragment
import app.grand.tafwak.presentation.userHome.UserFragmentDirections
import com.bumptech.glide.Glide
import com.bumptech.glide.request.RequestOptions
import com.google.gson.Gson
import com.structure.base_mvvm.R
import com.structure.base_mvvm.databinding.ItemOrderMiniDetailsBinding
import kotlinx.coroutines.delay
import kotlinx.coroutines.flow.first
import kotlinx.coroutines.flow.firstOrNull
import kotlinx.coroutines.launch
import kotlin.math.roundToInt

class VHItemOrderMiniDetails(parent: ViewGroup, private val gson: Gson) : RecyclerView.ViewHolder(
	parent.context.inflateLayout(R.layout.item_order_mini_details, parent)
) {
	
	private val binding = ItemOrderMiniDetailsBinding.bind(itemView)
	
	private val context get() = binding.materialCardView.context
	
	init {
		binding.detailsMaterialButton.setOnClickListener { view ->
			val id = binding.materialCardView.tag as? Int ?: return@setOnClickListener
			
			view.findNavControllerOfProject().navigate(
				UserFragmentDirections.actionDestUserToDestOrderDetails(id)
			)
		}
		binding.trackingMaterialButton.setOnClickListener { view ->
			val json = binding.nameTextView.tag as? String ?: return@setOnClickListener
			
			val fragment = view.findFragment<OrdersUserFragment>()
			fragment.lifecycleScope.launch {
				fragment.activityViewModel?.globalLoading?.value = true
				
				val details = json.fromJson<OrderMiniDetails>(gson)
				
				val myId = fragment.prefsUser.getId().first()!!
				val image = fragment.prefsUser.getImageUrl().firstOrNull().orEmpty()
				
				fragment.activityViewModel?.globalLoading?.value = false
				
				delay(150)
				
				val uri = Uri.Builder()
					.scheme("fragment-dest")
					.authority("app.grand.tafwak.global.location.tracking")
					.appendPath(myId.toString())
					.appendPath(details.providerId.toString())
					.appendPath(details.id.toString())
					.let {
						if (image.isEmpty()) it else it.appendPath(image)
					}
					.build()
				val request = NavDeepLinkRequest.Builder.fromUri(uri).build()
				val options = NavOptions.Builder()
					.setEnterAnim(R.anim.anim_slide_in_right)
					.setExitAnim(R.anim.anim_slide_out_left)
					.setPopEnterAnim(R.anim.anim_slide_in_left)
					.setPopExitAnim(R.anim.anim_slide_out_right)
					.build()
				view.findNavControllerOfProject().navigate(request, options)
			}
		}
	}
	
	fun bind(details: OrderMiniDetails) {
		binding.materialCardView.tag = details.id
		binding.nameTextView.tag = details.toJson(gson)
		
		Glide.with(binding.imageView.context)
			.load(details.provider?.imageUrl)
			.apply(RequestOptions().centerCrop())
			.placeholder(R.drawable.ic_logo_samee_placeholder)
			.error(R.drawable.ic_logo_samee_placeholder)
			.into(binding.imageView)
		
		binding.nameTextView.text = details.provider?.name
		binding.ratingBar.progress = (details.provider?.averageRate.orZero() ).roundToInt()
		
		binding.orderNumberTextView.text = buildString {
			append(context.getString(R.string.order_number))
			append(" : ")
			append(details.orderNumber)
		}
		
		binding.dateAndTimeTextView.text = buildString {
			append(context.getString(R.string.execution_time))
			append(" : ")
			append("(${details.time})")
			append(" - ")
			append("(${details.date})")
		}
		
		binding.trackingMaterialButton.isVisible = details.orderStatus == OrderStatus.ON_THE_WAY
	}
	
}
