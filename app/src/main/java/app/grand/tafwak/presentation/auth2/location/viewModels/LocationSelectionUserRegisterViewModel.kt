package app.grand.tafwak.presentation.auth2.location.viewModels

import android.view.View
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel
import androidx.navigation.findNavController
import dagger.hilt.android.lifecycle.HiltViewModel
import javax.inject.Inject

@HiltViewModel
class LocationSelectionUserRegisterViewModel @Inject constructor() : ViewModel() {
	
	val search = MutableLiveData("")
	
	fun goBack(view: View) {
		view.findNavController().navigateUp()
	}
	
	fun toSearchPlace(view: View) {
		// to-do even if another activity use it inside the navigation as well isa.
		
		// to-do maybe start activity and have on activity result of projet activity uset currentDestination.set el value after getting it from
		//  activity result isa.
	}
	
	fun moveToCurrentLocation(view: View) {
		// to-do
	}
	
	fun onSelectLocationClick(view: View) {
		// to-do nav up then current backstack to set result
	}
	
}
