package app.grand.tafwak.presentation.providerHome.viewModels

import android.app.Application
import android.net.Uri
import android.view.View
import androidx.fragment.app.findFragment
import androidx.lifecycle.AndroidViewModel
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.lifecycleScope
import androidx.navigation.NavController
import androidx.navigation.NavDeepLinkRequest
import androidx.navigation.NavOptions
import androidx.navigation.fragment.findNavController
import app.grand.tafwak.core.customTypes.RetryAbleFlow
import app.grand.tafwak.core.extensions.*
import app.grand.tafwak.data.auth.repository.MAAuthRepositoryImpl
import app.grand.tafwak.data.local.preferences.PrefsSplash
import app.grand.tafwak.data.provider.repository.ProviderRepositoryImpl
import app.grand.tafwak.data.user.repository.UserRepositoryImpl
import app.grand.tafwak.domain.base.entity.IconTextWithNumber
import app.grand.tafwak.domain.splash.entity.SplashInitialLaunch
import app.grand.tafwak.domain.utils.MAResult
import app.grand.tafwak.presentation.base.customTypes.DataOfInfoImageAndListOfTexts
import app.grand.tafwak.presentation.providerHome.PageProviderFragment
import app.grand.tafwak.presentation.providerHome.ProviderFragment
import app.grand.tafwak.presentation.providerHome.ProviderFragmentDirections
import app.grand.tafwak.presentation.providerHome.adapters.RVIconTextWithNumber
import app.grand.tafwak.presentation.userHome.MoreUserFragment
import app.grand.tafwak.presentation.userHome.UserFragmentDirections
import com.structure.base_mvvm.NavProviderBottomNavDirections
import com.structure.base_mvvm.R
import dagger.hilt.android.lifecycle.HiltViewModel
import kotlinx.coroutines.launch
import javax.inject.Inject

@HiltViewModel
class PageProviderViewModel @Inject constructor(
	application: Application,
	repository: UserRepositoryImpl,
	private val providerRepository: ProviderRepositoryImpl,
) : AndroidViewModel(application), RVIconTextWithNumber.Listener {
	
	val myPageData = RetryAbleFlow(repository::getMyPageData)
	
	var list = emptyList<IconTextWithNumber>()
	
	val adapter = RVIconTextWithNumber(this)
	
	override fun onItemIconTextClick(view: View, text: String) {
		val fragment = view.findFragment<PageProviderFragment>()
		
		view.context.apply {
			when (text) {
				getString(R.string.orders_count) -> {
					fragment.findNavControllerOfProject().currentBackStackEntry?.savedStateHandle?.set(
						ProviderFragment.KEY_FRAGMENT_RESULT_GO_TO_ORDERS,
						true
					)
				}
				getString(R.string.personal_data) -> {
					val uri = Uri.Builder()
						.scheme("fragment-dest")
						.authority("app.grand.tafwak.dest.personal.data")
						.appendPath(getString(R.string.the_profile))
						.appendPath(false.toString())
						.build()
					val request = NavDeepLinkRequest.Builder.fromUri(uri).build()
					fragment.findNavControllerOfProject().navigate(request)
				}
				getString(R.string.portfolio) -> {
					val uri = Uri.Builder()
						.scheme("fragment-dest")
						.authority("app.grand.tafwak.dest.wallet")
						.build()
					val request = NavDeepLinkRequest.Builder.fromUri(uri).build()
					fragment.findNavControllerOfProject().navigate(request)
				}
				getString(R.string.profits) -> {
					fragment.findNavControllerOfProject().navigate(ProviderFragmentDirections.actionDestProviderToDestProfits())
				}
				getString(R.string.clients_reviews) -> {
					fragment.findNavControllerOfProject().navigate(ProviderFragmentDirections.actionDestProviderToDestClientsReviews())
				}
				getString(R.string.from_my_works) -> {
					fragment.findNavControllerOfProject().navigate(
						ProviderFragmentDirections.actionDestProviderToDestShowAllPreviousWorks(-1, true)
					)
				}
				getString(R.string.going_to_client_for_free), getString(R.string.stop_receiving_orders) -> {
					fragment.activityViewModel?.globalLoading?.value = true
					
					fragment.lifecycleScope.launch {
						val prevFlag = if (list.first { it.text == text }.icon == R.drawable.ic_toggle_on_themed) 0 else 1
						val newFlag = if (prevFlag == 1) 0 else 1
						
						val result = if (text == getString(R.string.going_to_client_for_free)) {
							providerRepository.updateDeliveryFlag(newFlag)
						}else {
							providerRepository.updateOrdersFlag(newFlag)
						}
						when (result) {
							is MAResult.Failure -> {
								fragment.requireContext().showErrorToast(result.message ?: fragment.getString(R.string.something_went_wrong))
								
								fragment.activityViewModel?.globalLoading?.value = false
							}
							is MAResult.Success -> {
								val newRes = if (newFlag == 0) R.drawable.ic_toggle_on_themed else R.drawable.ic_toggle_off
								
								list = list.map {
									if (it.text == text) it.copy(icon = newRes) else it
								}
								
								adapter.submitList(list)
								
								fragment.activityViewModel?.globalLoading?.value = false
							}
						}
					}
				}
				else -> return
			}
		}
	}
	
}
