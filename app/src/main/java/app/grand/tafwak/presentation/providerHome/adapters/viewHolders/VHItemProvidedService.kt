package app.grand.tafwak.presentation.providerHome.adapters.viewHolders

import android.annotation.SuppressLint
import android.graphics.drawable.ColorDrawable
import android.view.ViewGroup
import androidx.appcompat.content.res.AppCompatResources
import androidx.recyclerview.widget.RecyclerView
import app.grand.tafwak.core.extensions.inflateLayout
import app.grand.tafwak.domain.user.entity.model.ProviderService
import app.grand.tafwak.domain.user.entity.model.ServiceCategory
import app.grand.tafwak.presentation.providerFlow.adapters.RVItemTextInCard2
import app.grand.tafwak.presentation.providerHome.adapters.RVItemProvidedService
import com.bumptech.glide.Glide
import com.bumptech.glide.request.RequestOptions
import com.structure.base_mvvm.R
import com.structure.base_mvvm.databinding.ItemProvidedServiceBinding
import com.structure.base_mvvm.databinding.ItemTextInCardBinding

class VHItemProvidedService(parent: ViewGroup, private val listener: RVItemProvidedService.Listener) : RecyclerView.ViewHolder(
	parent.context.inflateLayout(R.layout.item_provided_service, parent)
) {
	
	private val binding = ItemProvidedServiceBinding.bind(itemView)
	
	private val context get() = binding.root.context
	
	init {
		binding.editMaterialCardView.setOnClickListener {
			val id = binding.rootMaterialCardView.tag as? Int ?: return@setOnClickListener
			
			listener.editService(it, id)
		}
		binding.deleteMaterialCardView.setOnClickListener {
			val id = binding.rootMaterialCardView.tag as? Int ?: return@setOnClickListener
			
			listener.deleteService(it, id)
		}
	}
	
	@SuppressLint("SetTextI18n")
	fun bind(item: ProviderService) {
		binding.rootMaterialCardView.tag = item.id
		
		val options = RequestOptions().let {
			if (item.isVideo) it.frame(1) else it
		}.centerCrop()
		Glide.with(binding.imageView.context)
			.load(item.imageOrVideoUrl)
			.apply(options)
			.placeholder(R.drawable.ic_logo_samee_placeholder)
			.error(R.drawable.ic_logo_samee_placeholder)
			.into(binding.imageView)
		
		binding.serviceNameTextView.text = item.name
		
		binding.servicePriceTextView.text = "${item.price} ${context.getString(R.string.sar)}"
	}
	
}

