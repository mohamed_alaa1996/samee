package app.grand.tafwak.presentation.userFlow.adapters

import android.view.ViewGroup
import androidx.recyclerview.widget.DiffUtil
import androidx.recyclerview.widget.ListAdapter
import app.grand.tafwak.domain.user.entity.model.ProviderService
import app.grand.tafwak.presentation.userFlow.adapters.viewHolders.VHItemRequiredService

class RVItemRequiredService : ListAdapter<ProviderService, VHItemRequiredService>(COMPARATOR) {
	
	companion object {
		val COMPARATOR = object : DiffUtil.ItemCallback<ProviderService>() {
			override fun areItemsTheSame(
				oldItem: ProviderService,
				newItem: ProviderService
			): Boolean = oldItem.id == newItem.id
			
			override fun areContentsTheSame(
				oldItem: ProviderService,
				newItem: ProviderService
			): Boolean = oldItem == newItem
		}
	}
	
	override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): VHItemRequiredService {
		return VHItemRequiredService(parent)
	}
	
	override fun onBindViewHolder(holder: VHItemRequiredService, position: Int) {
		holder.bind(getItem(position))
	}
	
}
