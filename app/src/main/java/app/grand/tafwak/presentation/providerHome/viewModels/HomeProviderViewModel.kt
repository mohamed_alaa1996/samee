package app.grand.tafwak.presentation.providerHome.viewModels

import android.net.Uri
import android.view.View
import androidx.fragment.app.findFragment
import androidx.lifecycle.ViewModel
import androidx.lifecycle.asLiveData
import androidx.lifecycle.switchMap
import androidx.lifecycle.viewModelScope
import androidx.navigation.NavDeepLinkRequest
import androidx.navigation.NavOptions
import app.grand.tafwak.core.customTypes.RetryAbleFlow
import app.grand.tafwak.core.extensions.findNavControllerOfProject
import app.grand.tafwak.core.extensions.map
import app.grand.tafwak.core.extensions.showErrorToast
import app.grand.tafwak.data.local.preferences.PrefsUser
import app.grand.tafwak.data.provider.repository.ProviderRepositoryImpl
import app.grand.tafwak.data.user.repository.UserRepositoryImpl
import app.grand.tafwak.domain.auth.entity.model.LocationData
import app.grand.tafwak.domain.providerFlow.CategoryWithOrders
import app.grand.tafwak.domain.utils.MAResult
import app.grand.tafwak.presentation.providerFlow.adapters.RVItemTextInCardWithNotification
import app.grand.tafwak.presentation.providerHome.adapters.RVItemOrderShownToProvider2
import app.grand.tafwak.presentation.userHome.HomeUserFragment
import app.grand.tafwak.presentation.userHome.UserFragmentDirections
import app.grand.tafwak.presentation.userHome.adapters.RVItemHomeUserServices
import app.grand.tafwak.presentation.userHome.adapters.SliderRVSliders
import com.google.gson.Gson
import com.structure.base_mvvm.R
import dagger.hilt.android.lifecycle.HiltViewModel
import kotlinx.coroutines.launch
import javax.inject.Inject

@HiltViewModel
class HomeProviderViewModel @Inject constructor(
	private val prefsUser: PrefsUser,
	private val repository: ProviderRepositoryImpl,
	gson: Gson,
) : ViewModel(), RVItemTextInCardWithNotification.Listener {
	
	val response = RetryAbleFlow(repository::getProviderHome)
	
	var selectedIndex = 0
	
	val adapterSliders = SliderRVSliders(gson)
	
	val adapterCategories = RVItemTextInCardWithNotification(this)
	
	val adapterOrders = RVItemOrderShownToProvider2()
	
	var listOfCategoryWithOrders = emptyList<CategoryWithOrders>()
	
	override fun changeCategoryWithWorking(index: Int) {
		if (index == selectedIndex) {
			return
		}
		
		val oldIndex = selectedIndex
		
		selectedIndex = index
		
		adapterCategories.notifyItemChanged(oldIndex)
		adapterCategories.notifyItemChanged(selectedIndex)
		
		adapterOrders.submitList(listOfCategoryWithOrders[selectedIndex].orders)
	}
	
	override fun isSelected(index: Int): Boolean {
		return index == selectedIndex
	}
	
}
