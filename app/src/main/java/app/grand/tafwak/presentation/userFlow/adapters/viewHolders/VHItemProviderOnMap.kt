package app.grand.tafwak.presentation.userFlow.adapters.viewHolders

import android.view.ViewGroup
import androidx.recyclerview.widget.RecyclerView
import app.grand.tafwak.core.extensions.inflateLayout
import app.grand.tafwak.core.extensions.roundHalfUp
import app.grand.tafwak.core.extensions.toIntOrFloat
import app.grand.tafwak.domain.user.entity.model.ProviderMiniDetails
import app.grand.tafwak.presentation.userFlow.adapters.RVItemProviderOnMap
import com.bumptech.glide.Glide
import com.bumptech.glide.request.RequestOptions
import com.structure.base_mvvm.R
import com.structure.base_mvvm.databinding.ItemProviderOnMapBinding
import com.structure.base_mvvm.databinding.ItemTextSearchBinding
import timber.log.Timber
import java.math.BigDecimal
import kotlin.math.roundToInt

class VHItemProviderOnMap(parent: ViewGroup, private val listener: RVItemProviderOnMap.Listener) : RecyclerView.ViewHolder(
	parent.context.inflateLayout(R.layout.item_provider_on_map, parent)
) {
	
	private val binding = ItemProviderOnMapBinding.bind(itemView)
	
	private val context get() = binding.root.context
	
	init {
		binding.materialCardView.setOnClickListener { view ->
			val id = binding.materialCardView.tag as? Int ?: return@setOnClickListener
			
			listener.goToProviderDetails(view, id)
		}
	}
	
	fun bind(details: ProviderMiniDetails) {
		binding.materialCardView.tag = details.id
		
		Glide.with(binding.imageView.context)
			.load(details.imageUrl)
			.apply(RequestOptions().centerCrop())
			.placeholder(R.drawable.ic_logo_samee_placeholder)
			.error(R.drawable.ic_logo_samee_placeholder)
			.into(binding.imageView)
		
		binding.nameTextView.text = details.name
		
		binding.ratingBar.progress = (details.averageRate ).roundToInt()
		
		binding.ratingTextView.text = context.getString(R.string.between_brackets_str, details.averageRate.roundHalfUp(scale = 2).toString())
		
		binding.descriptionTextView.text = details.description
		
		val kmDistance = (details.distanceInMeters/* / 1000f*/)
		val scaledDistance = kmDistance.roundHalfUp(1).coerceAtLeast(0.1f)
		val distance = context.getString(R.string.away_by) + " " + scaledDistance.toIntOrFloat().toString() + " " + context.getString(R.string.km_notation)
		binding.distanceTextView.text = distance
	}
	
}
