package app.grand.tafwak.presentation.userFlow.viewModel

import android.view.View
import androidx.fragment.app.findFragment
import androidx.lifecycle.ViewModel
import androidx.lifecycle.lifecycleScope
import androidx.navigation.findNavController
import androidx.navigation.fragment.findNavController
import app.grand.tafwak.core.extensions.showErrorToast
import app.grand.tafwak.data.user.repository.UserRepositoryImpl
import app.grand.tafwak.domain.utils.MAResult
import app.grand.tafwak.presentation.userFlow.CancelOrderDialogFragment
import app.grand.tafwak.presentation.userFlow.CancelOrderDialogFragmentArgs
import com.structure.base_mvvm.R
import dagger.hilt.android.lifecycle.HiltViewModel
import kotlinx.coroutines.launch
import javax.inject.Inject

@HiltViewModel
class CancelOrderViewModel @Inject constructor(
	private val args: CancelOrderDialogFragmentArgs,
	private val repository: UserRepositoryImpl
) : ViewModel() {
	
	val title = args.title
	
	val body = args.body

	fun cancelOrder(view: View) {
		val fragment = view.findFragment<CancelOrderDialogFragment>()
		
		fragment.lifecycleScope.launch {
			fragment.activityViewModel?.globalLoading?.value = true
			
			val result = repository.cancelOrder(args.id)
			
			fragment.activityViewModel?.globalLoading?.value = false
			
			when (result) {
				is MAResult.Success -> {
					val navController = fragment.findNavController()
					
					navController.popBackStack(R.id.dest_user, false)
				}
				is MAResult.Failure -> {
					fragment.requireContext().showErrorToast(result.message ?: fragment.getString(R.string.something_went_wrong))
				}
			}
		}
	}

	fun goBack(view: View) {
		view.findFragment<CancelOrderDialogFragment>().findNavController().navigateUp()
	}

}
