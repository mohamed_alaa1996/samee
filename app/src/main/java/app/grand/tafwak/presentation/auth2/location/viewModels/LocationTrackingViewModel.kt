package app.grand.tafwak.presentation.auth2.location.viewModels

import android.graphics.Bitmap
import android.view.View
import androidx.fragment.app.findFragment
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel
import androidx.lifecycle.lifecycleScope
import androidx.navigation.findNavController
import androidx.navigation.fragment.findNavController
import app.grand.tafwak.core.extensions.getAddressFromLatitudeAndLongitude
import app.grand.tafwak.core.extensions.showErrorToast
import app.grand.tafwak.core.extensions.toJson
import app.grand.tafwak.domain.auth.entity.model.LocationData
import app.grand.tafwak.presentation.auth2.location.LocationSelectionFragment
import app.grand.tafwak.presentation.auth2.location.LocationSelectionFragmentArgs
import app.grand.tafwak.presentation.auth2.location.LocationTrackingFragmentArgs
import com.google.android.gms.common.api.Status
import com.google.android.gms.maps.CameraUpdateFactory
import com.google.android.gms.maps.model.LatLng
import com.google.android.libraries.places.api.Places
import com.google.android.libraries.places.api.model.Place
import com.google.android.libraries.places.widget.AutocompleteSupportFragment
import com.google.android.libraries.places.widget.listener.PlaceSelectionListener
import com.google.gson.Gson
import com.structure.base_mvvm.R
import dagger.hilt.android.lifecycle.HiltViewModel
import kotlinx.coroutines.delay
import kotlinx.coroutines.launch
import timber.log.Timber
import javax.inject.Inject

@HiltViewModel
class LocationTrackingViewModel @Inject constructor(
	val arg: LocationTrackingFragmentArgs,
) : ViewModel() {
	
	val otherUserLatitude = MutableLiveData("")
	val otherUserLongitude = MutableLiveData("")
	
	val myLatitude = MutableLiveData("")
	val myLongitude = MutableLiveData("")
	
	val search = MutableLiveData("")
	
	val showMapNotSearchResults = MutableLiveData(true)
	
	var bitmapMine: Bitmap? = null
	var bitmapOther: Bitmap? = null
	
	fun goBack(view: View) {
		view.findNavController().navigateUp()
	}
	
}
