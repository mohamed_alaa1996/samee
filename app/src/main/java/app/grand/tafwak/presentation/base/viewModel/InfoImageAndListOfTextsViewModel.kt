package app.grand.tafwak.presentation.base.viewModel

import androidx.lifecycle.ViewModel
import app.grand.tafwak.core.customTypes.RetryAbleFlow
import app.grand.tafwak.data.maSettings.repository.SettingsRepositoryImpl
import app.grand.tafwak.presentation.base.InfoImageAndListOfTextsFragmentArgs
import app.grand.tafwak.presentation.base.customTypes.DataOfInfoImageAndListOfTexts
import dagger.hilt.android.lifecycle.HiltViewModel
import javax.inject.Inject

@HiltViewModel
class InfoImageAndListOfTextsViewModel @Inject constructor(
	args: InfoImageAndListOfTextsFragmentArgs,
	repository: SettingsRepositoryImpl
) : ViewModel() {
	
	val response = when (args.data) {
		DataOfInfoImageAndListOfTexts.USER_ABOUT -> repository::getUserAbout
		DataOfInfoImageAndListOfTexts.USER_TERMS_AND_CONDITIONS -> repository::getUserTermsAndConditions
		DataOfInfoImageAndListOfTexts.USER_PRIVACY_POLICY -> repository::getUserPrivacyPolicy
		DataOfInfoImageAndListOfTexts.PROVIDER_ABOUT -> repository::getProviderAbout
		DataOfInfoImageAndListOfTexts.PROVIDER_TERMS_AND_CONDITIONS -> repository::getProviderTermsAndConditions
		DataOfInfoImageAndListOfTexts.PROVIDER_PRIVACY_POLICY -> repository::getProviderPrivacyPolicy
	}.let {
		RetryAbleFlow(it)
	}
	
}
