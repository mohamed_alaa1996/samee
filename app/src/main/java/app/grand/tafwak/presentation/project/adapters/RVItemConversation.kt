package app.grand.tafwak.presentation.project.adapters

import android.view.ViewGroup
import androidx.paging.PagingDataAdapter
import androidx.recyclerview.widget.DiffUtil
import app.grand.tafwak.data.local.preferences.PrefsUser
import app.grand.tafwak.domain.providerFlow.ResponseConversation
import app.grand.tafwak.domain.userFlow.response.CategoryWithWorkings
import app.grand.tafwak.presentation.project.adapters.viewHolders.VHItemConversation
import app.grand.tafwak.presentation.userFlow.adapters.viewHolders.VHItemTextInCard
import okhttp3.internal.userAgent

class RVItemConversation(private val userId: Int) :
  PagingDataAdapter<ResponseConversation, VHItemConversation>(COMPARATOR) {

  companion object {
    val COMPARATOR = object : DiffUtil.ItemCallback<ResponseConversation>() {
      override fun areItemsTheSame(
        oldItem: ResponseConversation,
        newItem: ResponseConversation
      ): Boolean = oldItem.id == newItem.id

      override fun areContentsTheSame(
        oldItem: ResponseConversation,
        newItem: ResponseConversation
      ): Boolean = oldItem == newItem
    }
  }

  override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): VHItemConversation {
    return VHItemConversation(parent)
  }

  override fun onBindViewHolder(holder: VHItemConversation, position: Int) {

    val model = getItem(position)

    if (model != null) {
      model.otherUserId = if(userId == model.userOneId) {
        model.userTwoId
      } else {
        model.userOneId
      }
      holder.bind(getItem(position) ?: return)
    }

  }

}
