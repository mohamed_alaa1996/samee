package app.grand.tafwak.presentation.userFlow.viewModel

import android.content.Context
import android.net.Uri
import android.view.View
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel
import androidx.lifecycle.map
import androidx.navigation.NavDeepLinkRequest
import androidx.navigation.NavOptions
import androidx.navigation.findNavController
import app.grand.tafwak.core.customTypes.MADateAndTime
import app.grand.tafwak.core.extensions.*
import app.grand.tafwak.data.user.repository.UserRepositoryImpl
import app.grand.tafwak.domain.auth.entity.model.LocationData
import app.grand.tafwak.domain.auth.entity.model.ManualLocation
import app.grand.tafwak.domain.user.entity.model.ProviderDetails
import app.grand.tafwak.domain.utils.MABaseResponse
import app.grand.tafwak.domain.utils.MAResult
import app.grand.tafwak.presentation.userFlow.OrderConfirmationFragmentArgs
import app.grand.tafwak.presentation.userFlow.OrderConfirmationFragmentDirections
import app.grand.tafwak.presentation.userFlow.adapters.RVItemRequiredService
import com.google.gson.Gson
import com.structure.base_mvvm.R
import dagger.hilt.android.lifecycle.HiltViewModel
import javax.inject.Inject

@HiltViewModel
class OrderConfirmationViewModel @Inject constructor(
	private val repository: UserRepositoryImpl,
	args: OrderConfirmationFragmentArgs,
	private val gson: Gson,
) : ViewModel() {
	
	val providerDetails = args.providerDetailsJson.fromJson<ProviderDetails>()
	private val maDateAndTime = args.maDateAndTimeJson.fromJson<MADateAndTime>()
	val locationData = MutableLiveData(args.locationDataJson.fromJson<LocationData>())
	private val numOfPeople = args.numOfPeople
	
	val address = locationData.map { it.address }
	
	val adapter = RVItemRequiredService()
	
	/** `null` means use [locationData] when calling api to confirm order, else use `this` isa */
	var manualLocation: ManualLocation? = null
	
	val performResponse = MutableLiveData(false)
	val response = performResponse.switchMap {
		if (it == true) {
			repository.createOrder(
				providerDetails.services.map { innerIt -> innerIt.id.orZero() },
				maDateAndTime,
				servicesTotalCost.toFloat(),
				tax,
				total,
				numOfPeople,
				providerDetails.id,
				delivery,
				locationData.value!!,
				manualLocation
			)
		}else {
			MutableLiveData()
		}
	}
	
	private val servicesTotalCost by lazy {
		providerDetails.services.sumOf { it.price.orZero() } * numOfPeople
	}
	
	private val delivery by lazy {
		if (providerDetails.deliveryIsForFree) {
			null
		}else {
			val price = providerDetails.deliveryCostPerKilo * (providerDetails.distanceInMeters/* / 1000f*/)
			price.roundHalfUp(1).coerceAtLeast(0.1f)
		}
	}
	
	private val tax by lazy {
		(providerDetails.tax * servicesTotalCost.toFloat()).roundHalfUp(1)
	}
	
	private val total by lazy {
		servicesTotalCost.toFloat() + delivery.orZero() + tax
	}

	fun onManualSelectionClick(view: View) {
		view.findNavController().navigate(
			OrderConfirmationFragmentDirections.actionDestOrderConfirmationToDestManualLocation(
				manualLocation?.toJson(gson)
			)
		)
	}
	
	fun onMapSelectionClick(view: View) {
		val uri = Uri.Builder()
			.scheme("fragment-dest")
			.authority("app.grand.tafwak.global.location.selection")
			.appendPath(locationData.value!!.latitude)
			.appendPath(locationData.value!!.longitude)
			.build()
		val request = NavDeepLinkRequest.Builder.fromUri(uri).build()
		val options = NavOptions.Builder()
			.setEnterAnim(R.anim.anim_slide_in_right)
			.setExitAnim(R.anim.anim_slide_out_left)
			.setPopEnterAnim(R.anim.anim_slide_in_left)
			.setPopExitAnim(R.anim.anim_slide_out_right)
			.build()
		view.findNavControllerOfProject().navigate(request, options)
	}
	
	fun confirmOrder() {
		performResponse.value = true
	}
	
	fun getNumOfPeople(context: Context): String {
		return context.resources.getQuantityString(R.plurals.num_people, numOfPeople, numOfPeople)
	}
	
	fun getDateAndTime(context: Context): String {
		val stringBuilder = StringBuilder()
		stringBuilder.append(context.getString(R.string.execution_time))
		stringBuilder.append(" : ")
		stringBuilder.append("${maDateAndTime.hour12}:${maDateAndTime.minute}")

    if(maDateAndTime.isAm) {
      stringBuilder.append(" ${context.getString(R.string.am_full)}")

    } else {
      stringBuilder.append(" ${context.getString(R.string.pm_full)}")
    }
		stringBuilder.append(" - ")
		stringBuilder.append("${maDateAndTime.day}")
		stringBuilder.append(" ${context.resources.getStringArray(R.array.year_months)[maDateAndTime.month.dec()]} ")
		stringBuilder.append("${maDateAndTime.year}")
		
		return stringBuilder.toString()
	}
	
	fun getServicesTotalCost(context: Context): String {
		return context.getString(R.string.num_sar, servicesTotalCost)
	}
	
	fun getDelivery(context: Context): String {
		return delivery?.let { context.getString(R.string.float_sar, it) } ?: context.getString(R.string.free)
	}
	
	fun getTax(context: Context): String {
		return context.getString(R.string.float_sar, tax)
	}
	
	fun getTotal(context: Context): String {
		return context.getString(R.string.float_sar, total)
	}
	
}
