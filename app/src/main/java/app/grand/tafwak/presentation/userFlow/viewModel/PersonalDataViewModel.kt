package app.grand.tafwak.presentation.userFlow.viewModel

import android.app.Application
import android.net.Uri
import android.text.style.ForegroundColorSpan
import android.view.View
import androidx.core.text.buildSpannedString
import androidx.core.text.set
import androidx.fragment.app.findFragment
import androidx.lifecycle.*
import androidx.lifecycle.map
import androidx.navigation.NavDeepLinkRequest
import androidx.navigation.NavOptions
import androidx.navigation.findNavController
import androidx.navigation.fragment.findNavController
import app.grand.tafwak.core.extensions.*
import app.grand.tafwak.data.local.preferences.PrefsUser
import app.grand.tafwak.data.user.repository.UserRepositoryImpl
import app.grand.tafwak.domain.auth.entity.model.LocationData
import app.grand.tafwak.domain.utils.MAResult
import app.grand.tafwak.presentation.auth2.logIn.LoginFragmentDirections
import app.grand.tafwak.presentation.userFlow.PersonalDataFragment
import app.grand.tafwak.presentation.userFlow.PersonalDataFragmentArgs
import com.google.android.material.textfield.TextInputLayout
import com.structure.base_mvvm.R
import dagger.hilt.android.lifecycle.HiltViewModel
import kotlinx.coroutines.delay
import kotlinx.coroutines.flow.first
import kotlinx.coroutines.flow.map
import kotlinx.coroutines.launch
import okhttp3.MultipartBody
import okhttp3.RequestBody
import okhttp3.RequestBody.Companion.asRequestBody
import okhttp3.RequestBody.Companion.toRequestBody
import retrofit2.http.Multipart
import retrofit2.http.Part
import timber.log.Timber
import java.io.File
import javax.inject.Inject
import kotlin.text.orEmpty

@HiltViewModel
class PersonalDataViewModel @Inject constructor(
	application: Application,
	private val repository: UserRepositoryImpl,
	val args: PersonalDataFragmentArgs,
) : AndroidViewModel(application) {

	val suffixOfEMail by lazy {
		app.getString(R.string.optional_between_brackets)
	}

	val isUserNotProvider = args.isUserNotProvider
	
	val imageUrl = MutableLiveData("")
	
	var imageUri: Uri? = null
	
	val name = MutableLiveData("")
	
	val phone = MutableLiveData("")

	val locationData = MutableLiveData<LocationData>()
	
	val address = locationData.map { Timber.e("how come $it"); it?.address.orEmpty() }

	val referCode = MutableLiveData("")

	val email = MutableLiveData("")
	
	// todo on receive api value adjust it, and on save if provider check not null then update it isa.
	val description = MutableLiveData("")

	val usedCodeBefore = MutableLiveData(false)
	val referredByText: LiveData<CharSequence> = usedCodeBefore.map { usedBefore ->
		buildSpannedString {
			if (usedBefore) {
				append(app.getString(R.string.used_code_before))
			}else {
				append(app.getString(R.string.did_not_used_code_before))

				this[0..length] = ForegroundColorSpan(R.color.dialog_background_window_primary_no_alpha)
			}
		}
	}

	fun useReferCode(view: View) {
		if (usedCodeBefore.value == true) return

		val fragment = view.findFragment<PersonalDataFragment>()
		val navController = fragment.findNavController()

		//      app:uri="dialog-dest://app.grand.tafwak.dest.check.referral.code.with.your.own.refer.code.dialog/{yourOwnReferCode}" />
		navController.navigateDeepLinkWithoutOptions(
			"dialog-dest",
			"app.grand.tafwak.dest.check.referral.code.with.your.own.refer.code.dialog",
			referCode.value.orEmpty()
		)
		/*
		dialog-dest://

        <deepLink
            android:id="@+id/deep_link_dest_global_error_dialog"
            android:autoVerify="true"
            app:uri="dialog-dest://app.grand.tafwak.global.error.dialog/{message}" />
		 */
	}
	
	fun changeImage(view: View) {
		view.findFragment<PersonalDataFragment>().pickImageOrRequestPermissions()
	}

	fun shareReferCode(view: View) {
		view.context?.launchShareText(referCode.value.orEmpty())
	}
	
	fun changeLocation(view: View) {
		val uri = Uri.Builder()
			.scheme("fragment-dest")
			.authority("app.grand.tafwak.global.location.selection")
			.appendPath(locationData.value!!.latitude)
			.appendPath(locationData.value!!.longitude)
			.build()
		val request = NavDeepLinkRequest.Builder.fromUri(uri).build()
		val options = NavOptions.Builder()
			.setEnterAnim(R.anim.anim_slide_in_right)
			.setExitAnim(R.anim.anim_slide_out_left)
			.setPopEnterAnim(R.anim.anim_slide_in_left)
			.setPopExitAnim(R.anim.anim_slide_out_right)
			.build()
		view.findNavController().navigate(request, options)
	}
	
	fun onSaveClick(view: View) {
		val locationData = locationData.value ?: return
		
		if (name.value.isNullOrEmpty() || phone.value.isNullOrEmpty() ||
			(!isUserNotProvider && description.value.isNullOrEmpty())) {
			return view.context.showErrorToast(view.context.getString(R.string.all_fields_required))
		}
		
		val fragment = view.findFragment<PersonalDataFragment>()
		viewModelScope.launch {
			fragment.activityViewModel?.globalLoading?.value = true
			
			val result = repository.updateProfile(
				name.value.orEmpty(),
				phone.value.orEmpty(),
				email.value.orEmpty(),
				locationData.latitude,
				locationData.longitude,
				locationData.address,
				if (isUserNotProvider) null else description.value.orEmpty(),
				imageUri?.createMultipartBodyPart(app, "image")
			)
			
			when (result) {
				is MAResult.Failure -> {
					fragment.requireContext().showErrorToast(result.message ?: fragment.getString(R.string.something_went_wrong))
					
					fragment.activityViewModel?.globalLoading?.value = false
				}
				is MAResult.Success -> {
					Timber.e("hello 212 -> ${result.value.data?.imageUrl} $imageUri")
					if (imageUri != null) {
						fragment.prefsUser.setImageUrl(result.value.data!!.imageUrl)
					}
					fragment.prefsUser.setName(name.value.orEmpty())
					fragment.prefsUser.setPhone(phone.value.orEmpty())
					fragment.prefsUser.setEMail(email.value.orEmpty())
					fragment.prefsUser.setLocation(locationData)
					fragment.prefsUser.setDescription(description.value.orEmpty())
					
					fragment.activityViewModel?.globalLoading?.value = false

					fragment.context?.showSuccessToast(fragment.getString(R.string.edit_done_successfully))
					
					delay(150)
					
					fragment.findNavController().navigateUp()
				}
			}
		}
	}

}
