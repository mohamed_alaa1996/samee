package app.grand.tafwak.presentation.userFlow

import android.os.Bundle
import android.view.View
import androidx.fragment.app.activityViewModels
import androidx.fragment.app.viewModels
import androidx.lifecycle.lifecycleScope
import androidx.navigation.fragment.findNavController
import androidx.navigation.fragment.navArgs
import androidx.recyclerview.widget.StaggeredGridLayoutManager
import app.grand.tafwak.core.extensions.showErrorToast
import app.grand.tafwak.data.local.preferences.PrefsUser
import app.grand.tafwak.domain.userFlow.model.SearchFilter
import app.grand.tafwak.domain.utils.MAResult
import app.grand.tafwak.presentation.base.MABaseFragment
import app.grand.tafwak.presentation.project.viewModel.ProjectViewModel
import app.grand.tafwak.presentation.userFlow.viewModel.SearchResultsProvidersViewModel
import com.structure.base_mvvm.R
import com.structure.base_mvvm.databinding.FragmentSearchResultsProvidersBinding
import dagger.hilt.android.AndroidEntryPoint
import kotlinx.coroutines.delay
import kotlinx.coroutines.flow.first
import kotlinx.coroutines.launch
import javax.inject.Inject

@AndroidEntryPoint
class SearchResultsProvidersFragment : MABaseFragment<FragmentSearchResultsProvidersBinding>() {
	
	private val viewModel by viewModels<SearchResultsProvidersViewModel>()
	
	@Inject
	protected lateinit var prefsUser: PrefsUser
	
	companion object {
		const val KEY_FRAGMENT_RESULT_FILTER_AS_STRING = "KEY_FRAGMENT_RESULT_FILTER_AS_STRING"
	}
	
	override fun getLayoutId(): Int = R.layout.fragment_search_results_providers
	
	override fun initializeBindingVariables() {
		binding?.viewModel = viewModel
	}
	
	override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
		findNavController().currentBackStackEntry?.savedStateHandle?.getLiveData(
			KEY_FRAGMENT_RESULT_FILTER_AS_STRING,
			SearchFilter.LATEST.name
		)?.observe(viewLifecycleOwner) {
			viewModel.filter.value = SearchFilter.valueOf(it)
		}

		binding?.recyclerView?.layoutManager = StaggeredGridLayoutManager(2, StaggeredGridLayoutManager.VERTICAL)
		binding?.recyclerView?.adapter = viewModel.adapter
		
		viewModel.response.observe(viewLifecycleOwner) {
			when (it) {
				is MAResult.Loading -> {
					activityViewModel?.globalLoading?.value = true
				}
				is MAResult.Failure -> {
					activityViewModel?.globalLoading?.value = false
					
					requireContext().showErrorToast(it.message ?: getString(R.string.something_went_wrong))
					
					viewModel.handleDataResult(emptyList())
				}
				is MAResult.Success -> {
					activityViewModel?.globalLoading?.value = false
					
					viewModel.handleDataResult(it.value.data?.providers.orEmpty())
				}
			}
		}
		
		lifecycleScope.launch {
			activityViewModel?.globalLoading?.value = true
			
			// leave time for animation
			delay(300)
			
			viewModel.latLongToken.value = prefsUser.getLatLongToken().first()
			
			activityViewModel?.titleToolbar?.postValue(viewModel.args.categoryName ?: getString(R.string.search))
		}
	}

}
