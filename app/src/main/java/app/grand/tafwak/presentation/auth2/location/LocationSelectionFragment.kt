package app.grand.tafwak.presentation.auth2.location

import android.Manifest
import android.annotation.SuppressLint
import android.content.Context
import android.content.pm.PackageManager
import android.location.Location
import android.os.Bundle
import android.view.View
import androidx.activity.result.contract.ActivityResultContracts
import androidx.annotation.RequiresPermission
import androidx.core.content.ContextCompat
import androidx.fragment.app.activityViewModels
import androidx.fragment.app.viewModels
import app.grand.tafwak.core.customTypes.LocationHandler
import app.grand.tafwak.core.extensions.showErrorToast
import app.grand.tafwak.core.extensions.showNormalToast
import app.grand.tafwak.presentation.auth2.location.viewModels.LocationSelectionViewModel
import app.grand.tafwak.presentation.base.MABaseFragment
import app.grand.tafwak.presentation.base.extensions.executeShowingErrorOnce
import app.grand.tafwak.presentation.project.viewModel.ProjectViewModel
import com.google.android.gms.location.FusedLocationProviderClient
import com.google.android.gms.location.LocationRequest
import com.google.android.gms.location.LocationServices
import com.google.android.gms.maps.*
import com.google.android.gms.maps.model.LatLng
import com.google.android.gms.tasks.CancellationToken
import com.google.android.gms.tasks.OnTokenCanceledListener
import com.structure.base_mvvm.R
import com.structure.base_mvvm.databinding.FragmentLocationSelectionBinding
import dagger.hilt.android.AndroidEntryPoint
import java.lang.Exception
import kotlin.math.min

@AndroidEntryPoint
class LocationSelectionFragment : MABaseFragment<FragmentLocationSelectionBinding>(),
	OnMapReadyCallback, LocationHandler.Listener {
	
	companion object {
		const val KEY_FRAGMENT_RESULT_LOCATION_DATA_AS_JSON = "KEY_FRAGMENT_RESULT_LOCATION_DATA_AS_JSON"
	}
	
	private val viewModel by viewModels<LocationSelectionViewModel>()
	
	var googleMap: GoogleMap? = null
		private set

	lateinit var locationHandler: LocationHandler
		private set
	
	/** Zoom levels https://developers.google.com/maps/documentation/android-sdk/views#zoom */
	val zoom get() = min(googleMap?.maxZoomLevel ?: 5f, 15f)
	
	override fun onCreate(savedInstanceState: Bundle?) {
		locationHandler = LocationHandler(
			this,
			lifecycle,
			requireContext(),
			this
		)

		super.onCreate(savedInstanceState)
	}
	
	override fun getLayoutId(): Int = R.layout.fragment_location_selection
	
	override fun initializeBindingVariables() {
		binding?.viewModel = viewModel
	}
	
	override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
		// Setup map
		(childFragmentManager.findFragmentById(R.id.mapFragmentContainerView) as? SupportMapFragment)
			?.getMapAsync(this)
	}
	
	override fun onMapReady(googleMap: GoogleMap) {
		this.googleMap = googleMap
		
		if (viewModel.arg.latitude == null || viewModel.arg.longitude == null) {
			locationHandler.requestCurrentLocation(true)
		}
		
		val location = LatLng(
			viewModel.arg.latitude?.toDouble() ?: 0.0,
			viewModel.arg.longitude?.toDouble() ?: 0.0
		)
		
		googleMap.moveCamera(CameraUpdateFactory.newLatLngZoom(location, zoom))
	}

	override fun onCurrentLocationResultSuccess(location: Location) {
		val latLng = LatLng(location.latitude, location.longitude)

		viewModel.myCurrentLocation = latLng

		googleMap?.moveCamera(CameraUpdateFactory.newLatLngZoom(latLng, zoom))
	}

	override fun onChangeLocationFailure(context: Context?, exception: Exception?) {
		executeShowingErrorOnce(
			false,
			getString(R.string.there_is_an_error_in_detecting_your_location)
		) {
			locationHandler.requestLocationUpdates()
		}
	}

	override fun onDestroyView() {
		locationHandler.stopLocationUpdates()

		super.onDestroyView()
	}
	
}
