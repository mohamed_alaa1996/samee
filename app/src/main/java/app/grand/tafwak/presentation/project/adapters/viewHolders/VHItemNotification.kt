package app.grand.tafwak.presentation.project.adapters.viewHolders

import android.net.Uri
import android.view.ViewGroup
import androidx.navigation.NavDeepLinkRequest
import androidx.navigation.findNavController
import androidx.recyclerview.widget.ItemTouchHelper
import androidx.recyclerview.widget.RecyclerView
import app.grand.tafwak.core.extensions.findNavControllerOfProject
import app.grand.tafwak.core.extensions.inflateLayout
import app.grand.tafwak.core.extensions.showErrorToast
import app.grand.tafwak.domain.providerFlow.ResponseConversation
import app.grand.tafwak.domain.providerFlow.ResponseNotification
import app.grand.tafwak.domain.providerFlow.ResponseNotification.Type.*
import app.grand.tafwak.presentation.project.ConversationsFragmentDirections
import app.grand.tafwak.presentation.project.NotificationsFragmentDirections
import com.bumptech.glide.Glide
import com.bumptech.glide.request.RequestOptions
import com.structure.base_mvvm.R
import com.structure.base_mvvm.databinding.ItemConversationBinding
import com.structure.base_mvvm.databinding.ItemNotificationBinding
import timber.log.Timber

class VHItemNotification(parent: ViewGroup) : RecyclerView.ViewHolder(
	parent.context.inflateLayout(R.layout.item_notification, parent)
) {
	
	val binding = ItemNotificationBinding.bind(itemView)
	
	fun canBeSwiped(): Boolean {
		val notificationType = ResponseNotification.toTypeOrNull(
			binding.materialCardView.tag as? String ?: return false
		)
		
		return notificationType == ORDER_PAYMENT
	}
	
	fun getTargetIdOrNull(): Int? {
		return binding.dateTextView.tag as? Int
	}
	
	init {
		binding.materialCardView.setOnClickListener {
			val notificationType = ResponseNotification.toTypeOrNull(
				binding.materialCardView.tag as? String ?: return@setOnClickListener
			)
			
			val targetId = binding.dateTextView.tag as? Int ?: return@setOnClickListener
			
			when (notificationType) {
				ADMIN -> { /* Not clickable (No function for it isa.) */ }
				NEW_ORDER -> {
					val goToProvider = it.findNavController().currentDestination?.id == R.id.dest_provider
					
					val uri = if (goToProvider) {
						Uri.Builder()
							.scheme("fragment-dest")
							.authority("app.grand.tafwak.dest.order.details.seen.by.provider")
							.appendPath(targetId.toString())
							.build()
					}else {
						// User seeing -> order details
						Uri.Builder()
							.scheme("fragment-dest")
							.authority("app.grand.tafwak.dest.order.details")
							.appendPath(targetId.toString())
							.build()
					}
					val request = NavDeepLinkRequest.Builder.fromUri(uri).build()
					it.findNavControllerOfProject().navigate(request)
				}
				WALLET -> {
					val uri = Uri.Builder()
						.scheme("fragment-dest")
						.authority("app.grand.tafwak.dest.wallet")
						.build()
					val request = NavDeepLinkRequest.Builder.fromUri(uri).build()
					it.findNavControllerOfProject().navigate(request)
				}
				ORDER_PAYMENT -> {
					it.findNavController().navigate(
						NotificationsFragmentDirections.actionDestNotificationsToDestContinuePayment(targetId)
					)
				}
				else -> {
					it.context.showErrorToast(binding.materialCardView.tag?.toString() ?: it.context.getString(R.string.something_went_wrong))
					
					Timber.e("Unknown notification type for ${binding.materialCardView.tag}")
				}
			}
		}
	}
	
	fun bind(item: ResponseNotification) {
		binding.materialCardView.tag = item.notificationType
		binding.dateTextView.tag = item.targetId
		
		binding.dateTextView.text = item.readAt
		binding.titleTextView.text = item.title.orEmpty()
		binding.bodyTextView.text = item.description.orEmpty()
	}
	
}
