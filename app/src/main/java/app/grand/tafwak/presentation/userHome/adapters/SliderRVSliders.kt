package app.grand.tafwak.presentation.userHome.adapters

import android.view.ViewGroup
import app.grand.tafwak.domain.user.entity.model.Slider
import app.grand.tafwak.presentation.userHome.adapters.viewHolders.SliderVHSliders
import com.google.gson.Gson
import com.smarteist.autoimageslider.SliderViewAdapter

class SliderRVSliders(private val gson: Gson) : SliderViewAdapter<SliderVHSliders>() {

	private var list = emptyList<Slider>()
	
	override fun getCount(): Int = list.size
	
	override fun onCreateViewHolder(parent: ViewGroup): SliderVHSliders {
		return SliderVHSliders(parent, gson)
	}
	
	override fun onBindViewHolder(holder: SliderVHSliders, position: Int) {
		holder.bind(list[position])
	}
	
	fun updateList(list: List<Slider>) {
		this.list = list
		notifyDataSetChanged()
	}

}
