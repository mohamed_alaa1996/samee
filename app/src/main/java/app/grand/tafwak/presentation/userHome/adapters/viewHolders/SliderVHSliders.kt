package app.grand.tafwak.presentation.userHome.adapters.viewHolders

import android.net.Uri
import android.view.ViewGroup
import android.widget.ImageView
import androidx.core.view.setPadding
import androidx.fragment.app.Fragment
import androidx.fragment.app.findFragment
import androidx.lifecycle.viewModelScope
import androidx.navigation.NavDeepLinkRequest
import androidx.navigation.Navigation
import androidx.navigation.findNavController
import androidx.navigation.fragment.FragmentNavigatorExtras
import androidx.paging.PagingData
import app.grand.tafwak.core.extensions.*
import app.grand.tafwak.domain.user.entity.model.Slider
import app.grand.tafwak.presentation.base.customTypes.DataOfInfoImageAndListOfTexts
import app.grand.tafwak.presentation.base.extensions.getTagViaGson
import app.grand.tafwak.presentation.base.extensions.setTagViaGson
import app.grand.tafwak.presentation.userHome.HomeUserFragment
import app.grand.tafwak.presentation.userHome.UserFragmentDirections
import com.bumptech.glide.Glide
import com.bumptech.glide.request.RequestOptions
import com.google.gson.Gson
import com.smarteist.autoimageslider.SliderViewAdapter
import com.structure.base_mvvm.R
import com.structure.base_mvvm.databinding.ItemImageBinding
import kotlinx.coroutines.launch
import timber.log.Timber
import kotlin.math.roundToInt

class SliderVHSliders(parent: ViewGroup, private val gson: Gson) : SliderViewAdapter.ViewHolder(
	parent.context.inflateLayout(R.layout.item_image, parent)
) {
	
	private val binding = ItemImageBinding.bind(itemView)

	init {
		binding.imageView.setOnClickListener {
			val slider = binding.imageView.getTagViaGson<Slider>(gson, R.id.view_holder_id)
				?: return@setOnClickListener
			
			if (slider.linkType == Slider.LINK_TYPE_PUBLIC) {
				val uri = Uri.Builder()
					.scheme("dialog-dest")
					.authority("app.grand.tafwak.global.showing.image.dialog")
					.appendPath(slider.imageUrl)
					.build()
				val request = NavDeepLinkRequest.Builder.fromUri(uri).build()
				
				Navigation.findNavController(
					binding.imageView.findFragment<Fragment>().requireActivity(),
					R.id.mainNavHostFragment
				).navigate(request)
			}else {
				it.findFragment<HomeUserFragment>().viewModel.apply {
					viewModelScope.launch {
						adapterCategories.submitData(PagingData.empty())
					}
				}
				
				it.findNavControllerOfProject().navigate(
					UserFragmentDirections.actionDestUserToDestProviderDetails(slider.linkId)
				)
			}
		}
	}
	
	fun bind(slider: Slider) {
		binding.imageView.setTagViaGson(slider, gson, R.id.view_holder_id)


    try {
      Glide.with(binding.imageView.context)
        .load(slider.imageUrl)
        //.apply(RequestOptions().centerCrop())
        .placeholder(R.drawable.ic_logo_samee_placeholder)
        .error(R.drawable.ic_logo_samee_placeholder)
        .listenerOnResourceReadyFitXY(binding.imageView)
        .into(binding.imageView)

    }catch (e : Exception) {

    }
    	}

}
