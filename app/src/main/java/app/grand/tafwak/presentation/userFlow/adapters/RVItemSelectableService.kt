package app.grand.tafwak.presentation.userFlow.adapters

import android.view.ViewGroup
import androidx.recyclerview.widget.DiffUtil
import androidx.recyclerview.widget.ListAdapter
import app.grand.tafwak.domain.user.entity.model.ProviderService
import app.grand.tafwak.presentation.userFlow.adapters.viewHolders.VHItemSelectableService

class RVItemSelectableService(private val listener: Listener) : ListAdapter<ProviderService, VHItemSelectableService>(COMPARATOR) {
	
	companion object {
		val COMPARATOR = object : DiffUtil.ItemCallback<ProviderService>() {
			override fun areItemsTheSame(
				oldItem: ProviderService,
				newItem: ProviderService
			): Boolean = oldItem.id == newItem.id
			
			override fun areContentsTheSame(
				oldItem: ProviderService,
				newItem: ProviderService
			): Boolean = oldItem == newItem
		}
	}
	
	private val selectedIds: MutableList<Int> = mutableListOf()
	
	override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): VHItemSelectableService {
		return VHItemSelectableService(parent, this)
	}
	
	override fun onBindViewHolder(holder: VHItemSelectableService, position: Int) {
		val item = getItem(position)
		holder.bind(item, item.id in selectedIds, position)
	}
	
	fun toggleSelection(id: Int, positionInAdapter: Int) {
		if (id in selectedIds) {
			selectedIds.remove(id)
		}else{
			selectedIds.add(id)
		}
		
		listener.afterChangeSelection(getSelectedServices())
		
		notifyItemChanged(positionInAdapter)
	}
	
	fun getSelectedServices(): List<ProviderService> {
		return currentList.filter { it.id in selectedIds }
	}
	
	interface Listener {
		fun afterChangeSelection(newlySelectedItems: List<ProviderService>)
	}
	
}
