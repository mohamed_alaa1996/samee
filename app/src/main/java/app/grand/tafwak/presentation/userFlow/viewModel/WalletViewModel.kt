package app.grand.tafwak.presentation.userFlow.viewModel

import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel
import app.grand.tafwak.core.customTypes.RetryAbleFlow
import app.grand.tafwak.data.user.repository.UserRepositoryImpl
import app.grand.tafwak.presentation.userFlow.adapters.RVWalletItem
import dagger.hilt.android.lifecycle.HiltViewModel
import javax.inject.Inject

@HiltViewModel
class WalletViewModel @Inject constructor(
	repository: UserRepositoryImpl
) : ViewModel() {
	
	val response = RetryAbleFlow(repository::getWallet)
	
	val wallet = MutableLiveData("")
	
	val adapter = RVWalletItem()
	
}
