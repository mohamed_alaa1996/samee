package app.grand.tafwak.presentation.userHome.viewModel

import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel
import app.grand.tafwak.data.user.repository.UserRepositoryImpl
import app.grand.tafwak.presentation.providerHome.adapters.RVItemOrderShownToProvider
import app.grand.tafwak.presentation.userHome.OrdersUserFragmentArgs
import app.grand.tafwak.presentation.userHome.adapters.RVItemOrderMiniDetails
import com.google.gson.Gson
import dagger.hilt.android.lifecycle.HiltViewModel
import javax.inject.Inject

@HiltViewModel
class OrdersUserViewModel @Inject constructor(
	repository: UserRepositoryImpl,
	args: OrdersUserFragmentArgs,
	gson: Gson,
) : ViewModel() {
	
	val showCurrentOrders = MutableLiveData(true)
	
	val currentOrders = repository.getCurrentOrders()
	
	val finishedOrders = repository.getFinishedOrders()
	
	val adapterCurrentOrders = if (args.isUserNotProvider) {
		RVItemOrderMiniDetails(gson)
	}else {
		RVItemOrderShownToProvider()
	}
	
	val adapterPreviousOrders = if (args.isUserNotProvider) {
		RVItemOrderMiniDetails(gson)
	}else {
		RVItemOrderShownToProvider()
	}
	
	fun toggleShowCurrentOrders() {
		val value = showCurrentOrders.value ?: false
		showCurrentOrders.value = !value
	}

}
