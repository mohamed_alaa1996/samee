package app.grand.tafwak.presentation.base.extensions

import android.view.View
import androidx.annotation.DrawableRes
import androidx.appcompat.content.res.AppCompatResources
import androidx.appcompat.widget.AppCompatRatingBar
import androidx.databinding.BindingAdapter

@BindingAdapter("appCompatRatingBar_setProgressBA")
fun AppCompatRatingBar.setProgressBA(progress: Int?) {
	this.progress = progress ?: 0
}
