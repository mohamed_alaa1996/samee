package app.grand.tafwak.presentation.userFlow.adapters

import android.view.View
import android.view.ViewGroup
import androidx.paging.PagingDataAdapter
import androidx.recyclerview.widget.DiffUtil
import androidx.recyclerview.widget.ListAdapter
import app.grand.tafwak.domain.user.entity.model.ResponseSearch
import app.grand.tafwak.presentation.userFlow.adapters.viewHolders.VHItemTextSearch

class RVItemTextSearch(private val listener: Listener) : PagingDataAdapter<ResponseSearch, VHItemTextSearch>(COMPARATOR) {
	
	companion object {
		val COMPARATOR = object : DiffUtil.ItemCallback<ResponseSearch>() {
			override fun areItemsTheSame(
				oldItem: ResponseSearch,
				newItem: ResponseSearch
			): Boolean = oldItem.id == newItem.id
			
			override fun areContentsTheSame(
				oldItem: ResponseSearch,
				newItem: ResponseSearch
			): Boolean = oldItem == newItem
		}
	}
	
	override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): VHItemTextSearch {
		return VHItemTextSearch(parent, listener)
	}
	
	override fun onBindViewHolder(holder: VHItemTextSearch, position: Int) {
		holder.bind(getItem(position) ?: return)
	}
	
	interface Listener {
		fun onQueryClick(view: View, id: Int, query: String)
	}
	
}
