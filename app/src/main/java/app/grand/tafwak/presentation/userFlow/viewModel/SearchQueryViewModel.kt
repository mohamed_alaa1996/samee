package app.grand.tafwak.presentation.userFlow.viewModel

import android.view.KeyEvent
import android.view.View
import android.view.inputmethod.EditorInfo
import android.widget.TextView
import androidx.fragment.app.findFragment
import androidx.lifecycle.*
import androidx.navigation.findNavController
import androidx.navigation.fragment.findNavController
import androidx.paging.PagingData
import app.grand.tafwak.core.customTypes.TransformationsUtils
import app.grand.tafwak.core.extensions.showErrorToast
import app.grand.tafwak.core.extensions.switchMapMultiple
import app.grand.tafwak.data.auth.repository.MAAuthRepositoryImpl
import app.grand.tafwak.data.local.preferences.PrefsProvider
import app.grand.tafwak.data.user.dataSource.remote.UserRemoteDataSource
import app.grand.tafwak.data.user.repository.UserRepositoryImpl
import app.grand.tafwak.domain.utils.MAResult
import app.grand.tafwak.presentation.userFlow.SearchQueryFragment
import app.grand.tafwak.presentation.userFlow.SearchQueryFragmentDirections
import app.grand.tafwak.presentation.userFlow.adapters.RVItemTextSearch
import com.structure.base_mvvm.R
import dagger.hilt.android.lifecycle.HiltViewModel
import kotlinx.coroutines.ExperimentalCoroutinesApi
import kotlinx.coroutines.delay
import kotlinx.coroutines.flow.MutableStateFlow
import kotlinx.coroutines.flow.flatMapLatest
import kotlinx.coroutines.flow.flowOf
import kotlinx.coroutines.launch
import javax.inject.Inject

@HiltViewModel
class SearchQueryViewModel @Inject constructor(
	private val repository: UserRepositoryImpl
) : ViewModel(), RVItemTextSearch.Listener {

	val query = MutableLiveData("")
	
	private val apiSearchQuery = MutableStateFlow(query.value.orEmpty())
	
	@OptIn(ExperimentalCoroutinesApi::class)
	val categories = apiSearchQuery.flatMapLatest {
		if (it.isEmpty()) flowOf(PagingData.empty()) else repository.searchCategories(it)
	}
	
	val showResults = switchMapMultiple(query, apiSearchQuery.asLiveData()) {
		MutableLiveData(!query.value.isNullOrEmpty() && apiSearchQuery.value == query.value)
	}
	
	val adapter = RVItemTextSearch(this)
	
	fun onSearchTextSubmit() = TextView.OnEditorActionListener { textView, actionId, _ ->
		if (actionId == EditorInfo.IME_ACTION_SEARCH) {
			val query = textView.text?.toString().orEmpty()
			
			apiSearchQuery.value = query
			
			return@OnEditorActionListener true
		}
		
		false
	}
	
	override fun onQueryClick(view: View, id: Int, query: String) {
		val fragment = view.findFragment<SearchQueryFragment>()
		
		fragment.findNavController().navigate(
			SearchQueryFragmentDirections.actionDestSearchQueryToDestSearchResultsProviders(query, id, null)
		)
	}
	
}
