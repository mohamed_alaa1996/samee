package app.grand.tafwak.presentation.providerHome.adapters

import android.view.ViewGroup
import androidx.paging.PagingDataAdapter
import androidx.recyclerview.widget.DiffUtil
import app.grand.tafwak.domain.userFlow.response.OrderMiniDetails
import app.grand.tafwak.presentation.providerHome.adapters.viewHolders.VHItemOrderShownToProvider
import app.grand.tafwak.presentation.userHome.adapters.viewHolders.VHItemOrderMiniDetails

class RVItemOrderShownToProvider : PagingDataAdapter<OrderMiniDetails, VHItemOrderShownToProvider>(COMPARATOR) {
	
	companion object {
		val COMPARATOR = object : DiffUtil.ItemCallback<OrderMiniDetails>() {
			override fun areItemsTheSame(
				oldItem: OrderMiniDetails,
				newItem: OrderMiniDetails
			): Boolean = oldItem.id == newItem.id
			
			override fun areContentsTheSame(
				oldItem: OrderMiniDetails,
				newItem: OrderMiniDetails
			): Boolean = oldItem == newItem
		}
	}
	
	override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): VHItemOrderShownToProvider {
		return VHItemOrderShownToProvider(parent)
	}
	
	override fun onBindViewHolder(holder: VHItemOrderShownToProvider, position: Int) {
		// Ignore null value as I will not use placeholders isa.
		holder.bind(getItem(position) ?: return)
	}
	
}
