package app.grand.tafwak.presentation.userFlow.adapters.viewHolders

import android.view.ViewGroup
import androidx.navigation.findNavController
import androidx.recyclerview.widget.RecyclerView
import app.grand.tafwak.core.extensions.inflateLayout
import app.grand.tafwak.domain.user.entity.model.ResponseSearch
import app.grand.tafwak.domain.user.entity.model.ServiceCategory
import app.grand.tafwak.presentation.userFlow.adapters.RVItemTextSearch
import com.bumptech.glide.Glide
import com.bumptech.glide.request.RequestOptions
import com.structure.base_mvvm.R
import com.structure.base_mvvm.databinding.ItemHomeUserServicesBinding
import com.structure.base_mvvm.databinding.ItemTextSearchBinding

class VHItemTextSearch(parent: ViewGroup, private val listener: RVItemTextSearch.Listener) : RecyclerView.ViewHolder(
	parent.context.inflateLayout(R.layout.item_text_search, parent)
) {
	
	private val binding = ItemTextSearchBinding.bind(itemView)
	
	init {
		binding.textView.setOnClickListener { view ->
			val id = binding.textView.tag as? Int ?: return@setOnClickListener
			
			listener.onQueryClick(view, id, binding.textView.text?.toString().orEmpty())
		}
	}
	
	fun bind(response: ResponseSearch) {
		binding.textView.tag = response.id
		
		binding.textView.text = response.name
	}
	
}
