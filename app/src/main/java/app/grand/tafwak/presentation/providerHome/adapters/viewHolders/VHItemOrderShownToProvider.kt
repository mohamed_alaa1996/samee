package app.grand.tafwak.presentation.providerHome.adapters.viewHolders

import android.text.style.ForegroundColorSpan
import android.view.ViewGroup
import androidx.core.text.buildSpannedString
import androidx.core.text.set
import androidx.core.view.isVisible
import androidx.recyclerview.widget.RecyclerView
import app.grand.tafwak.core.extensions.findNavControllerOfProject
import app.grand.tafwak.core.extensions.inflateLayout
import app.grand.tafwak.domain.userFlow.response.OrderMiniDetails
import app.grand.tafwak.domain.userFlow.response.OrderStatus
import app.grand.tafwak.presentation.providerHome.ProviderFragmentDirections
import app.grand.tafwak.presentation.userHome.UserFragmentDirections
import com.bumptech.glide.Glide
import com.bumptech.glide.request.RequestOptions
import com.structure.base_mvvm.R
import com.structure.base_mvvm.databinding.ItemOrderMiniDetailsBinding
import com.structure.base_mvvm.databinding.ItemOrderShownToProviderBinding
import kotlin.math.roundToInt

class VHItemOrderShownToProvider(parent: ViewGroup) : RecyclerView.ViewHolder(
	parent.context.inflateLayout(R.layout.item_order_shown_to_provider, parent)
) {
	
	private val binding = ItemOrderShownToProviderBinding.bind(itemView)
	
	private val context get() = binding.materialCardView.context
	
	private val colorHalfBlack by lazy {
		context.getColor(R.color.black_alpha_50)
	}
	
	init {
		binding.detailsMaterialButton.setOnClickListener { view ->
			val id = binding.materialCardView.tag as? Int ?: return@setOnClickListener
			
			view.findNavControllerOfProject().navigate(
				ProviderFragmentDirections.actionDestProviderToDestOrderDetailsSeenByProvider(id)
			)
		}
	}
	
	fun bind(details: OrderMiniDetails) {
		binding.materialCardView.tag = details.id
		
		binding.orderNumberTextView.text = buildSpannedString {
			append(context.getString(R.string.order_number))
			append(" : ")
			
			val start = length
			append(details.orderNumber)
			this[start..length] = ForegroundColorSpan(colorHalfBlack)
		}
		
		binding.dateAndTimeTextView.text = buildSpannedString {
			append(context.getString(R.string.execution_time))
			append(" : ")

			val start = length
			append("(${details.time})")
			append(" - ")
			append("(${details.date})")
			this[start..length] = ForegroundColorSpan(colorHalfBlack)
		}
		
		binding.peopleNumberTextView.text = buildSpannedString {
			append(context.getString(R.string.people_count))
			append(" : ")
			
			val start = length
			append(details.peopleCount.toString())
			this[start..length] = ForegroundColorSpan(colorHalfBlack)
		}
		
		binding.priceTextView.text = buildString {
			append(context.getString(R.string.sar_caps))
			
			appendLine()
			appendLine()
			
			append("${details.price} ${context.getString(R.string.sar)}")
		}
	}
	
}
