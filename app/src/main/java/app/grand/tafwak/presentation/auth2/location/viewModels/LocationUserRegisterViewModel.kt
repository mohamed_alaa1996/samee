package app.grand.tafwak.presentation.auth2.location.viewModels

import android.Manifest
import android.content.pm.PackageManager
import android.location.Geocoder
import android.location.Location
import android.net.Uri
import android.os.Bundle
import android.view.View
import androidx.appcompat.app.AlertDialog
import androidx.core.content.ContextCompat
import androidx.fragment.app.findFragment
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel
import androidx.lifecycle.liveData
import androidx.lifecycle.viewModelScope
import androidx.navigation.*
import androidx.navigation.fragment.findNavController
import app.grand.tafwak.core.customTypes.RetryAbleFlow
import app.grand.tafwak.core.extensions.checkSelfPermissionGranted
import app.grand.tafwak.core.extensions.getAddressFromLatitudeAndLongitude
import app.grand.tafwak.core.extensions.showErrorToast
import app.grand.tafwak.core.extensions.switchMap
import app.grand.tafwak.data.auth.repository.AuthRepositoryImpl
import app.grand.tafwak.data.auth.repository.MAAuthRepositoryImpl
import app.grand.tafwak.data.local.preferences.PrefsSplash
import app.grand.tafwak.data.local.preferences.PrefsUser
import app.grand.tafwak.data.user.repository.UserRepositoryImpl
import app.grand.tafwak.domain.auth.entity.model.LocationData
import app.grand.tafwak.domain.splash.entity.SplashInitialLaunch
import app.grand.tafwak.domain.utils.MABaseResponse
import app.grand.tafwak.domain.utils.MAResult
import app.grand.tafwak.presentation.auth2.location.LocationUserRegisterFragment
import app.grand.tafwak.presentation.auth2.location.LocationUserRegisterFragmentArgs
import app.grand.tafwak.presentation.userHome.UserFragmentArgs
import com.google.android.datatransport.runtime.backends.BackendResponse.ok
import com.google.android.gms.common.GoogleApiAvailability
import com.google.android.gms.location.LocationRequest
import com.google.android.gms.tasks.CancellationToken
import com.google.android.gms.tasks.OnTokenCanceledListener
import com.structure.base_mvvm.R
import dagger.hilt.android.lifecycle.HiltViewModel
import kotlinx.coroutines.delay
import kotlinx.coroutines.flow.Flow
import kotlinx.coroutines.launch
import timber.log.Timber
import java.lang.ref.WeakReference
import javax.inject.Inject

@HiltViewModel
class LocationUserRegisterViewModel @Inject constructor(
	private val repository: MAAuthRepositoryImpl,
	private val userRepository: UserRepositoryImpl,
	private val prefsUser: PrefsUser,
	private val prefsSplash: PrefsSplash,
	private val args: LocationUserRegisterFragmentArgs
) : ViewModel() {
	
	val performSendLocation = MutableLiveData(false)
	
	val sentLocation = performSendLocation.switchMap {
		if (it == true) {
			if (args.token.isEmpty()) {
				MutableLiveData(MAResult.Success(MABaseResponse(null, "", 200)))
			}else {
				repository.registerUserSendLocation(
					locationData!!.latitude,
					locationData!!.longitude,
					locationData!!.address,
					args.token
				)
			}
		}else {
			MutableLiveData()
		}
	}
	
	var locationData: LocationData? = null
	
	// https://developer.android.com/training/permissions/requesting
	// https://developer.android.com/training/location/retrieve-current
	fun enableDetectLocationAuto(view: View) {
		val fragment = view.findFragment<LocationUserRegisterFragment>()

		fragment.locationHandler.requestCurrentLocation(true)
	}
	
	fun selectOnMap(view: View) {
		val uri = Uri.Builder()
			.scheme("fragment-dest")
			.authority("app.grand.tafwak.global.location.selection.no.args")
			.build()
		val request = NavDeepLinkRequest.Builder.fromUri(uri).build()
		val options = NavOptions.Builder()
			.setEnterAnim(R.anim.anim_slide_in_right)
			.setExitAnim(R.anim.anim_slide_out_left)
			.setPopEnterAnim(R.anim.anim_slide_in_left)
			.setPopExitAnim(R.anim.anim_slide_out_right)
			.build()
		view.findNavController().navigate(request, options)
	}
	
	fun onSuccessSendLocationToApi(fragment: LocationUserRegisterFragment) {
		viewModelScope.launch {
			if (fragment.activityViewModel?.globalLoading?.value != true) {
				fragment.activityViewModel?.globalLoading?.value = true
			}
			
			if (args.token.isNotEmpty() /* Not guest */) {
				val maResult = userRepository.updateLocation(
					locationData!!.latitude,
					locationData!!.longitude,
					locationData!!.address
				)
				
				prefsSplash.setInitialLaunch(SplashInitialLaunch.USER)
				
				if (maResult is MAResult.Failure) {
					fragment.activityViewModel?.globalLoading?.value = false
					
					fragment.requireContext().showErrorToast(maResult.message ?: fragment.getString(R.string.something_went_wrong))
					
					return@launch
				}
			}
			
			prefsUser.setLocation(locationData!!)
			
			fragment.activityViewModel?.globalLoading?.value = false
			
			val options = NavOptions.Builder()
				.setEnterAnim(R.anim.anim_slide_in_right)
				.setExitAnim(R.anim.anim_slide_out_left)
				.setPopEnterAnim(R.anim.anim_slide_in_left)
				.setPopExitAnim(R.anim.anim_slide_out_right)
				.build()
			// A better solution is to navigate easily without any pop
			// and in the destination back press override it after getting an argument here
			// and if true close the whole application isa.
			if (args.token.isNotEmpty()) {
				while (fragment.findNavController().popBackStack()) {
					continue
				}
			}else {
				fragment.findNavController().popBackStack(R.id.dest_log_in, false)
			}
			fragment.findNavController().navigate(
				R.id.nav_user,
				UserFragmentArgs(args.token.isEmpty() /* isGuest */).toBundle(),
				options
			)
		}
	}
	
}
