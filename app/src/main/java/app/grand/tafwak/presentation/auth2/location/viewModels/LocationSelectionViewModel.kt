package app.grand.tafwak.presentation.auth2.location.viewModels

import android.view.View
import androidx.fragment.app.findFragment
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel
import androidx.lifecycle.lifecycleScope
import androidx.navigation.findNavController
import androidx.navigation.fragment.findNavController
import app.grand.tafwak.core.extensions.getAddressFromLatitudeAndLongitude
import app.grand.tafwak.core.extensions.showErrorToast
import app.grand.tafwak.core.extensions.toJson
import app.grand.tafwak.domain.auth.entity.model.LocationData
import app.grand.tafwak.presentation.auth2.location.LocationSelectionFragment
import app.grand.tafwak.presentation.auth2.location.LocationSelectionFragmentArgs
import app.grand.tafwak.presentation.userFlow.SelectProviderOnMapFragment
import com.google.android.gms.common.api.Status
import com.google.android.gms.maps.CameraUpdateFactory
import com.google.android.gms.maps.model.LatLng
import com.google.android.libraries.places.api.Places
import com.google.android.libraries.places.api.model.Place
import com.google.android.libraries.places.widget.AutocompleteSupportFragment
import com.google.android.libraries.places.widget.listener.PlaceSelectionListener
import com.google.gson.Gson
import com.structure.base_mvvm.R
import dagger.hilt.android.lifecycle.HiltViewModel
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.delay
import kotlinx.coroutines.launch
import kotlinx.coroutines.withContext
import timber.log.Timber
import javax.inject.Inject

@HiltViewModel
class LocationSelectionViewModel @Inject constructor(
	val arg: LocationSelectionFragmentArgs,
	private val gson: Gson,
) : ViewModel() {
	
	val search = MutableLiveData("")
	
	val showMapNotSearchResults = MutableLiveData(true)
	
	var myCurrentLocation: LatLng? = null
	
	fun goBack(view: View) {
		view.findNavController().navigateUp()
	}
	
	fun toSearchPlace(view: View) {
		showMapNotSearchResults.value = false
		
		val fragment = view.findFragment<LocationSelectionFragment>()
		
		if (!Places.isInitialized()) {
			Places.initialize(view.context.applicationContext, view.context.getString(R.string.google_geo_api_key))
		}
		
		val autocompleteSupportFragment = (fragment.childFragmentManager
			.findFragmentById(R.id.placesFragmentContainerView) as AutocompleteSupportFragment)
		
		autocompleteSupportFragment.setPlaceFields(listOf(
			Place.Field.ID, Place.Field.NAME, Place.Field.ADDRESS, Place.Field.LAT_LNG
		))
		
		autocompleteSupportFragment.setOnPlaceSelectedListener(object : PlaceSelectionListener {
			override fun onPlaceSelected(place: Place) {
				val latLng = LatLng(place.latLng?.latitude ?: 0.0, place.latLng?.longitude ?: 0.0)
				
				fragment.googleMap?.moveCamera(CameraUpdateFactory.newLatLngZoom(latLng, fragment.zoom))
				
				showMapNotSearchResults.postValue(true)
			}
			
			override fun onError(status: Status) {
				Timber.e("Places API error with status $status")
				
				fragment.requireContext().showErrorToast(fragment.getString(R.string.something_went_wrong))
				
				showMapNotSearchResults.postValue(true)
			}
		})
	}
	
	fun moveToCurrentLocation(view: View) {
		val fragment = view.findFragment<LocationSelectionFragment>()

		myCurrentLocation?.also { myCurrentLocation ->
			fragment.googleMap?.moveCamera(
				CameraUpdateFactory.newLatLngZoom(myCurrentLocation, fragment.zoom)
			)
		} ?: fragment.locationHandler.requestCurrentLocation(true)
	}
	
	fun onSelectLocationClick(view: View) {
		val fragment = view.findFragment<LocationSelectionFragment>()
		
		val googleMap = fragment.googleMap ?: return
		
		fragment.activityViewModel?.globalLoading?.value = true
		
		fragment.lifecycleScope.launch {
			val address = withContext(Dispatchers.IO) {
				fragment.requireContext().getAddressFromLatitudeAndLongitude(
					withContext(Dispatchers.Main) {
						googleMap.cameraPosition.target.latitude
					},
					withContext(Dispatchers.Main) {
						googleMap.cameraPosition.target.longitude
					}
				)
			}
			
			val locationData = LocationData(
				googleMap.cameraPosition.target.latitude.toString(),
				googleMap.cameraPosition.target.longitude.toString(),
				address
			)
			
			fragment.activityViewModel?.globalLoading?.value = false
			
			delay(300)
			
			val navController = fragment.findNavController()
			
			navController.navigateUp()
			
			navController.currentBackStackEntry?.savedStateHandle?.set(
				LocationSelectionFragment.KEY_FRAGMENT_RESULT_LOCATION_DATA_AS_JSON,
				locationData.toJson(gson)
			)
		}
	}
	
}
