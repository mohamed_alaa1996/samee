package app.grand.tafwak.presentation.base.extensions

import android.view.View
import androidx.annotation.DrawableRes
import androidx.annotation.Px
import androidx.appcompat.content.res.AppCompatResources
import androidx.databinding.BindingAdapter
import com.google.android.material.card.MaterialCardView

@BindingAdapter("materialCardView_setStrokeWidthBA")
fun MaterialCardView.setStrokeWidthBA(@Px widthInPixels: Int?) {
	strokeWidth = widthInPixels ?: return
}
