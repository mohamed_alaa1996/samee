package app.grand.tafwak.presentation.providerFlow.viewModels

import android.app.Application
import androidx.lifecycle.*
import androidx.lifecycle.switchMap
import app.grand.tafwak.core.customTypes.RetryAbleFlow
import app.grand.tafwak.core.extensions.app
import app.grand.tafwak.data.provider.repository.ProviderRepositoryImpl
import app.grand.tafwak.data.user.repository.UserRepositoryImpl
import app.grand.tafwak.domain.providerFlow.ProfitType
import app.grand.tafwak.domain.utils.toSuccessOrNull
import app.grand.tafwak.presentation.userFlow.adapters.RVWalletItem
import com.structure.base_mvvm.R
import dagger.hilt.android.lifecycle.HiltViewModel
import javax.inject.Inject
import kotlin.math.roundToInt

@HiltViewModel
class ProfitsViewModel @Inject constructor(
	application: Application,
	repository: ProviderRepositoryImpl
) : AndroidViewModel(application) {
	
	private val dp2AsPx by lazy {
		app.resources.getDimension(R.dimen.dimen2).roundToInt()
	}
	
	private val stringSar by lazy {
		app.getString(R.string.sar)
	}
	
	private val profitType = MutableLiveData(ProfitType.DAILY)
	
	val profits = profitType.switchMap {
		repository.getProfits(it)
	}
	
	val strokeDaily = profitType.map {
		if (it == ProfitType.DAILY) dp2AsPx else 0
	}
	
	val strokeWeekly = profitType.map {
		if (it == ProfitType.WEEKLY) dp2AsPx else 0
	}
	
	val strokeMonthly = profitType.map {
		if (it == ProfitType.MONTHLY) dp2AsPx else 0
	}
	
	val strokeYearly = profitType.map {
		if (it == ProfitType.YEARLY) dp2AsPx else 0
	}
	
	val totalProfit = profits.switchMap { result ->
		MutableLiveData(result.toSuccessOrNull()?.value?.data?.stringRoundedTotalProfit?.let { "$it $stringSar" } ?: "0 ريال")
	}
	
	val netProfit = profits.switchMap { result ->
		MutableLiveData(result.toSuccessOrNull()?.value?.data?.stringRoundedNetProfit?.let { "$it $stringSar" } ?: "0 ريال")
	}
	
	fun changeProfitType(profitType: ProfitType) {
		this.profitType.value = profitType
	}
	
}
