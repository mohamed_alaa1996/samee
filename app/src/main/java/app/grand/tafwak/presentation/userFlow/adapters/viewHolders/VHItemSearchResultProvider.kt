package app.grand.tafwak.presentation.userFlow.adapters.viewHolders

import android.view.View
import android.view.ViewGroup
import androidx.recyclerview.widget.RecyclerView
import app.grand.tafwak.core.extensions.inflateLayout
import app.grand.tafwak.domain.user.entity.model.ProviderMiniDetails
import app.grand.tafwak.presentation.userFlow.adapters.RVItemSearchResultProvider
import com.bumptech.glide.Glide
import com.bumptech.glide.request.RequestOptions
import com.structure.base_mvvm.R
import com.structure.base_mvvm.databinding.ItemSearchResultProviderBinding
import kotlin.math.roundToInt

class VHItemSearchResultProvider(parent: ViewGroup, private val listener: RVItemSearchResultProvider.Listener) : RecyclerView.ViewHolder(
	parent.context.inflateLayout(R.layout.item_search_result_provider, parent)
) {
	
	private val binding = ItemSearchResultProviderBinding.bind(itemView)
	
	init {
		val listener = View.OnClickListener {
			val id = binding.constraintLayout.tag as? Int ?: return@OnClickListener
			
			listener.onProviderClick(it, id)
		}
		
		binding.shapeMaterialCardView.setOnClickListener(listener)
		binding.imageMaterialCardView.setOnClickListener(listener)
	}
	
	fun bind(details: ProviderMiniDetails) {
		binding.constraintLayout.tag = details.id
		
		Glide.with(binding.imageView.context)
			.load(details.imageUrl)
			.apply(RequestOptions().centerCrop())
			.placeholder(R.drawable.ic_logo_samee_placeholder)
			.error(R.drawable.ic_logo_samee_placeholder)
			.into(binding.imageView)
		
		binding.nameTextView.text = details.name
		
		binding.ratingBar.progress = (details.averageRate ).roundToInt()
		
		binding.descriptionTextView.text = details.description
	}
	
}
