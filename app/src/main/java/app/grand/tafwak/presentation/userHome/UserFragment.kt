package app.grand.tafwak.presentation.userHome

import android.os.Bundle
import android.view.View
import androidx.fragment.app.activityViewModels
import androidx.fragment.app.viewModels
import androidx.navigation.NavController
import androidx.navigation.fragment.NavHostFragment
import app.grand.tafwak.core.extensions.findNavControllerOfProject
import app.grand.tafwak.data.local.preferences.PrefsSplash
import app.grand.tafwak.domain.userFlow.model.MenuItemVisibility
import app.grand.tafwak.presentation.base.MABaseFragment
import app.grand.tafwak.presentation.base.extensions.handleRetryAbleFlowWithMustHaveResultWithNullability
import app.grand.tafwak.presentation.base.extensions.inflateMenuViaRes
import app.grand.tafwak.presentation.project.viewModel.ProjectViewModel
import app.grand.tafwak.presentation.providerHome.ProviderFragment
import app.grand.tafwak.presentation.userHome.viewModel.HomeUserViewModel
import app.grand.tafwak.presentation.userHome.viewModel.UserViewModel
import com.structure.base_mvvm.NavUserBottomNavDirections
import com.structure.base_mvvm.R
import com.structure.base_mvvm.databinding.FragmentHomeUserBinding
import com.structure.base_mvvm.databinding.FragmentUserBinding
import dagger.hilt.android.AndroidEntryPoint
import timber.log.Timber
import javax.inject.Inject

@AndroidEntryPoint
class UserFragment : MABaseFragment<FragmentUserBinding>() {
	
	private val viewModel by viewModels<UserViewModel>()
	
	private val onDestinationChangedListener = NavController.OnDestinationChangedListener { _, destination, _ ->
		run {
			binding?.root?.post {
				// They should be changed after calling api from PageUserFragment isa.
				activityViewModel?.menuItemNotificationsVisibility?.value = MenuItemVisibility.HIDE
				activityViewModel?.menuItemConversationsVisibility?.value = MenuItemVisibility.HIDE
				
				activityViewModel?.showToolbar?.value = destination.id != R.id.dest_home_user
				destination.label?.toString().orEmpty().also {
					if (it.isNotEmpty()) {
						activityViewModel?.titleToolbar?.value = it
					}
				}
			}
		}
	}
	
	override fun getLayoutId(): Int = R.layout.fragment_user
	
	override fun initializeBindingVariables() {
		binding?.viewModel = viewModel
	}

	private fun adjustBottomNavigationView(navController: NavController) {
		binding?.bottomNavigationView?.inflateMenuViaRes(
			if (viewModel.isGuest) R.menu.menu_guest_bottom_nav else R.menu.menu_user_bottom_nav
		)
		binding?.bottomNavigationView?.selectedItemId = when (navController.currentDestination?.id) {
			R.id.dest_home_user -> R.id.action_home
			R.id.dest_orders_user -> R.id.action_requests
			R.id.dest_page_user -> R.id.action_my_page
			R.id.dest_more_user -> R.id.action_mode_bar
			else -> return
		}
		
		if (binding?.bottomNavigationView?.selectedItemId == R.id.action_home) {
			binding?.bottomNavigationView?.menu?.findItem(R.id.action_home)?.setIcon(
				R.drawable.ic_home_focused
			)
		}else {
			binding?.bottomNavigationView?.menu?.findItem(R.id.action_home)?.setIcon(
				R.drawable.ic_home_not_focused
			)
		}

		binding?.bottomNavigationView?.setOnItemSelectedListener {
			if (it.itemId == R.id.action_home) {
				binding?.bottomNavigationView?.menu?.findItem(R.id.action_home)?.setIcon(
					R.drawable.ic_home_focused
				)
			}else {
				binding?.bottomNavigationView?.menu?.findItem(R.id.action_home)?.setIcon(
					R.drawable.ic_home_not_focused
				)
			}

			when (it.itemId) {
				R.id.action_home -> {
					navController.navigate(NavUserBottomNavDirections.actionGlobalDestHomeUser())
				}
				R.id.action_requests -> {
					navController.navigate(NavUserBottomNavDirections.actionGlobalDestOrdersUser(true))
				}
				R.id.action_my_page -> {
					navController.navigate(NavUserBottomNavDirections.actionGlobalDestPageUser())
				}
				R.id.action_more -> {
					navController.navigate(NavUserBottomNavDirections.actionGlobalDestMoreUser(true))
				}
			}
			
			true // change to be selected instead of ignoring the click.
		}
	}
	
	override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
		findNavControllerOfProject().currentBackStackEntry?.savedStateHandle?.getLiveData(
			ProviderFragment.KEY_FRAGMENT_RESULT_GO_TO_MENU_ITEM_ID,
			-1
		)?.observe(viewLifecycleOwner) {
			if (it != -1 && it != null) {
				findNavControllerOfProject().currentBackStackEntry?.savedStateHandle?.set(
					ProviderFragment.KEY_FRAGMENT_RESULT_GO_TO_MENU_ITEM_ID,
					-1
				)

				binding?.bottomNavigationView?.selectedItemId = it
			}
		}

		val navHostFragment = childFragmentManager
			.findFragmentById(R.id.bottomNavNavHostFragment) as NavHostFragment
		val navController = navHostFragment.navController
		
		val isDoneInflatingGraph = try {
			navController.graph; true
		}catch (e: IllegalStateException) {
			false
		}
		
		if (isDoneInflatingGraph) {
			navController.removeOnDestinationChangedListener(onDestinationChangedListener)
			navController.addOnDestinationChangedListener(onDestinationChangedListener)
			
			adjustBottomNavigationView(navController)
		}else {
			handleRetryAbleFlowWithMustHaveResultWithNullability(viewModel.retryAbleFlow) {
				val navGraph = navController.navInflater.inflate(
					if (viewModel.isGuest) {
						R.navigation.nav_user_bottom_nav_guest
					}else {
						R.navigation.nav_user_bottom_nav
					}
				)
				navController.graph = navGraph
				navController.removeOnDestinationChangedListener(onDestinationChangedListener)
				navController.addOnDestinationChangedListener(onDestinationChangedListener)
				
				adjustBottomNavigationView(navController)
			}
		}
	}
	
}
