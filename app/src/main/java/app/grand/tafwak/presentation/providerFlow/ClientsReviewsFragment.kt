package app.grand.tafwak.presentation.providerFlow

import android.os.Bundle
import android.view.View
import androidx.fragment.app.activityViewModels
import androidx.fragment.app.viewModels
import androidx.lifecycle.Lifecycle
import androidx.lifecycle.lifecycleScope
import androidx.lifecycle.repeatOnLifecycle
import androidx.recyclerview.widget.LinearLayoutManager
import app.grand.tafwak.core.extensions.roundHalfUp
import app.grand.tafwak.core.extensions.withCustomAdapters
import app.grand.tafwak.data.local.preferences.PrefsSplash
import app.grand.tafwak.presentation.base.MABaseFragment
import app.grand.tafwak.presentation.base.extensions.handleRetryAbleFlowWithMustHaveResultWithNullability
import app.grand.tafwak.presentation.project.viewModel.ProjectViewModel
import app.grand.tafwak.presentation.providerFlow.viewModels.ClientsReviewsViewModel
import app.grand.tafwak.presentation.userFlow.viewModel.WalletViewModel
import app.grand.tafwak.presentation.userHome.adapters.LSAdapterLoadingErrorEmpty
import com.structure.base_mvvm.R
import com.structure.base_mvvm.databinding.FragmentClientsReviewsBinding
import com.structure.base_mvvm.databinding.FragmentWalletBinding
import dagger.hilt.android.AndroidEntryPoint
import kotlinx.coroutines.flow.collectLatest
import kotlinx.coroutines.launch
import javax.inject.Inject
import kotlin.math.roundToInt

@AndroidEntryPoint
class ClientsReviewsFragment : MABaseFragment<FragmentClientsReviewsBinding>() {
	
	private val viewModel by viewModels<ClientsReviewsViewModel>()
	
	override fun getLayoutId(): Int = R.layout.fragment_clients_reviews
	
	override fun initializeBindingVariables() {
		binding?.viewModel = viewModel
	}
	
	override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
		binding?.recyclerView?.layoutManager = LinearLayoutManager(requireContext(), LinearLayoutManager.VERTICAL, false)
		val headerAdapterCurrent2 = LSAdapterLoadingErrorEmpty(viewModel.adapter, false)
		val footerAdapterFinished2 = LSAdapterLoadingErrorEmpty(viewModel.adapter, true)
		binding?.recyclerView?.adapter = viewModel.adapter.withCustomAdapters(
			headerAdapterCurrent2,
			footerAdapterFinished2
		)
		
		handleRetryAbleFlowWithMustHaveResultWithNullability(viewModel.response) { response ->
			viewModel.ratingText.value = "(${response.averageRate.roundHalfUp(1)})"
			viewModel.ratingProgress.value = (response.averageRate ).roundToInt()
		}
		
		viewLifecycleOwner.lifecycleScope.launch {
			viewLifecycleOwner.repeatOnLifecycle(Lifecycle.State.STARTED) {
				viewModel.reviews.collectLatest { pagingData ->
					viewModel.adapter.submitData(pagingData)
				}
			}
		}
	}
	
}
