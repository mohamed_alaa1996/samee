package app.grand.tafwak.domain.providerFlow

import androidx.annotation.FloatRange
import com.google.gson.annotations.SerializedName

data class ItemReview(
	var id: Int,
	@FloatRange(from = 0.0, to = 0.0) var rate: Float,
	var review: String,
	@SerializedName("created_at") var createdAt: String,
	@SerializedName("image") var imageUrl: String,
	var name: String?,
)
