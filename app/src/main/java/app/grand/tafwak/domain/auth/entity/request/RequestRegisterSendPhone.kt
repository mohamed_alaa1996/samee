package app.grand.tafwak.domain.auth.entity.request

import com.google.gson.annotations.SerializedName

data class RequestRegisterSendPhone(
	@SerializedName("account_type") val accountType: String,
	val phone: String,
) {
	@SerializedName("register_step") val registerStep: Int = 1
}

data class RequestRegisterSendPhoneWithReferralCode(
	@SerializedName("account_type") val accountType: String,
	val phone: String,
	@SerializedName("refer_code") val referCode: String?,
) {
	@SerializedName("register_step") val registerStep: Int = 1
}
