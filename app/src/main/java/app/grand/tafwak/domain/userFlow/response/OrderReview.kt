package app.grand.tafwak.domain.userFlow.response

import androidx.annotation.FloatRange
import com.google.gson.annotations.SerializedName

data class OrderReview(
	@FloatRange(from = 0.0, to = 5.0) var rate: Float = 0f,
	var review: String? = "",
  var name : String? = "",
  var image : String? = ""
)
