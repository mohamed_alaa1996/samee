package app.grand.tafwak.domain.user.entity.model

enum class FileType(val apiKey: String) {
	IMAGE("image"),
	VIDEO("video"),
}
