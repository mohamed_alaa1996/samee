package app.grand.tafwak.domain.providerFlow

import app.grand.tafwak.domain.user.entity.model.FileType
import com.google.gson.annotations.SerializedName

/**
 * @param senderId == sender_id == other user id isa.
 */
data class ResponseChatMessage(
	var id: Int,
	@SerializedName("conversation_id") var conversationId: Int,
	@SerializedName("sender_id") var senderId: Int,
	@SerializedName("receiver_id") var receiverId: Int,
	@SerializedName("order_id") var orderId: Int,
	var message: String?,
	@SerializedName("read_at") var readAt: String,
	@SerializedName("created_at") var createdAt: String,
	var media: MediaOfChatMsg?,
) {

	val isImage get() = media?.isImage ?: true


  companion object {
    fun mapToResponseChatMessage(map: Map<String, Any>): ResponseChatMessage {
      val id = map["id"] as Int
      val conversationId = map["conversation_id"] as Int
      val senderId = map["sender_id"] as Int
      val receiverId = map["receiver_id"] as Int
      val orderId = map["order_id"] as Int
      val message = map["message"] as String?
      val readAt = map["read_at"] as String
      val createdAt = map["created_at"] as String
      val mediaMap = map["media"] as Map<String, Any>?
      val media = mediaMap?.let { mapToMediaOfChatMsg(it) }

      return ResponseChatMessage(
        id,
        conversationId,
        senderId,
        receiverId,
        orderId,
        message,
        readAt,
        createdAt,
        media
      )
    }

    fun mapToMediaOfChatMsg(map: Map<String, Any>): MediaOfChatMsg {
      val id = map["id"] as Int
      val mediaUrl = map["media"] as String?
      val mediaType = map["media_type"] as String?

      return MediaOfChatMsg(id, mediaUrl, mediaType)
    }

  }
}
/*
            {
                "id": 1,
                "": 1,
                "sender_id": 1,
                "": 3,
                "order_id": 1,
                "message": "yrdydry",
                "read_at": "2022-03-11 18:22:55",
                "created_at": "2022-02-15T21:18:56.000000Z"
            },
            {
                "id": 2,
                "conversation_id": 1,
                "sender_id": 1,
                "receiver_id": 3,
                "order_id": 1,
                "message": "yrdydry",
                "read_at": "2022-03-11 18:22:55",
                "created_at": "2022-02-15T21:19:46.000000Z"
            },
            {
                "id": 3,
                "conversation_id": 1,
                "sender_id": 1,
                "receiver_id": 3,
                "order_id": 1,
                "message": "yrdydry",
                "read_at": "2022-03-11 18:22:55",
                "created_at": "2022-03-09T14:13:16.000000Z"
            },
            {
                "id": 4,
                "conversation_id": 1,
                "sender_id": 1,
                "receiver_id": 3,
                "order_id": 1,
                "message": "yrdydry",
                "read_at": "2022-03-11 18:22:55",
                "created_at": "2022-03-09T14:29:15.000000Z"
            },
            {
                "id": 5,
                "conversation_id": 2,
                "sender_id": 3,
                "receiver_id": 1,
                "order_id": 1,
                "message": "yrdydry",
                "read_at": "2022-03-11 18:22:55",
                "created_at": "2022-03-10T14:24:31.000000Z"
            },
            {
                "id": 6,
                "conversation_id": 2,
                "sender_id": 3,
                "receiver_id": 1,
                "order_id": 1,
                "message": "yrdydry",
                "read_at": "2022-03-11 18:22:55",
                "created_at": "2022-03-10T14:25:12.000000Z"
            },
            {
                "id": 7,
                "conversation_id": 2,
                "sender_id": 3,
                "receiver_id": 1,
                "order_id": 1,
                "message": "yrdydry",
                "read_at": "2022-03-11 18:22:55",
                "created_at": "2022-03-11T16:20:37.000000Z"
            },
            {
                "id": 8,
                "conversation_id": 2,
                "sender_id": 3,
                "receiver_id": 1,
                "order_id": 1,
                "message": "yrdydry",
                "read_at": "2022-03-11 18:22:55",
                "created_at": "2022-03-11T16:20:50.000000Z"
            },
            {
                "id": 9,
                "conversation_id": 2,
                "sender_id": 3,
                "receiver_id": 1,
                "order_id": 1,
                "message": "yrdydry",
                "read_at": "2022-03-11 18:22:55",
                "created_at": "2022-03-11T16:21:34.000000Z"
            },
            {
                "id": 10,
                "conversation_id": 2,
                "sender_id": 3,
                "receiver_id": 1,
                "order_id": 1,
                "message": "aaaaaaaaaaaaaaaaaaaaaaaaaaaa",
                "read_at": "2022-03-11 18:22:55",
                "created_at": "2022-03-11T16:22:52.000000Z"
            }
        ],
        "first_page_url": "https://samee.my-staff.net/api/v1/chat?page=1",
        "from": 1,
        "last_page": 1,
        "last_page_url": "https://samee.my-staff.net/api/v1/chat?page=1",
        "links": [
            {
                "url": null,
                "label": "&laquo; Previous",
                "active": false
            },
            {
                "url": "https://samee.my-staff.net/api/v1/chat?page=1",
                "label": "1",
                "active": true
            },
            {
                "url": null,
                "label": "Next &raquo;",
                "active": false
            }
        ],
        "next_page_url": null,
        "path": "https://samee.my-staff.net/api/v1/chat",
        "per_page": 15,
        "prev_page_url": null,
        "to": 10,
        "total": 10
    }
}
 */
