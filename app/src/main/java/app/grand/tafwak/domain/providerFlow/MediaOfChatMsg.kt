package app.grand.tafwak.domain.providerFlow

import app.grand.tafwak.domain.user.entity.model.FileType
import com.google.gson.annotations.SerializedName

data class MediaOfChatMsg(
	var id: Int,
	@SerializedName("media") var mediaUrl: String?,
	@SerializedName("media_type") var mediaType: String?,
) {
	val isImage get() = mediaType == FileType.IMAGE.apiKey
}
