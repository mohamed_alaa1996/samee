package app.grand.tafwak.domain.splash.entity

enum class SplashInitialLaunch {
	LOGIN,
	/** main user fragment isa. */
	USER,
	SERVICE_PROVIDER,
}