package app.grand.tafwak.domain.providerFlow

import com.google.gson.annotations.Expose
import com.google.gson.annotations.SerializedName

data class ResponseConversation(
  var id: Int,
  @SerializedName("user_one_id") var userOneId: Int,
  @SerializedName("user_two_id") var userTwoId: Int,
  var message: ResponseMsg,

  @Expose
  var otherUserId: Int,
)
/*
{
    "code": 200,
    "message": "The action ran successfully!",
    "data": {
        "current_page": 1,
        "data": [
            {
                "id": 1,
                "": 1,
                "user_two_id": 3,
                "message": {
                    "id": 4,
                    "": 1,
                    "": 1,
                    "": 3,
                    "": 1,
                    "message": "yrdydry",
                    "read_at": null,
                    "created_at": "2022-03-09T14:29:15.000000Z",
                    "user": {
                        "id": 3,
                        "account_type": "provider",
                        "verified": 1,
                        "name": "n",
                        "email": "mohamed.alaa636@hotmail.com",
                        "phone": "01205577043",
                        "password": null,
                        "bank_name": "CIB",
                        "bank_number": "123456",
                        "bank_account_holder": "EMAN AHMED",
                        "iban": "1234567",
                        "average_rate": 0,
                        "rate_count": 0,
                        "wallet": 0,
                        "latitude": 30.125907453022,
                        "longitude": 31.375149004161,
                        "address": "3 هاشم الأشقر، الهايكستب، قسم النزهة، محافظة القاهرة‬، مصر",
                        "register_step": 5,
                        "complete_register": 1,
                        "delivery_flag": 1,
                        "orders_flag": 0,
                        "email_verified_at": null,
                        "image": "https://samee.my-staff.net/uploads/users/164682656762289447b39c3.jpg",
                        "description": "ubub",
                        "platform": "android"
                    }
                }
            }
        ],
        "first_page_url": "https://samee.my-staff.net/api/v1/chat-history?page=1",
        "from": 1,
        "last_page": 1,
        "last_page_url": "https://samee.my-staff.net/api/v1/chat-history?page=1",
        "links": [
            {
                "url": null,
                "label": "&laquo; Previous",
                "active": false
            },
            {
                "url": "https://samee.my-staff.net/api/v1/chat-history?page=1",
                "label": "1",
                "active": true
            },
            {
                "url": null,
                "label": "Next &raquo;",
                "active": false
            }
        ],
        "next_page_url": null,
        "path": "https://samee.my-staff.net/api/v1/chat-history",
        "per_page": 15,
        "prev_page_url": null,
        "to": 1,
        "total": 1
    }
}
 */
