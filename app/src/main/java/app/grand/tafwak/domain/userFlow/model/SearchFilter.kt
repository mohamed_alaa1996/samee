package app.grand.tafwak.domain.userFlow.model

enum class SearchFilter(val keyOnApi: String) {
	LATEST("latest"),

	LESS_PRICE("less-price"),
	
	CLOSEST("closest"),
	
	MOST_RATED("most-rated"),
	
}
