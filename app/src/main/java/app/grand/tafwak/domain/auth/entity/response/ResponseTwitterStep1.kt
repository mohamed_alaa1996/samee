package app.grand.tafwak.domain.auth.entity.response

import com.google.gson.annotations.SerializedName

data class ResponseTwitterStep1(
	@SerializedName("oauth_token") var oauthToken: String,
	@SerializedName("oauth_token_secret") var oauthTokenSecret: String,
	@SerializedName("oauth_callback_confirmed") var oauthCallbackConfirmed: Boolean,
)
