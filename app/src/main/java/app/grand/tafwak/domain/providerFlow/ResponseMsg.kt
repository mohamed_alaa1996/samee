package app.grand.tafwak.domain.providerFlow

import app.grand.tafwak.domain.user.entity.model.ResponseUpdateProfile
import com.google.gson.annotations.SerializedName

data class ResponseMsg(
	var id: Int,
	@SerializedName("conversation_id") var conversationId: Int,
	@SerializedName("sender_id") var senderId: Int,
	@SerializedName("receiver_id") var receiverId: Int,
	@SerializedName("order_id") var orderId: Int,
	var message: String,
	//"read_at": null,
	@SerializedName("created_at") var createdAt: String,
	var user: ResponseUpdateProfile,
)

