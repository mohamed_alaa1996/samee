package app.grand.tafwak.domain.userFlow.response

import androidx.annotation.FloatRange
import androidx.annotation.IntRange
import com.google.gson.annotations.SerializedName

data class ProviderMiniDetailsInOrderDetails(
	var id: Int,
	@SerializedName("image") var imageUrl: String,
	@SerializedName("average_rate") @FloatRange(from = 0.0, to = 5.0) var averageRate: Float,
	var name: String,
	var description: String?,
	/** `0` delivery is for free, if 1 api see deliveryCostPerKilo inshallah */
	@SerializedName("delivery_flag") @IntRange(from = 0, to = 1) var deliveryFlag: Int,
	@SerializedName("distance") var distanceInMeters: Float,
	@SerializedName("order_rate") var review: OrderReview?,
	var phone: String,
) {

	val deliveryIsForFree get() = deliveryFlag == 0

}
