package app.grand.tafwak.domain.providerFlow

import app.grand.tafwak.core.extensions.roundHalfUp
import app.grand.tafwak.core.extensions.toIntOrFloat
import com.google.gson.annotations.SerializedName

data class ResponseProfit(
	@SerializedName("total_profit") var totalProfit: Float,
	@SerializedName("profit") var netProfit: Float
) {
	val stringRoundedTotalProfit get() = totalProfit.roundHalfUp(1).toIntOrFloat().toString()
	val stringRoundedNetProfit get() = netProfit.roundHalfUp(1).toIntOrFloat().toString()
}
