package app.grand.tafwak.domain.user.entity.model

import androidx.annotation.FloatRange
import androidx.annotation.IntRange
import com.google.gson.annotations.SerializedName

data class ProviderDetails(
	var id: Int = 0,
	@SerializedName("image") var imageUrl: String = "",
	var name: String = "",
	var description: String? = "",
	@SerializedName("average_rate") @FloatRange(from = 0.0, to = 5.0) var averageRate: Float = 0f,
	/** `0` delivery is for free, if 1 api see [deliveryCostPerKilo] inshallah */
	@SerializedName("delivery_flag") @IntRange(from = 0, to = 1) var deliveryFlag: Int = 0,
	@SerializedName("kilo_price") @FloatRange(from = 0.0) var deliveryCostPerKilo: Float = 0f,
	@SerializedName("distance") var distanceInMeters: Float = 0f,
	/** percentage, from subtotal (sum of services) only */
	var tax: Float = 0f,
	@SerializedName("previous_works") var previousWorks: List<PreviousWork> = emptyList(),
	@SerializedName("provider_services") var services: List<ProviderService> = emptyList(),
) {
	
	val deliveryIsForFree get() = deliveryFlag == 0
	
}
