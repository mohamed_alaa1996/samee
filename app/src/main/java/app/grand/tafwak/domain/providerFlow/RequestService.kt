package app.grand.tafwak.domain.providerFlow

import com.google.gson.annotations.SerializedName

data class RequestService(
	@SerializedName("category_id") var categoryId: Int,
	var name: String,
	var price: Int,
	@SerializedName("file_type") var fileType: String,
)
