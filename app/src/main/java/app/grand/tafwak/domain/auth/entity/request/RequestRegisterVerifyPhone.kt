package app.grand.tafwak.domain.auth.entity.request

import com.google.gson.annotations.SerializedName

data class RequestRegisterVerifyPhone(
	val code: String,
	val phone: String,
	@SerializedName("account_type") val accountType: String,
)