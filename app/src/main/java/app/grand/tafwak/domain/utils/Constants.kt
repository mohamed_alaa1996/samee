package app.grand.tafwak.domain.utils

object Constants {
  const val MIN_POSSIBLE_DISTANCE = 0.1f
  
  const val PAGE_INDEX = 1
  const val NETWORK_PAGE_SIZE = 2

  const val PICK_IMAGE = 1
  const val REGISTER = 2
  const val FORGET_PASSWORD = 3
  const val CONFIRM_CODE = 4
  const val TEACHER_PROFILE = 5
  const val REVIEW_DIALOG = 6
  const val REVIEWS = 7
  const val BACK = 8
  const val CLICK_EVENT = 9
  const val CONTINUE_PROGRESS = 10
  const val CONTACT = 11
  const val LOGOUT = 12
  const val TEACHERS: Int = 13
  const val OPEN_BROWSER: Int = 14
  const val GROUP_DETAILS: Int = 15
  const val FIRST_TIME: Int = 16
  const val IS_LOGGED: Int = 17
  const val AUTH: Int = 18
  const val BUNDLE = "BUNDLE"
  const val STUDENT_TYPE = "student"
  const val TEACHER_TYPE = "instructor"
  const val IMAGE: String = "image"
  const val Verify: String = "verify"
  const val FORGET: String = "reset"
  const val COUNTRY_ID = "COUNTRY_ID"
  const val REGISTER_STEP = "REGISTER_STEP"
  const val ABOUT_TYPE: String = "about_us"
  const val SOCIAL_TYPE: String = "social_media"
  const val PRIVACY_TYPE: String = "privacy_policy"
  const val TERMS_TYPE: String = "terms_conditions"
  const val APP_TYPE_GENERAL: String = "general"
  const val BOTH: String = "both"
  const val MALE: String = "male"
  const val FEMALE: String = "female"
  const val PUBLIC: String = "public"
  const val PRIVATE: String = "private"

}