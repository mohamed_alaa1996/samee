package app.grand.tafwak.domain.userFlow.response

/** one of pending, accepted, on the way, finished, rejected */
enum class OrderStatus(val apiKey: String) {
	PENDING("pending"),
	ACCEPTED("accepted"),
	ON_THE_WAY("on-the-way"),
	FINISHED("finished"),
	REJECTED("rejected"),
	CANCELLED("cancelled")
}
