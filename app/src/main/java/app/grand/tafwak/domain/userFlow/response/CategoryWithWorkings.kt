package app.grand.tafwak.domain.userFlow.response

import com.google.gson.annotations.SerializedName

data class CategoryWithWorkings(
	var id: Int,
	var name: String,
	@SerializedName("image") var imageUrl: String,
	var workings: List<Working>
)
