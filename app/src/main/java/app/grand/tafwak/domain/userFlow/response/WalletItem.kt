package app.grand.tafwak.domain.userFlow.response

import com.google.gson.annotations.SerializedName

/**
 * @param walletType `0` khasm else addition (edafa) isa.
 */
data class WalletItem(
	var id: Int,
	@SerializedName("user_id") var userId: Int,
	@SerializedName("order_id") var orderId: Int,
	@SerializedName("wallet_type") var walletType: Int,
	var amount: Float,
	@SerializedName("created_at") var date: String
)
/*
"id": 1,
                "user_id": 4,
                "order_id": 3,
                "wallet_type": 0,
                "amount": 2.736
 */
