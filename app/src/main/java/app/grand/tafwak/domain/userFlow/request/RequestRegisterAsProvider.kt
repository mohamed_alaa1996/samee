package app.grand.tafwak.domain.userFlow.request

import app.grand.tafwak.data.samee.SameeAPI
import retrofit2.http.Field

data class RequestRegisterAsProvider(
	var name: String,
	var phone: String,
	var categories: List<Int>,
)
