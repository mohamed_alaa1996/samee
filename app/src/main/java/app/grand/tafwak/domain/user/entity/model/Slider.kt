package app.grand.tafwak.domain.user.entity.model

import androidx.annotation.Keep
import com.google.gson.annotations.SerializedName

/**
 * @param linkType either [LINK_TYPE_PROVIDER] OR [LINK_TYPE_PUBLIC]
 * @param linkId represents service provider id if [linkType] == [LINK_TYPE_PROVIDER], else 0 (not used)
 */
@Keep
data class Slider(
	@SerializedName("link_type") var linkType: String,
	@SerializedName("link_id") var linkId: Int,
	@SerializedName("image") var imageUrl: String
) {
	
	companion object {
		const val LINK_TYPE_PUBLIC = "public"
		const val LINK_TYPE_PROVIDER = "provider"
	}
	
}