package app.grand.tafwak.domain.auth.entity.request

import com.google.gson.annotations.SerializedName

data class RequestFirebaseToken(
	@SerializedName("firebase_token") var firebaseToken: String,
	@SerializedName("device_id") var deviceId: String
) {
	
	val platform = "android"
	
}
