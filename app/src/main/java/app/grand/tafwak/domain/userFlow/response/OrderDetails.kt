package app.grand.tafwak.domain.userFlow.response

import app.grand.tafwak.domain.user.entity.model.ProviderMiniDetails
import app.grand.tafwak.domain.user.entity.model.ProviderService
import com.google.gson.annotations.SerializedName

data class OrderDetails(
	var id: Int,
	@SerializedName("order_number") var orderNumber: String,
	@SerializedName("user_id") var userId: Int,
	@SerializedName("provider_id") var providerId: Int,

	@SerializedName("subtotal") var subTotal: Float,
	@SerializedName("tax") var tax: Float,
	@SerializedName("delivery_cost") var deliveryCost: Float?,
	@SerializedName("total") var total: Float,

	/** 0=>cash,1=>online */
	@SerializedName("payment_method") var paymentMethod: Int,
	/** 0=>pending,1=>paid */
	@SerializedName("payment_status") var paymentStatus: Int,

	@SerializedName("customer_number") var numOfPeople: Int,

	var latitude: Double,
	var longitude: Double,
	var address: String,

	var city: String,
	var area: String,
	var street: String,
	var floor: String,
	var special_sign: String,

	/**
	 * one of pending, accepted, on the way, finished, rejected
	 *
	 * @see orderStatus
	 */
	var status: String,

	/** date": "25 February" */
	var date: String,
	/** "time": "12:00 AM" */
	var time: String,
	// "read_at": "2022-02-28 10:17:52",
	// 2022-02-15T14:02:03.000000Z
	//@SerializedName("created_at") var createdAt: String,

	var provider: ProviderMiniDetailsInOrderDetails,

	var user: ProviderMiniDetailsInOrderDetails,

	/** if `true` then no [cancellationFees] */
	@SerializedName("past_24_hours") var past24hours: Boolean,
	/** percentage */
	@SerializedName("cancellation_fees") var cancellationFees: Float,

	@SerializedName("order_services") var services: List<ProviderService>,
) {
	val orderStatus get() = OrderStatus.values().first { it.apiKey == status }
	
	fun getPerson(isUser: Boolean) = if (isUser) user else provider
}
