package app.grand.tafwak.domain.base.entity

import androidx.annotation.DrawableRes

/**
 * @param icon `null` means no icon.
 */
data class IconText(
	@DrawableRes val icon: Int?,
	val text: String
)
