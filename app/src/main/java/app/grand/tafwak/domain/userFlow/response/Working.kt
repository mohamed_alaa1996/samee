package app.grand.tafwak.domain.userFlow.response

import app.grand.tafwak.domain.user.entity.model.FileType
import com.google.gson.annotations.SerializedName

/**
 * @param fileType corresponds to -> [FileType]
 */
data class Working(
	var id: Int,
	@SerializedName("category_id") var categoryId: Int,
	@SerializedName("file_type") var fileType: String,
	@SerializedName("file") var imageOrVideoUrl: String,
)
