package app.grand.tafwak.domain.auth.entity.request

import app.grand.tafwak.data.samee.SameeAPI
import com.google.gson.annotations.SerializedName

/**
 * @param accountType [SameeAPI.Query.Value.USER]
 */
data class RequestRegisterLocation(
	val latitude: String,
	val longitude: String,
	val address: String,
	@SerializedName("account_type") val accountType: String,
) {
	@SerializedName("register_step") val registerStep: Int = 2
}
