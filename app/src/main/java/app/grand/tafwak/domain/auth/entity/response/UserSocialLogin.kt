package app.grand.tafwak.domain.auth.entity.response

import com.google.gson.annotations.SerializedName

/**
 * - Social login is for user only not provider isa.
 *
 * todo latitude if `null` go to select location or always do it afdal isa.
 * todo save locally all these values isa then redirect like after verify code so to location isa.
 */
data class UserSocialLogin(
	var id: Int?,
	var phone: String?,
	var token: String?,
	@SerializedName("social_id") var socialId: String?,
	
	var name: String?,
	var email: String?,
	@SerializedName("image") var imageUrl: String?
)
