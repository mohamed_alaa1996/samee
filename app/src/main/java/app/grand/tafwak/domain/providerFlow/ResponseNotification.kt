package app.grand.tafwak.domain.providerFlow

import com.google.gson.annotations.SerializedName

/** todo
type
target_id
title
body
sound

'admin=> by admin (has no action)','new-order=> if has new order','wallet=>if added to wallet and if deducted ','order-payment => pay order money'
no action open notification list but on click on notificaction item on list isn't clickable
 */
data class ResponseNotification(
	var id: Int,
	@SerializedName("type") var notificationType: String,
	@SerializedName("target_id") var targetId: Int,
	@SerializedName("read_at") var readAt: String,
	@SerializedName("created_at") var createdAt: String,
	var title: String?,
	var description: String?,
) {
	
	companion object {
		fun toTypeOrNull(notificationType: String) = Type.values().firstOrNull { it.apiKey == notificationType }
		fun toType(notificationType: String) = Type.values().first { it.apiKey == notificationType }
	}
	
	fun asTypeOrNull() = Type.values().firstOrNull { it.apiKey == notificationType }
	fun asType() = Type.values().first { it.apiKey == notificationType }
	
	/**
	 * - 'admin=> by admin (has no action)','new-order=> if has new order','wallet=>if added to wallet and if deducted ','order-payment => pay order money'
	 * - no action open notification list but on click on notificaction item on list isn't clickable
	 */
	enum class Type(val apiKey: String) {
		/** see [Type] */
		ADMIN("admin"),
		/** see [Type] */
		NEW_ORDER("new-order"),
		/** see [Type] */
		WALLET("wallet"),
		/** see [Type] */
		ORDER_PAYMENT("order-payment"),
	}
	
}
/*
                "id": 2,
                "type": "admin",
                "target_id": 0,
                "read_at": "2022-03-11 20:59:28",
                "created_at": "3 weeks ago",
                "title": null,
                "description": null
            },
            {
                "id": 3,
                "type": "new-order",
                "target_id": 1,
                "read_at": "2022-03-11 20:59:28",
                "created_at": "3 weeks ago",
                "title": "New Order",
                "description": "You have a new order"
            },
            {
                "id": 4,
                "type": "new-order",
                "target_id": 1,
                "read_at": "2022-03-11 20:59:28",
                "created_at": "3 weeks ago",
                "title": "Order cancelled",
                "description": "Order is cancelled by client"
            },
            {
                "id": 7,
                "type": "new-order",
                "target_id": 3,
                "read_at": "2022-03-11 20:59:28",
                "created_at": "1 week ago",
                "title": "New order",
                "description": "You have a new order"
            },
            {
                "id": 8,
                "type": "new-order",
                "target_id": 4,
                "read_at": "2022-03-11 20:59:28",
                "created_at": "1 week ago",
                "title": "New order",
                "description": "You have a new order"
            },
            {
                "id": 9,
                "type": "new-order",
                "target_id": 6,
                "read_at": "2022-03-11 20:59:28",
                "created_at": "1 week ago",
                "title": "New order",
                "description": "You have a new order"
            },
            {
                "id": 10,
                "type": "new-order",
                "target_id": 7,
                "read_at": "2022-03-11 20:59:28",
                "created_at": "1 week ago",
                "title": "New order",
                "description": "You have a new order"
            },
            {
                "id": 11,
                "type": "new-order",
                "target_id": 8,
                "read_at": "2022-03-11 20:59:28",
                "created_at": "1 week ago",
                "title": "New order",
                "description": "You have a new order"
            },
            {
                "id": 12,
                "type": "new-order",
                "target_id": 9,
                "read_at": "2022-03-11 20:59:28",
                "created_at": "1 week ago",
                "title": "New order",
                "description": "You have a new order"
            },
            {
                "id": 13,
                "type": "new-order",
                "target_id": 10,
                "read_at": "2022-03-11 20:59:28",
                "created_at": "1 week ago",
                "title": "New order",
                "description": "You have a new order"
            },
            {
                "id": 14,
                "type": "new-order",
                "target_id": 11,
                "read_at": "2022-03-11 20:59:28",
                "created_at": "1 week ago",
                "title": "New order",
                "description": "You have a new order"
            },
            {
                "id": 23,
                "type": "new-order",
                "target_id": 12,
                "read_at": "2022-03-11 20:59:28",
                "created_at": "6 days ago",
                "title": "New order",
                "description": "You have a new order"
            },
            {
                "id": 41,
                "type": "new-order",
                "target_id": 23,
                "read_at": "2022-03-11 20:59:28",
                "created_at": "3 days ago",
                "title": "New order",
                "description": "You have a new order"
            },
            {
                "id": 42,
                "type": "new-order",
                "target_id": 24,
                "read_at": "2022-03-11 20:59:28",
                "created_at": "3 days ago",
                "title": "New order",
                "description": "You have a new order"
            },
            {
                "id": 43,
                "type": "new-order",
                "target_id": 26,
                "read_at": "2022-03-11 20:59:28",
                "created_at": "3 days ago",
                "title": "New order",
                "description": "You have a new order"
            },
            {
                "id": 44,
                "type": "new-order",
                "target_id": 27,
                "read_at": "2022-03-11 20:59:28",
                "created_at": "3 days ago",
                "title": "New order",
                "description": "You have a new order"
            },
            {
                "id": 45,
                "type": "new-order",
                "target_id": 28,
                "read_at": "2022-03-11 20:59:28",
                "created_at": "2 days ago",
                "title": "New order",
                "description": "You have a new order"
            }
        ],
        "first_page_url": "https://samee.my-staff.net/api/v1/notifications?page=1",
        "from": 1,
        "last_page": 1,
        "last_page_url": "https://samee.my-staff.net/api/v1/notifications?page=1",
        "links": [
            {
                "url": null,
                "label": "&laquo; Previous",
                "active": false
            },
            {
                "url": "https://samee.my-staff.net/api/v1/notifications?page=1",
                "label": "1",
                "active": true
            },
            {
                "url": null,
                "label": "Next &raquo;",
                "active": false
            }
        ],
        "next_page_url": null,
        "path": "https://samee.my-staff.net/api/v1/notifications",
        "per_page": 20,
        "prev_page_url": null,
        "to": 17,
        "total": 17
    }
}
 */