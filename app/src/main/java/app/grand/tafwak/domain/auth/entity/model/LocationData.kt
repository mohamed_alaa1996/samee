package app.grand.tafwak.domain.auth.entity.model

data class LocationData(
	val latitude: String,
	val longitude: String,
	val address: String,
) : SpecialLocationData

fun LocationData?.orEmpty() = this?.copy() ?: LocationData("", "", "")
