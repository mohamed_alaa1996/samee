package app.grand.tafwak.domain.auth.entity.request

import com.google.gson.annotations.SerializedName

data class RequestRegister(
	@SerializedName("account_type") var accountType: String,
	var phone: String,
	@SerializedName("register_step") var registerStep: Int = 0,
	
	val latitude: String,
	val longitude: String,
	val address: String,
	
	val name: String,
	val email: String,
	val description: String,
)
