package app.grand.tafwak.domain.utils

data class MABaseResponse<T>(
	val data: T?,
	val message: String,
	val code: Int,
)
