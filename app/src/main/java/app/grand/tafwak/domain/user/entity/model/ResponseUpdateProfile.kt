package app.grand.tafwak.domain.user.entity.model

import com.google.gson.annotations.SerializedName

data class ResponseUpdateProfile(
	var id: Int,
	
	@SerializedName("image") var imageUrl: String,
	
	var name: String,
	
	var email: String,
	
	var latitude: String,
	var longitude: String,
	var address: String,
	
	@SerializedName("account_type") var accountType: String,
)
/*
{
    "code": 200,
    "message": "The action ran successfully!",
    "data": {
        "id": 3,
        "account_type": "provider",
        "verified": 1,
        "name": "Eman",
        "email": "h_rp1995@hotmail.com",
        "phone": "01205577043",
        "password": null,
        "bank_name": "CIB",
        "bank_number": "123456",
        "bank_account_holder": "EMAN AHMED",
        "iban": "1234567",
        "average_rate": 0,
        "rate_count": 0,
        "wallet": 0,
        "latitude": "30.010726833725097",
        "longitude": "31.17672722786666",
        "address": "test",
        "register_step": 5,
        "complete_register": 1,
        "delivery_flag": 1,
        "orders_flag": 1,
        "email_verified_at": null,
        "image": "https://samee.my-staff.net/uploads/users/16456063856215f5f170fb0.jpg",
        "description": "test",
        "platform": "android"
    }
}
 */
