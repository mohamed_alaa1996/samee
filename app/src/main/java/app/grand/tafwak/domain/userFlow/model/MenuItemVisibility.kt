package app.grand.tafwak.domain.userFlow.model

enum class MenuItemVisibility {
	SHOW, HIDE, SHOW_HAVING_NEW_DATA
}
