package app.grand.tafwak.domain.user.entity.model

import androidx.annotation.Keep

@Keep
data class ResponseUserHome(
	var sliders: List<Slider>,
)