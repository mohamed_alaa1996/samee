package app.grand.tafwak.domain.providerFlow

import app.grand.tafwak.domain.user.entity.model.ProviderService
import com.google.gson.annotations.SerializedName

data class CategoryWithServices(
	var id: Int?,
	@SerializedName("image") var imageUrl: String?,
	var name: String?,
	var services: List<ProviderService>?,
)
