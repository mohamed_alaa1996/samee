package app.grand.tafwak.domain.userFlow.response

import com.google.gson.annotations.SerializedName

/**
 * @param accountType "user" OR "provider"
 */
data class ResponseWallet(
	var id: Int,
	@SerializedName("account_type") var accountType: String,
	@SerializedName("wallet") var money: Float,
	var wallets: List<WalletItem>
)
/*
        "id": 4,
        "account_type": "user",
        "verified": 1,
        "name": null,
        "email": null,
        "phone": "01122161354",
        "password": null,
        "bank_name": null,
        "bank_number": null,
        "bank_account_holder": null,
        "iban": null,
        "average_rate": 0,
        "rate_count": 0,
        "wallet": -2.74,
        "latitude": 30.1259211,
        "longitude": 31.3751809,
        "address": "3 هاشم الأشقر، الهايكستب، قسم النزهة، محافظة القاهرة‬، مصر",
        "register_step": 2,
        "complete_register": 0,
        "delivery_flag": 0,
        "orders_flag": 0,
        "email_verified_at": null,
        "image": "https://samee.my-staff.net/uploads/logo.png",
        "description": null,
        "platform": "android",
        "wallets": [
            {
                "id": 1,
                "user_id": 4,
                "order_id": 3,
                "wallet_type": 0,
                "amount": 2.736
            }
        ]
    }
}
 */
