package app.grand.tafwak.domain.auth.entity.response

import com.google.gson.annotations.SerializedName

data class ResponseVerifyPhone(
	var id: Int?,
	@SerializedName("account_type") var accountType: String?,
	var phone: String?,
	var token: String?,
	@SerializedName("image") var imageUrl: String?,
	
	@SerializedName("complete_register") var completeRegister: Int?,

	@SerializedName("refer_code") var referCode: String?,
	@SerializedName("referred_by_id") var referredById: Int?,

	//region Step 1
	var latitude: String?,
	var longitude: String?,
	var address: String?,
	//endregion

	//region Step 2
	var name: String?,
	var email: String?,
	var description: String?,
	//endregion
	
	//region Step 3
	@SerializedName("bank_name") var bankName: String?,
	@SerializedName("bank_number") var bankNumber: String?,
	@SerializedName("bank_account_holder") var bankAccountHolder: String?,
	var iban: String?,
	//endregion
)
