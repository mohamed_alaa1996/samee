package app.grand.tafwak.domain.settings.models

data class ResponseSettings(
	val id: Int?,
	val image: String?,
	val description: String?,
)
