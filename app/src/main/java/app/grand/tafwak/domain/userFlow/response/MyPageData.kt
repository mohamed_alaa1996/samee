package app.grand.tafwak.domain.userFlow.response

import com.google.gson.annotations.SerializedName

data class MyPageData(
	var id: Int,
	/** 0 for free isa. */
	@SerializedName("delivery_flag") var deliveryFlag: Int,
	/** 0 -> off (stop receiving requests) */
	@SerializedName("orders_flag") var ordersFlag: Int, // 0 off
	/** provider OR user isa. */
	@SerializedName("account_type") var accountType: String,
	@SerializedName("messages_count") var messagesCount: Int,
	@SerializedName("notifications_count") var notificationsCount: Int,
	@SerializedName("orders_no") var ordersCount: Int,
)
