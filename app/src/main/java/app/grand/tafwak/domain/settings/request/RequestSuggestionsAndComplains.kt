package app.grand.tafwak.domain.settings.request

data class RequestSuggestionsAndComplains(
	var name: String,
	var email: String,
	var message: String,
	var phone: String,
)
