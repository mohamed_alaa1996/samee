package app.grand.tafwak.domain.providerFlow

import app.grand.tafwak.domain.userFlow.response.OrderMiniDetails
import com.google.gson.annotations.SerializedName

data class CategoryWithOrders(
	var id: Int,
	@SerializedName("image") var imageUrl: String,
	var name: String,
	@SerializedName("orders_count") var ordersCount: Int,
	var orders: List<OrderMiniDetails>
)
