package app.grand.tafwak.domain.userFlow.response

import com.google.gson.annotations.SerializedName

data class OrderMiniDetails(
	var id: Int,
	@SerializedName("provider_id") var providerId: Int,
	@SerializedName("order_number") var orderNumber: String,
	/** date": "25 February" */
	var date: String,
	/** "time": "12:00 AM" */
	var time: String,
	/** one of pending, accepted, on the way, finished, rejected */
	var status: String,
	// 2022-02-15T14:02:03.000000Z
	//@SerializedName("created_at") var createdAt: String,
	/** if `true` then no [cancellationFees] */
	@SerializedName("past_24_hours") var past24hours: Boolean,
	@SerializedName("cancellation_fees") var cancellationFees: Float,
	var provider: ProviderMiniDetailsInOrder?,
	@SerializedName("customer_number") var peopleCount: Int,
	@SerializedName("total") var price: Float,
) {
	
	val orderStatus get() = OrderStatus.values().first { it.apiKey == status }
	
}
/*
                "orders": [
                        "id": 1,
                        "provider_id": 3,
                        "order_number": "620bb25b1d031",
                        "category_id": 1,
                        "total": 22.8,
                        "customer_number": 5,
                        "date": "25 February",
                        "time": "12:00 AM",
                        "created_at": "2022-02-15T14:02:03.000000Z",
                        "past_24_hours": false,
                        "cancellation_fees": 0.12
                    }
                ]
            }
        ]
    }
}
 */

