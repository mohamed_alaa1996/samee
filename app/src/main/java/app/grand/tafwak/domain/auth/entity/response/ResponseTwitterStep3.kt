package app.grand.tafwak.domain.auth.entity.response

import com.google.gson.annotations.SerializedName

data class ResponseTwitterStep3(
	@SerializedName("oauth_token") var oauthToken: String,
	@SerializedName("oauth_token_secret") var oauthTokenSecret: String,
	@SerializedName("user_id") var userId: String,
	@SerializedName("screen_name") var screenName: String,
)
