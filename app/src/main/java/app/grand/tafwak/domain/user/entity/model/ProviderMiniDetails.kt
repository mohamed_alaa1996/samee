package app.grand.tafwak.domain.user.entity.model

import androidx.annotation.FloatRange
import androidx.annotation.IntRange
import com.google.gson.annotations.SerializedName

data class ProviderMiniDetails(
	var id: Int,
	@SerializedName("image") var imageUrl: String,
	var name: String,
	var description: String,
	@SerializedName("average_rate") @FloatRange(from = 0.0, to = 5.0) var averageRate: Float,
	
	@SerializedName("distance") var distanceInMeters: Float,
	var latitude: Double,
	var longitude: Double,
	var address: String,
)
