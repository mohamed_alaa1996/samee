package app.grand.tafwak.domain.userFlow.request

import com.google.gson.annotations.SerializedName

/**
 * @param date yyyy-mm-dd even if a single digit use a prefixed `0` ex. April -> 04 isa.
 *
 * @param time hh:mm
 */
data class RequestCreateOrder(
	@SerializedName("provider_service_id") var servicesIds: List<Int>,
	var date: String,
	var time: String,
	
	var subtotal: String,
	var tax: String,
	var total: String,
	
	@SerializedName("customer_number") var numOfCustomers: Int,
	
	@SerializedName("provider_id") var providerId: Int,
)
