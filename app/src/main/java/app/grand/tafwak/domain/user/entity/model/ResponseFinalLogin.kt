package app.grand.tafwak.domain.user.entity.model

import com.google.gson.annotations.SerializedName

data class ResponseFinalLogin(
	@SerializedName("image") var imageUrl: String?,
)
/*
"data": {
	"id": 3,
	"account_type": "provider",
	"verified": 1,
	"name": "Eman",
	"email": "h_rp1995@hotmail.com",
	"phone": "01205577043",
	"password": null,
	"bank_name": "CIB",
	"bank_number": "123456",
	"bank_account_holder": "EMAN AHMED",
	"iban": "1234567",
	"average_rate": 0,
	"rate_count": 0,
	"wallet": 0,
	"latitude": 30.125900300000001408307070960290729999542236328125,
	"longitude": 31.37285729999999972505975165404379367828369140625,
	"address": "3 هاشم الأشقر، الهايكستب، قسم النزهة، محافظة القاهرة\u202c 11511",
	"register_step": 3,
	"complete_register": 0,
	"delivery_flag": 0,
	"orders_flag": 0,
	"email_verified_at": null,
	"image": "https://samee.my-staff.net/uploads/users/1644930048620ba4005680c.jpg",
	"description": "test"
}
}
 */

