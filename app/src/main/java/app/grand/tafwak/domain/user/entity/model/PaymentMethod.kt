package app.grand.tafwak.domain.user.entity.model

enum class PaymentMethod {
	
	CASH, VISA, PAY_PAL, AMAZON_PAY
	
}
