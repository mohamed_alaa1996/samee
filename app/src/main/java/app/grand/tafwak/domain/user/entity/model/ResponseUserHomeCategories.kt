package app.grand.tafwak.domain.user.entity.model

import app.grand.tafwak.domain.utils.MABasePaging

data class ResponseUserHomeCategories(
	var categories: MABasePaging<ServiceCategory>
)
