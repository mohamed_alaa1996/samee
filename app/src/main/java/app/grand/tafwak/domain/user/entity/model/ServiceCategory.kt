package app.grand.tafwak.domain.user.entity.model

import androidx.annotation.Keep
import com.google.gson.annotations.SerializedName

@Keep
data class ServiceCategory(
	var id: Int,
	@SerializedName("image")
	var imageUrl: String,
	var name: String
)
