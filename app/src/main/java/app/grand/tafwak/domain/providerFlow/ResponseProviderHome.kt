package app.grand.tafwak.domain.providerFlow

import app.grand.tafwak.domain.user.entity.model.Slider
import app.grand.tafwak.domain.userFlow.response.OrderDetails
import com.google.gson.annotations.SerializedName

data class ResponseProviderHome(
	var sliders: List<Slider>,
	var categories: List<CategoryWithOrders>,
	@SerializedName("on_the_way_orders") var onTheWayOrders: List<ResponseId?>?,
)

data class ResponseId(
	var id: Int?,
)
