package app.grand.tafwak.domain.user.entity.model

import com.google.gson.annotations.SerializedName

/**
 * @param fileType corresponds to -> [FileType]
 */
data class PreviousWork(
	var id: Int,
	@SerializedName("category_id") var categoryId: Int,
	@SerializedName("file_type") var fileType: String,
	@SerializedName("file") var imageOrVideoUrl: String,
)
