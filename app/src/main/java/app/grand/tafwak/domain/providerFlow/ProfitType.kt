package app.grand.tafwak.domain.providerFlow

enum class ProfitType(val apiKey: String) {
	DAILY("daily"),
	WEEKLY("weekly"),
	MONTHLY("monthly"),
	YEARLY("annually"),
}
