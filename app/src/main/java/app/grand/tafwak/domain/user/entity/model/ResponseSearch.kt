package app.grand.tafwak.domain.user.entity.model

import com.google.gson.annotations.SerializedName

data class ResponseSearch(
	var id: Int,
	@SerializedName("image") var imageUrl: String,
	var name: String,
	var providers: List<ProviderMiniDetails>,
)
