package app.grand.tafwak.domain.user.entity.model

import com.google.gson.annotations.SerializedName

data class RequestProviderFinalStep(
	var categories: List<Int>,
	@SerializedName("service_name") var serviceName: List<String>,
	var price: List<Int>,
	@SerializedName("file_type") var fileType: List<String>,
)
