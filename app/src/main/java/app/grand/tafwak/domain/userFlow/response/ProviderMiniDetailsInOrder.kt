package app.grand.tafwak.domain.userFlow.response

import androidx.annotation.FloatRange
import com.google.gson.annotations.SerializedName

data class ProviderMiniDetailsInOrder(
	var id: Int?,
	@SerializedName("image") var imageUrl: String?,
	var name: String?,
	@SerializedName("average_rate") @FloatRange(from = 0.0, to = 5.0) var averageRate: Float?,
)
