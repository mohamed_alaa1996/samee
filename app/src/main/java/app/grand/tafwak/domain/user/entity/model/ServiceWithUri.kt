package app.grand.tafwak.domain.user.entity.model

import android.net.Uri

/**
 * @param uri a uri of an image or a video and this can be known via [type] isa.
 * @param type is the type of the [uri] isa.
 */
data class ServiceWithUri(
	var id: Int,
	var name: String,
	var price: String,
	var uri: Uri?,
	var type: FileType
) {
	
	val imageUri: Uri? get() = if (type == FileType.IMAGE) uri else null
	
	val videoUri: Uri? get() = if (type == FileType.VIDEO) uri else null
	
}
