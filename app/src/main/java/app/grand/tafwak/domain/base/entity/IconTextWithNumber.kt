package app.grand.tafwak.domain.base.entity

import androidx.annotation.DrawableRes

/**
 * @param number can be `null` to hide it isa.
 */
data class IconTextWithNumber(
	@DrawableRes var icon: Int,
	var text: String,
	var number: Int? = null,
)
