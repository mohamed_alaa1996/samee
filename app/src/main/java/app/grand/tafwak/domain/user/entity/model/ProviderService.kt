package app.grand.tafwak.domain.user.entity.model

import com.google.gson.annotations.SerializedName

/**
 * @param fileType corresponds to -> [FileType]
 */
data class ProviderService(
	var id: Int?,
	var name: String?,
	var price: Int?,
	@SerializedName("file_type") var fileType: String?,
	@SerializedName("file") var imageOrVideoUrl: String?,
) {
	
	val isImage get() = fileType == FileType.IMAGE.name
	
	val isVideo get() = fileType == FileType.VIDEO.name
	
}
