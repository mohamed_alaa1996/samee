package app.grand.tafwak.domain.providerFlow

import androidx.annotation.FloatRange
import app.grand.tafwak.domain.utils.MABasePaging
import com.google.gson.annotations.SerializedName

data class ResponseReview(
	@SerializedName("average_rate") @FloatRange(from = 0.0, to = 5.0) var averageRate: Float,
	var reviews: MABasePaging<ItemReview>
)
/*
{
    "data": {
        "average_rate": 0,
        "reviews": {
            "current_page": 1,
            "data": [
                {
                    "id": 1,
                    "rate": 5,
                    "review": "he is wonderfull",
                    "created_at": "15-02-2022"
                }
            ],
            "first_page_url": "https://samee.my-staff.net/api/v1/reviews?page=1",
            "from": 1,
            "last_page": 1,
            "last_page_url": "https://samee.my-staff.net/api/v1/reviews?page=1",
            "links": [
                {
                    "url": null,
                    "label": "&laquo; Previous",
                    "active": false
                },
                {
                    "url": "https://samee.my-staff.net/api/v1/reviews?page=1",
                    "label": "1",
                    "active": true
                },
                {
                    "url": null,
                    "label": "Next &raquo;",
                    "active": false
                }
            ],
            "next_page_url": null,
            "path": "https://samee.my-staff.net/api/v1/reviews",
            "per_page": 20,
            "prev_page_url": null,
            "to": 1,
            "total": 1
        }
    }
}
 */
