package app.grand.tafwak.domain.user.entity.model

import com.google.gson.annotations.SerializedName

data class RequestUpdateProfile(
	var name: String,
	
	var phone: String,
	
	var email: String,
	
	var latitude: String,
	var longitude: String,
	var address: String,
)
