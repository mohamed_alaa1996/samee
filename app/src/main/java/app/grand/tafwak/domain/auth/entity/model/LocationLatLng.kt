package app.grand.tafwak.domain.auth.entity.model

data class LocationLatLng(
	val latitude: String = "",
	val longitude: String = "",
)
