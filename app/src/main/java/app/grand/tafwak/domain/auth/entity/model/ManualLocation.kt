package app.grand.tafwak.domain.auth.entity.model

import com.google.gson.annotations.SerializedName

/**
 * @param specialSign can be optional and in that case provide empty string isa.
 */
data class ManualLocation(
	var city: String,
	var area: String,
	var street: String,
	@SerializedName("floor") var floorNumber: Int,
	@SerializedName("special_sign") var specialSign: String,
) : SpecialLocationData
