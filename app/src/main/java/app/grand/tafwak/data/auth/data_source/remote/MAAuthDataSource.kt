package app.grand.tafwak.data.auth.data_source.remote

import app.grand.tafwak.data.remote.MABaseRemoteDataSource
import app.grand.tafwak.data.samee.SameeAPI
import app.grand.tafwak.domain.auth.entity.request.*
import app.grand.tafwak.domain.auth.entity.response.ResponseVerifyPhone
import app.grand.tafwak.domain.utils.MABaseResponse
import app.grand.tafwak.domain.utils.MAResult
import okhttp3.MultipartBody
import okhttp3.RequestBody.Companion.toRequestBody
import retrofit2.http.Field
import retrofit2.http.HeaderMap
import retrofit2.http.Part
import javax.inject.Inject

class MAAuthDataSource @Inject constructor(private val apiService: SameeAuthServices) : MABaseRemoteDataSource() {
	
	suspend fun checkReferralCode(code: String): MAResult.Immediate<MABaseResponse<ResponseVerifyPhone?>> = safeApiCall {
		apiService.checkReferralCode(code)
	}

	suspend fun registerUserSendPhone(phone: String, referralCode: String?) = safeApiCall {
		apiService.sendPhone(RequestRegisterSendPhoneWithReferralCode(SameeAPI.Query.Value.USER, phone, referralCode))
	}

	suspend fun registerProviderSendPhone(phone: String, referralCode: String?) = safeApiCall {
		apiService.sendPhone(RequestRegisterSendPhoneWithReferralCode(SameeAPI.Query.Value.PROVIDER, phone, referralCode))
	}
	
	suspend fun verifyUserPhone(code: String, phone: String) = safeApiCall {
		apiService.verifyPhone(RequestRegisterVerifyPhone(code, phone, SameeAPI.Query.Value.USER))
	}
	
	suspend fun verifyProviderPhone(code: String, phone: String) = safeApiCall {
		apiService.verifyPhone(RequestRegisterVerifyPhone(code, phone, SameeAPI.Query.Value.PROVIDER))
	}
	
	suspend fun resendUserVerificationCode(phone: String) = safeApiCall {
		apiService.resendVerificationCode(RequestRegisterSendPhone(SameeAPI.Query.Value.USER, phone))
	}
	
	suspend fun registerSendLocation(latitude: String, longitude: String, address: String, isUser: Boolean, authToken: String) = safeApiCall {
		apiService.sendLocation(
			RequestRegisterLocation(
				latitude,
				longitude,
				address,
				if (isUser) SameeAPI.Query.Value.USER else SameeAPI.Query.Value.PROVIDER
			),
			getAuthorizationHeader(authToken)
		)
	}
	
	suspend fun setFirebaseToken(firebaseToken: String?, deviceId: String, authToken: String) = safeApiCall {
		apiService.setFirebaseToken(RequestFirebaseToken(firebaseToken ?: "", deviceId), getAuthorizationHeader(authToken))
	}
	
	suspend fun registerProviderStep2PersonalData(
		image: MultipartBody.Part,
		name: String,
		email: String,
		description: String,
		authToken: String,
	) = safeApiCall {
		val map = mutableMapOf(
			"name" to name.toRequestBody(),
			"description" to description.toRequestBody(),
			"account_type" to "provider".toRequestBody(),
			"register_step" to "3".toRequestBody(),
		)
		if (email.isNotEmpty()) {
			map["email"] = email.toRequestBody()
		}
		
		apiService.registerProviderStep2PersonalData(image, map, getAuthorizationHeader(authToken))
	}
	
	suspend fun registerProviderStep3PersonalData(
		bankName: String,
		bankNumber: String,
		iban: String,
		bankAccountHolder: String,
		authToken: String,
	) = safeApiCall {
		apiService.registerProviderStep3PersonalData(bankName, bankNumber, iban, bankAccountHolder, getAuthorizationHeader(authToken))
	}
	
	suspend fun registerProviderStepFinalStep(
		categoriesIds: List<Int>,
		servicesNames: List<String>,
		prices: List<Int>,
		files: List<MultipartBody.Part>,
		typesOfFiles: List<String>,
		authToken: String,
	) = safeApiCall {
		apiService.registerProviderStepFinalStep(categoriesIds, servicesNames.map { it.toRequestBody() }, prices, files, typesOfFiles.map { it.toRequestBody() }, getAuthorizationHeader(authToken))
	}
	
	suspend fun socialLogin(
		socialId: String,
	) = safeApiCall {
		apiService.socialLogin(socialId)
	}
	
	suspend fun socialRegister(
		socialId: String,
		phone: String,
	) = safeApiCall {
		apiService.socialRegister(socialId, phone)
	}
	
}