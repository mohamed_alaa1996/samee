package app.grand.tafwak.data.auth.data_source.remote

import app.grand.tafwak.data.samee.SameeAPI
import app.grand.tafwak.domain.auth.entity.request.*
import app.grand.tafwak.domain.auth.entity.response.ResponseVerifyPhone
import app.grand.tafwak.domain.auth.entity.response.UserSocialLogin
import app.grand.tafwak.domain.user.entity.model.ResponseFinalLogin
import app.grand.tafwak.domain.user.entity.model.ResponseUpdateProfile
import app.grand.tafwak.domain.utils.MABaseResponse
import okhttp3.MultipartBody
import okhttp3.RequestBody
import retrofit2.http.*

interface SameeAuthServices {
	
	@POST("api/v1/auth/login")
	suspend fun sendPhone(
		@Body request: RequestRegisterSendPhoneWithReferralCode,
	): MABaseResponse<Any>
	
	@POST("api/v1/auth/verify")
	suspend fun verifyPhone(
		@Body request: RequestRegisterVerifyPhone,
	): MABaseResponse<ResponseVerifyPhone>
	
	@POST("api/v1/auth/send-code")
	suspend fun resendVerificationCode(
		@Body request: RequestRegisterSendPhone,
	): MABaseResponse<Any>
	
	@POST("api/v1/auth/login")
	suspend fun sendLocation(
		@Body request: RequestRegisterLocation,
		@HeaderMap headerMap: Map<String, String>,
	): MABaseResponse<Any>
	
	@POST("api/v1/set-token")
	suspend fun setFirebaseToken(
		@Body request: RequestFirebaseToken,
		@HeaderMap headerMap: Map<String, String>
	): MABaseResponse<Any>
	
	@Multipart
	@POST("api/v1/auth/login")
	suspend fun registerProviderStep2PersonalData(
		@Part image: MultipartBody.Part,
		@PartMap mapOfOtherParams: Map<String, @JvmSuppressWildcards RequestBody>,
		@HeaderMap headerMap: Map<String, String>,
		//@Part(SameeAPI.Query.ACCOUNT_TYPE_KEY) accountType: String = SameeAPI.Query.Value.PROVIDER,
		@Part(SameeAPI.Query.REGISTER_STEP) registerStep: Int = 3,
	): MABaseResponse<Any>
	
	@FormUrlEncoded
	@POST("api/v1/auth/login")
	suspend fun registerProviderStep3PersonalData(
		@Field(SameeAPI.Query.BANK_NAME) bankName: String,
		@Field(SameeAPI.Query.BANK_NUMBER) bankNumber: String,
		@Field(SameeAPI.Query.IBAN) iban: String,
		@Field(SameeAPI.Query.BANK_ACCOUNT_HOLDER) bankAccountHolder: String,
		@HeaderMap headerMap: Map<String, String>,
		//@Field(SameeAPI.Query.ACCOUNT_TYPE_KEY) accountType: String = SameeAPI.Query.Value.PROVIDER,
		@Field(SameeAPI.Query.REGISTER_STEP) registerStep: Int = 4,
	): MABaseResponse<Any>
	
	@Multipart
	@POST("api/v1/auth/login")
	suspend fun registerProviderStepFinalStep(
		@Part(SameeAPI.Query.CATEGORIES + "[]") categoriesIds: List<Int>,
		@Part(SameeAPI.Query.SERVICE_NAME + "[]") servicesNames: List<@JvmSuppressWildcards RequestBody>,
		@Part(SameeAPI.Query.PRICE + "[]") prices: List<Int>,
		@Part/*(SameeAPI.Query.FILE) */ files: List<MultipartBody.Part>,
		@Part(SameeAPI.Query.FILE_TYPE + "[]") typesOfFiles: List<@JvmSuppressWildcards RequestBody>,
		@HeaderMap headerMap: Map<String, String>,
		//@Part(SameeAPI.Query.ACCOUNT_TYPE_KEY) accountType: String = SameeAPI.Query.Value.PROVIDER,
		@Part(SameeAPI.Query.REGISTER_STEP) registerStep: Int = 5,
	): MABaseResponse<ResponseFinalLogin>
	
	@FormUrlEncoded
	@POST("api/v1/auth/social-login")
	suspend fun socialLogin(
		@Field(SameeAPI.Query.SOCIAL_ID) socialId: String
	): MABaseResponse<UserSocialLogin>
	
	@FormUrlEncoded
	@POST("api/v1/auth/social-login")
	suspend fun socialRegister(
		@Field(SameeAPI.Query.SOCIAL_ID) socialId: String,
		@Field(SameeAPI.Query.PHONE) phone: String,
	): MABaseResponse<Any>

	@GET("api/v1/auth/refer-code")
	suspend fun checkReferralCode(
		@Query(SameeAPI.Query.REFER_CODE) refer_code: String,
	): MABaseResponse<ResponseVerifyPhone?>
	
}