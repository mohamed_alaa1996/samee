package app.grand.tafwak.data.maSettings

import app.grand.tafwak.data.remote.MABaseRemoteDataSource
import app.grand.tafwak.data.samee.SameeAPI
import app.grand.tafwak.domain.settings.request.RequestSuggestionsAndComplains
import javax.inject.Inject

class SettingsDataSource @Inject constructor(private val apiService: SameeSettingsServices) : MABaseRemoteDataSource() {
	
	suspend fun getUserAbout(authToken: String? = null) = safeApiCall {
		apiService.getSettings(0, SameeAPI.Query.Value.USER)
	}
	suspend fun getUserTermsAndConditions(authToken: String? = null) = safeApiCall {
		apiService.getSettings(1, SameeAPI.Query.Value.USER)
	}
	suspend fun getUserPrivacyPolicy(authToken: String? = null) = safeApiCall {
		apiService.getSettings(2, SameeAPI.Query.Value.USER)
	}
	suspend fun getProviderAbout(authToken: String? = null) = safeApiCall {
		apiService.getSettings(0, SameeAPI.Query.Value.PROVIDER)
	}
	suspend fun getProviderTermsAndConditions(authToken: String? = null) = safeApiCall {
		apiService.getSettings(1, SameeAPI.Query.Value.PROVIDER)
	}
	suspend fun getProviderPrivacyPolicy(authToken: String? = null) = safeApiCall {
		apiService.getSettings(2, SameeAPI.Query.Value.PROVIDER)
	}
	
	suspend fun sendSuggestionsAndComplains(request: RequestSuggestionsAndComplains) = safeApiCall {
		apiService.sendSuggestionsAndComplains(request)
	}
	
}
