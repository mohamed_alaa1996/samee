package app.grand.tafwak.data.provider.repository

import androidx.paging.Pager
import androidx.paging.PagingConfig
import androidx.paging.PagingData
import app.grand.tafwak.core.customTypes.MADateAndTime
import app.grand.tafwak.core.extensions.flowInitialLoadingWithMinExecutionTime
import app.grand.tafwak.core.extensions.liveDataInitialLoadingWithMinExecutionTime
import app.grand.tafwak.data.basePaging.BasePaging
import app.grand.tafwak.data.basePaging.BasePagingForChat
import app.grand.tafwak.data.local.preferences.PrefsUser
import app.grand.tafwak.data.provider.dataSource.remote.ProviderRemoteDataSource
import app.grand.tafwak.data.user.dataSource.remote.UserRemoteDataSource
import app.grand.tafwak.data.user.dataSource.remotePaging.PagingSourceOrders
import app.grand.tafwak.domain.auth.entity.model.LocationData
import app.grand.tafwak.domain.auth.entity.model.ManualLocation
import app.grand.tafwak.domain.providerFlow.*
import app.grand.tafwak.domain.user.entity.model.*
import app.grand.tafwak.domain.userFlow.model.SearchFilter
import app.grand.tafwak.domain.userFlow.response.*
import app.grand.tafwak.domain.utils.*
import kotlinx.coroutines.flow.Flow
import kotlinx.coroutines.flow.first
import okhttp3.MultipartBody
import javax.inject.Inject

class ProviderRepositoryImpl @Inject constructor(
	private val dataSource: ProviderRemoteDataSource,
	private val prefsUser: PrefsUser
) {
	
	fun getServices() = flowInitialLoadingWithMinExecutionTime<MABaseResponse<List<CategoryWithServices>>> {
		emit(dataSource.getServices(prefsUser.getToken().first()!!))
	}
	
	suspend fun addService(
		categoryId: Int,
		name: String,
		price: Int,
		fileType: String,
		file: MultipartBody.Part,
	) = dataSource.addService(categoryId, name, price, fileType, file, prefsUser.getToken().first()!!)
	
	suspend fun editService(
		id: Int,
		name: String?,
		price: Int?,
		fileType: String?,
		file: MultipartBody.Part?,
	) = dataSource.editService(id, name, price, fileType, file, prefsUser.getToken().first()!!)
	
	suspend fun deleteService(
		id: Int,
	) = dataSource.deleteService(id, prefsUser.getToken().first()!!)
	
	suspend fun deleteWorking(
		id: Int,
	) = dataSource.deleteWorking(id, prefsUser.getToken().first()!!)
	
	fun getProfits(
		profitType: ProfitType,
	) = liveDataInitialLoadingWithMinExecutionTime<MABaseResponse<ResponseProfit>> {
		emit(dataSource.getProfits(profitType, prefsUser.getToken().first()!!))
	}
	
	fun getReviews() = flowInitialLoadingWithMinExecutionTime<MABaseResponse<ResponseReview>> {
		emit(dataSource.getReviews(1, prefsUser.getToken().first()!!))
	}
	
	fun getReviewsItems(): Flow<PagingData<ItemReview>> {
		return BasePaging.createFlowViaPager { page ->
			val authToken = prefsUser.getToken().first()!!
			
			dataSource.getReviews(page, authToken).mapImmediate {
				MABaseResponse(it.data?.reviews, it.message, it.code)
			}
		}
	}
	
	suspend fun updateDeliveryFlag(
		deliveryFlag: Int,
	) = dataSource.updateDeliveryFlag(deliveryFlag, prefsUser.getToken().first()!!)
	
	suspend fun updateOrdersFlag(
		ordersFlag: Int,
	) = dataSource.updateOrdersFlag(ordersFlag, prefsUser.getToken().first()!!)
	
	suspend fun createWorking(
		categoryId: Int,
		fileType: String,
		file: MultipartBody.Part,
	) = dataSource.createWorking(categoryId, fileType, file, prefsUser.getToken().first()!!)
	
	fun getProviderHome() = flowInitialLoadingWithMinExecutionTime<MABaseResponse<ResponseProviderHome>> {
		emit(dataSource.getProviderHome(prefsUser.getToken().first()!!))
	}
	
	fun getConversations(
		query: String,
	): Flow<PagingData<ResponseConversation>> {
		return BasePaging.createFlowViaPager { page ->
			val authToken = prefsUser.getToken().first()!!
			
			dataSource.getConversations(page, query, authToken)
		}
	}
	
	fun getNotifications(): Flow<PagingData<ResponseNotification>> {
		return BasePaging.createFlowViaPager { page ->
			val authToken = prefsUser.getToken().first()!!
			
			dataSource.getNotifications(page, authToken)
		}
	}
	
	fun getChatMessages(otherUserId: Int): Flow<PagingData<ResponseChatMessage>> {
		return BasePagingForChat.createFlowViaPager { page ->
			val authToken = prefsUser.getToken().first()!!
			
			dataSource.getChatMessages(page, otherUserId, authToken)
		}
	}
	
	/*fun getChatMessagesLastPage() = flowInitialLoadingWithMinExecutionTime<MABaseResponse<Int>> {
		val authToken = prefsUser.getToken().first()!!
		
		val result = dataSource.getChatMessages(1, authToken)
		val lastPage = result.toSuccessOrNull()?.value?.data?.lastPage
		
		val newResult = when (result) {
			is MAResult.Failure -> {
				MAResult.Failure(result.failureStatus, result.code, result.message)
			}
			is MAResult.Success -> {
				if (lastPage != null) {
					MAResult.Success(MABaseResponse(lastPage, "", 0))
				}else {
					MAResult.Failure(MAResult.Failure.Status.OTHER, null, null)
				}
			}
		}
		emit(newResult)
	}*/
	
	suspend fun sendChatMessage(
		orderId: Int,
		receiverId: Int,
		message: String,
		fileType: FileType = FileType.IMAGE,
		media: MultipartBody.Part? = null,
	) = dataSource.sendChatMessage(orderId, receiverId, message, fileType, media, prefsUser.getToken().first()!!)
	
	suspend fun changeOrderStatus(
		orderId: Int,
		orderStatus: OrderStatus,
	) = dataSource.changeOrderStatus(orderId, orderStatus, prefsUser.getToken().first()!!)
	
}