package app.grand.tafwak.data.user.dataSource.remote

import app.grand.tafwak.core.customTypes.MADateAndTime
import app.grand.tafwak.data.local.preferences.PrefsUser
import app.grand.tafwak.data.remote.MABaseRemoteDataSource
import app.grand.tafwak.data.samee.SameeAPI
import app.grand.tafwak.domain.auth.entity.model.LocationData
import app.grand.tafwak.domain.auth.entity.model.ManualLocation
import app.grand.tafwak.domain.auth.entity.model.SpecialLocationData
import app.grand.tafwak.domain.user.entity.model.ProviderDetails
import app.grand.tafwak.domain.user.entity.model.RequestUpdateProfile
import app.grand.tafwak.domain.user.entity.model.ServiceCategory
import app.grand.tafwak.domain.userFlow.request.RequestCreateOrder
import app.grand.tafwak.domain.userFlow.request.RequestRegisterAsProvider
import app.grand.tafwak.domain.utils.MABasePaging
import app.grand.tafwak.domain.utils.MABaseResponse
import kotlinx.coroutines.flow.first
import okhttp3.MultipartBody
import okhttp3.RequestBody.Companion.toRequestBody
import retrofit2.http.*
import javax.inject.Inject

class UserRemoteDataSource @Inject constructor(
	private val apiService: SameeUserServices,
	private val prefsUser: PrefsUser
) : MABaseRemoteDataSource() {
	
	/** @param authToken `null` if a guest not a user isa. */
	suspend fun getUserHome(authToken: String? = null) = safeApiCall {
		apiService.getUserHome(
			if (authToken.isNullOrEmpty()) emptyMap() else getAuthorizationHeader(authToken)
		)
	}
	
	suspend fun getUserHomeCategories(page: Int, authToken: String? = null) = safeApiCall {
		apiService.getUserHomeCategories(
			page,
			if (authToken.isNullOrEmpty()) emptyMap() else getAuthorizationHeader(authToken)
		)
	}
	
	suspend fun searchCategories(
		categoryName: String,
		latitude: String,
		longitude: String,
		sortBy: String,
		authToken: String,
	) = safeApiCall {
		apiService.searchCategories(
			categoryName,
			latitude,
			longitude,
			sortBy,
			getAuthorizationHeader(authToken)
		)
	}
	
	suspend fun searchCategories(
		page: Int,
		categoryName: String,
		latitude: String,
		longitude: String,
	) = safeApiCall {
		apiService.searchCategories(
			page,
			categoryName,
			latitude,
			longitude,
		)
	}
	
	suspend fun searchProvidersOfSpecificCategory(
		categoryId: Int,
		name: String,
		latitude: String,
		longitude: String,
		sortBy: String,
		authToken: String,
	) = safeApiCall {
		val queryMap = mutableMapOf<String, String>()
		if (name.isNotEmpty()) {
			queryMap[SameeAPI.Query.NAME] = name
		}
		
		apiService.searchProvidersOfSpecificCategory(
			categoryId,
			latitude,
			longitude,
			sortBy,
			queryMap,
			getAuthorizationHeader(authToken)
		)
	}
	
	suspend fun getProviderDetails(
		id: Int,
		latitude: String,
		longitude: String,
	) = safeApiCall {
		apiService.getProviderDetails(
			id,
			latitude,
			longitude,
		)
	}
	
	suspend fun createOrder(
		providerServicesIds: List<Int>,
		maDateAndTime: MADateAndTime,
		subTotal: Float,
		tax: Float,
		total: Float,
		numOfCustomers: Int,
		providerId: Int,
		deliveryCost: Float?,
		locationData: LocationData,
		manualLocation: ManualLocation?,
	) = safeApiCall {
		val queryMap = mutableMapOf<String, String>()
		deliveryCost?.also { queryMap[SameeAPI.Query.DELIVERY_COST] = it.toString() }
		
		queryMap[SameeAPI.Query.LATITUDE] = locationData.latitude
		queryMap[SameeAPI.Query.LONGITUDE] = locationData.longitude
		queryMap[SameeAPI.Query.ADDRESS] = locationData.address
		
		if (manualLocation != null) {
			queryMap[SameeAPI.Query.CITY] = manualLocation.city
			queryMap[SameeAPI.Query.AREA] = manualLocation.area
			queryMap[SameeAPI.Query.STREET] = manualLocation.street
			queryMap[SameeAPI.Query.FLOOR] = manualLocation.floorNumber.toString()
			if (manualLocation.specialSign.isNotEmpty()) {
				queryMap[SameeAPI.Query.SPECIAL_SIGN] = manualLocation.specialSign
			}
		}
		
		apiService.createOrder(
			RequestCreateOrder(
				providerServicesIds,
				maDateAndTime.getApiDateFormat(),
				maDateAndTime.getApiTimeFormat(),
				subTotal.toString(),
				tax.toString(),
				total.toString(),
				numOfCustomers,
				providerId
			),
			queryMap,
			getAuthorizationHeader(prefsUser.getToken().first()!!),
		)
	}
	
	suspend fun currentOrders(
		page: Int,
		authToken: String,
	) = safeApiCall {
		apiService.getMyOrders(
			SameeAPI.Query.Value.CURRENT,
			page,
			getAuthorizationHeader(authToken)
		)
	}
	
	suspend fun finishedOrders(
		page: Int,
		authToken: String,
	) = safeApiCall {
		apiService.getMyOrders(
			SameeAPI.Query.Value.FINISHED,
			page,
			getAuthorizationHeader(authToken)
		)
	}
	
	suspend fun getOrderDetails(
		id: Int,
		authToken: String,
	) = safeApiCall {
		apiService.getOrdersDetails(
			id,
			getAuthorizationHeader(authToken)
		)
	}
	
	suspend fun getMyPageData(
		authToken: String,
	) = safeApiCall {
		apiService.getMyPageData(getAuthorizationHeader(authToken))
	}
	
	suspend fun getWallet(
		authToken: String,
	) = safeApiCall {
		apiService.getWallet(getAuthorizationHeader(authToken))
	}
	
	suspend fun getRegisterAsProviderCategories(
		page: Int,
		authToken: String,
	) = safeApiCall {
		apiService.getRegisterAsProviderCategories(page, getAuthorizationHeader(authToken))
	}
	
	suspend fun registerAsProviderViaForm(
		name: String,
		phone: String,
		categories: List<Int>,
		authToken: String,
	) = safeApiCall {
		apiService.registerAsProviderViaForm(RequestRegisterAsProvider(name, phone, categories), getAuthorizationHeader(authToken))
	}
	
	suspend fun updateLocation(
		latitude: String,
		longitude: String,
		address: String,
		authToken: String,
	) = safeApiCall {
		apiService.updateLocation(latitude, longitude, address, getAuthorizationHeader(authToken))
	}
	
	suspend fun cancelOrder(
		id: Int,
		authToken: String,
	) = safeApiCall {
		apiService.cancelOrder(id, getAuthorizationHeader(authToken))
	}
	
	suspend fun getAllWorkings(
		page: Int,
		authToken: String,
	) = safeApiCall {
		apiService.getAllWorkings(page, getAuthorizationHeader(authToken))
	}
	
	suspend fun getAllWorkingsOfProvider(
		page: Int,
		providerId: Int,
	) = safeApiCall {
		apiService.getAllWorkingsOfProvider(page, providerId)
	}
	
	suspend fun addReview(
		orderId: Int,
		reviewableId: Int,
		rate: Float,
		comment: String,
		authToken: String,
	) = safeApiCall {
		apiService.addReview(orderId, reviewableId, rate, comment, getAuthorizationHeader(authToken))
	}
	
	suspend fun updateProfile(
		name: String,
		phone: String,
		email: String,
		latitude: String,
		longitude: String,
		address: String,
		description: String,
		authToken: String,
		image: MultipartBody.Part? = null,
	) = safeApiCall {
		val map = mutableMapOf(
			"name" to name.toRequestBody(),
			"phone" to phone.toRequestBody(),
			"latitude" to latitude.toRequestBody(),
			"longitude" to longitude.toRequestBody(),
			"address" to address.toRequestBody(),
		)
		if (email.isNotEmpty()) {
			map["email"] = email.toRequestBody()
		}
		if (description.isNotEmpty()) {
			map["description"] = description.toRequestBody()
		}
		
		if (image != null) {
			apiService.updateProfile(
				image,
				map,
				getAuthorizationHeader(authToken)
			)
		}else {
			apiService.updateProfile(
				map,
				getAuthorizationHeader(authToken)
			)
		}
	}
	
}