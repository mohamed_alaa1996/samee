package app.grand.tafwak.data.local.preferences

import android.content.Context
import com.google.gson.Gson
import dagger.hilt.android.qualifiers.ApplicationContext
import javax.inject.Inject
import javax.inject.Singleton

@Singleton
class PrefsProvider @Inject constructor(
	@ApplicationContext context: Context,
	gson: Gson,
) : PrefsBase(context, gson, "PREFS_PROVIDER") {
	
	companion object {
		private const val KEY_USER_SEARCH_QUERIES_ON_PROVIDERS = "KEY_USER_SEARCH_QUERIES_ON_PROVIDERS"
	}
	
	suspend fun setUserSearchQueriesOnProviders(queries: List<String>) =
		setValue(KEY_USER_SEARCH_QUERIES_ON_PROVIDERS, queries)
	
	fun getUserSearchQueriesOnProviders() = getValue<List<String>?>(KEY_USER_SEARCH_QUERIES_ON_PROVIDERS)
	
}
