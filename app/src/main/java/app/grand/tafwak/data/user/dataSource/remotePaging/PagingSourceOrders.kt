package app.grand.tafwak.data.user.dataSource.remotePaging

import androidx.paging.PagingSource
import androidx.paging.PagingState
import app.grand.tafwak.core.customTypes.MAPagingException
import app.grand.tafwak.data.local.preferences.PrefsUser
import app.grand.tafwak.data.user.dataSource.remote.UserRemoteDataSource
import app.grand.tafwak.domain.userFlow.response.OrderMiniDetails
import app.grand.tafwak.domain.utils.MAResult
import kotlinx.coroutines.flow.first

class PagingSourceOrders(
	private val dataSource: UserRemoteDataSource,
	private val isCurrentNotFinishedOrders: Boolean,
	private val prefsUser: PrefsUser
) : PagingSource<Int, OrderMiniDetails>() {
	
	override suspend fun load(params: LoadParams<Int>): LoadResult<Int, OrderMiniDetails> {
		val pageNumber = params.key ?: 1 // As 1 is the minimum page number.
		
		val result = if (isCurrentNotFinishedOrders) {
			dataSource.currentOrders(pageNumber, getAuthToken())
		}else {
			dataSource.finishedOrders(pageNumber, getAuthToken())
		}
		
		return when (result) {
			is MAResult.Success -> LoadResult.Page(
				result.value.data?.data.orEmpty(),
				if (pageNumber == 1) null else pageNumber.dec(),
				if (result.value.data?.nextPageUrl == null) null else pageNumber.inc()
			)
			is MAResult.Failure -> LoadResult.Error(MAPagingException(result))
		}
	}
	
	override fun getRefreshKey(state: PagingState<Int, OrderMiniDetails>): Int? {
		return state.anchorPosition?.let { anchorPosition ->
			val anchorPage = state.closestPageToPosition(anchorPosition)
			anchorPage?.prevKey?.plus(1) ?: anchorPage?.nextKey?.minus(1)
		}
	}
	
	private suspend fun getAuthToken(): String {
		return prefsUser.getToken().first()!!
	}
	
}
