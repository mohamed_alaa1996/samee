package app.grand.tafwak.data.provider.dataSource.remote

import app.grand.tafwak.data.samee.SameeAPI
import app.grand.tafwak.domain.providerFlow.*
import app.grand.tafwak.domain.user.entity.model.ResponseUserHome
import app.grand.tafwak.domain.utils.MABasePaging
import app.grand.tafwak.domain.utils.MABaseResponse
import okhttp3.MultipartBody
import okhttp3.RequestBody
import retrofit2.http.*

interface SameeProviderServices {
	
	@GET("api/v1/services")
	suspend fun getServices(
		@HeaderMap headerMap: Map<String, String>
	): MABaseResponse<List<CategoryWithServices>>
	
	@Multipart
	@POST("api/v1/services")
	suspend fun addService(
		@Part(SameeAPI.Query.CATEGORY_ID) categoryId: Int,
		@Part(SameeAPI.Query.NAME) name: RequestBody,
		@Part(SameeAPI.Query.PRICE) price: Int,
		@Part(SameeAPI.Query.FILE_TYPE) fileType: RequestBody,
		@Part file: MultipartBody.Part,
		@HeaderMap headerMap: Map<String, String>
	): MABaseResponse<Any>
	
	@Multipart
	@POST("api/v1/services/{${SameeAPI.Path.ID}}")
	suspend fun editService(
		@Path(SameeAPI.Path.ID) id: Int,
		@PartMap mapOfOtherParams: Map<String, @JvmSuppressWildcards RequestBody>,
		//@Part(SameeAPI.Query.NAME) name: String,
		//@Part(SameeAPI.Query.PRICE) price: Int,
		@Part(SameeAPI.Query.FILE_TYPE) fileType: RequestBody,
		@Part file: MultipartBody.Part,
		@HeaderMap headerMap: Map<String, String>
	): MABaseResponse<Any>
	
	@Multipart
	@POST("api/v1/services/{${SameeAPI.Path.ID}}")
	suspend fun editService(
		@Path(SameeAPI.Path.ID) id: Int,
		@PartMap mapOfOtherParams: Map<String, @JvmSuppressWildcards RequestBody>,
		//@Part(SameeAPI.Query.NAME) name: String,
		//@Part(SameeAPI.Query.PRICE) price: Int,
		@HeaderMap headerMap: Map<String, String>
	): MABaseResponse<Any>
	
	@FormUrlEncoded
	@POST("api/v1/services/{${SameeAPI.Path.ID}}")
	suspend fun deleteService(
		@Path(SameeAPI.Path.ID) id: Int,
		@HeaderMap headerMap: Map<String, String>,
		@Field("_method") method: String = "DELETE",
	): MABaseResponse<Any>

	@GET("api/v1/profits")
	suspend fun getProfits(
		@Query(SameeAPI.Query.TYPE) type: String,
		@HeaderMap headerMap: Map<String, String>,
	): MABaseResponse<ResponseProfit>

	@GET("api/v1/reviews")
	suspend fun getReviews(
		@Query(SameeAPI.Query.PAGE) page: Int,
		@HeaderMap headerMap: Map<String, String>,
	): MABaseResponse<ResponseReview>

	@GET("api/v1/toggle-flags")
	suspend fun updateDeliveryFlag(
		@Query(SameeAPI.Query.DELIVERY_FLAG) deliveryFlag: Int,
		@HeaderMap headerMap: Map<String, String>,
	): MABaseResponse<Any>

	@GET("api/v1/toggle-flags")
	suspend fun updateOrdersFlag(
		@Query(SameeAPI.Query.ORDERS_FLAG) ordersFlag: Int,
		@HeaderMap headerMap: Map<String, String>,
	): MABaseResponse<Any>

	@Multipart
	@POST("api/v1/workings")
	suspend fun createWorking(
		@Part(SameeAPI.Query.CATEGORY_ID) categoryId: Int,
		@Part(SameeAPI.Query.FILE_TYPE) fileType: RequestBody,
		@Part file: MultipartBody.Part,
		@HeaderMap headerMap: Map<String, String>,
	): MABaseResponse<Any>
	
	@FormUrlEncoded
	@POST("api/v1/workings/{${SameeAPI.Path.ID}}")
	suspend fun deleteWorking(
		@Path(SameeAPI.Path.ID) id: Int,
		@HeaderMap headerMap: Map<String, String>,
		@Field("_method") method: String = "DELETE",
	): MABaseResponse<Any>
	
	@GET("api/v1/home?" + SameeAPI.Query.ACCOUNT_TYPE_PROVIDER_KEY_AND_VALUE)
	suspend fun getProviderHome(
		@HeaderMap headerMap: Map<String, String> = emptyMap()
	): MABaseResponse<ResponseProviderHome>
	
	@GET("api/v1/chat-history")
	suspend fun getConversations(
		@Query(SameeAPI.Query.PAGE) page: Int,
		@Query(SameeAPI.Query.NAME_CAMEL_CASE) query: String,
		@HeaderMap headerMap: Map<String, String>,
	): MABaseResponse<MABasePaging<ResponseConversation>>
	
	@GET("api/v1/notifications")
	suspend fun getNotifications(
		@Query(SameeAPI.Query.PAGE) page: Int,
		@HeaderMap headerMap: Map<String, String>,
	): MABaseResponse<MABasePaging<ResponseNotification>>
	
	@GET("api/v1/chat")
	suspend fun getChatMessages(
		@Query(SameeAPI.Query.PAGE) page: Int,
		@Query(SameeAPI.Query.RECEIVER_ID) receiverId: Int,
		@HeaderMap headerMap: Map<String, String>,
	): MABaseResponse<MABasePaging<ResponseChatMessage>>
	
	@Multipart
	@POST("api/v1/send-message")
	suspend fun sendChatMessages(
		@Part(SameeAPI.Query.ORDER_ID) orderId: Int,
		@Part(SameeAPI.Query.RECEIVER_ID) receiverId: Int,
		//@Part(SameeAPI.Query.MESSAGE) message: RequestBody,
		@Part(SameeAPI.Query.MEDIA_TYPE) mediaType: RequestBody,
		@Part media: MultipartBody.Part,
		@PartMap map: Map<String, @JvmSuppressWildcards RequestBody>,
		@HeaderMap headerMap: Map<String, String>,
	): MABaseResponse<MABasePaging<ResponseChatMessage>>
	@Multipart
	@POST("api/v1/send-message")
	suspend fun sendChatMessages(
		@Part(SameeAPI.Query.ORDER_ID) orderId: Int,
		@Part(SameeAPI.Query.RECEIVER_ID) receiverId: Int,
		//@Part(SameeAPI.Query.MESSAGE) message: RequestBody,
		@PartMap map: Map<String, @JvmSuppressWildcards RequestBody>,
		@HeaderMap headerMap: Map<String, String>,
	): MABaseResponse<MABasePaging<ResponseChatMessage>>
	
	@GET("api/v1/orders/{${SameeAPI.Path.ID}}/toggle-status")
	suspend fun changeOrderStatus(
		@Path(SameeAPI.Path.ID) id: Int,
		@Query(SameeAPI.Query.STATUS) status: String,
		@HeaderMap headerMap: Map<String, String>,
	): MABaseResponse<Any>
	
}
