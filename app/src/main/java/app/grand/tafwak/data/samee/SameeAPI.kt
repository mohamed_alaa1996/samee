package app.grand.tafwak.data.samee

object SameeAPI {
	
	object Path {
		const val ID = "id"
	}
	
	object Query {
		const val REFER_CODE = "refer_code"

		const val OAUTH_TOKEN = "oauth_token"
		const val OAUTH_VERIFIER = "oauth_verifier"
		
		const val STATUS = "status"
		const val SERVICE_NAME = "service_name"
		const val PRICE = "price"
		const val FILE_TYPE = "file_type"
		const val SOCIAL_ID = "social_id"
		
		const val BANK_NAME = "bank_name"
		const val BANK_NUMBER = "bank_number"
		const val IBAN = "iban"
		const val BANK_ACCOUNT_HOLDER = "bank_account_holder"
		
		const val REVIEWABLE_ID = "reviewable_id"
		const val ORDER_ID = "order_id"
		const val RECEIVER_ID = "receiver_id"
		const val MEDIA_TYPE = "media_type"
		const val MESSAGE = "message"
		const val TOGGLE_STATUS = "toggle-status"
		const val RATE = "rate"
		const val REVIEW = "review"
		
		const val PROVIDER_ID = "provider_id"
		
		const val DELIVERY_COST = "delivery_cost"
		const val ACCOUNT_TYPE_KEY = "account_type"
		const val REGISTER_STEP = "register_step"
		const val FILE = "file"
		private const val ACCOUNT_TYPE_USER = "user"
		private const val ACCOUNT_TYPE_PROVIDER = "provider"
		const val ACCOUNT_TYPE_USER_KEY_AND_VALUE = "$ACCOUNT_TYPE_KEY=$ACCOUNT_TYPE_USER"
		const val ACCOUNT_TYPE_PROVIDER_KEY_AND_VALUE = "$ACCOUNT_TYPE_KEY=$ACCOUNT_TYPE_PROVIDER"
		
		const val TYPE = "type"
		const val PAGE = "page"
		const val NAME_CAMEL_CASE = "Name"
		const val DELIVERY_FLAG = "delivery_flag"
		const val ORDERS_FLAG = "orders_flag"
		const val APP_TYPE = "app_type"
		
		const val CATEGORY_NAME = "category_name"
		const val CATEGORY_ID = "category_id"
		const val NAME = "name"
		const val PHONE = "phone"
		const val CATEGORIES = "categories"
		const val LATITUDE = "latitude"
		const val LONGITUDE = "longitude"
		const val ADDRESS = "address"
		const val SORT_BY = "sort_by"
		
		const val CITY = "city"
		const val AREA = "area"
		const val STREET = "street"
		const val FLOOR = "floor"
		const val SPECIAL_SIGN = "special_sign"
		
		object Value {
			
			const val CURRENT = "current"
			const val FINISHED = "finished"
			
			const val USER = "user"
			
			const val PROVIDER = "provider"
			
		}
	}
	
}
