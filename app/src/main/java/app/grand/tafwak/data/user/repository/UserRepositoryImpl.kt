package app.grand.tafwak.data.user.repository

import androidx.paging.Pager
import androidx.paging.PagingConfig
import androidx.paging.PagingData
import androidx.paging.liveData
import app.grand.tafwak.core.customTypes.MADateAndTime
import app.grand.tafwak.core.extensions.flowInitialLoadingWithMinExecutionTime
import app.grand.tafwak.core.extensions.liveDataInitialLoadingWithMinExecutionTime
import app.grand.tafwak.data.basePaging.BasePaging
import app.grand.tafwak.data.local.preferences.PrefsUser
import app.grand.tafwak.data.user.dataSource.remote.UserRemoteDataSource
import app.grand.tafwak.data.user.dataSource.remotePaging.PagingSourceOrders
import app.grand.tafwak.domain.auth.entity.model.LocationData
import app.grand.tafwak.domain.auth.entity.model.ManualLocation
import app.grand.tafwak.domain.auth.entity.model.SpecialLocationData
import app.grand.tafwak.domain.user.entity.model.ProviderDetails
import app.grand.tafwak.domain.user.entity.model.ResponseSearch
import app.grand.tafwak.domain.user.entity.model.ResponseUserHome
import app.grand.tafwak.domain.user.entity.model.ServiceCategory
import app.grand.tafwak.domain.userFlow.model.SearchFilter
import app.grand.tafwak.domain.userFlow.response.*
import app.grand.tafwak.domain.utils.*
import kotlinx.coroutines.delay
import kotlinx.coroutines.flow.Flow
import kotlinx.coroutines.flow.FlowCollector
import kotlinx.coroutines.flow.first
import kotlinx.coroutines.flow.flow
import okhttp3.MultipartBody
import timber.log.Timber
import javax.inject.Inject

class UserRepositoryImpl @Inject constructor(
	private val apiUserService: UserRemoteDataSource,
	private val prefsUser: PrefsUser
) {
	
	fun getUserHome() = flowInitialLoadingWithMinExecutionTime<MABaseResponse<ResponseUserHome>> {
		emit(apiUserService.getUserHome())
	}
	
	fun getUserHomeCategories(): Flow<PagingData<ServiceCategory>> {
		return BasePaging.createFlowViaPager { page ->
			apiUserService.getUserHomeCategories(page).mapImmediate {
				MABaseResponse(it.data?.categories, it.message, it.code)
			}
		}
	}
	
	fun searchCategories(
		categoryName: String,
		latitude: String,
		longitude: String,
		filter: SearchFilter,
		authToken: String,
	) = liveDataInitialLoadingWithMinExecutionTime<MABaseResponse<ResponseSearch>> {
		emit(apiUserService.searchCategories(categoryName, latitude, longitude, filter.keyOnApi, authToken))
	}
	
	fun searchCategories(
		categoryName: String,
	): Flow<PagingData<ResponseSearch>> {
		return BasePaging.createFlowViaPager { page ->
			prefsUser.getLocation().first()!!.let {
				apiUserService.searchCategories(page, categoryName, it.latitude, it.longitude)
			}
		}
	}
	
//	fun searchProvidersOfSpecificCategory(
//		categoryId: Int,
//		name: String,
//		filter: SearchFilter,
//	) = liveDataInitialLoadingWithMinExecutionTime<MABaseResponse<ResponseSearch>> {
//		emit(apiUserService.searchProvidersOfSpecificCategory(categoryId, name, latitude, longitude, filter.keyOnApi, authToken))
//	}
	
	fun searchProvidersOfSpecificCategory(
		categoryId: Int,
		name: String,
		latitude: String,
		longitude: String,
		filter: SearchFilter,
		authToken: String,
	) = liveDataInitialLoadingWithMinExecutionTime<MABaseResponse<ResponseSearch>> {
		emit(apiUserService.searchProvidersOfSpecificCategory(categoryId, name, latitude, longitude, filter.keyOnApi, authToken))
	}
	
	fun getProviderDetails(
		id: Int,
		latitude: String,
		longitude: String,
	) = flowInitialLoadingWithMinExecutionTime<MABaseResponse<ProviderDetails>> {
		emit(apiUserService.getProviderDetails(id, latitude, longitude))
	}
	
	fun getProviderDetails(
		id: Int,
	) = flowInitialLoadingWithMinExecutionTime<MABaseResponse<ProviderDetails>> {
		val location = prefsUser.getLocation().first()!!
		
		emit(apiUserService.getProviderDetails(id, location.latitude, location.longitude))
	}
	
	fun getProviderDetailsWithLocation(
		id: Int
	) = flowInitialLoadingWithMinExecutionTime<Pair<LocationData, MABaseResponse<ProviderDetails>>> {
		val location = prefsUser.getLocation().first()!!
		
		emit(
			apiUserService.getProviderDetails(id, location.latitude, location.longitude).map {
				location to it
			}
		)
	}
	
	fun createOrder(
		providerServicesIds: List<Int>,
		maDateAndTime: MADateAndTime,
		subTotal: Float,
		tax: Float,
		total: Float,
		numOfCustomers: Int,
		providerId: Int,
		deliveryCost: Float?,
		locationData: LocationData,
		manualLocation: ManualLocation?,
	) = liveDataInitialLoadingWithMinExecutionTime<MABaseResponse<Any>> {
		emit(
			apiUserService.createOrder(
				providerServicesIds, maDateAndTime, subTotal, tax, total, numOfCustomers,
				providerId, deliveryCost, locationData, manualLocation
			)
		)
	}
	
	fun getCurrentOrders(): Flow<PagingData<OrderMiniDetails>> = getMyOrders(true)
	
	fun getFinishedOrders(): Flow<PagingData<OrderMiniDetails>> = getMyOrders(false)
	
	private fun getMyOrders(isCurrent: Boolean): Flow<PagingData<OrderMiniDetails>> {
		return Pager(
			PagingConfig(10, enablePlaceholders = false)
		) {
			PagingSourceOrders(apiUserService, isCurrent, prefsUser)
		}.flow
	}
	
	fun getOrderDetails(
		id: Int,
	) = liveDataInitialLoadingWithMinExecutionTime<MABaseResponse<OrderDetails>> {
		val authToken = prefsUser.getToken().first()!!
		
		emit(
			apiUserService.getOrderDetails(
				id,
				authToken
			)
		)
	}
	
	fun getMyPageData() = flowInitialLoadingWithMinExecutionTime<MABaseResponse<MyPageData>> {
		val authToken = prefsUser.getToken().first()!!
		
		emit(apiUserService.getMyPageData(authToken))
	}
	
	fun getWallet() = flowInitialLoadingWithMinExecutionTime<MABaseResponse<ResponseWallet>> {
		val authToken = prefsUser.getToken().first()!!
		
		emit(apiUserService.getWallet(authToken))
	}
	
	fun getRegisterAsProviderCategories(): Flow<PagingData<ServiceCategory>> {
		return BasePaging.createFlowViaPager {
			val authToken = prefsUser.getToken().first()!!
			
			apiUserService.getRegisterAsProviderCategories(it, authToken)
		}
	}
	
	fun registerAsProviderViaForm(
		name: String,
		phone: String,
		categories: List<Int>,
	) = liveDataInitialLoadingWithMinExecutionTime<MABaseResponse<Any>> {
		val authToken = prefsUser.getToken().first()!!
		
		emit(apiUserService.registerAsProviderViaForm(name, phone, categories, authToken))
	}
	
	/** @param authToken if empty then it will be retrieved via [prefsUser] */
	suspend fun updateLocation(
		latitude: String,
		longitude: String,
		address: String,
		authToken: String = ""
	) = apiUserService.updateLocation(
		latitude,
		longitude,
		address,
		if (authToken.isEmpty()) prefsUser.getToken().first()!! else authToken
	)
	
	suspend fun cancelOrder(id: Int) = apiUserService.cancelOrder(id, prefsUser.getToken().first()!!)
	
	fun getAllWorkingsOfMe() = BasePaging.createFlowViaPager {
		val authToken = prefsUser.getToken().first()!!
		
		apiUserService.getAllWorkings(it, authToken)
	}
	
	fun getAllWorkingsOfProvider(providerId: Int) = BasePaging.createFlowViaPager {
		apiUserService.getAllWorkingsOfProvider(it, providerId)
	}
	
	suspend fun addReview(
		orderId: Int,
		reviewableId: Int,
		rate: Float,
		comment: String,
	) = apiUserService.addReview(orderId, reviewableId, rate, comment, prefsUser.getToken().first()!!)
	
	suspend fun updateProfile(
		name: String,
		phone: String,
		email: String,
		latitude: String,
		longitude: String,
		address: String,
		description: String?,
		image: MultipartBody.Part? = null,
	) = apiUserService.updateProfile(
		name,
		phone,
		email,
		latitude,
		longitude,
		address,
		description.orEmpty(),
		prefsUser.getToken().first()!!,
		image
	)
	
}
