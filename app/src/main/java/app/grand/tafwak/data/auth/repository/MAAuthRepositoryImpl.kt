package app.grand.tafwak.data.auth.repository

import android.app.Application
import app.grand.tafwak.core.extensions.app
import app.grand.tafwak.core.extensions.flowInitialLoadingWithMinExecutionTime
import app.grand.tafwak.core.extensions.getDeviceIdWithoutPermission
import app.grand.tafwak.core.extensions.liveDataInitialLoadingWithMinExecutionTime
import app.grand.tafwak.data.auth.data_source.remote.MAAuthDataSource
import app.grand.tafwak.data.local.preferences.PrefsUser
import app.grand.tafwak.domain.auth.entity.response.ResponseVerifyPhone
import app.grand.tafwak.domain.utils.MABaseResponse
import app.grand.tafwak.domain.utils.MAResult
import com.google.firebase.messaging.FirebaseMessaging
import kotlinx.coroutines.flow.first
import okhttp3.MultipartBody
import timber.log.Timber
import javax.inject.Inject
import kotlin.coroutines.resume
import kotlin.coroutines.suspendCoroutine

class MAAuthRepositoryImpl @Inject constructor(
	private val apiService: MAAuthDataSource,
	private val prefsUser: PrefsUser
) {

	suspend fun checkReferralCode(code: String): MAResult.Immediate<MABaseResponse<ResponseVerifyPhone?>> = apiService.checkReferralCode(code)

	fun getCurrentFirebaseTokenAsFlow(deviceId: String) = flowInitialLoadingWithMinExecutionTime<MABaseResponse<String>> {
		val (exception, token) = getCurrentFirebaseToken()
		
		if (!token.isNullOrEmpty()) {
			val result = setFirebaseTokenSuspend(token, deviceId)
			
			if (result is MAResult.Failure) {
				emit(MAResult.Failure(result.failureStatus, result.code, result.message))
				
				return@flowInitialLoadingWithMinExecutionTime
			}
		}
		
		emit(
			if (token.isNullOrEmpty()) {
				MAResult.Failure(MAResult.Failure.Status.OTHER, null, exception?.message)
			}else {
				MAResult.Success(MABaseResponse(token, "", 0))
			}
		)
	}
	
	private suspend fun getCurrentFirebaseToken(): Pair<Exception?, String?> {
		return suspendCoroutine { continuation ->
			FirebaseMessaging.getInstance().token.addOnCompleteListener { task ->
				if (!task.isSuccessful) {
					Timber.d("Firebase Token exception -> ${task.exception}")
					continuation.resume(task.exception to null)
				}else {
					Timber.d("Firebase Token -> ${task.result}")
					continuation.resume(null to task.result)
				}
			}
		}
	}
	
	fun registerUserSendPhone(phone: String, referralCode: String) = flowInitialLoadingWithMinExecutionTime<MABaseResponse<Any>> {
		emit(apiService.registerUserSendPhone(phone, referralCode))
	}
	
	fun registerProviderSendPhone(phone: String, referralCode: String) = flowInitialLoadingWithMinExecutionTime<MABaseResponse<Any>> {
		emit(apiService.registerProviderSendPhone(phone, referralCode))
	}

	suspend fun registerUserSendPhone2(phone: String, referralCode: String?) = apiService.registerUserSendPhone(phone, referralCode)

	suspend fun registerProviderSendPhone2(phone: String, referralCode: String?) = apiService.registerProviderSendPhone(phone, referralCode)
	
	fun verifyUserPhone(code: String, phone: String) = liveDataInitialLoadingWithMinExecutionTime<MABaseResponse<ResponseVerifyPhone>> {
		emit(apiService.verifyUserPhone(code, phone))
	}

	suspend fun verifyUserPhone2(code: String, phone: String) = apiService.verifyUserPhone(code, phone)

	fun verifyProviderPhone(code: String, phone: String) = liveDataInitialLoadingWithMinExecutionTime<MABaseResponse<ResponseVerifyPhone>> {
		emit(apiService.verifyProviderPhone(code, phone))
	}

	suspend fun verifyProviderPhone2(code: String, phone: String) = apiService.verifyProviderPhone(code, phone)
	
	fun resendUserVerificationCode(phone: String) = liveDataInitialLoadingWithMinExecutionTime<MABaseResponse<Any>> {
		emit(apiService.resendUserVerificationCode(phone))
	}

	suspend fun resendUserVerificationCode2(phone: String) = apiService.resendUserVerificationCode(phone)
	
	fun registerUserSendLocation(latitude: String, longitude: String, address: String, authToken: String) = liveDataInitialLoadingWithMinExecutionTime<MABaseResponse<Any>> {
		emit(apiService.registerSendLocation(latitude, longitude, address, true, authToken))
	}
	
	suspend fun registerProviderSendLocation(latitude: String, longitude: String, address: String) =
		apiService.registerSendLocation(latitude, longitude, address, false, prefsUser.getToken().first()!!)
	
	suspend fun registerProviderStep2PersonalData(
		image: MultipartBody.Part,
		name: String,
		email: String,
		description: String,
	) = apiService.registerProviderStep2PersonalData(image, name, email, description, prefsUser.getToken().first()!!)
	
	fun setFirebaseToken(firebaseToken: String?, deviceId: String) = liveDataInitialLoadingWithMinExecutionTime<MABaseResponse<Any>> {
		val authToken = prefsUser.getToken().first()!!
		
		emit(apiService.setFirebaseToken(firebaseToken, deviceId, authToken))
	}
	
	suspend fun setFirebaseTokenSuspend(firebaseToken: String?, deviceId: String) =
		apiService.setFirebaseToken(firebaseToken, deviceId, prefsUser.getToken().first()!!)
	
	suspend fun registerProviderStep3PersonalData(
		bankName: String,
		bankNumber: String,
		iban: String,
		bankAccountHolder: String,
	) = apiService.registerProviderStep3PersonalData(bankName, bankNumber, iban, bankAccountHolder, prefsUser.getToken().first()!!)
	
	suspend fun registerProviderStepFinalStep(
		categoriesIds: List<Int>,
		servicesNames: List<String>,
		prices: List<Int>,
		files: List<MultipartBody.Part>,
		typesOfFiles: List<String>,
	) = apiService.registerProviderStepFinalStep(categoriesIds, servicesNames, prices, files, typesOfFiles, prefsUser.getToken().first()!!)
	
	suspend fun socialLogin(
		socialId: String,
	) = apiService.socialLogin(socialId)
	
	suspend fun socialRegister(
		socialId: String,
		phone: String,
	) = apiService.socialRegister(socialId, phone)
	
}