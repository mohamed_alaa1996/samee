package app.grand.tafwak.data.maSettings

import app.grand.tafwak.data.samee.SameeAPI
import app.grand.tafwak.domain.settings.models.ResponseSettings
import app.grand.tafwak.domain.settings.request.RequestSuggestionsAndComplains
import app.grand.tafwak.domain.user.entity.model.ResponseUserHome
import app.grand.tafwak.domain.utils.MABaseResponse
import retrofit2.http.*

interface SameeSettingsServices {
	
	@GET("api/v1/settings")
	suspend fun getSettings(
		@Query(SameeAPI.Query.TYPE) type: Int,
		@Query(SameeAPI.Query.APP_TYPE) appType: String,
	): MABaseResponse<ResponseSettings>
	
	@POST("api/v1/complaint")
	suspend fun sendSuggestionsAndComplains(
		@Body request: RequestSuggestionsAndComplains
	): MABaseResponse<Any>
	
}
