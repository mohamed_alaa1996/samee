package app.grand.tafwak.data.auth.repository

import app.grand.tafwak.data.auth.data_source.remote.TwitterRemoteDataSource
import kotlinx.coroutines.flow.first
import okhttp3.MultipartBody
import javax.inject.Inject

class TwitterRepositoryImpl @Inject constructor(
	private val dataSource: TwitterRemoteDataSource
) {
	
	suspend fun obtainRequestToken() = dataSource.obtainRequestToken()

	suspend fun obtainAccessToken(
		oauthToken: String,
		oauthVerifier: String
	) = dataSource.obtainAccessToken(oauthToken, oauthVerifier)
	
}