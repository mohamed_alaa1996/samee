package app.grand.tafwak.data.maSettings.repository

import app.grand.tafwak.core.extensions.flowInitialLoadingWithMinExecutionTime
import app.grand.tafwak.core.extensions.liveDataInitialLoadingWithMinExecutionTime
import app.grand.tafwak.data.maSettings.SettingsDataSource
import app.grand.tafwak.data.user.dataSource.remote.UserRemoteDataSource
import app.grand.tafwak.domain.settings.models.ResponseSettings
import app.grand.tafwak.domain.settings.request.RequestSuggestionsAndComplains
import app.grand.tafwak.domain.user.entity.model.ResponseUserHome
import app.grand.tafwak.domain.utils.MABaseResponse
import javax.inject.Inject

class SettingsRepositoryImpl @Inject constructor(
	private val apiService: SettingsDataSource
) {
	
	fun getUserAbout() = flowInitialLoadingWithMinExecutionTime<MABaseResponse<ResponseSettings>> {
		emit(apiService.getUserAbout())
	}	
	fun getUserTermsAndConditions() = flowInitialLoadingWithMinExecutionTime<MABaseResponse<ResponseSettings>> {
		emit(apiService.getUserTermsAndConditions())
	}	
	fun getUserPrivacyPolicy() = flowInitialLoadingWithMinExecutionTime<MABaseResponse<ResponseSettings>> {
		emit(apiService.getUserPrivacyPolicy())
	}
	
	fun getProviderAbout() = flowInitialLoadingWithMinExecutionTime<MABaseResponse<ResponseSettings>> {
		emit(apiService.getProviderAbout())
	}	
	fun getProviderTermsAndConditions() = flowInitialLoadingWithMinExecutionTime<MABaseResponse<ResponseSettings>> {
		emit(apiService.getProviderTermsAndConditions())
	}	
	fun getProviderPrivacyPolicy() = flowInitialLoadingWithMinExecutionTime<MABaseResponse<ResponseSettings>> {
		emit(apiService.getProviderPrivacyPolicy())
	}
	
	fun sendSuggestionsAndComplains(request: RequestSuggestionsAndComplains) = liveDataInitialLoadingWithMinExecutionTime<MABaseResponse<Any>> {
		emit(apiService.sendSuggestionsAndComplains(request))
	}
	
}
