package app.grand.tafwak.data.user.dataSource.remote

import app.grand.tafwak.data.samee.SameeAPI
import app.grand.tafwak.domain.user.entity.model.*
import app.grand.tafwak.domain.userFlow.request.RequestCreateOrder
import app.grand.tafwak.domain.userFlow.request.RequestRegisterAsProvider
import app.grand.tafwak.domain.userFlow.response.*
import app.grand.tafwak.domain.utils.BaseResponse
import app.grand.tafwak.domain.utils.MABasePaging
import app.grand.tafwak.domain.utils.MABaseResponse
import okhttp3.MultipartBody
import okhttp3.RequestBody
import retrofit2.http.*

interface SameeUserServices {

	@GET("api/v1/home?" + SameeAPI.Query.ACCOUNT_TYPE_USER_KEY_AND_VALUE)
	suspend fun getUserHome(
		@HeaderMap headerMap: Map<String, String> = emptyMap()
	): MABaseResponse<ResponseUserHome>
	
	@GET("api/v1/home?" + SameeAPI.Query.ACCOUNT_TYPE_USER_KEY_AND_VALUE)
	suspend fun getUserHomeCategories(
		@Query(SameeAPI.Query.PAGE) page: Int,
		@HeaderMap headerMap: Map<String, String> = emptyMap()
	): MABaseResponse<ResponseUserHomeCategories>
	
	@GET("api/v1/providers")
	suspend fun searchCategories(
		@Query(SameeAPI.Query.CATEGORY_NAME) categoryName: String,
		@Query(SameeAPI.Query.LATITUDE) latitude: String,
		@Query(SameeAPI.Query.LONGITUDE) longitude: String,
		@Query(SameeAPI.Query.SORT_BY) sortBy: String,
		@HeaderMap headerMap: Map<String, String>
	): MABaseResponse<ResponseSearch>
	
	@GET("api/v1/providers")
	suspend fun searchCategories(
		@Query(SameeAPI.Query.PAGE) page: Int,
		@Query(SameeAPI.Query.CATEGORY_NAME) categoryName: String,
		@Query(SameeAPI.Query.LATITUDE) latitude: String,
		@Query(SameeAPI.Query.LONGITUDE) longitude: String,
	): MABaseResponse<MABasePaging<ResponseSearch>>
	
	@GET("api/v1/providers")
	suspend fun searchProvidersOfSpecificCategory(
		@Query(SameeAPI.Query.CATEGORY_ID) categoryId: Int,
		@Query(SameeAPI.Query.LATITUDE) latitude: String,
		@Query(SameeAPI.Query.LONGITUDE) longitude: String,
		@Query(SameeAPI.Query.SORT_BY) sortBy: String,
		@QueryMap queryMap: Map<String, String>,
		@HeaderMap headerMap: Map<String, String>,
	): MABaseResponse<ResponseSearch>
	
	@GET("api/v1/provider/{${SameeAPI.Path.ID}}")
	suspend fun getProviderDetails(
		@Path(SameeAPI.Path.ID) id: Int,
		@Query(SameeAPI.Query.LATITUDE) latitude: String,
		@Query(SameeAPI.Query.LONGITUDE) longitude: String,
	): MABaseResponse<ProviderDetails>
	
	@POST("api/v1/store")
	suspend fun createOrder(
		@Body request: RequestCreateOrder,
		@QueryMap queryMap: Map<String, String>,
		@HeaderMap headerMap: Map<String, String>,
	): MABaseResponse<Any>
	
	@GET("api/v1/orders")
	suspend fun getMyOrders(
		@Query(SameeAPI.Query.TYPE) type: String,
		@Query(SameeAPI.Query.PAGE) page: Int,
		@HeaderMap headerMap: Map<String, String>,
	): MABaseResponse<MABasePaging<OrderMiniDetails>>
	
	@GET("api/v1/orders/{${SameeAPI.Path.ID}}")
	suspend fun getOrdersDetails(
		@Path(SameeAPI.Path.ID) id: Int,
		@HeaderMap headerMap: Map<String, String>,
	): MABaseResponse<OrderDetails>
	
	@GET("api/v1/notifications-count")
	suspend fun getMyPageData(
		@HeaderMap headerMap: Map<String, String>,
	): MABaseResponse<MyPageData>
	
	@GET("api/v1/wallet")
	suspend fun getWallet(
		@HeaderMap headerMap: Map<String, String>,
	): MABaseResponse<ResponseWallet>
	
	@GET("api/v1/categories")
	suspend fun getRegisterAsProviderCategories(
		@Query(SameeAPI.Query.PAGE) page: Int,
		@HeaderMap headerMap: Map<String, String>,
	): MABaseResponse<MABasePaging<ServiceCategory>>
	
	@POST("api/v1/send-provider-request")
	suspend fun registerAsProviderViaForm(
		@Body request: RequestRegisterAsProvider,
		@HeaderMap headerMap: Map<String, String>,
	): MABaseResponse<Any>
	
	@FormUrlEncoded
	@POST("api/v1/auth/update-profile")
	suspend fun updateLocation(
		@Field(SameeAPI.Query.LATITUDE) latitude: String,
		@Field(SameeAPI.Query.LONGITUDE) longitude: String,
		@Field(SameeAPI.Query.ADDRESS) address: String,
		@HeaderMap headerMap: Map<String, String>,
	): MABaseResponse<Any>
	
	@GET("api/v1/orders/{${SameeAPI.Path.ID}}/cancel")
	suspend fun cancelOrder(
		@Path(SameeAPI.Path.ID) id: Int,
		@HeaderMap headerMap: Map<String, String>,
	): MABaseResponse<Any>
	
	@GET("api/v1/workings")
	suspend fun getAllWorkings(
		@Query(SameeAPI.Query.PAGE) page: Int,
		@HeaderMap headerMap: Map<String, String>,
	): MABaseResponse<MABasePaging<CategoryWithWorkings>>
	
	@GET("api/v1/workings")
	suspend fun getAllWorkingsOfProvider(
		@Query(SameeAPI.Query.PAGE) page: Int,
		@Query(SameeAPI.Query.PROVIDER_ID) providerId: Int,
	): MABaseResponse<MABasePaging<CategoryWithWorkings>>
	
	@FormUrlEncoded
	@POST("api/v1/reviews")
	suspend fun addReview(
		@Field(SameeAPI.Query.ORDER_ID) orderId: Int,
		@Field(SameeAPI.Query.REVIEWABLE_ID) reviewableId: Int,
		@Field(SameeAPI.Query.RATE) rate: Float,
		@Field(SameeAPI.Query.REVIEW) comment: String,
		@HeaderMap headerMap: Map<String, String>,
	): MABaseResponse<Any>
	
	@Multipart
	@POST("api/v1/auth/update-profile")
	suspend fun updateProfile(
		@Part image: MultipartBody.Part,
		@PartMap mapOfOtherParams: Map<String, @JvmSuppressWildcards RequestBody>,
		//@Body request: RequestUpdateProfile,
		@HeaderMap headerMap: Map<String, String>,
	): MABaseResponse<ResponseUpdateProfile>
	
	@Multipart
	@POST("api/v1/auth/update-profile")
	suspend fun updateProfile(
		@PartMap mapOfOtherParams: Map<String, @JvmSuppressWildcards RequestBody>,
		@HeaderMap headerMap: Map<String, String>,
	): MABaseResponse<ResponseUpdateProfile>
	
}
