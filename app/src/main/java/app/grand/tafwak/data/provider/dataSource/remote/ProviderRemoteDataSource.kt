package app.grand.tafwak.data.provider.dataSource.remote

import app.grand.tafwak.data.local.preferences.PrefsUser
import app.grand.tafwak.data.remote.MABaseRemoteDataSource
import app.grand.tafwak.data.samee.SameeAPI
import app.grand.tafwak.data.user.dataSource.remote.SameeUserServices
import app.grand.tafwak.domain.providerFlow.ProfitType
import app.grand.tafwak.domain.user.entity.model.FileType
import app.grand.tafwak.domain.userFlow.response.OrderStatus
import okhttp3.MultipartBody
import okhttp3.RequestBody
import okhttp3.RequestBody.Companion.toRequestBody
import retrofit2.http.HeaderMap
import retrofit2.http.Part
import retrofit2.http.PartMap
import retrofit2.http.Path
import javax.inject.Inject

class ProviderRemoteDataSource @Inject constructor(
	private val apiService: SameeProviderServices,
	private val prefsUser: PrefsUser
) : MABaseRemoteDataSource() {
	
	suspend fun getServices(authToken: String) = safeApiCall {
		apiService.getServices(getAuthorizationHeader(authToken))
	}
	
	suspend fun addService(
		categoryId: Int,
		name: String,
		price: Int,
		fileType: String,
		file: MultipartBody.Part,
		authToken: String
	) = safeApiCall {
		apiService.addService(categoryId, name.toRequestBody(), price, fileType.toRequestBody(), file, getAuthorizationHeader(authToken))
	}
	
	suspend fun editService(
		id: Int,
		name: String?,
		price: Int?,
		fileType: String?,
		file: MultipartBody.Part?,
		authToken: String
	) = safeApiCall {
		val map = mutableMapOf(
			"_method" to "PATCH".toRequestBody()
		)
		if (!name.isNullOrEmpty()) map["name"] = name.toRequestBody()
		if (price != null) map["price"] = price.toString().toRequestBody()
		
		if (file != null) {
			apiService.editService(id, map, fileType!!.toRequestBody(), file, getAuthorizationHeader(authToken))
		}else {
			apiService.editService(id, map, getAuthorizationHeader(authToken))
		}
	}
	
	suspend fun deleteService(
		id: Int,
		authToken: String
	) = safeApiCall {
		apiService.deleteService(id, getAuthorizationHeader(authToken))
	}
	
	suspend fun deleteWorking(
		id: Int,
		authToken: String
	) = safeApiCall {
		apiService.deleteWorking(id, getAuthorizationHeader(authToken))
	}
	
	suspend fun getProfits(
		profitType: ProfitType,
		authToken: String
	) = safeApiCall {
		apiService.getProfits(profitType.apiKey, getAuthorizationHeader(authToken))
	}
	
	suspend fun getReviews(
		page: Int,
		authToken: String
	) = safeApiCall {
		apiService.getReviews(page, getAuthorizationHeader(authToken))
	}
	
	suspend fun updateDeliveryFlag(
		deliveryFlag: Int,
		authToken: String
	) = safeApiCall {
		apiService.updateDeliveryFlag(deliveryFlag, getAuthorizationHeader(authToken))
	}
	
	suspend fun updateOrdersFlag(
		ordersFlag: Int,
		authToken: String
	) = safeApiCall {
		apiService.updateOrdersFlag(ordersFlag, getAuthorizationHeader(authToken))
	}
	
	suspend fun createWorking(
		categoryId: Int,
		fileType: String,
		file: MultipartBody.Part,
		authToken: String
	) = safeApiCall {
		apiService.createWorking(categoryId, fileType.toRequestBody(), file, getAuthorizationHeader(authToken))
	}
	
	suspend fun getProviderHome(authToken: String) = safeApiCall {
		apiService.getProviderHome(getAuthorizationHeader(authToken))
	}
	
	suspend fun getConversations(
		page: Int,
		query: String,
		authToken: String
	) = safeApiCall {
		apiService.getConversations(page, query, getAuthorizationHeader(authToken))
	}
	
	suspend fun getNotifications(
		page: Int,
		authToken: String
	) = safeApiCall {
		apiService.getNotifications(page, getAuthorizationHeader(authToken))
	}
	
	suspend fun getChatMessages(
		page: Int,
		receiverId: Int,
		authToken: String
	) = safeApiCall {
		apiService.getChatMessages(page, receiverId, getAuthorizationHeader(authToken))
	}
	
	suspend fun changeOrderStatus(
		orderId: Int,
		orderStatus: OrderStatus,
		authToken: String
	) = safeApiCall {
		apiService.changeOrderStatus(orderId, orderStatus.apiKey, getAuthorizationHeader(authToken))
	}
	
	suspend fun sendChatMessage(
		orderId: Int,
		receiverId: Int,
		message: String,
		fileType: FileType,
		media: MultipartBody.Part?,
		authToken: String
	) = safeApiCall {
		val map = mutableMapOf<String, RequestBody>()
		if (message.isNotEmpty()) {
			map[SameeAPI.Query.MESSAGE] = message.toRequestBody()
		}

		if (media != null) {
			apiService.sendChatMessages(orderId, receiverId, fileType.apiKey.toRequestBody(), media, map, getAuthorizationHeader(authToken))
		}else {
			apiService.sendChatMessages(orderId, receiverId, map, getAuthorizationHeader(authToken))
		}
	}

}