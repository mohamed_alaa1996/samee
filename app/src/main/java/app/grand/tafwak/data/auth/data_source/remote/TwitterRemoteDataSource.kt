package app.grand.tafwak.data.auth.data_source.remote

import app.grand.tafwak.data.remote.MABaseRemoteDataSource
import app.grand.tafwak.domain.auth.entity.response.ResponseTwitterStep1
import app.grand.tafwak.domain.auth.entity.response.ResponseTwitterStep3
import com.google.gson.Gson
import javax.inject.Inject

class TwitterRemoteDataSource @Inject constructor(
	private val apiService: TwitterServices,
	private val gson: Gson,
) : MABaseRemoteDataSource() {
	
	suspend fun obtainRequestToken() = safeApiCall2<ResponseTwitterStep1>(gson) {
		//fragment-dest-auth://app.grand.tafwak.twitter.callback/{oauth_token}/{oauth_token_secret}
		apiService.obtainRequestToken("fragment-dest-auth://app.grand.tafwak.twitter.callback")
	}
	
	suspend fun obtainAccessToken(
		oauthToken: String,
		oauthVerifier: String,
	) = safeApiCall2<ResponseTwitterStep3>(gson) {
		apiService.obtainAccessToken(oauthToken, oauthVerifier)
	}
	
}
