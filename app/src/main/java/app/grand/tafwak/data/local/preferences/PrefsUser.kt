package app.grand.tafwak.data.local.preferences

import android.content.Context
import androidx.datastore.preferences.core.edit
import androidx.datastore.preferences.core.stringPreferencesKey
import app.grand.tafwak.core.extensions.fromJsonOrNull
import app.grand.tafwak.core.extensions.toJsonOrNull
import app.grand.tafwak.domain.auth.entity.model.LocationData
import app.grand.tafwak.domain.auth.entity.model.orEmpty
import app.grand.tafwak.domain.splash.entity.SplashInitialLaunch
import com.google.gson.Gson
import dagger.hilt.android.qualifiers.ApplicationContext
import kotlinx.coroutines.flow.Flow
import kotlinx.coroutines.flow.map
import javax.inject.Inject
import javax.inject.Singleton

@Singleton
class PrefsUser @Inject constructor(
	@ApplicationContext context: Context,
	gson: Gson,
) : PrefsBase(context, gson, "PREFS_AUTH") {
	
	companion object {
		private const val KEY_ADDRESS = "KEY_ADDRESS"
		private const val KEY_TOKEN = "KEY_TOKEN"
		private const val KEY_IS_USER_NOT_SERVICE_PROVIDER = "KEY_IS_USER_NOT_SERVICE_PROVIDER"
		private const val KEY_PHONE = "KEY_PHONE"
		private const val KEY_IMAGE_URL = "KEY_IMAGE_URL"
		private const val KEY_ID = "KEY_ID"
		private const val KEY_NAME = "KEY_NAME"
		private const val KEY_E_MAIL = "KEY_E_MAIL"
		private const val KEY_DESCRIPTION = "KEY_DESCRIPTION"
		private const val KEY_BANK_NAME = "KEY_BANK_NAME"
		private const val KEY_BANK_ACCOUNT_NUMBER = "KEY_BANK_ACCOUNT_NUMBER"
		private const val KEY_BANK_IBAN_NUMBER = "KEY_BANK_IBAN_NUMBER"
		private const val KEY_BANK_CLIENT_NAME = "KEY_BANK_CLIENT_NAME"
		private const val KEY_TWITTER_OAUTH_TOKEN = "KEY_TWITTER_OAUTH_TOKEN"
		private const val KEY_REFERRED_BY_ID = "KEY_REFERRED_BY_ID"
		private const val KEY_REFER_CODE = "KEY_REFER_CODE"
	}
	
	/** clears all values */
	suspend fun logOut() {
		context.dataStore.edit { prefs ->
			prefs.clear()
		}
	}
	
	suspend fun setLocation(locationData: LocationData) =
		setValue(KEY_ADDRESS, locationData)
	
	fun getLocation() = getValue<LocationData?>(KEY_ADDRESS)
	
	/** bearer token */
	suspend fun setApiToken(token: String) =
		setStringValue(KEY_TOKEN, token)
	
	fun getToken() = getStringValue(KEY_TOKEN)
	
	fun getIsGuest() = getToken().map { it.isNullOrEmpty() }
	
	suspend fun setIsUserNotServiceProvider(isUser: Boolean) =
		setBooleanValue(KEY_IS_USER_NOT_SERVICE_PROVIDER, isUser)
	
	fun getIsUserNotServiceProvider() = getBooleanValue(KEY_IS_USER_NOT_SERVICE_PROVIDER)
	
	suspend fun setId(id: Int) =
		setIntValue(KEY_ID, id)
	
	fun getId() = getIntValue(KEY_ID)

	suspend fun setReferredById(id: Int?) {
		if (id == null) {
			removeIntValue(KEY_REFERRED_BY_ID)
		}else {
			setIntValue(KEY_REFERRED_BY_ID, id)
		}
	}
	fun getReferredById() = getIntValue(KEY_REFERRED_BY_ID)

	suspend fun setReferCode(phone: String) =
		setStringValue(KEY_REFER_CODE, phone)
	fun getReferCode() = getStringValue(KEY_REFER_CODE)
	
	suspend fun setPhone(phone: String) =
		setStringValue(KEY_PHONE, phone)
	
	fun getPhone() = getStringValue(KEY_PHONE)
	
	suspend fun setName(name: String) =
		setStringValue(KEY_NAME, name)
	
	fun getName() = getStringValue(KEY_NAME)
	
	suspend fun setEMail(email: String) =
		setStringValue(KEY_E_MAIL, email)
	
	fun getEMail() = getStringValue(KEY_E_MAIL)
	
	/** for provider only isa. */
	suspend fun setDescription(email: String) =
		setStringValue(KEY_DESCRIPTION, email)
	
	fun getDescription() = getStringValue(KEY_DESCRIPTION)
	
	suspend fun setImageUrl(imageUrl: String) =
		setStringValue(KEY_IMAGE_URL, imageUrl)
	
	fun getImageUrl() = getStringValue(KEY_IMAGE_URL)
	
	fun getLatLongToken(): Flow<LatLongToken> {
		return context.dataStore.data.map { prefs ->
			val locationData = prefs[stringPreferencesKey(KEY_ADDRESS)]
				.fromJsonOrNull<LocationData>(gson).orEmpty()
			val token = prefs[stringPreferencesKey(KEY_TOKEN)].orEmpty()
			
			LatLongToken(locationData.latitude, locationData.longitude, token)
		}
	}
	
	fun getBankName() = getStringValue(KEY_BANK_NAME)
	
	suspend fun setBankName(imageUrl: String) =
		setStringValue(KEY_BANK_NAME, imageUrl)
	
	fun getBankAccountNumber() = getStringValue(KEY_BANK_ACCOUNT_NUMBER)
	
	suspend fun setBankAccountNumber(imageUrl: String) =
		setStringValue(KEY_BANK_ACCOUNT_NUMBER, imageUrl)
	
	fun getBankIBANNumber() = getStringValue(KEY_BANK_IBAN_NUMBER)
	
	suspend fun setBankIBANNumber(imageUrl: String) =
		setStringValue(KEY_BANK_IBAN_NUMBER, imageUrl)
	
	fun getBankClientName() = getStringValue(KEY_BANK_CLIENT_NAME)
	
	suspend fun setBankClientName(imageUrl: String) =
		setStringValue(KEY_BANK_CLIENT_NAME, imageUrl)
	
	/*
	bankAccountBankName.value.orEmpty(),
				bankAccountAccountNumber.value.orEmpty(),
				bankAccountIBANNumber.value.orEmpty(),
				bankAccountClientName
	 */
	
	fun getTwitterOAuthToken() = getStringValue(KEY_TWITTER_OAUTH_TOKEN)
	
	suspend fun setTwitterOAuthToken(oauthToken: String) =
		setStringValue(KEY_TWITTER_OAUTH_TOKEN, oauthToken)
	
	data class LatLongToken(
		val latitude: String,
		val longitude: String,
		val token: String,
	)
	
}
