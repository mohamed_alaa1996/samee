package app.grand.tafwak.data.auth.data_source.remote

import app.grand.tafwak.data.samee.SameeAPI
import app.grand.tafwak.domain.auth.entity.response.ResponseTwitterStep1
import app.grand.tafwak.domain.auth.entity.response.ResponseTwitterStep3
import app.grand.tafwak.domain.utils.MABaseResponse
import okhttp3.ResponseBody
import retrofit2.Response
import retrofit2.http.Field
import retrofit2.http.FormUrlEncoded
import retrofit2.http.POST
import retrofit2.http.Query

interface TwitterServices {

	@FormUrlEncoded
	@POST("oauth/request_token")
	suspend fun obtainRequestToken(
		@Field("oauth_callback") urlCallback: String,
	): ResponseBody

	@FormUrlEncoded
	@POST("oauth/access_token")
	suspend fun obtainAccessToken(
		@Field(SameeAPI.Query.OAUTH_TOKEN) oauthToken: String,
		@Field(SameeAPI.Query.OAUTH_VERIFIER) oauthVerifier: String,
	): ResponseBody/*Response<ResponseTwitterStep3>*/

}
